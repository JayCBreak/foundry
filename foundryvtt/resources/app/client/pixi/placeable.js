/**
 * An Abstract Base Class which defines a Placeable Object which represents a Document placed on the Canvas
 * @extends {PIXI.Container}
 * @abstract
 * @interface
 *
 * @param {abstract.Document} document      The Document instance which is represented by this object
 */
class PlaceableObject extends PIXI.Container {
  constructor(document) {
    super();
    if ( !(document instanceof foundry.abstract.Document) || !document.isEmbedded ) {
      throw new Error("You must provide an embedded Document instance as the input for a PlaceableObject");
    }

    /**
     * Retain a reference to the Scene within which this Placeable Object resides
     * @type {Scene}
     */
    this.scene = document.parent;

    /**
     * A reference to the Scene embedded Document instance which this object represents
     * @type {abstract.Document}
     */
    this.document = document;

    /**
     * The underlying data object which provides the basis for this placeable object
     * @type {abstract.DocumentData}
     */
    this.data = document.data;

    /**
     * Track the field of vision for the placeable object.
     * This is necessary to determine whether a player has line-of-sight towards a placeable object or vice-versa
     * @type {{fov: PIXI.Circle, los: PointSourcePolygon}}
     */
    this.vision = {fov: undefined, los: undefined};

    /**
     * A control icon for interacting with the object
     * @type {ControlIcon}
     */
    this.controlIcon = null;

    /**
     * A mouse interaction manager instance which handles mouse workflows related to this object.
     * @type {MouseInteractionManager}
     */
    this.mouseInteractionManager = null;

    /**
     * An indicator for whether the object is currently controlled
     * @type {boolean}
     */
    this._controlled = false;

    /**
     * An indicator for whether the object is currently a hover target
     * @type {boolean}
     */
    this._hover = false;
  }

  /**
   * Identify the official Document name for this PlaceableObject class
   * @type {string}
   */
  static embeddedName;

  /* -------------------------------------------- */
  /*  Properties                                  */
  /* -------------------------------------------- */

  /**
   * The bounding box for this PlaceableObject.
   * This is required if the layer uses a Quadtree, otherwise it is optional
   * @return {Rectangle}
   */
  get bounds() {
    throw new Error("Each subclass of PlaceableObject must define its own bounds rectangle");
  }

  /* -------------------------------------------- */

  /**
   * The central coordinate pair of the placeable object based on it's own width and height
   * @type {PIXI.Point}
   */
  get center() {
    if ( "width" in this.data && "height" in this.data ) {
      return new PIXI.Point(this.data.x + (this.data.width / 2), this.data.y + (this.data.height / 2));
    }
    return new PIXI.Point(this.data.x, this.data.y);
  }

  /* -------------------------------------------- */

  /**
   * The id of the corresponding Document which this PlaceableObject represents.
   * @type {string}
   */
  get id() {
    return this.document.id;
  }

  /* -------------------------------------------- */

  /**
   * The field-of-vision polygon for the object, if it has been computed
   * @type {PIXI.Circle}
   */
  get fov() {
    return this.vision.fov;
  }

  /* -------------------------------------------- */

  /**
   * Provide a reference to the CanvasLayer which contains this PlaceableObject.
   * @type {PIXI.Container}
   */
  get layer() {
    return this.document.layer;
  }

  /* -------------------------------------------- */

  /**
   * The line-of-sight polygon for the object, if it has been computed
   * @type {PointSourcePolygon|null}
   */
  get los() {
    return this.vision.los;
  }

  /* -------------------------------------------- */

  /**
   * A Form Application which is used to configure the properties of this Placeable Object or the Document it
   * represents.
   * @type {FormApplication}
   */
  get sheet() {
    return this.document.sheet;
  }

  /* -------------------------------------------- */
  /*  Permission Controls                         */
  /* -------------------------------------------- */

  /**
   * Test whether a user can perform a certain interaction with regards to a Placeable Object
   * @param {User} user       The User performing the action
   * @param {string} action   The named action being attempted
   * @return {boolean}        Does the User have rights to perform the action?
   */
  can(user, action) {
    const fn = this[`_can${action.titleCase()}`];
    return fn ? fn.call(this, user) : false;
  }

  /**
   * Can the User access the HUD for this Placeable Object?
   * @protected
   */
  _canHUD(user, event) {
    return this._controlled;
  }

  /**
   * Does the User have permission to configure the Placeable Object?
   * @protected
   */
  _canConfigure(user, event) {
    return this.document.canUserModify(game.user, "update");
  }

  /**
   * Does the User have permission to control the Placeable Object?
   * @protected
   */
  _canControl(user, event) {
    if ( !this.layer._active ) return false;
    return this.document.canUserModify(game.user, "update");
  }

  /**
   * Does the User have permission to view details of the Placeable Object?
   * @protected
   */
  _canView(user, event) {
    return this.document.testUserPermission(game.user, "LIMITED");
  }

  /**
   * Does the User have permission to create the underlying Document?
   * @protected
   */
  _canCreate(user, event) {
    return user.isGM;
  }

  /**
   * Does the User have permission to drag this Placeable Object?
   * @protected
   */
  _canDrag(user, event) {
    return this._canControl(user);
  }

  /**
   * Does the User have permission to hover on this Placeable Object?
   * @protected
   */
  _canHover(user, event) {
    return this._canControl(user, event);
  }

  /**
   * Does the User have permission to update the underlying Document?
   * @protected
   */
  _canUpdate(user, event) {
    return this._canControl(user);
  }

  /**
   * Does the User have permission to delete the underlying Document?
   * @protected
   */
  _canDelete(user, event) {
    return this._canControl(user);
  }

  /* -------------------------------------------- */
  /*  Rendering                                   */
  /* -------------------------------------------- */

  /**
   * Clear the display of the existing object
   * @return {PlaceableObject}    The cleared object
   */
  clear() {
    this.removeChildren().forEach(c => c.destroy({children: true}));
    return this;
  }

  /* -------------------------------------------- */

  /**
   * Clone the placeable object, returning a new object with identical attributes
   * The returned object is non-interactive, and has no assigned ID
   * If you plan to use it permanently you should call the create method
   *
   * @return {PlaceableObject}  A new object with identical data
   */
  clone() {
    const cloneDoc = this.document.clone();
    const clone = new this.constructor(cloneDoc);
    clone.interactive = false;
    clone._original = this;
    clone._controlled = this._controlled;
    return clone;
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  destroy(options) {
    this.document._object = null;
    this.document._destroyed = true;
    if ( this.controlIcon ) this.controlIcon.destroy();
    return super.destroy(options);
  }

  /* -------------------------------------------- */

  /**
   * Draw the placeable object into its parent container
   * @returns {Promise<PlaceableObject>}  The drawn object
   */
  async draw() {
    throw new Error("A PlaceableObject subclass must define initial drawing procedure.");
  }

  /* -------------------------------------------- */

  /**
   * Refresh the current display state of the Placeable Object
   * @return {PlaceableObject}    The refreshed object
   */
  refresh() {
    throw new Error("A PlaceableObject subclass must define an refresh drawing procedure.");
  }

  /* -------------------------------------------- */

  /**
   * Register pending canvas operations which should occur after a new PlaceableObject of this type is created
   * @protected
   */
  _onCreate(data, options, userId) {
    this.draw();
  }

  /* -------------------------------------------- */

  /**
   * Define additional steps taken when an existing placeable object of this type is updated with new data
   * @protected
   */
  _onUpdate(changed, options, userId) {
    const layer = this.layer;

    // Z-index sorting
    if ( "z" in changed ) {
      this.zIndex = parseInt(changed.z) || 0;
    }

    // Quadtree location update
    if ( layer.quadtree ) {
      layer.quadtree.remove(this).insert({r: this.bounds, t: this});
    }

    // Refresh display
    this.refresh();
  }

  /* -------------------------------------------- */

  /**
   * Define additional steps taken when an existing placeable object of this type is deleted
   * @protected
   */
  _onDelete(options, userId) {
    this.release({trigger: false});
    const layer = this.layer;
    if ( layer._hover === this ) layer._hover = null;
    if ( layer.quadtree ) layer.quadtree.remove(this);
    this.destroy({children: true});
  }

  /* -------------------------------------------- */
  /*  Methods                                     */
  /* -------------------------------------------- */

  /**
   * Assume control over a PlaceableObject, flagging it as controlled and enabling downstream behaviors
   * @param {Object} options                  Additional options which modify the control request
   * @param {boolean} options.releaseOthers   Release any other controlled objects first
   * @return {boolean}                        A flag denoting whether or not control was successful
   */
  control(options={}) {
    if ( !this.layer.options.controllableObjects ) return false;

    // Release other controlled objects
    const wasControlled = this._controlled;
    this._controlled = false;
    if ( options.releaseOthers !== false ) {
      for ( let o of Object.values(this.layer._controlled) ) {
        if ( o !== this ) o.release();
      }
    }
    this._controlled = wasControlled;

    // Bail out if this object is already controlled, or not controllable
    if (this._controlled) return true;
    if (!this.can(game.user, "control")) return false;

    // Toggle control status
    this._controlled = true;
    this.layer._controlled[this.id] = this;

    // Trigger follow-up events and fire an on-control Hook
    this._onControl(options);
    /**
     * A hook event that fires when any PlaceableObject is selected or
     * deselected. Substitute the PlaceableObject name in the hook event to
     * target a specific PlaceableObject type, for example "controlToken".
     * @function controlPlaceableObject
     * @memberof hookEvents
     * @param {PlaceableObject} object The PlaceableObject
     * @param {boolean} controlled     Whether the PlaceableObject is selected or not
     */
    Hooks.callAll("control"+this.constructor.embeddedName, this, this._controlled);
    canvas.triggerPendingOperations();
    return true;
  }

  /* -------------------------------------------- */

  /**
   * Additional events which trigger once control of the object is established
   * @param {Object} options    Optional parameters which apply for specific implementations
   * @protected
   */
  _onControl(options) {
    this.refresh();
  }

  /* -------------------------------------------- */

  /**
   * Release control over a PlaceableObject, removing it from the controlled set
   * @param {Object} options          Options which modify the releasing workflow
   * @return {boolean}                A Boolean flag confirming the object was released.
   */
  release(options={}) {
    delete this.layer._controlled[this.id];
    if (!this._controlled) return true;
    this._controlled = false;

    // Trigger follow-up events
    this._onRelease(options);

    // Fire an on-release Hook
    Hooks.callAll("control"+this.constructor.embeddedName, this, this._controlled);
    if ( options.trigger !== false ) canvas.triggerPendingOperations();
    return true;
  }

  /* -------------------------------------------- */

  /**
   * Additional events which trigger once control of the object is released
   * @param {Object} options          Options which modify the releasing workflow
   * @protected
   */
  _onRelease(options) {
    const layer = this.layer;
    if ( layer.hud && (layer.hud.object === this) ) layer.hud.clear();
    this.refresh();
  }

  /* -------------------------------------------- */

  /**
   * Rotate the PlaceableObject to a certain angle of facing
   * @param {number} angle        The desired angle of rotation
   * @param {number} snap         Snap the angle of rotation to a certain target degree increment
   * @return {Promise<PlaceableObject>} The rotated object
   */
  async rotate(angle, snap) {
    if ( this.data.rotation === undefined ) return this;
    const rotation = this._updateRotation({angle, snap});
    this.layer.hud?.clear();
    await this.document.update({rotation});
    return this;
  }

  /* -------------------------------------------- */

  /**
   * Determine a new angle of rotation for a PlaceableObject either from an explicit angle or from a delta offset.
   * @param {object} options    An object which defines the rotation update parameters
   * @param {number} [options.angle]    An explicit angle, either this or delta must be provided
   * @param {number} [options.delta=0]  A relative angle delta, either this or the angle must be provided
   * @param {number} [options.snap=0]   A precision (in degrees) to which the resulting angle should snap. Default is 0.
   * @return {number}           The new rotation angle for the object
   */
  _updateRotation({angle, delta=0, snap=0}={}) {
    let degrees = Number.isNumeric(angle) ? angle : this.data.rotation + delta;
    if ( snap > 0 ) degrees = degrees.toNearest(snap);
    let isHexRow = [CONST.GRID_TYPES.HEXODDR, CONST.GRID_TYPES.HEXEVENR].includes(canvas.grid.type);
    const offset = isHexRow && (snap > 30) ? 30 : 0;
    return Math.normalizeDegrees(degrees - offset);
  }

  /* -------------------------------------------- */

  /**
   * Obtain a shifted position for the Placeable Object
   * @param {number} dx         The number of grid units to shift along the X-axis
   * @param {number} dy         The number of grid units to shift along the Y-axis
   * @return {{x:number, y:number}} The shifted target coordinates
   */
  _getShiftedPosition(dx, dy) {
    let [x, y] = canvas.grid.grid.shiftPosition(this.data.x, this.data.y, dx, dy);
    return {x, y};
  }

  /* -------------------------------------------- */
  /*  Interactivity                               */
  /* -------------------------------------------- */

  /**
   * Activate interactivity for the Placeable Object
   */
  activateListeners() {
    const mgr = this._createInteractionManager();
    this.mouseInteractionManager = mgr.activate();
  }

  /* -------------------------------------------- */

  /**
   * Create a standard MouseInteractionManager for the PlaceableObject
   * @protected
   */
  _createInteractionManager() {

    // Handle permissions to perform various actions
    const permissions = {
      hoverIn: this._canHover,
      hoverOut: this._canHover,
      clickLeft: this._canControl,
      clickLeft2: this._canView,
      clickRight: this._canHUD,
      clickRight2: this._canConfigure,
      dragStart: this._canDrag
    };

    // Define callback functions for each workflow step
    const callbacks = {
      hoverIn: this._onHoverIn,
      hoverOut: this._onHoverOut,
      clickLeft: this._onClickLeft,
      clickLeft2: this._onClickLeft2,
      clickRight: this._onClickRight,
      clickRight2: this._onClickRight2,
      dragLeftStart: this._onDragLeftStart,
      dragLeftMove: this._onDragLeftMove,
      dragLeftDrop: this._onDragLeftDrop,
      dragLeftCancel: this._onDragLeftCancel,
      dragRightStart: null,
      dragRightMove: null,
      dragRightDrop: null,
      dragRightCancel: null
    };

    // Define options
    const options = { target: this.controlIcon ? "controlIcon" : null };

    // Create the interaction manager
    return new MouseInteractionManager(this, canvas.stage, permissions, callbacks, options);
  }

  /* -------------------------------------------- */

  /**
   * Actions that should be taken for this Placeable Object when a mouseover event occurs
   * @see MouseInteractionManager#_handleMouseOver
   * @param {PIXI.InteractionEvent} event   The triggering canvas interaction event
   * @param {object} options                Options which customize event handling
   * @param {boolean} [options.hoverOutOthers=true] Trigger hover-out behavior on sibling objects
   */
  _onHoverIn(event, {hoverOutOthers=true}={}) {
    if ( this._hover === true ) return false;
    if ( this.data.locked ) return false;
    const layer = this.layer;

    // Update the hover state of all objects in the layer
    if ( hoverOutOthers ) {
      layer.placeables.forEach(o => {
        if ( o !== this ) o._onHoverOut(event);
      });
    }
    this._hover = true;
    layer._hover = this;

    // Refresh the object display
    this.refresh();
    /**
     * A hook event that fires when any PlaceableObject is hovered over or out.
     * Substitute the PlaceableObject name in the hook event to target a specific
     * PlaceableObject type, for example "hoverToken".
     * @function hoverPlaceableObject
     * @memberof hookEvents
     * @param {PlaceableObject} object The PlaceableObject
     * @param {boolean} hovered        Whether the PlaceableObject is hovered over or not
     */
    Hooks.callAll("hover"+this.constructor.embeddedName, this, this._hover);
  }

  /* -------------------------------------------- */

  /**
   * Actions that should be taken for this Placeable Object when a mouseout event occurs
   * @see MouseInteractionManager#_handleMouseOut
   * @param {PIXI.InteractionEvent} event  The triggering canvas interaction event
   */
  _onHoverOut(event) {
    if ( this._hover !== true ) return false;
    if ( this.data.locked ) return false;
    const layer = this.layer;
    this._hover = false;
    layer._hover = null;
    this.refresh();
    Hooks.callAll("hover"+this.constructor.embeddedName, this, this._hover);
  }

  /* -------------------------------------------- */

  /**
   * Callback actions which occur on a single left-click event to assume control of the object
   * @see MouseInteractionManager#_handleClickLeft
   * @param {PIXI.InteractionEvent} event  The triggering canvas interaction event
   */
  _onClickLeft(event) {
    const hud = this.layer.hud;
    if ( hud ) hud.clear();

    // Add or remove the Placeable Object from the currently controlled set
    const oe = event.data.originalEvent;
    if ( this._controlled ) {
      if ( oe.shiftKey ) return this.release();
    } else {
      return this.control({releaseOthers: !oe.shiftKey});
    }
  }

  /* -------------------------------------------- */

  /**
   * Callback actions which occur on a double left-click event to activate
   * @see MouseInteractionManager#_handleClickLeft2
   * @param {PIXI.InteractionEvent} event  The triggering canvas interaction event
   */
  _onClickLeft2(event) {
    const sheet = this.sheet;
    if ( sheet ) sheet.render(true);
  }

  /* -------------------------------------------- */

  /**
   * Callback actions which occur on a single right-click event to configure properties of the object
   * @see MouseInteractionManager#_handleClickRight
   * @param {PIXI.InteractionEvent} event  The triggering canvas interaction event
   */
  _onClickRight(event) {
    const hud = this.layer.hud;
    if ( hud ) {
      const releaseOthers = !this._controlled && !event.data.originalEvent.shiftKey;
      this.control({releaseOthers});
      if ( hud.object === this) hud.clear();
      else hud.bind(this);
    }
  }

  /* -------------------------------------------- */

  /**
   * Callback actions which occur on a double right-click event to configure properties of the object
   * @see MouseInteractionManager#_handleClickRight2
   * @param {PIXI.InteractionEvent} event  The triggering canvas interaction event
   */
  _onClickRight2(event) {
    const sheet = this.sheet;
    if ( sheet ) sheet.render(true);
  }

  /* -------------------------------------------- */

  /**
   * Callback actions which occur when a mouse-drag action is first begun.
   * @see MouseInteractionManager#_handleDragStart
   * @param {PIXI.InteractionEvent} event  The triggering canvas interaction event
   */
  _onDragLeftStart(event) {
    const targets = this.layer.options.controllableObjects ? this.layer.controlled : [this];
    const clones = [];
    for ( let o of targets ) {
      if ( o.data.locked ) continue;
      o.data.locked = true;

      // Clone the object
      const c = o.clone();
      clones.push(c);

      // Draw the clone
      c.draw().then(c => {
        o.alpha = 0.4;
        c.alpha = 0.8;
        c.visible = true;
        this.layer.preview.addChild(c);
      });
    }
    event.data.clones = clones;
  }

  /* -------------------------------------------- */

  /**
   * Callback actions which occur on a mouse-move operation.
   * @see MouseInteractionManager#_handleDragMove
   * @param {PIXI.InteractionEvent} event  The triggering canvas interaction event
   */
  _onDragLeftMove(event) {
    const {clones, destination, origin, originalEvent} = event.data;
    canvas._onDragCanvasPan(originalEvent);
    const dx = destination.x - origin.x;
    const dy = destination.y - origin.y;
    for ( let c of clones || [] ) {
      c.data.x = c._original.data.x + dx;
      c.data.y = c._original.data.y + dy;
      c.refresh();
    }
  }

  /* -------------------------------------------- */

  /**
   * Callback actions which occur on a mouse-move operation.
   * @see MouseInteractionManager#_handleDragDrop
   * @param {PIXI.InteractionEvent} event  The triggering canvas interaction event
   * @returns {Promise<*>}
   */
  async _onDragLeftDrop(event) {
    const {clones, destination, originalEvent} = event.data;
    if ( !clones || !canvas.grid.hitArea.contains(destination.x, destination.y) ) return false;
    const updates = clones.map(c => {
      let dest = {x: c.data.x, y: c.data.y};
      if ( !originalEvent.shiftKey ) {
        dest = canvas.grid.getSnappedPosition(c.data.x, c.data.y, this.layer.gridPrecision);
      }
      return {_id: c._original.id, x: dest.x, y: dest.y, rotation: c.data.rotation};
    });
    return canvas.scene.updateEmbeddedDocuments(this.document.documentName, updates);
  }

  /* -------------------------------------------- */

  /**
   * Callback actions which occur on a mouse-move operation.
   * @see MouseInteractionManager#_handleDragCancel
   * @param {MouseEvent} event  The triggering mouse click event
   */
  _onDragLeftCancel(event) {
    this.layer.clearPreviewContainer();
  }
}
