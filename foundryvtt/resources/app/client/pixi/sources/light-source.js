/**
 * @typedef {Object}                      LightSourceData
 * @see {@link foundry.data.LightData}
 * @property {number} x                   The x-coordinate of the source location
 * @property {number} y                   The y-coordinate of the source location
 * @property {number} z                   An optional z-index sorting for the source
 * @property {number} rotation            The angle of rotation for this point source
 * @property {number} alpha               An opacity for the emitted light, if any
 * @property {object} animation           An animation configuration for the source
 * @property {number} angle               The angle of emission for this point source
 * @property {number} bright              The allowed radius of bright vision or illumination
 * @property {number} color               A tint color for the emitted light, if any
 * @property {number} coloration          The coloration technique applied in the shader
 * @property {number} contrast            The amount of contrast this light applies to the background texture
 * @property {object} darkness            A darkness range (min and max) for which the source should be active
 * @property {number} dim                 The allowed radius of dim vision or illumination
 * @property {boolean} gradual            Fade the difference between bright, dim, and dark gradually?
 * @property {number} luminosity          The luminosity applied in the shader
 * @property {number} saturation          The amount of color saturation this light applies to the background texture
 * @property {number} shadows             The depth of shadows this light applies to the background texture
 * @property {boolean} walls              Whether or not the source is constrained by walls
 * @property {boolean} vision             Whether or not this source provides a source of vision
 * @property {number} seed                An integer seed to synchronize (or de-synchronize) animations
 */

/**
 * @typedef {Object} LightAnimationConfiguration
 * @property {string} label                                     The human-readable (localized) label for the animation
 * @property {Function} animation                               The animation function that runs every frame
 * @property {AdaptiveIlluminationShader} illuminationShader    A custom illumination shader used by this animation
 * @property {AdaptiveColorationShader} colorationShader        A custom coloration shader used by this animation
 * @property {AdaptiveBackgroundShader} backgroundShader        A custom background shader used by this animation
 * @property {number} [seed]                                    The animation seed
 * @property {number} [time]                                    The animation time
 */

/**
 * A specialized subclass of the PointSource abstraction which is used to control the rendering of light sources.
 * @extends {PointSource}
 * @param {AmbientLight|Token} object     The light-emitting object that generates this light source
 */
class LightSource extends PointSource {
  constructor(object) {
    super(object);

    /**
     * The light or darkness container for this source
     * @type {PIXI.Container}
     */
    this.background = this._createMesh(AdaptiveBackgroundShader);

    /**
     * The light or darkness container for this source
     * @type {PIXI.Container}
     */
    this.illumination = this._createMesh(AdaptiveIlluminationShader);

    /**
     * This visible color container for this source
     * @type {PIXI.Container}
     */
    this.coloration = this._createMesh(AdaptiveColorationShader);
  }

  /** @inheritdoc */
  static sourceType = "light";

  /**
   * Strength of the blur for light source edges
   * @type {number}
   */
  static BLUR_STRENGTH = 3;

  /**
   * Keys in the LightSourceData structure which, when modified, change the appearance of the light
   * @type {string[]}
   * @private
   */
  static _appearanceKeys = [
    "dim", "bright", "gradual", "alpha", "coloration", "color",
    "contrast", "saturation", "shadows", "luminosity"
  ]

  /* -------------------------------------------- */
  /*  Light Source Attributes                     */
  /* -------------------------------------------- */

  /**
   * The object of data which configures how the source is rendered
   * @type {LightSourceData}
   */
  data = {};

  /**
   * The animation configuration applied to this source
   * @type {LightAnimationConfiguration}
   */
  animation = {};

  /**
   * Internal flag for whether this is a darkness source
   * @type {boolean}
   */
  isDarkness = false;

  /**
   * The rendered field-of-vision texture for the source for use within shaders.
   * @type {PIXI.RenderTexture}
   */
  fovTexture;

  /**
   * To know if a light source is a preview or not. False by default.
   * @type {boolean}
   */
  preview = false;

  /**
   * The ratio of dim:bright as part of the source radius
   * @type {number}
   */
  ratio;

  /**
   * Track which uniforms need to be reset
   * @type {{background: boolean, illumination: boolean, coloration: boolean}}
   * @private
   */
  _resetUniforms = {
    background: true,
    illumination: true,
    coloration: true
  }

  /**
   * To track if a source is temporarily shutdown to avoid glitches
   * @type {{illumination: boolean}}
   * @private
   */
  _shutdown = {
    illumination: false
  }

  /* -------------------------------------------- */
  /*  Light Source Initialization                 */
  /* -------------------------------------------- */

  /**
   * Initialize the source with provided object data.
   * @param {object} data             Initial data provided to the point source
   * @return {LightSource}            A reference to the initialized source
   */
  initialize(data={}) {

    // Initialize new input data
    const changes = this._initializeData(data);

    // Record the requested animation configuration
    const seed = this.animation.seed ?? data.seed ?? Math.floor(Math.random() * 100000);
    this.animation = foundry.utils.deepClone(CONFIG.Canvas.lightAnimations[this.data.animation.type]) || {};
    this.animation.seed = seed;

    // Compute data attributes
    this.colorRGB = foundry.utils.hexToRGB(this.data.color);
    this.radius = Math.max(Math.abs(this.data.dim), Math.abs(this.data.bright));
    this.ratio = Math.clamped(Math.abs(this.data.bright) / this.radius, 0, 1);
    this.isDarkness = this.data.luminosity < 0;

    // Compute the source polygon
    const origin = {x: this.data.x, y: this.data.y};
    this.los = CONFIG.Canvas.losBackend.create(origin, {
      type: this.data.walls ? "light" : "universal",
      angle: this.data.angle,
      density: 60,
      radius: this.radius,
      rotation: this.data.rotation,
      source: this
    });

    // Update shaders if the animation type or the constrained wall option changed
    const updateShaders = ("animation.type" in changes || "walls" in changes);
    if ( updateShaders ) this._initializeShaders();

    // Record status flags
    this._flags.useFov = !this.preview && canvas.performance.textures.enabled;
    this._flags.hasColor = !!(this.data.color && this.data.alpha);
    this._flags.renderFOV = true;
    if ( updateShaders || this.constructor._appearanceKeys.some(k => k in changes) ) {
      for ( let k of Object.keys(this._resetUniforms) ) {
        this._resetUniforms[k] = true;
      }
    }

    // Initialize blend modes and sorting
    this._initializeBlending();
    return this;
  }

  /* -------------------------------------------- */

  /**
   * Initialize the PointSource with new input data
   * @param {object} data             Initial data provided to the light source
   * @returns {object}                The changes compared to the prior data
   * @private
   */
  _initializeData(data) {

    // Clean input data
    if ( data.animation instanceof foundry.abstract.DocumentData ) data.animation = data.animation.toObject();
    data.animation = data.animation || {type: null};
    data.angle = data.angle ?? 360;
    data.alpha = data.alpha ?? 0.5;
    data.bright = data.bright ?? 0;
    data.color = typeof data.color === "string" ? foundry.utils.colorStringToHex(data.color) : (data.color ?? null);
    data.darkness = {min: data.darkness?.min ?? 0, max: data.darkness?.max ?? 1};
    data.dim = data.dim ?? 0;
    data.rotation = data.rotation ?? 0;
    data.walls = data.walls ?? true;
    data.vision = data.vision ?? false;
    data.x = data.x ?? 0;
    data.y = data.y ?? 0;
    data.z = data.z ?? null;
    data.contrast = data.contrast ?? 0.0;
    data.shadows = data.shadows ?? 0.0;
    data.saturation = data.saturation ?? 0.0;
    data.luminosity = data.luminosity ?? 0.0;
    data.coloration = data.coloration ?? 1;

    // Identify changes compared to the current object
    const changes = foundry.utils.flattenObject(foundry.utils.diffObject(this.data, data));
    this.data = data;
    return changes;
  }

  /* -------------------------------------------- */

  /**
   * Initialize the shaders used for this source, swapping to a different shader if the animation has changed.
   * @private
   */
  _initializeShaders() {

    // Create each shader
    const createShader = (cls, container) => {
      const current = container.shader;
      if ( current?.constructor.name === cls.name ) return;
      const shader = cls.create({
        uBkgSampler: canvas.primary.renderTexture,
        fovTexture: this._flags.useFov ? this.fovTexture : null
      });
      shader.container = container;
      container.shader = shader;
      if ( current ) current.destroy();
    }

    // Initialize shaders
    createShader(this.animation.backgroundShader || AdaptiveBackgroundShader, this.background);
    createShader(this.animation.illuminationShader || AdaptiveIlluminationShader, this.illumination);
    createShader(this.animation.colorationShader || AdaptiveColorationShader, this.coloration);

    /**
     * A hook event that fires after LightSource shaders have initialized.
     * @function initializeLightSourceShaders
     * @memberof hookEvents
     * @param {PointSource} source   The LightSource being initialized
     */
    Hooks.callAll("initializeLightSourceShaders", this);
  }

  /* -------------------------------------------- */

  /**
   * Initialize the blend mode and vertical sorting of this source relative to others in the container.
   * @private
   */
  _initializeBlending() {
    const defaultZ = this.isDarkness ? 10 : 0;
    const BM = PIXI.BLEND_MODES;

    // Background
    this.background.blendMode = BM.MAX_COLOR;
    this.background.zIndex = 0;

    // Illumination
    let blend = BM[this.isDarkness ? "MIN_COLOR" : "MAX_COLOR"];
    if ( this._resetUniforms.illumination && this.illumination.blendMode !== blend ) {
      this._shutdown.illumination = !(this.illumination.renderable = false);
    }
    this.illumination.blendMode = blend;
    this.illumination.zIndex = this.data.z ?? defaultZ;

    // Coloration
    this.coloration.blendMode = BM[this.isDarkness ? "MULTIPLY" : "SCREEN"];
    this.coloration.zIndex = this.data.z ?? defaultZ;
  }

  /* -------------------------------------------- */
  /*  Light Source Rendering                      */
  /* -------------------------------------------- */

  /**
   * Render the containers used to represent this light source within the LightingLayer
   * @return {Object<string,PIXI.Mesh>}
   */
  drawMeshes() {

    // Destroy an existing render texture
    if ( !this._flags.useFov ) {
      if ( this.fovTexture ) this.fovTexture.destroy(true);
      this.fovTexture = null;
    }

    // Update the render texture
    if ( this._flags.renderFOV ) {
      this.losMask.clear().beginFill(0xFFFFFF).drawShape(this.los).endFill();
      if ( this._flags.useFov ) this._renderTexture();
      else this._flags.renderFOV = false;
    }

    // Draw new meshes
    const background = this.drawBackground();
    const light = this.drawLight();
    const color = this.drawColor();
    return {background, light, color};
  }

  /* -------------------------------------------- */

  /**
   * Draw the display of this source for background container.
   * @return {PIXI.Container|null}         The rendered light container
   */
  drawBackground() {

    // Protect against cases where no background is present
    const shader = this.background.shader;
    if ( !shader ) return null;

    // Update background uniforms
    if ( this._resetUniforms.background ) {
      this._updateBackgroundUniforms(shader);
      this._resetUniforms.background = false;
      this.background.visible = shader.isRequired;
    }

    // Draw the container
    return this._updateMesh(this.background);
  }

  /* -------------------------------------------- */

  /**
   * Draw the display of this source for the darkness/light container of the SightLayer.
   * @return {PIXI.Container|null}         The rendered light container
   */
  drawLight() {

    // Protect against cases where the canvas is being deactivated
    const shader = this.illumination.shader;
    if ( !shader ) return null;

    // Update illumination uniforms
    const ic = this.illumination;
    const updateChannels = !(this._flags.lightingVersion >= canvas.lighting.version);
    if ( this._resetUniforms.illumination || updateChannels ) {
      this._updateIlluminationUniforms(shader);
      if ( this._shutdown.illumination ) this._shutdown.illumination = !(ic.renderable = true);
      this._flags.lightingVersion = canvas.lighting.version;
    }
    if ( this._resetUniforms.illumination ) {
      this._resetUniforms.illumination = false;
    }

    // Draw the container
    return this._updateMesh(ic);
  }

  /* -------------------------------------------- */

  /**
   * Draw and return a container used to depict the visible color tint of the light source on the LightingLayer
   * @return {PIXI.Container|null}           An updated color container for the source
   */
  drawColor() {

    // Protect against cases where no coloration is present
    const shader = this.coloration.shader;
    if ( !shader || !(this._flags.hasColor || this.isDarkness) ) return null;

    // Update coloration uniforms
    if ( this._resetUniforms.coloration ) {
      this._updateColorationUniforms(shader);
      this._resetUniforms.coloration = false;
    }

    // Draw the container
    return this._updateMesh(this.coloration);
  }

  /* -------------------------------------------- */
  /*  Shader Management                           */
  /* -------------------------------------------- */

  /**
   * Update shader uniforms by providing data from this PointSource
   * @param {AdaptiveColorationShader} shader      The shader being updated
   * @private
   */
  _updateColorationUniforms(shader) {
    const u = shader.uniforms;
    const d = shader._defaults;
    this._updateCommonUniforms(shader);

    // Adapting color intensity to the coloration technique
    switch (this.data.coloration) {
      case 0: // legacy
        // Default 0.25 -> Legacy technique needs quite low intensity default to avoid washing background
        u.alpha = Math.pow(this.data.alpha, 2);
        break;
      case 4: // color burn
      case 5: // Internal burn
      case 6: // External burn
      case 9: // invert absorption
        // Default 0.5 -> Theses techniques are better at low color intensity
        u.alpha = this.data.alpha;
        break;
      default:
        // Default 1 -> The remaining techniques use adaptive lighting, which produces interesting results in the [0, 2] range.
        u.alpha = this.data.alpha * 2;
    }

    u.color = this.data.color ? this.colorRGB : d.color;
    u.technique = this.data.coloration;
  }

  /* -------------------------------------------- */

  /**
   * Update shader uniforms by providing data from this PointSource
   * @param {AdaptiveIlluminationShader} shader   The shader being updated
   * @private
   */
  _updateIlluminationUniforms(shader) {
    const u = shader.uniforms;
    const d = shader._defaults;
    const c = canvas.lighting.channels;
    const colorIntensity = this.data.alpha * 2;
    const blend = (rgb1, rgb2, w) => rgb1.map((x, i) => (w * x) + ((1 - w) * (rgb2[i]))); // linear interpolation

    // Darkness [-1, 0)
    if ( this.isDarkness ) {
      let lc, cdim1, cdim2, cbr1, cbr2;

      // Construct intensity-adjusted darkness colors for "black" and the midpoint between dark and black
      const iMid = c.background.rgb.map((x, i) => (x + c.black.rgb[i]) / 2);
      const mid = this.data.color ? this.colorRGB.map((x, i) => x * iMid[i] * colorIntensity) : iMid;
      const black = this.data.color ? this.colorRGB.map((x, i) => x * c.black.rgb[i] * colorIntensity) : c.black.rgb;

      // For darkness [-1, -0.5), blend between the chosen darkness color and black
      if ( this.data.luminosity < -0.5 ) {
        lc = Math.abs(this.data.luminosity) - 0.5;

        // Darkness Dim colors -> tend to darker tone
        cdim1 = black;
        cdim2 = black.map(x => x * 0.625);

        // Darkness Bright colors -> tend to darkest tone
        cbr1 = black.map(x => x * 0.5);
        cbr2 = black.map(x => x * 0.125);
      }

      // For darkness [-0.5, 0) blend between the chosen darkness color and the dark midpoint
      else {
        lc = Math.pow((Math.abs(this.data.luminosity) * 2), 0.4); // Accelerating easing toward dark tone with pow

        // Darkness Dim colors -> tend to medium tone
        cdim1 = mid;
        cdim2 = black;

        // Darkness Bright colors -> tend to dark tone
        cbr1 = mid;
        cbr2 = black.map(x => x * 0.5);
      }

      // Linear interpolation between tones according to luminosity
      u.colorDim = blend(cdim1, cdim2, 1 - lc);
      u.colorBright = blend(cbr1, cbr2, 1 - lc);
    }

    // Light [0,1]
    else {
      const ll = CONFIG.Canvas.lightLevels;
      const penalty = shader.getDarknessPenalty(c.darkness.level, this.data.luminosity);
      const lumPenalty = Math.clamped(this.data.luminosity * 2, 0, 1);
      u.colorBright = [1,1,1].map(x => ll.bright * x * (1-penalty) * lumPenalty);
      u.colorDim = u.colorBright.map((x, i) => (ll.dim * x) + ((1 - ll.dim) * c.background.rgb[i]));
    }

    // Apply standard uniforms for this PointSource
    this._updateCommonUniforms(shader);
    u.color = this.data.color ? this.colorRGB : d.color;
    u.colorBackground = c.background.rgb;
  }

  /* -------------------------------------------- */

  /**
   * Update shader uniforms by providing data from this PointSource
   * @param {AdaptiveBackgroundShader} shader   The shader being updated
   * @private
   */
  _updateBackgroundUniforms(shader) {
    const u = shader.uniforms;
    u.alpha = 1.0;
    this._updateCommonUniforms(shader);
  }

  /* -------------------------------------------- */

  /**
   * Update shader uniforms shared by all shader types
   * @param {AdaptiveLightingShader} shader        The shader being updated
   * @private
   */
  _updateCommonUniforms(shader) {
    const u = shader.uniforms;

    // Passing advanced color correction values
    u.exposure = this._mapLuminosity(this.data.luminosity);
    u.contrast = (this.data.contrast < 0 ? this.data.contrast * 0.5 : this.data.contrast);
    u.saturation = this.data.saturation;
    u.shadows = this.data.shadows;
    u.darkness = this.isDarkness;
    u.ratio = this.ratio;
    u.gradual = this.data.gradual;
    u.useFov = this._flags.useFov;

    // Passing screenDimensions to use screen size render textures
    u.screenDimensions = canvas.screenDimensions;
    if ( !u.uBkgSampler ) u.uBkgSampler = canvas.primary.renderTexture;
    u.fovTexture = this._flags.useFov ? this.fovTexture : null;
  }

  /* -------------------------------------------- */

  /**
   * Map luminosity value to exposure value
   * luminosity[-1  , 0  [ => Darkness => map to exposure ]   0, 1]
   * luminosity[ 0  , 0.5[ => Light    => map to exposure [-0.5, 0[
   * luminosity[ 0.5, 1  ] => Light    => map to exposure [   0, 1]
   * @param {number} lum        The luminosity value
   * @return {number}           The exposure value
   * @private
   */
  _mapLuminosity(lum) {
    if ( lum < 0 ) return lum + 1;
    if ( lum < 0.5 ) return lum - 0.5;
    return ( lum - 0.5 ) * 2;
  }

  /* -------------------------------------------- */
  /*  Animation Functions                         */
  /* -------------------------------------------- */

  /**
   * Animate the PointSource, if an animation is enabled and if it currently has rendered containers.
   * @param {number} dt         Delta time
   */
  animate(dt) {
    const animation = this.data.animation;
    if ( !animation.type || (this.radius === 0) || !this.illumination.shader ) return;
    const fn = this.animation.animation;
    if ( fn ) fn.call(this, dt, animation);
  }

  /* -------------------------------------------- */

  /**
   * A torch animation where the luminosity and coloration decays each frame and is revitalized by flashes
   * @param {number} dt         Delta time
   * @param {number} speed      The animation speed, from 1 to 10
   * @param {number} intensity  The animation intensity, from 1 to 10
   */
  animateTorch(dt, { speed = 5, intensity = 5 } = {}) {
    const t = canvas.app.ticker.lastTime;
    const e = (canvas.app.ticker.elapsedMS / 20) * (speed / 5);
    this.animation.time = ((speed * t) / 5000) + this.animation.seed;
    const iu = this.illumination.uniforms;
    const cu = this.coloration.uniforms;
    const w = 0.5 * (Math.cos(t * 0.01) + 1);
    const wave = (a, b, w) => (a - b) * w + b;

    // When a "flash" occurs
    if ( t > (this._flashTime || 0) ) {

      // Randomize the next time to flash
      const s = (11 - speed) * 50;
      const deltaMS = Math.random() * s;
      this._flashTime = t + deltaMS;

      // Randomize the color intensity of the flash
      const mu = this.data.alpha - cu.alpha;                                      // On average, increase to the normal alpha
      const sigma = intensity * 0.05;                                             // Variance of the spikes
      const epsilon = twist.normal(mu, sigma);
      const maxA = Math.min(this.data.alpha * 1.1, 0.5 + (this.data.alpha / 2));  // Maximum alpha that the spike can reach
      cu.brightnessPulse = Math.clamped(cu.alpha + epsilon, cu.alpha, maxA);
      const ei = twist.normal(1 - iu.alpha, sigma);
      iu.alpha = Math.clamped(iu.alpha + ei, iu.alpha, 1);

      // Boost the ratio of bright:dim
      iu.ratio = this.ratio * iu.alpha;
      cu.ratio = iu.ratio;
    }

    // Animate the dampening process according to the time passed since the last frame (e)
    const dc = 0.0005 * (speed + intensity) * e; 
    const dw = (dc / 2);
    const wv = wave(1.0 + dw, 1.0 - dw, w);
    iu.alpha = iu.alpha * (1.0 - dc) * wv;
    iu.ratio = iu.ratio * (1.0 - dc) * wv;
    cu.brightnessPulse = cu.brightnessPulse * (1.0 - dc) * wv;
    cu.ratio = cu.ratio * (1.0 - dc) * wv;
    cu.time = iu.time = this.animation.time;
  }

  /* -------------------------------------------- */

  /**
   * A basic "pulse" animation which expands and contracts.
   * @param {number} dt         Delta time
   * @param {number} speed      The animation speed, from 1 to 10
   * @param {number} intensity  The animation intensity, from 1 to 10
   * @param {boolean} reverse   Is the animation reversed?
   */
  animatePulse(dt, {speed=5, intensity=5, reverse=false}={}) {

    // Determine the animation timing
    let t = canvas.app.ticker.lastTime;
    if ( reverse ) t *= -1;
    this.animation.time = ((speed * t)/5000) + this.animation.seed;

    // Define parameters
    const i = (10 - intensity) * 0.1;
    const si = Math.sqrt(i);
    const w = 0.5 * (Math.cos(this.animation.time * 2.5) + 1);
    const wave = (a, b, w) => (a - b) * w + b;

    // Pulse coloration
    const co = this.coloration;
    co.uniforms.intensity = intensity;
    co.uniforms.time = this.animation.time;
    co.uniforms.alpha = wave(this.data.alpha, this.data.alpha * si, w);
    co.uniforms.pulse = wave(1.2, i, w);

    // Pulse illumination
    const il = this.illumination;
    il.uniforms.intensity = intensity;
    il.uniforms.time = this.animation.time;
    il.uniforms.alpha = wave(1.1, 1.1 * si, w);
    il.uniforms.ratio = wave(this.ratio, this.ratio * i, w);
  }

  /* -------------------------------------------- */

  /**
   * Emanate waves of light from the source origin point
   * @param {number} dt         Delta time
   * @param {number} speed      The animation speed, from 1 to 10
   * @param {number} intensity  The animation intensity, from 1 to 10
   * @param {boolean} reverse   Is the animation reversed?
   */
  animateTime(dt, {speed=5, intensity=5, reverse=false}={}) {

    // Determine the animation timing
    let t = canvas.app.ticker.lastTime;
    if ( reverse ) t *= -1;
    this.animation.time = ((speed * t)/5000) + this.animation.seed;

    // Update uniforms
    const co = this.coloration;
    co.uniforms.intensity = intensity;
    co.uniforms.time = this.animation.time;
    const il = this.illumination;
    il.uniforms.intensity = intensity;
    il.uniforms.time = this.animation.time;
  }

  /* -------------------------------------------- */

  /**
   * Evolve a value using a stochastic AR(1) process
   * @param {number} y        The current value
   * @param {number} phi      The decay rate of prior values
   * @param {number} center   The stationary mean of the series
   * @param {number} sigma    The volatility of the process - standard deviation of the error term
   * @param {number|null} max The maximum allowed outcome, or null
   * @param {number|null} min The minimum allowed outcome, or null
   * @return {number}         The new value of the process
   * @private
   */
  _ar1(y, {phi=0.5, center=0, sigma=0.1, max=null, min=null}={}) {
    let yt = center + (phi * (y - center)) + twist.normal(0, sigma);
    if ( max !== null ) yt = Math.min(yt, max);
    if ( min !== null ) yt = Math.max(yt, min);
    return yt;
  }
}
