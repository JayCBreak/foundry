/**
 * A Wall is an implementation of PlaceableObject which represents a physical or visual barrier within the Scene.
 * Walls are used to restrict Token movement or visibility as well as to define the areas of effect for ambient lights
 * and sounds.
 * @extends {PlaceableObject}
 * @see {@link WallDocument}
 * @see {@link WallsLayer}
 * @see {@link WallConfig}
 */
class Wall extends PlaceableObject {

  /**
   * An reference the Door Control icon associated with this Wall, if any
   * @type {DoorControl|null}
   * @private
   */
  doorControl;

  /**
   * A reference to an overhead Tile that is a roof, interior to which this wall is contained
   * @type {Tile}
   */
  roof;

  /**
   * A set which tracks other Wall instances that this Wall intersects with (excluding shared endpoints)
   * @type {Map<Wall,LineIntersection>}
   */
  intersectsWith = new Map();

  /**
   * Cached representation of this wall's endpoints as {@link PolygonVertex}es.
   * @type {{a: PolygonVertex, b: PolygonVertex}|null}
   * @private
   */
  _vertices = null;

  /**
   * Cached representation of the set of this wall's vertices.
   * @type {Set<string>|null}
   * @private
   */
  _wallKeys = null;

  /** @inheritdoc */
  static embeddedName = "Wall";

  /* -------------------------------------------- */
  /*  Properties                                  */
  /* -------------------------------------------- */

  /**
   * A convenience reference to the coordinates Array for the Wall endpoints, [x0,y0,x1,y1].
   * @type {number[]}
   */
  get coords() {
    return this.data.c;
  }

  /* -------------------------------------------- */

  /**
   * The initial endpoint of the Wall
   * @type {Point}
   */
  get A() {
    return new PIXI.Point(this.data.c[0], this.data.c[1]);
  }

  /* -------------------------------------------- */

  /**
   * The second endpoint of the Wall
   * @type {Point}
   */
  get B() {
    return new PIXI.Point(this.data.c[2], this.data.c[3]);
  }

  /* -------------------------------------------- */

  /**
   * The endpoints of the wall as {@link PolygonVertex}es.
   * @type {{a: PolygonVertex, b: PolygonVertex}}
   */
  get vertices() {
    if ( this._vertices ) return this._vertices;
    return {a: PolygonVertex.fromPoint(this.A), b: PolygonVertex.fromPoint(this.B)};
  }

  /* -------------------------------------------- */

  /**
   * The set of keys for this wall's endpoints.
   * @type {Set<string>}
   */
  get wallKeys() {
    if ( this._wallKeys ) return this._wallKeys;
    return new Set([this.vertices.a.key, this.vertices.b.key]);
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  get bounds() {
    const [x0, y0, x1, y1] = this.data.c;
    return new NormalizedRectangle(x0, y0, x1-x0, y1-y0);
  }

  /* -------------------------------------------- */

  /**
   * A boolean for whether this wall contains a door
   * @type {boolean}
   */
  get isDoor() {
    return this.data.door > CONST.WALL_DOOR_TYPES.NONE;
  }

  /* -------------------------------------------- */

  /**
   * A boolean for whether the wall contains an open door
   * @returns {boolean}
   */
  get isOpen() {
    return this.isDoor && (this.data.ds === CONST.WALL_DOOR_STATES.OPEN);
  }

  /* -------------------------------------------- */

  /**
   * Is this Wall interior to a non-occluded roof Tile?
   * @returns {boolean}
   */
  get hasActiveRoof() {
    return this.roof?.occluded === false;
  }

  /* -------------------------------------------- */

  /**
   * Return the coordinates [x,y] at the midpoint of the wall segment
   * @return {Array.<number>}
   */
  get midpoint() {
    return [(this.coords[0] + this.coords[2]) / 2, (this.coords[1] + this.coords[3]) / 2]
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  get center() {
    const [x,y] = this.midpoint;
    return new PIXI.Point(x, y)
  }

  /* -------------------------------------------- */

  /**
   * Get the direction of effect for a directional Wall
   * @type {number|null}
   */
  get direction() {
    let d = this.data.dir;
    if ( !d ) return null;
    let c = this.coords;
    let angle = Math.atan2(c[3] - c[1], c[2] - c[0]);
    if ( d === CONST.WALL_DIRECTIONS.LEFT ) return angle + (Math.PI / 2);
    else return angle - (Math.PI / 2);
  }

  /* -------------------------------------------- */
  /*  Methods                                     */
  /* -------------------------------------------- */

  /**
   * This helper converts the wall segment to a Ray
   * @return {Ray}    The wall in Ray representation
   */
  toRay() {
    return Ray.fromArrays(this.coords.slice(0, 2), this.coords.slice(2,));
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  async draw() {
    this.clear();

    // Draw wall components
    this.directionIcon = this.data.dir ? this.addChild(this._drawDirection()) : null;
    this.line = this.addChild(new PIXI.Graphics());
    this.endpoints = this.addChild(new PIXI.Graphics());

    // Draw a door control icon
    if ( this.isDoor ) this.createDoorControl();

    // Draw current wall
    this.refresh();

    // Enable interactivity, only if the Tile has a true ID
    if ( this.id ) this.activateListeners();
    return this;
  }

  /* -------------------------------------------- */

  /**
   * Draw a control icon that is used to manipulate the door's open/closed state
   * @returns {DoorControl}
   */
  createDoorControl() {
    if ((this.data.door === CONST.WALL_DOOR_TYPES.SECRET) && !game.user.isGM) return null;
    this.doorControl = canvas.controls.doors.addChild(new DoorControl(this));
    this.doorControl.draw();
    return this.doorControl;
  }

  /* -------------------------------------------- */

  /**
   * Determine the orientation of this wall with respect to a reference point
   * @param {Point} point       Some reference point, relative to which orientation is determined
   * @returns {number}          An orientation in CONST.WALL_DIRECTIONS which indicates whether the Point is left,
   *                            right, or collinear (both) with the Wall
   */
  orientPoint(point) {
    const orientation = foundry.utils.orient2dFast(this.A, this.B, point);
    if ( orientation === 0 ) return CONST.WALL_DIRECTIONS.BOTH;
    return orientation < 0 ? CONST.WALL_DIRECTIONS.LEFT : CONST.WALL_DIRECTIONS.RIGHT;
  }

  /* -------------------------------------------- */
  /*  Interactivity                               */
  /* -------------------------------------------- */

  /** @inheritdoc */
  _createInteractionManager() {
    const mgr = super._createInteractionManager();
    mgr.options.target = ["endpoints"];
    return mgr;
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  activateListeners() {
    super.activateListeners();
    this.line.interactive = true;
    this.line.on("mouseover", this._onMouseOverLine, this)
             .on("mouseout", this._onHoverOut, this);
  }

  /* -------------------------------------------- */

  /**
   * Draw a directional prompt icon for one-way walls to illustrate their direction of effect.
   * @return {PIXI.Sprite|null}    The drawn icon
   * @private
   */
  _drawDirection() {
    if (this.directionIcon) this.removeChild(this.directionIcon);
    let d = this.data.dir;
    if ( !d ) return null;

    // Create the icon
    const icon = PIXI.Sprite.from("icons/svg/wall-direction.svg");
    icon.width = icon.height = 32;

    // Rotate the icon
    let iconAngle = -Math.PI / 2;
    let angle = this.direction;
    icon.anchor.set(0.5, 0.5);
    icon.rotation = iconAngle + angle;
    return icon;
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  refresh() {
    const p = this.coords;
    const mp = [(p[0] + p[2]) / 2, (p[1] + p[3]) / 2];
    const wc = this._getWallColor();

    // Determine circle radius and line width
    let lw = 2;
    if ( canvas.dimensions.size > 150 ) lw = 4;
    else if ( canvas.dimensions.size > 100 ) lw = 3;
    const cr = this._hover ? lw * 4 : lw * 3;
    let lw3 = lw * 3;

    // Draw line
    this.line.clear()
      .lineStyle(lw3, 0x000000, 1.0)  // background black
      .moveTo(p[0], p[1])
      .lineTo(p[2], p[3]);
    this.line.lineStyle(lw, wc, 1.0)  // foreground color
      .lineTo(p[0], p[1]);

    // Draw endpoints
    this.endpoints.clear()
      .lineStyle(lw, 0x000000, 1.0)
      .beginFill(wc, 1.0)
      .drawCircle(p[0], p[1], cr)
      .drawCircle(p[2], p[3], cr)
      .endFill();

    // Tint direction icon
    if ( this.directionIcon ) {
      this.directionIcon.position.set(mp[0], mp[1]);
      this.directionIcon.tint = wc;
    }

    // Re-position door control icon
    if ( this.doorControl ) this.doorControl.reposition();

    // Update line hit area
    this.line.hitArea = this._getWallHitPolygon(p, lw3);
    return this;
  }

  /* -------------------------------------------- */

  /**
   * Compute an approximate Polygon which encloses the line segment providing a specific hitArea for the line
   * @param {number[]} coords     The original wall coordinates
   * @param {number} pad          The amount of padding to apply
   * @return {PIXI.Polygon}       A constructed Polygon for the line
   * @private
   */
  _getWallHitPolygon(coords, pad) {

    // Identify wall orientation
    const dx = coords[2] - coords[0];
    const dy = coords[3] - coords[1];

    // Define the array of polygon points
    let points;
    if ( Math.abs(dx) >= Math.abs(dy) ) {
      const sx = Math.sign(dx);
      points = [
        coords[0]-(pad*sx), coords[1]-pad,
        coords[2]+(pad*sx), coords[3]-pad,
        coords[2]+(pad*sx), coords[3]+pad,
        coords[0]-(pad*sx), coords[1]+pad
      ];
    } else {
      const sy = Math.sign(dy);
      points = [
        coords[0]-pad, coords[1]-(pad*sy),
        coords[2]-pad, coords[3]+(pad*sy),
        coords[2]+pad, coords[3]+(pad*sy),
        coords[0]+pad, coords[1]-(pad*sy)
      ];
    }

    // Return a Polygon which pads the line
    return new PIXI.Polygon(points);
  }

  /* -------------------------------------------- */

  /**
   * Given the properties of the wall - decide upon a color to render the wall for display on the WallsLayer
   * @private
   */
  _getWallColor() {

    // Invisible Walls
    if ( this.data.sight === CONST.WALL_SENSE_TYPES.NONE ) return 0x77E7E8;

    // Terrain Walls
    else if ( this.data.sight === CONST.WALL_SENSE_TYPES.LIMITED ) return 0x81B90C;

    // Ethereal Walls
    else if ( this.data.move === CONST.WALL_SENSE_TYPES.NONE ) return 0xCA81FF;

    // Doors
    else if ( this.data.door === CONST.WALL_DOOR_TYPES.DOOR ) {
      let ds = this.data.ds || CONST.WALL_DOOR_STATES.CLOSED;
      if ( ds === CONST.WALL_DOOR_STATES.CLOSED ) return 0x6666EE;
      else if ( ds === CONST.WALL_DOOR_STATES.OPEN ) return 0x66CC66;
      else if ( ds === CONST.WALL_DOOR_STATES.LOCKED ) return 0xEE4444;
    }

    // Secret Doors
    else if ( this.data.door === CONST.WALL_DOOR_TYPES.SECRET ) {
      let ds = this.data.ds || CONST.WALL_DOOR_STATES.CLOSED;
      if ( ds === CONST.WALL_DOOR_STATES.CLOSED ) return 0xA612D4;
      else if ( ds === CONST.WALL_DOOR_STATES.OPEN ) return 0x7C1A9b;
      else if ( ds === CONST.WALL_DOOR_STATES.LOCKED ) return 0xEE4444;
    }

    // Standard Walls
    else return 0xFFFFBB;
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  _onControl({chain=false}={}) {

    // Add chained walls
    if ( chain ) {
      const links = this.getLinkedSegments();
      for ( let l of links.walls ) {
        l._controlled = true;
        this.layer._controlled[l.id] = l;
      }
    }

    // Draw control highlights
    this.layer.highlightControlledSegments();
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  _onRelease(options) {
    this.layer.highlightControlledSegments();
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  destroy(options) {
    if ( this.doorControl ) {
      this.doorControl.destroy({children: true});
      this.doorControl = undefined;
    }
    super.destroy(options);
  }

  /* -------------------------------------------- */

  /**
   * Test whether the Wall direction lies between two provided angles
   * This test is used for collision and vision checks against one-directional walls
   * @param {number} lower    The lower-bound limiting angle in radians
   * @param {number} upper    The upper-bound limiting angle in radians
   * @return {boolean}
   */
  isDirectionBetweenAngles(lower, upper) {
    let d = this.direction;
    if ( d < lower ) {
      while ( d < lower ) d += (2 * Math.PI);
    } else if ( d > upper ) {
      while ( d > upper ) d -= (2 * Math.PI);
    }
    return ( d > lower && d < upper );
  }

  /* -------------------------------------------- */

  /**
   * A simple test for whether a Ray can intersect a directional wall
   * @param {Ray} ray     The ray to test
   * @return {boolean}    Can an intersection occur?
   */
  canRayIntersect(ray) {
    if ( this.direction === null ) return true;
    return this.isDirectionBetweenAngles(ray.angle - (Math.PI/2), ray.angle + (Math.PI/2));
  }

  /* -------------------------------------------- */

  /**
   * Get an Array of Wall objects which are linked by a common coordinate
   * @returns {Object}    An object reporting ids and endpoints of the linked segments
   */
  getLinkedSegments() {
    const test = new Set();
    const done = new Set();
    const ids = new Set();
    const objects = [];

    // Helper function to add wall points to the set
    const _addPoints = w => {
      let p0 = w.coords.slice(0,2).join(".");
      if ( !done.has(p0) ) test.add(p0);
      let p1 = w.coords.slice(2,).join(".");
      if ( !done.has(p1) ) test.add(p1);
    };

    // Helper function to identify other walls which share a point
    const _getWalls = p => {
      return canvas.walls.placeables.filter(w => {
        if ( ids.has(w.id) ) return false;
        let p0 = w.coords.slice(0,2).join(".");
        let p1 = w.coords.slice(2,).join(".");
        return ( p === p0 ) || ( p === p1 );
      })
    };

    // Seed the initial search with this wall's points
    _addPoints(this);

    // Begin recursively searching
    while ( test.size > 0 ) {
      const testIds = new Array(...test);
      for ( let p of testIds ) {
        let walls = _getWalls(p);
        walls.forEach(w => {
          _addPoints(w);
          if ( !ids.has(w.id) ) objects.push(w);
          ids.add(w.id);
        });
        test.delete(p);
        done.add(p);
      }
    }

    // Return the wall IDs and their endpoints
    return {
      ids: new Array(...ids),
      walls: objects,
      endpoints: new Array(...done).map(p => p.split(".").map(Number))
    };
  }

  /* -------------------------------------------- */

  /**
   * Determine whether this wall is beneath a roof tile, and is considered "interior", or not.
   */
  identifyInteriorState() {
    this.roof = null;
    for ( const r of canvas.foreground.roofs ) {
      const isInterior =
        r.containsPixel(this.data.c[0], this.data.c[1]) && r.containsPixel(this.data.c[2], this.data.c[3]);
      if ( isInterior ) this.roof = r;
    }
  }

  /* -------------------------------------------- */

  /**
   * Update any intersections with this wall.
   */
  updateIntersections() {
    this._removeIntersections();
    for ( let other of canvas.walls.placeables ) {
      this._identifyIntersectionsWith(other)
    }
    for ( let boundary of canvas.walls.boundaries ) {
      this._identifyIntersectionsWith(boundary);
    }
  }

  /* -------------------------------------------- */

  /**
   * Record the intersection points between this wall and another, if any.
   * @param {Wall} other  The other wall.
   */
  _identifyIntersectionsWith(other) {
    if ( this === other ) return;
    const {a: wa, b: wb} = this.vertices;
    const {a: oa, b: ob} = other.vertices;

    // Ignore walls which share an endpoint
    if ( this.wallKeys.intersects(other.wallKeys) ) return;

    // Record any intersections
    if ( !foundry.utils.lineSegmentIntersects(wa, wb, oa, ob) ) return;
    const x = foundry.utils.lineLineIntersection(wa, wb, oa, ob);
    if ( !x ) return;  // This eliminates co-linear lines, should not be necessary
    this.intersectsWith.set(other, x);
    other.intersectsWith.set(this, x);
  }

  /* -------------------------------------------- */

  /**
   * Remove this wall's intersections.
   * @private
   */
  _removeIntersections() {
    for ( const other of this.intersectsWith.keys() ) {
      other.intersectsWith.delete(this);
    }
    this.intersectsWith.clear();
  }

  /* -------------------------------------------- */
  /*  Socket Listeners and Handlers               */
  /* -------------------------------------------- */

  /** @inheritdoc */
  _onCreate(...args) {
    super._onCreate(...args);
    this.layer._cloneType = this.document.toJSON();
    this.updateIntersections();
    this.identifyInteriorState();
    this._onModifyWall(this.data.door !== CONST.WALL_DOOR_TYPES.NONE);
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  _onUpdate(data, ...args) {
    super._onUpdate(data, ...args);

    // Re-draw if we have a direction marker
    if ( ("dir" in data) || this.data.dir ) this.draw();

    // If the wall is controlled, update the highlighted segments
    if ( this._controlled ) {
      canvas.addPendingOperation("WallsLayer.highlightControlledSegments", this.layer.highlightControlledSegments, this.layer);
    }

    // Downstream layer operations
    this.layer._cloneType = this.document.toJSON();

    // // If the type of door or door state has changed also modify the door icon
    const rebuildEndpoints = ("c" in data) || CONST.WALL_RESTRICTION_TYPES.some(k => k in data);
    const doorChange = ("door" in data) || ("ds" in data);
    if ( rebuildEndpoints ) {
      // Clear cached vertex information.
      this._vertices = null;
      this._wallKeys = null;
      this.updateIntersections();
      this.identifyInteriorState();
    }
    if ( rebuildEndpoints || doorChange ) this._onModifyWall(doorChange);
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  _onDelete(...args) {
    super._onDelete(...args);
    const wasControlled = this._controlled;

    // Release the deleted wall and update highlighted segments
    this.release();
    if ( wasControlled ) {
      canvas.addPendingOperation("WallsLayer.highlightControlledSegments", this.layer.highlightControlledSegments, this.layer);
    }

    // Refresh the display
    this.doorControl = null;
    this._removeIntersections();
    this._onModifyWall(false);
  }

  /* -------------------------------------------- */

  /**
   * Callback actions when a wall that contains a door is moved or its state is changed
   * @param {boolean} doorChange   Update vision and sound restrictions
   * @private
   */
  _onModifyWall(doorChange=false) {

    const perceptionUpdate = foundry.utils.mergeObject(PerceptionManager.DEFAULTS, {
      lighting: {initialize: true, refresh: true},
      sight: {initialize: true, refresh: true},
      sounds: {initialize: true, refresh: true},
      foreground: {refresh: true}
    }, {inplace: false});

    // Re-draw door icons
    if ( doorChange ) {
      perceptionUpdate.sight.forceUpdateFog = true;
      const dt = this.data.door;
      const hasCtrl = (dt === CONST.WALL_DOOR_TYPES.DOOR) || ((dt === CONST.WALL_DOOR_TYPES.SECRET) && game.user.isGM);
      if ( hasCtrl ) {
        if ( this.doorControl ) this.doorControl.draw(); // Asynchronous
        else this.createDoorControl();
      }
      else if ( this.doorControl ) {
        this.doorControl.parent.removeChild(this.doorControl);
        this.doorControl = null;
      }
    }

    // Re-initialize perception
    canvas.perception.schedule(perceptionUpdate);
  }

  /* -------------------------------------------- */
  /*  Interaction Event Callbacks                 */
  /* -------------------------------------------- */

  /** @inheritdoc */
  _canControl(user, event) {
    // If the User is chaining walls, we don't want to control the last one
    const isChain = this._hover && (game.keyboard.downKeys.size === 1) &&
      game.keyboard.isModifierActive(KeyboardManager.MODIFIER_KEYS.CONTROL);
    return !isChain;
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  _onHoverIn(event, options) {
    this.zIndex = 1;
    if ( !this.layer._chain && event.data ) {
      const dest = event.data.getLocalPosition(this.layer);
      this.layer.last = {
        point: WallsLayer.getClosestEndpoint(dest, this)
      };
    }
    return super._onHoverIn(event, options);
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  _onHoverOut(event) {
    this.zIndex = 0;
    if ( this._hover && !this.layer._chain ) this.layer.last = {point: null};
    return super._onHoverOut(event);
  }

  /* -------------------------------------------- */

  /**
   * Handle mouse-hover events on the line segment itself, pulling the Wall to the front of the container stack
   * @private
   */
  _onMouseOverLine(event) {
    event.stopPropagation();
    if ( this.layer.preview.children.length ) return;
    this.zIndex = 1;
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  _onClickLeft(event) {
    const oe = event.data.originalEvent;
    if ( this._controlled ) {
      if ( oe.shiftKey ) return this.release();
    }
    else return this.control({releaseOthers: !oe.shiftKey, chain: oe.altKey});
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  _onClickLeft2(event) {
    const sheet = this.sheet;
    sheet.render(true, {walls: this.layer.controlled});
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  _onClickRight2(event) {
    return this._onClickLeft2(event);
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  _onDragLeftStart(event) {
    const { origin } = event.data;
    const dLeft = Math.hypot(origin.x - this.coords[0], origin.y - this.coords[1]);
    const dRight = Math.hypot(origin.x - this.coords[2], origin.y - this.coords[3]);
    event.data["fixed"] = dLeft < dRight ? 1 : 0; // Affix the opposite point
    return super._onDragLeftStart(event);
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  _onDragLeftMove(event) {
    const {clones, destination, fixed, origin, originalEvent} = event.data;

    // Pan the canvas if the drag event approaches the edge
    canvas._onDragCanvasPan(originalEvent);

    // Group movement
    if ( clones.length > 1 ) {
      const dx = destination.x - origin.x;
      const dy = destination.y - origin.y;
      for ( let c of clones ) {
        c.data.c = c._original.data.c.map((p, i) => i % 2 ? p + dy : p + dx);
      }
    }

    // Single-wall pivot
    else if ( clones.length === 1 ) {
      const w = clones[0];
      const pt = [destination.x, destination.y];
      w.data.c = fixed ? pt.concat(this.coords.slice(2,4)) : this.coords.slice(0, 2).concat(pt);
    }

    // Refresh display
    clones.forEach(c => c.refresh());
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  async _onDragLeftDrop(event) {
    const {clones, destination, fixed, originalEvent} = event.data;
    const layer = this.layer;
    const snap = layer._forceSnap || !originalEvent.shiftKey;

    // Get the snapped final point
    const pt = this.layer._getWallEndpointCoordinates(destination, {snap});

    // Pivot a single wall
    if ( clones.length === 1 ) {
      const p0 = fixed ? this.coords.slice(2,4) : this.coords.slice(0,2);
      const coords = fixed ? pt.concat(p0) : p0.concat(pt);
      if ( (coords[0] === coords[2]) && (coords[1] === coords[3]) ) {
        return this.document.delete(); // If we collapsed the wall, delete it
      }
      this.layer.last.point = pt;
      return this.document.update({c: coords});
    }

    // Drag a group of walls - snap to the end point maintaining relative positioning
    const p0 = fixed ? this.coords.slice(0,2) : this.coords.slice(2,4);
    const dx = pt[0] - p0[0];
    const dy = pt[1] - p0[1];
    const updates = clones.map(w => {
      const c = w._original.data.c;
      return {_id: w._original.id, c: [c[0]+dx, c[1]+dy, c[2]+dx, c[3]+dy]};
    });
    return canvas.scene.updateEmbeddedDocuments("Wall", updates);
  }
}
