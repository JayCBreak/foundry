/**
 * A Note is an implementation of PlaceableObject which represents an annotated location within the Scene.
 * Each Note links to a JournalEntry document and represents its location on the map.
 * @extends {PlaceableObject}
 */
class Note extends PlaceableObject {

  /** @inheritdoc */
  static embeddedName = "Note";

  /* -------------------------------------------- */

  /** @override */
  get bounds() {
    const r = this.data.iconSize / 2;
    return new NormalizedRectangle(this.data.x - r, this.data.y - r, 2*r, 2*r);
  }

  /* -------------------------------------------- */

  /**
   * The associated JournalEntry which is referenced by this Note
   * @type {JournalEntry}
   */
  get entry() {
    return this.document.entry;
  }

  /* -------------------------------------------- */

  /**
   * The text label used to annotate this Note
   * @type {string}
   */
  get text() {
    return this.document.label;
  }

  /* -------------------------------------------- */

  /**
   * The Map Note icon size
   * @type {number}
   */
  get size() {
    return this.data.iconSize || 40;
  }

  /* -------------------------------------------- */

  /**
   * Determine whether the Note is visible to the current user based on their perspective of the Scene.
   * Visibility depends on permission to the underlying journal entry, as well as the perspective of controlled Tokens.
   * If Token Vision is required, the user must have a token with vision over the note to see it.
   * @type {boolean}
   */
  get isVisible() {
    const access = this.entry?.testUserPermission(game.user, "LIMITED") ?? true;
    if ( (access === false) || !canvas.sight.tokenVision ) return access;
    const point = {x: this.data.x, y: this.data.y};
    const tolerance = this.data.iconSize / 4;
    return canvas.sight.testVisibility(point, {tolerance, object: this});
  }

  /* -------------------------------------------- */
  /* Rendering
  /* -------------------------------------------- */

  /** @inheritdoc */
  async draw() {
    this.clear();

    // Draw the control icon
    this.controlIcon = this.addChild(this._drawControlIcon());

    // Draw the note tooltip
    this.tooltip = this.addChild(this._drawTooltip());

    // Refresh the current display
    this.refresh();

    // Add control interactivity if the placeable has an ID
    if ( this.id ) this.activateListeners();
    return this;
  }

  /* -------------------------------------------- */

  /**
   * Draw the ControlIcon for the Map Note
   * @returns {ControlIcon}
   * @protected
   */
  _drawControlIcon() {
    let tint = this.data.iconTint ? foundry.utils.colorStringToHex(this.data.iconTint) : null;
    let icon = new ControlIcon({texture: this.data.icon, size: this.size, tint: tint});
    icon.x -= (this.size / 2);
    icon.y -= (this.size / 2);
    return icon;
  }

  /* -------------------------------------------- */

  /**
   * Draw the map note Tooltip as a Text object
   * @returns {PIXI.Text}
   * @protected
   */
  _drawTooltip() {

    // Create the Text object
    const textStyle = this._getTextStyle();
    const text = new PreciseText(this.text, textStyle);
    text.visible = false;
    const halfPad = (0.5 * this.size) + 12;

    // Configure Text position
    switch ( this.data.textAnchor ) {
      case CONST.TEXT_ANCHOR_POINTS.CENTER:
        text.anchor.set(0.5, 0.5);
        text.position.set(0, 0);
        break;
      case CONST.TEXT_ANCHOR_POINTS.BOTTOM:
        text.anchor.set(0.5, 0);
        text.position.set(0, halfPad);
        break;
      case CONST.TEXT_ANCHOR_POINTS.TOP:
        text.anchor.set(0.5, 1);
        text.position.set(0, -halfPad);
        break;
      case CONST.TEXT_ANCHOR_POINTS.LEFT:
        text.anchor.set(1, 0.5);
        text.position.set(-halfPad, 0);
        break;
      case CONST.TEXT_ANCHOR_POINTS.RIGHT:
        text.anchor.set(0, 0.5);
        text.position.set(halfPad, 0);
        break;
    }
    return text;
  }

  /* -------------------------------------------- */

  /**
   * Define a PIXI TextStyle object which is used for the tooltip displayed for this Note
   * @returns {PIXI.TextStyle}
   * @protected
   */
  _getTextStyle() {
    const style = CONFIG.canvasTextStyle.clone();

    // Positioning
    if ( this.data.textAnchor === CONST.TEXT_ANCHOR_POINTS.LEFT ) style.align = "right";
    else if ( this.data.textAnchor === CONST.TEXT_ANCHOR_POINTS.RIGHT ) style.align = "left";

    // Font preferences
    style.fontFamily = this.data.fontFamily || CONFIG.defaultFontFamily;
    style.fontSize = this.data.fontSize;

    // Toggle stroke style depending on whether the text color is dark or light
    const color = this.data.textColor ? foundry.utils.colorStringToHex(this.data.textColor) : 0xFFFFFF;
    const hsv = foundry.utils.rgbToHsv(...foundry.utils.hexToRGB(color));
    style.fill = color;
    style.strokeThickness = 4;
    style.stroke = hsv[2] > 0.6 ? 0x000000 : 0xFFFFFF;
    return style;
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  refresh() {
    this.position.set(this.data.x, this.data.y);
    this.controlIcon.border.visible = this._hover;
    this.tooltip.visible = this._hover;
    this.visible = this.isVisible;
    return this;
  }

  /* -------------------------------------------- */
  /*  Event Handlers                              */
  /* -------------------------------------------- */

  /** @override */
  _onUpdate(data) {
    this.draw();
  }

  /* -------------------------------------------- */

  /** @override */
  _canHover(user) {
    return true;
  }

  /* -------------------------------------------- */

  /** @override */
  _canView(user) {
    return this.entry?.testUserPermission(game.user, "LIMITED") ?? false;
  }

  /* -------------------------------------------- */

  /** @override */
  _canConfigure(user) {
    return canvas.notes._active && this.document.canUserModify(game.user, "update");
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  _onHoverIn(event, options) {
    this.zIndex = Math.max(...this.layer.placeables.map(n => n.data.z || 0)) + 1;
    return super._onHoverIn(event, options);
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  _onHoverOut(event) {
    this.zIndex = this.data.z;
    return super._onHoverOut(event);
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  _onClickLeft2(event) {
    if ( this.entry ) this.entry.sheet.render(true);
  }
}
