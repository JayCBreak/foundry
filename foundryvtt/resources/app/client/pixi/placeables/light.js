/**
 * An AmbientLight is an implementation of PlaceableObject which represents a dynamic light source within the Scene.
 * @extends {PlaceableObject}
 */
class AmbientLight extends PlaceableObject {
  constructor(document) {
    super(document);

    /**
     * A reference to the PointSource object which defines this light source area of effect
     * @type {LightSource}
     */
    this.source = new LightSource(this);
  }

  /**
   * A reference to the ControlIcon used to configure this light
   * @type {ControlIcon}
   */
  controlIcon;

  /* --------------------c------------------------ */

  /** @inheritdoc */
  static embeddedName = "AmbientLight";

  /* -------------------------------------------- */

  /** @inheritdoc */
  get bounds() {
    const r = Math.max(this.dimRadius, this.brightRadius);
    return new NormalizedRectangle(this.data.x-r, this.data.y-r, 2*r, 2*r);
  }

  /* -------------------------------------------- */

  /**
   * A convenience accessor to the LightData configuration object
   * @returns {foundry.data.LightData}
   */
  get config() {
    return this.data.config;
  }

  /* -------------------------------------------- */

  /**
   * Test whether a specific AmbientLight source provides global illumination
   * @type {boolean}
   */
  get global() {
    return this.document.isGlobal;
  }

  /* -------------------------------------------- */

  /**
   * The maximum radius in pixels of the light field
   * @type {number}
   */
  get radius() {
    return Math.max(Math.abs(this.dimRadius), Math.abs(this.brightRadius));
  }

  /* -------------------------------------------- */

  /**
   * Get the pixel radius of dim light emitted by this light source
   * @type {number}
   */
  get dimRadius() {
    let d = canvas.dimensions;
    return ((this.config.dim / d.distance) * d.size);
  }

  /* -------------------------------------------- */

  /**
   * Get the pixel radius of bright light emitted by this light source
   * @type {number}
   */
  get brightRadius() {
    let d = canvas.dimensions;
    return ((this.config.bright / d.distance) * d.size);
  }

  /* -------------------------------------------- */

  /**
   * Is this ambient light is currently visible based on its hidden state and the darkness level of the Scene?
   * @type {boolean}
   */
  get isVisible() {
    if ( this.data.hidden ) return false;
    return this.layer.darknessLevel.between(this.config.darkness.min ?? 0, this.config.darkness.max ?? 1);
  }

  /* -------------------------------------------- */
  /* Rendering
  /* -------------------------------------------- */

  /** @inheritdoc */
  async draw() {

    // Draw containers
    this.clear();
    this.field = this.addChild(new PIXI.Graphics());
    this.controlIcon = this.addChild(this._drawControlIcon());

    // Initial rendering
    this.updateSource({defer: true});
    this.refresh();
    if ( this.id ) this.activateListeners();
    return this;
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  destroy(options) {
    this.source.destroy();
    return super.destroy(options);
  }

  /* -------------------------------------------- */

  /**
   * Draw the ControlIcon for the AmbientLight
   * @return {ControlIcon}
   * @private
   */
  _drawControlIcon() {
    const size = Math.max(Math.round((canvas.dimensions.size * 0.5) / 20) * 20, 40);
    let icon = new ControlIcon({texture: CONFIG.controlIcons.light, size: size });
    icon.x -= (size * 0.5);
    icon.y -= (size * 0.5);
    return icon;
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  refresh() {
    const active = this.layer._active;

    // Update position and FOV
    this.position.set(this.data.x, this.data.y);
    this.field.position.set(-this.data.x, -this.data.y);

    // Draw the light preview field
    const l = this.field.clear();
    if ( active ) l.lineStyle(2, 0xEEEEEE, 0.4).drawShape(this.source.los);

    // Update control icon appearance
    this.refreshControl();
    return this;
  }

  /* -------------------------------------------- */

  /**
   * Refresh the display of the ControlIcon for this AmbientLight source
   */
  refreshControl() {
    this.controlIcon.texture = getTexture(this.isVisible ? CONFIG.controlIcons.light : CONFIG.controlIcons.lightOff);
    this.controlIcon.tintColor = this.data.hidden ? 0xFF3300 : 0xFFFFFF;
    this.controlIcon.borderColor = this.data.hidden ? 0xFF3300 : 0xFF5500;
    this.controlIcon.draw();
    this.controlIcon.visible = this.layer._active;
    this.controlIcon.border.visible = this._hover;
  }

  /* -------------------------------------------- */
  /*  Light Source Management                     */
  /* -------------------------------------------- */

  /**
   * The named identified for the source object associated with this light
   * @return {string}
   */
  get sourceId() {
    return `${this.document.documentName}.${this._original?.id ?? this.document.id ?? "preview"}`;
  }

  /* -------------------------------------------- */

  /**
   * Update the source object associated with this light
   * @param {boolean} defer     Defer refreshing the LightingLayer to manually call that refresh later.
   * @param {boolean} deleted   Indicate that this light source has been deleted.
   */
  updateSource({defer=false, deleted=false}={}) {
    if ( deleted ) {
      this.layer.sources.delete(this.sourceId);
      return defer ? null : this.layer.refresh();
    }

    // Update source data
    const d = canvas.dimensions;
    const sourceData = foundry.utils.mergeObject(this.config.toObject(false), {
      x: this.data.x,
      y: this.data.y,
      rotation: this.data.rotation,
      dim: Math.clamped(this.dimRadius, 0, d.maxR),
      bright: Math.clamped(this.brightRadius, 0, d.maxR),
      walls: this.data.walls,
      vision: this.data.vision,
      z: this.document.getFlag("core", "priority") ?? null,
      seed: this.document.getFlag("core", "animationSeed")
    });
    this.source.initialize(sourceData);

    // Update the lighting layer sources
    const isActive = (this.source.radius > 0) && !this.data.hidden;
    if ( isActive ) this.layer.sources.set(this.sourceId, this.source);
    else this.layer.sources.delete(this.sourceId);

    // Refresh the layer, unless we are deferring that update
    if ( !defer ) canvas.perception.schedule({
      lighting: {refresh: true},
      sight: {
        refresh: true,
        forceUpdateFog: true // Update exploration even if the token hasn't moved
      }
    });
  }

  /* -------------------------------------------- */
  /*  Socket Listeners and Handlers               */
  /* -------------------------------------------- */

  /** @inheritdoc */
  _onCreate(...args) {
    super._onCreate(...args);
    this.updateSource();
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  _onUpdate(...args) {
    this.updateSource();
    super._onUpdate(...args);
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  _onDelete(...args) {
    super._onDelete(...args);
    this.updateSource({deleted: true});
  }

  /* -------------------------------------------- */
  /*  Mouse Interaction Handlers                  */
  /* -------------------------------------------- */

  /** @inheritdoc */
  _canHUD(user, event) {
    return user.isGM; // Allow GMs to single right-click
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  _canConfigure(user, event) {
    return false; // Double-right does nothing
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  _onClickRight(event) {
    this.document.update({hidden: !this.data.hidden});
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  _onDragLeftStart(event) {
    this.layer.deactivateAnimation();
    super._onDragLeftStart(event);
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  _onDragLeftMove(event) {
    super._onDragLeftMove(event);
    const clones = event.data["clones"] || [];
    for ( let c of clones ) {
      c.updateSource({defer: true});
    }
    this.layer.refresh();
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  _onDragLeftCancel(event) {
    super._onDragLeftCancel(event);
    this.updateSource();
    this.layer.activateAnimation();
  }
}
