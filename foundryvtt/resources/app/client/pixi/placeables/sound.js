/**
 * An AmbientSound is an implementation of PlaceableObject which represents a dynamic audio source within the Scene.
 * @extends {PlaceableObject}
 */
class AmbientSound extends PlaceableObject {
  constructor(...args) {
    super(...args);

    /**
     * The Sound which manages playback for this AmbientSound effect
     * @type {Sound|null}
     */
    this.sound = this._createSound();

    /**
     * A SoundSource object which manages the area of effect for this ambient sound
     * @type {SoundSource}
     */
    this.source = new SoundSource(this);
  }

  /** @inheritdoc */
  static embeddedName ="AmbientSound";

  /* -------------------------------------------- */

  /**
   * Create a Sound used to play this AmbientSound object
   * @returns {Sound|null}
   * @private
   */
  _createSound() {
    if ( !this.id || !this.data.path ) return null;
    return game.audio.create({
      src: this.data.path,
      preload: true,
      autoplay: false,
      singleton: true
    });
  }

  /* -------------------------------------------- */
  /* Properties
  /* -------------------------------------------- */

  /**
   * Is this ambient sound is currently audible based on its hidden state and the darkness level of the Scene?
   * @type {boolean}
   */
  get isAudible() {
    if ( this.data.hidden ) return false;
    return canvas.lighting.darknessLevel.between(this.data.darkness.min ?? 0, this.data.darkness.max ?? 1);
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  get bounds() {
    const r = this.radius;
    return new NormalizedRectangle(this.data.x - r, this.data.y - r, 2*r, 2*r);
  }

  /* -------------------------------------------- */

  /**
   * The named identified for the source object associated with this ambient sound
   * @return {string}
   */
  get sourceId() {
    return `${this.document.documentName}.${this._original?.id ?? this.document.id ?? "preview"}`;
  }

  /* -------------------------------------------- */

  /**
   * A convenience accessor for the sound radius in pixels
   * @type {number}
   */
  get radius() {
    let d = canvas.dimensions;
    return ((this.data.radius / d.distance) * d.size);
  }

  /* -------------------------------------------- */
  /* Methods
  /* -------------------------------------------- */

  /**
   * Toggle playback of the sound depending on whether or not it is audible
   * @param {boolean} isAudible     Is the sound audible?
   * @param {number} volume         The target playback volume
   * @param {object} [options={}]   Additional options which affect sound synchronization
   * @param {number} [options.fade=250]  A duration in milliseconds to fade volume transition
   */
  sync(isAudible, volume, {fade=250}={}) {
    const sound = this.sound;
    if ( !sound ) return;
    if ( !sound.loaded ) {
      if ( sound.loading instanceof Promise ) {
        sound.loading.then(() => this.sync(isAudible, volume, {fade}));
      }
      return;
    }

    // Fade the sound out if not currently audible
    if ( !isAudible ) {
      if ( !sound.playing || (sound.volume === 0) ) return;
      if ( fade ) sound.fade(0, {duration: fade});
      else sound.volume = 0;
      return;
    }

    // Begin playback at the desired volume
    if ( !sound.playing ) sound.play({volume: 0, loop: true});

    // Adjust the target volume
    const targetVolume = (volume ?? this.data.volume) * game.settings.get("core", "globalAmbientVolume");
    if ( fade ) sound.fade(targetVolume, {duration: fade});
    else sound.volume = targetVolume;
  }

  /* -------------------------------------------- */
  /* Rendering
  /* -------------------------------------------- */

  /** @inheritdoc */
  clear() {
    if ( this.controlIcon ) {
      this.controlIcon.parent.removeChild(this.controlIcon).destroy();
      this.controlIcon = null;
    }
    return super.clear();
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  async draw() {

    // Draw containers
    this.clear();
    this.field = this.addChild(new PIXI.Graphics());
    this.controlIcon = this.addChild(this._drawControlIcon());

    // Initial rendering
    this.updateSource();
    if ( this.id ) this.activateListeners();

    // Refresh the current display
    this.refresh();
    return this;
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  destroy(options) {
    this.source.destroy();
    return super.destroy(options);
  }

  /* -------------------------------------------- */

  /**
   * Draw the ControlIcon for the AmbientLight
   * @return {ControlIcon}
   * @private
   */
  _drawControlIcon() {
    const size = Math.max(Math.round((canvas.dimensions.size * 0.5) / 20) * 20, 40);
    let icon = new ControlIcon({texture: CONFIG.controlIcons.sound, size: size});
    icon.x -= (size * 0.5);
    icon.y -= (size * 0.5);
    return icon;
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  refresh() {

    // Update position
    this.position.set(this.data.x, this.data.y);
    this.field.position.set(-this.data.x, -this.data.y);

    // Draw the light field
    this.field.clear();
    if ( this.data.radius > 0 ) {
      this.field.beginFill(0xAADDFF, 0.15)
        .lineStyle(1, 0xFFFFFF, 0.5)
        .drawShape(this.source.los)
        .endFill();
    }

    // Update control icon appearance
    this.refreshControl();
    return this;
  }

  /* -------------------------------------------- */

  /**
   * Refresh the display of the ControlIcon for this AmbientSound source
   */
  refreshControl() {
    const isHidden = this.id && (this.data.hidden || !this.data.path);
    this.controlIcon.tintColor = isHidden ? 0xFF3300 : 0xFFFFFF;
    this.controlIcon.borderColor = isHidden ? 0xFF3300 : 0xFF5500;
    this.controlIcon.texture = getTexture(this.isAudible ? CONFIG.controlIcons.sound : CONFIG.controlIcons.soundOff);
    this.controlIcon.draw();
    this.controlIcon.visible = this.layer._active;
    this.controlIcon.border.visible = this._hover;
  }

  /* -------------------------------------------- */

  /**
   * Compute the field-of-vision for an object, determining its effective line-of-sight and field-of-vision polygons
   * @param {boolean} defer     Defer refreshing the SoundsLayer to manually call that refresh later.
   * @param {boolean} deleted   Indicate that this SoundSource has been deleted.
   */
  updateSource({defer=false, deleted=false}={}) {
    if ( deleted ) {
      this.layer.sources.delete(this.sourceId);
      return defer ? null : this.layer.refresh();
    }

    // Re-initialize the source
    this.source.initialize({
      x: this.data.x,
      y: this.data.y,
      radius: Math.clamped(this.radius, 0, canvas.dimensions.maxR),
      walls: this.data.walls,
      z: this.document.getFlag("core", "priority") ?? null
    });

    // Update the sound layer sources
    const isActive = (this.source.data.radius > 0) && !this.data.hidden;
    if ( isActive ) this.layer.sources.set(this.sourceId, this.source);
    else this.layer.sources.delete(this.sourceId);

    // Refresh the layer unless the update is deferred
    if ( !defer ) canvas.perception.schedule({sounds: {refresh: true}});
  }

  /* -------------------------------------------- */
  /*  Document Event Handlers                     */
  /* -------------------------------------------- */

  /** @inheritdoc */
  _onCreate(...args) {
    super._onCreate(...args);
    this.updateSource();
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  _onUpdate(data, ...args) {
    if ( "path" in data ) {
      if ( this.sound ) this.sound.stop();
      this.sound = this._createSound();
    }
    this.updateSource();
    return super._onUpdate(data, ...args);
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  _onDelete(...args) {
    super._onDelete(...args);
    if ( this.sound ) {
      if ( !this.sound.loaded && (this.sound.loading instanceof Promise) ) {
        this.sound.loading.then(() => this.sound.stop());
      }
      else this.sound.stop();
    }
    this.updateSource({deleted: true});
  }

  /* -------------------------------------------- */
  /*  Interaction Event Handlers                  */
  /* -------------------------------------------- */

  /** @inheritdoc */
  _canHUD(user, event) {
    return user.isGM; // Allow GMs to single right-click
  }

  /** @inheritdoc */
  _canConfigure(user, event) {
    return false; // Double-right does nothing
  }

  /** @inheritdoc */
  _onClickRight(event) {
    this.document.update({hidden: !this.data.hidden});
  }

  /** @override */
  _onDragLeftMove(event) {
    const {clones, destination, origin, originalEvent} = event.data;
    canvas._onDragCanvasPan(originalEvent);
    const dx = destination.x - origin.x;
    const dy = destination.y - origin.y;
    for ( let c of clones || [] ) {
      c.data.x = c._original.data.x + dx;
      c.data.y = c._original.data.y + dy;
      c.updateSource();
      c.refresh();
    }
  }
}
