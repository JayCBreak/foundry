/**
 * A Tile is an implementation of PlaceableObject which represents a static piece of artwork or prop within the Scene.
 * Tiles are drawn inside a {@link BackgroundLayer} container.
 * @extends {PlaceableObject}
 *
 * @see {@link TileDocument}
 * @see {@link BackgroundLayer}
 * @see {@link TileSheet}
 * @see {@link TileHUD}
 */
class Tile extends PlaceableObject {

  /* -------------------------------------------- */
  /*  Attributes                                  */
  /* -------------------------------------------- */

  /**
   * The Tile border frame
   * @extends {PIXI.Container}
   * @property {PIXI.Graphics} border
   * @property {ResizeHandle} handle
   */
  frame;

  /**
   * The primary tile image texture
   * @type {PIXI.Texture}
   */
  texture;

  /**
   * The Tile image sprite
   * @type {PIXI.Sprite}
   */
  tile;

  /**
   * The occlusion image sprite
   * @type {PIXI.Sprite}
   */
  occlusionTile;

  /**
   * A Tile background which is displayed if no valid image texture is present
   * @type {PIXI.Graphics}
   */
  bg;

  /**
   * A cached mapping of non-transparent pixels
   * @type {{minX: number, minY: number, maxX: number, maxY: number, pixels: Uint8Array|undefined, texture: PIXI.RenderTexture|undefined}}
   * @private
   */
  _alphaMap;

  /**
   * A flag which tracks whether the overhead tile is currently in an occluded state
   * @type {boolean}
   */
  occluded = false;

  /**
   * A flag which tracks if the Tile is currently playing
   * @type {boolean}
   */
  playing = false;

  /** @inheritdoc */
  static embeddedName = "Tile";

  /* -------------------------------------------- */

  /**
   * Get the native aspect ratio of the base texture for the Tile sprite
   * @type {number}
   */
  get aspectRatio() {
    if (!this.texture) return 1;
    let tex = this.texture.baseTexture;
    return (tex.width / tex.height);
  }

  /* -------------------------------------------- */

  /** @override */
  get bounds() {
    const {x, y, width, height} = this.data;
    return new NormalizedRectangle(x, y, Math.abs(width), Math.abs(height));
  }

  /* -------------------------------------------- */

  /**
   * The HTML source element for the primary Tile texture
   * @type {HTMLImageElement|HTMLVideoElement}
   */
  get sourceElement() {
    return this.texture?.baseTexture.resource.source;
  }

  /* -------------------------------------------- */

  /**
   * Does this Tile depict an animated video texture?
   * @type {boolean}
   */
  get isVideo() {
    const source = this.sourceElement;
    return source?.tagName === "VIDEO";
  }

  /* -------------------------------------------- */

  /**
   * Is this tile a roof
   * @returns {boolean}
   */
  get isRoof() {
    const inForeground = this.parent?.parent === canvas.foreground;
    return inForeground && (this.data.occlusion.mode === CONST.TILE_OCCLUSION_MODES.ROOF);
  }

  /* -------------------------------------------- */

  /**
   * The effective volume at which this Tile should be playing, including the global ambient volume modifier
   * @type {number}
   */
  get volume() {
    return this.data.video.volume * game.settings.get("core", "globalAmbientVolume");
  }

  /* -------------------------------------------- */
  /*  Rendering                                   */
  /* -------------------------------------------- */

  /** @inheritdoc */
  async draw() {
    this.clear();

    // Create the image texture
    this.tile = undefined;
    if ( this.data.img ) {
      this.texture = await loadTexture(this.data.img, {fallback: 'icons/svg/hazard.svg'});
      if ( this.texture?.valid ) {

        // Allow for a TilingSprite to be used, if desired by API developers
        const isTiling = this.document.getFlag("core", "isTilingSprite");
        const spriteCls = isTiling ? PIXI.TilingSprite : PIXI.Sprite;

        // Create and anchor the Sprite to the parent container
        this.tile = this.addChild(new spriteCls(this.texture));
        this.tile.anchor.set(0.5, 0.5);

        // Additional configuration for tiling sprites
        if ( isTiling ) {
          this.tile.width = this.data.width;
          this.tile.height = this.data.height;
          // FIXME: This is a workaround currently needed for TilingSprite textures due to a presumed upstream PIXI bug
          this.texture.baseTexture.mipmap = PIXI.MIPMAP_MODES.OFF;
          this.texture.baseTexture.update();
        }
      }
      this.bg = undefined;
    }

    // If there was no valid texture, display a background
    if ( !this.tile ) {
      this.texture = null;
      this.tile = undefined;
      this.bg = this.addChild(new PIXI.Graphics());
    }

    // Create the outer frame for the border and interaction handles
    this.frame = this.addChild(new PIXI.Container());
    this.frame.border = this.frame.addChild(new PIXI.Graphics());
    this.frame.handle = this.frame.addChild(new ResizeHandle([1, 1]));

    // Refresh the current display
    this.refresh({refreshPerception: true});
    this.zIndex = this.data.z;

    // The following options do not apply to preview tiles
    if ( this.id && this.parent ) {

      // Configure Video playback
      if ( this.isVideo ) {
        await this._unlinkVideoPlayback(this.sourceElement);
        this.play();
      }

      // Create an alpha map of the tile's pixels
      if ( this.data.overhead ) {
        this._createAlphaMap({
          keepPixels: true,
          keepTexture: this.isRoof
        });
      }

      // Apply an overhead occlusion filter
      if ( this.tile ) {
        this.occlusionFilter = this.data.overhead ? this._createOcclusionFilter() : undefined;
        this.tile.filters = this.data.overhead ? [this.occlusionFilter] : [];
        if ( this.isRoof ) canvas.foreground.addRoofSprite(this);
        else if ( this.occlusionTile ) canvas.foreground.removeRoofSprite(this);
      }

      // Enable interactivity
      this.activateListeners();
    }
    return this;
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  destroy(options) {
    if ( this.isVideo && this.id && this.parent ) this.texture.baseTexture?.destroy();
    return super.destroy(options);
  }

  /* -------------------------------------------- */

  /**
   * @param {object} [options]
   * @param {boolean} [refreshPerception=false]  Also refresh the perception layer.
   * @override
   */
  refresh({refreshPerception=false}={}) {
    const aw = Math.abs(this.data.width);
    const ah = Math.abs(this.data.height);
    const r = Math.toRadians(this.data.rotation);

    // Update tile appearance
    this.position.set(this.data.x, this.data.y);
    if ( this.tile ) {

      // Tile position
      this.tile.scale.x = this.data.width / this.texture.width;
      this.tile.scale.y = this.data.height / this.texture.height;
      this.tile.position.set(aw/2, ah/2);
      this.tile.rotation = r;
      this.tile.tint = this.data.tint ? foundry.utils.colorStringToHex(this.data.tint) : 0xFFFFFF;

      // If the tile is hidden, constrain its alpha
      if ( this.data.hidden ) this.tile.alpha = Math.min(0.5, this.data.alpha);

      // If the occlusion filter is enabled, alpha will be handled inside the filter
      else if ( this.occlusionFilter?.enabled ) {
        this.tile.alpha = 1.0;
        this.occlusionFilter.uniforms.alpha = this.data.alpha;
      }

      // Otherwise, toggle alpha based on occlusion
      else this.tile.alpha = this.occluded ? Math.min(this.data.occlusion.alpha, this.data.alpha) : this.data.alpha;
    }

    // Temporary tile background
    if ( this.bg ) this.bg.clear().beginFill(0xFFFFFF, 0.5).drawRect(0, 0, aw, ah).endFill();

    // Define bounds and update the border frame
    let bounds = ( aw === ah ) ?
      new NormalizedRectangle(0, 0, aw, ah) : // Square tiles
      NormalizedRectangle.fromRotation(0, 0, aw, ah, r); // Non-square tiles
    this.hitArea = this._controlled ? bounds.clone().pad(20) : bounds;
    if ( this.frame ) {
      this._refreshBorder(bounds);
      this._refreshHandle(bounds);
    }

    // Set visibility
    this.visible = !this.data.hidden || game.user.isGM;
    if ( refreshPerception ) this._refreshPerception();
    return this;
  }

  /* -------------------------------------------- */

  /**
   * Refresh the display of the Tile border
   * @private
   */
  _refreshBorder(b) {
    const border = this.frame.border;

    // Determine border color
    const colors = CONFIG.Canvas.dispositionColors;
    let bc = colors.INACTIVE;
    if ( this._controlled ) {
      bc = this.data.locked ? colors.HOSTILE : colors.CONTROLLED;
    }

    // Draw the tile border
    const t = CONFIG.Canvas.objectBorderThickness;
    const h = Math.round(t/2);
    const o = Math.round(h/2);
    border.clear()
      .lineStyle(t, 0x000000, 1.0).drawRoundedRect(b.x-o, b.y-o, b.width+h, b.height+h, 3)
      .lineStyle(h, bc, 1.0).drawRoundedRect(b.x-o, b.y-o, b.width+h, b.height+h, 3);
    border.visible = this._hover || this._controlled;
  }

  /* -------------------------------------------- */

  /**
   * Refresh the display of the Tile resizing handle
   * @protected
   */
  _refreshHandle(b) {
    this.frame.handle.refresh(b);
    this.frame.handle.visible = this._controlled && !this.data.locked;
  }

  /* -------------------------------------------- */
  /*  Methods                                     */
  /* -------------------------------------------- */

  /**
   * Play video for this Tile (if applicable).
   * @param {boolean} [playing]     Should the Tile video be playing?
   * @param {object} [options={}]   Additional options for modifying video playback
   * @param {boolean} [options.loop]    Should the video loop?
   * @param {number} [options.offset]   A specific timestamp between 0 and the video duration to begin playback
   * @param {number} [options.volume]   Desired volume level of the video's audio channel (if any)
   */
  play(playing, {loop, offset, volume}={}) {
    const el = this.sourceElement;
    if ( el?.tagName !== "VIDEO" ) return;
    playing = playing ?? this.data.video.autoplay;
    this.playing = playing;
    el.loop = loop ?? this.data.video.loop;
    el.volume = volume ?? this.volume;
    el.muted = el.volume === 0;
    if ( typeof offset === "number" ) el.currentTime = Math.clamped(offset, 0, el.duration);
    if ( playing ) game.video.play(el);
    else el.pause();
  }

  /* -------------------------------------------- */

  /**
   * Unlink the playback of this video tile from the playback of other tokens which are using the same base texture.
   * @param {HTMLVideoElement} source     The video element source
   * @returns {Promise<void>}
   * @private
   */
  async _unlinkVideoPlayback(source) {
    const s = source.cloneNode();
    if ( this.data.video.autoplay ) s.onplay = () => s.currentTime = Math.random() * source.duration;
    await new Promise(resolve => s.oncanplay = resolve);
    this.texture = this.tile.texture = PIXI.Texture.from(s, {resourceOptions: {autoPlay: false}});
  }

  /* -------------------------------------------- */

  /**
   * Update the occlusion rendering for this overhead Tile for a given controlled Token.
   * @param {Token[]} tokens     The set of currently controlled Token objects
   */
  updateOcclusion(tokens) {
    const m = this.data.occlusion.mode;
    let occluded = false;
    if ( m !== CONST.TILE_OCCLUSION_MODES.NONE ) {
      for ( let token of tokens ) {
        occluded = this.testOcclusion(token, {corners: m !== CONST.TILE_OCCLUSION_MODES.ROOF});
        if (occluded) break;
      }
    }
    this.occluded = occluded;
  }

  /* -------------------------------------------- */

  /**
   * Test whether a specific Token occludes this overhead tile.
   * Occlusion is tested against 9 points, the center, the four corners-, and the four cardinal directions
   * @param {Token} token       The Token to test
   * @param {object} [options]  Additional options that affect testing
   * @param {boolean} [options.corners=true]  Test corners of the hit-box in addition to the token center?
   * @returns {boolean}         Is the Token occluded by the Tile?
   */
  testOcclusion(token, {corners=true}={}) {
    const w = token.w;
    const h = token.h;
    let testPoints = [[w/2,h/2]];
    if ( corners ) {
      const pad = 2;
      const cornerPoints = [
        [pad,pad],
        [w/2,pad],
        [w-pad,pad],
        [w-pad,h/2],
        [w-pad,h-pad],
        [w/2,h-pad],
        [pad,h-pad],
        [pad,h/2]
      ];
      testPoints = testPoints.concat(cornerPoints);
    }
    for ( let p of testPoints ) {
      if ( this.containsPixel(token.data.x + p[0], token.data.y + p[1]) ) return true;
    }
    return false;
  }

  /* -------------------------------------------- */

  /**
   * Test whether the Tile pixel data contains a specific point in canvas space
   * @param {number} x
   * @param {number} y
   * @returns {boolean}
   */
  containsPixel(x, y) {
    if ( !this._alphaMap?.pixels ) return false;

    // Normalize to Tile coordinates
    x -= this.data.x;
    y -= this.data.y;

    // Account for tile rotation
    if ( this.data.rotation !== 0 ) {
      const anchor = {x: this.tile.anchor.x * this.data.width, y: this.tile.anchor.y * this.data.height};
      let r = new Ray(anchor, {x, y});
      r = r.shiftAngle(-this.tile.rotation);
      x = r.B.x;
      y = r.B.y;
    }

    // First test against the bounding box
    if ( (x < this._alphaMap.minX) || (x > this._alphaMap.maxX) ) return false;
    if ( (y < this._alphaMap.minY) || (y > this._alphaMap.maxY) ) return false;

    // Next test a specific pixel
    const px = (Math.round(y) * Math.round(Math.abs(this.data.width))) + Math.round(x);
    return this._alphaMap.pixels[px] === 1;
  }

  /* -------------------------------------------- */

  /**
   * Draw a sprite for the Roof which can be deducted from the fog exploration container
   * @returns {PIXI.Sprite}
   */
  getRoofSprite() {
    if ( !this._alphaMap?.texture ) return undefined;
    const s = new PIXI.Sprite(this._alphaMap.texture);
    const t = this.tile;
    s.width = t.width;
    s.height = t.height;
    s.anchor.set(0.5, 0.5);
    s.position.set(this.data.x + t.position.x, this.data.y + t.position.y);
    s.rotation = t.rotation;
    return s;
  }

  /* -------------------------------------------- */

  /**
   * Swap a Tile from the background to the foreground - or vice versa
   * TODO: Refactor to private _onSwapLayer
   */
  swapLayer() {
    // Remove from last layer
    const lastLayer = this.data.overhead ? canvas.background : canvas.foreground;
    delete lastLayer._controlled[this.id];
    lastLayer.objects.removeChild(this);

    // Add to new layer
    const newLayer = this.data.overhead ? canvas.foreground : canvas.background;
    newLayer.objects.addChild(this);
    if ( this._controlled ) newLayer._controlled[this.id] = this;
  }

  /* -------------------------------------------- */

  /**
   * Created a cached mapping of pixel alpha for this Tile.
   * Cache the bounding box of non-transparent pixels for the un-rotated shape.
   * Store an array of booleans for whether each pixel has a non-transparent value.
   * @param {object} [options={}]                     Options which customize the return value
   * @param {boolean} [options.keepPixels=false]      Keep the Uint8Array of pixel alphas?
   * @param {boolean} [options.keepTexture=false]     Keep the pure white RenderTexture?
   * @returns {{minX: number, minY: number, maxX: number, maxY: number, pixels: Uint8Array|undefined, texture: PIXI.RenderTexture|undefined}}
   * @private
   */
  _createAlphaMap({keepPixels=false, keepTexture=false}={}) {

    // Destroy the previous texture
    if ( this._alphaMap?.texture ) {
      this._alphaMap.texture.destroy(true);
      delete this._alphaMap.texture;
    }

    // If no tile texture is present
    const aw = Math.abs(this.data.width);
    const ah = Math.abs(this.data.height);
    if ( !this.texture ) return this._alphaMap = { minX: 0, minY: 0, maxX: aw, maxY: ah };

    // Create a temporary Sprite
    const sprite = new PIXI.Sprite(this.texture);
    sprite.width = this.data.width;
    sprite.height = this.data.height;
    sprite.anchor.set(0.5, 0.5);
    sprite.position.set(aw/2, ah/2);

    // Filter the sprite to discard shadows and create a solid mask
    const zsh = RoofMaskFilter.create({
      alphaThreshold: 0.75,
      turnToColor: true
    });
    sprite.filters = [zsh];

    // Render to a texture and extract pixels
    const tex = PIXI.RenderTexture.create({ width: aw, height: ah });
    canvas.app.renderer.render(sprite, tex);
    const pixels = canvas.app.renderer.extract.pixels(tex);

    // Construct an alpha mapping
    const map = {
      pixels: new Uint8Array(pixels.length / 4),
      texture: undefined,
      minX: undefined,
      minY: undefined,
      maxX: undefined,
      maxY: undefined
    }

    // Keep the texture?
    if ( keepTexture ) map.texture = tex;
    else tex.destroy(true);

    // Map the alpha pixels
    for ( let i=0; i<pixels.length; i+=4 ) {
      const n = i / 4;
      const a = pixels[i+3];
      map.pixels[n] = a > 0 ? 1 : 0;
      if ( a > 0 ) {
        const x = n % aw;
        const y = Math.floor(n / aw);
        if ( (map.minX === undefined) || (x < map.minX) ) map.minX = x;
        else if ( (map.maxX === undefined) || (x > map.maxX) ) map.maxX = x;
        if ( (map.minY === undefined) || (y < map.minY) ) map.minY = y;
        else if ( (map.maxY === undefined) || (y > map.maxY) ) map.maxY = y;
      }
    }

    // Maybe discard the raw pixels
    if ( !keepPixels ) map.pixels = undefined;
    return this._alphaMap = map;
  }

  /* -------------------------------------------- */

  /**
   * Compute the alpha-based bounding box for the tile, including an angle of rotation.
   * @returns {NormalizedRectangle}
   * @private
   */
  _getAlphaBounds() {
    const m = this._alphaMap;
    const r = Math.toRadians(this.data.rotation);
    return NormalizedRectangle.fromRotation(m.minX, m.minY, m.maxX - m.minX, m.maxY - m.minY, r);
  }

  /* -------------------------------------------- */

  /**
   * Create the filter instance used to reverse-mask overhead tiles using radial or vision-based occlusion.
   * @returns {AbstractBaseMaskFilter}
   * @private
   */
  _createOcclusionFilter() {
    const filter = InverseOcclusionMaskFilter.create({
      uMaskSampler: this.layer.occlusionMask.renderTexture,
      alphaOcclusion: this.data.occlusion.alpha
    }, "g");
    filter.enabled = false;
    return filter;
  }

  /* -------------------------------------------- */
  /*  Event Handlers                              */
  /* -------------------------------------------- */

  /** @override */
  _onUpdate(data, options={}, userId) {

    // Swap the overhead state
    if ( "overhead" in data ) {
      this.swapLayer();
      canvas.foreground.initialize(); // Re-initialize foreground layer
      return this.draw();
    }

    // Re-draw the image
    if ( "img" in data ) return this.draw();

    // Alter the z-index in the parent container
    if ( "z" in data ) this.zIndex = parseInt(data.z) || 0;

    // Re-create the alpha map if the occlusion shape changed
    const positionChange = ["x", "y", "rotation"].some(k => k in data);
    const shapeChange = ["width", "height", "scale", "mirrorX", "mirrorY"].some(k => k in data);
    const occlusionChange = foundry.utils.hasProperty(data, "occlusion.mode");
    const newAlphaMap = this.data.overhead && (shapeChange || occlusionChange);
    if ( newAlphaMap ) {
      canvas.foreground.initialize(); // Re-initialize foreground layer
      if ( this.occlusionTile ) canvas.foreground.removeRoofSprite(this); // before destroying it!
      this._createAlphaMap({
        keepPixels: true,
        keepTexture: this.isRoof
      });
      if ( this.isRoof ) canvas.foreground.addRoofSprite(this); // Adding roof to foreground cached container
    }

    // Refresh the tile display
    const refreshPerception = newAlphaMap || (this.isRoof && positionChange);
    this.refresh({refreshPerception});

    // Start or Stop Video
    if ( Object.keys(data).some(k => k.startsWith("video")) ) {
      this.play(options.playVideo);
      if ( this.layer.hud.object === this ) this.layer.hud.render();
    }
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  _onDelete(options, userId) {
    super._onDelete(options, userId);
    if ( this.data.overhead && (this.data.occlusion.mode === CONST.TILE_OCCLUSION_MODES.ROOF) ) {
      canvas.foreground.removeRoofSprite(this);
      this._refreshPerception();
    }
  }

  /* --------------------------------------------- */

  /**
   * Update wall states and refresh lighting and vision when a tile becomes a roof, or when an existing roof tile's
   * state changes.
   * @private
   */
  _refreshPerception() {
    canvas.walls.identifyInteriorWalls();
    canvas.perception.schedule({
      lighting: {initialize: true, refresh: true},
      sight: {initialize: true, refresh: true, forceUpdateFog: true},
      foreground: {refresh: true}
    });
  }

  /* -------------------------------------------- */
  /*  Interactivity                               */
  /* -------------------------------------------- */

  /** @inheritdoc */
  activateListeners() {
    super.activateListeners();
    this.frame.handle.off("mouseover").off("mouseout").off("mousedown")
      .on("mouseover", this._onHandleHoverIn.bind(this))
      .on("mouseout", this._onHandleHoverOut.bind(this))
      .on("mousedown", this._onHandleMouseDown.bind(this));
    this.frame.handle.interactive = true;
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  _canConfigure(user, event) {
    if ( this.data.locked && !this._controlled ) return false;
    return super._canConfigure(user);
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  _onClickLeft2(event) {
    this._dragHandle = false;
    return super._onClickLeft2(event);
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  _onDragLeftStart(event) {
    if ( this._dragHandle ) return this._onHandleDragStart(event);
    return super._onDragLeftStart(event);
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  _onDragLeftMove(event) {
    if ( this._dragHandle ) return this._onHandleDragMove(event);
    return super._onDragLeftMove(event);
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  _onDragLeftDrop(event) {
    if ( this._dragHandle ) return this._onHandleDragDrop(event);
    return super._onDragLeftDrop(event);
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  _onDragLeftCancel(event) {
    if ( this._dragHandle ) return this._onHandleDragCancel(event);
    return super._onDragLeftCancel(event);
  }

  /* -------------------------------------------- */
  /*  Resize Handling                             */
  /* -------------------------------------------- */

  /**
   * Handle mouse-over event on a control handle
   * @param {PIXI.InteractionEvent} event   The mouseover event
   * @protected
   */
  _onHandleHoverIn(event) {
    const handle = event.target;
    handle.scale.set(1.5, 1.5);
    event.data["handle"] = event.target;
  }

  /* -------------------------------------------- */

  /**
   * Handle mouse-out event on a control handle
   * @param {PIXI.InteractionEvent} event   The mouseout event
   * @protected
   */
  _onHandleHoverOut(event) {
    const {handle} = event.data;
    handle.scale.set(1.0, 1.0);
  }

  /* -------------------------------------------- */

  /**
   * When we start a drag event - create a preview copy of the Tile for re-positioning
   * @param {PIXI.InteractionEvent} event   The mousedown event
   * @protected
   */
  _onHandleMouseDown(event) {
    if ( !this.data.locked ) this._dragHandle = true;
  }

  /* -------------------------------------------- */

  /**
   * Handle the beginning of a drag event on a resize handle
   * @param {PIXI.InteractionEvent} event   The mousedown event
   * @protected
   */
  _onHandleDragStart(event) {
    const {handle} = event.data;
    const aw = Math.abs(this.data.width);
    const ah = Math.abs(this.data.height);
    const x0 = this.data.x + (handle.offset[0] * aw);
    const y0 = this.data.y + (handle.offset[1] * ah);
    event.data["origin"] = {x: x0, y: y0, width: aw, height: ah};
  }

  /* -------------------------------------------- */

  /**
   * Handle mousemove while dragging a tile scale handler
   * @param {PIXI.InteractionEvent} event   The mousemove event
   * @protected
   */
  _onHandleDragMove(event) {
    const {destination, origin, originalEvent} = event.data;
    canvas._onDragCanvasPan(originalEvent);
    const d = this._getResizedDimensions(originalEvent, origin, destination);
    this.data.x = d.x;
    this.data.y = d.y;
    this.data.width = d.width;
    this.data.height = d.height;
    this.data.rotation = 0;
    this.refresh();
  }

  /* -------------------------------------------- */

  /**
   * Handle mouseup after dragging a tile scale handler
   * @param {PIXI.InteractionEvent} event   The mouseup event
   * @protected
   */
  _onHandleDragDrop(event) {
    let {destination, origin, originalEvent} = event.data;
    if ( !originalEvent.shiftKey ) {
      destination = canvas.grid.getSnappedPosition(destination.x, destination.y, this.layer.gridPrecision);
    }
    const d = this._getResizedDimensions(originalEvent, origin, destination);
    return this.document.update({x: d.x, y: d.y, width: d.width, height: d.height});
  }

  /* -------------------------------------------- */

  /**
   * Get resized Tile dimensions
   * @returns {Rectangle}
   * @private
   */
  _getResizedDimensions(event, origin, destination) {
    const o = this.data._source;

    // Identify the new width and height as positive dimensions
    const dx = destination.x - origin.x;
    const dy = destination.y - origin.y;
    let w = Math.abs(o.width) + dx;
    let h = Math.abs(o.height) + dy;

    // Constrain the aspect ratio using the ALT key
    if ( event.altKey ) {
      const ar = this.texture.width / this.texture.height;
      if ( Math.abs(w) > Math.abs(h) ) h = w / ar;
      else w = h * ar;
    }

    // Create a normalized rectangle which adjusts the coordinates for negative positioning
    const nr = new NormalizedRectangle(o.x, o.y, w, h);

    // Re-apply sign to flip the tile horizontally or vertically
    nr.width *= (Math.sign(o.width) * (w < 0 ? -1 : 1));
    nr.height *= (Math.sign(o.height) * (h < 0 ? -1 : 1));
    return nr;
  }

  /* -------------------------------------------- */

  /**
   * Handle cancellation of a drag event for one of the resizing handles
   * @protected
   */
  _onHandleDragCancel() {
    this.data.reset();
    this._dragHandle = false;
    this.refresh();
  }

  /* -------------------------------------------- */

  /**
   * Create a preview tile with a background texture instead of an image
   * @param {object} data     Initial data with which to create the preview Tile
   * @return {PlaceableObject}
   */
  static createPreview(data) {
    data.overhead = data.overhead ?? canvas.foreground._active;
    const doc = new TileDocument(data, {parent: canvas.scene});
    const tile = doc.object;
    tile._controlled = true;
    tile.draw().then(() => {  // Swap the z-order of the tile and the frame
      tile.removeChild(tile.frame);
      tile.addChild(tile.frame);
    });
    return tile;
  }
}
