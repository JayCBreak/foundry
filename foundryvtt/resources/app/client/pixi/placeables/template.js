/**
 * A type of Placeable Object which highlights an area of the grid as covered by some area of effect.
 * @extends {PlaceableObject}
 * @see {@link MeasuredTemplateDocument}
 * @see {@link TemplateLayer}
 */
class MeasuredTemplate extends PlaceableObject {

  /**
   * The geometry shape used for testing point intersection
   * @type {PIXI.Circle | PIXI.Ellipse | PIXI.Polygon | PIXI.Rectangle | PIXI.RoundedRectangle}
   */
  shape;

  /**
   * The tiling texture used for this template, if any
   * @type {PIXI.Texture}
   */
  texture;

  /**
   * The template graphics
   * @type {PIXI.Graphics}
   */
  template;

  /**
   * The UI frame container which depicts Token metadata and status, displayed in the ControlsLayer.
   * @type {ObjectHUD}
   * @property {ControlIcon} icon           Template control icon
   * @property {PreciseText} ruler          Ruler text tooltip
   */
  hud = new ObjectHUD(this);

  /**
   * Internal property used to configure the control border thickness
   * @type {number}
   * @private
   */
  _borderThickness = 3;

  /** @inheritdoc */
  static embeddedName = "MeasuredTemplate";

  /* -------------------------------------------- */
  /*  Properties                                  */
  /* -------------------------------------------- */

  /** @inheritdoc */
  get bounds() {
    const d = canvas.dimensions;
    const r = this.data.distance * (d.size / d.distance);
    return new NormalizedRectangle(this.data.x - r, this.data.y - r, 2*r, 2*r);
  }

  /* -------------------------------------------- */

  /**
   * A convenience accessor for the border color as a numeric hex code
   * @return {number}
   */
  get borderColor() {
    return this.data.borderColor ? this.data.borderColor.replace("#", "0x") : 0x000000;
  }

  /* -------------------------------------------- */

  /**
   * A convenience accessor for the fill color as a numeric hex code
   * @return {number}
   */
  get fillColor() {
    return this.data.fillColor ? this.data.fillColor.replace("#", "0x") : 0x000000;
  }

  /* -------------------------------------------- */

  /**
   * A flag for whether the current User has full ownership over the MeasuredTemplate document.
   * @type {boolean}
   */
  get owner() {
    return this.document.isOwner;
  }

  /* -------------------------------------------- */
  /*  Rendering
  /* -------------------------------------------- */

  /** @override */
  async draw() {
    this.clear();

    // Load the texture
    if ( this.data.texture ) {
      this.texture = await loadTexture(this.data.texture, {fallback: 'icons/svg/hazard.svg'});
    } else {
      this.texture = null;
    }

    // Template shape
    this.template = this.addChild(new PIXI.Graphics());

    // Draw the interface frame
    this._drawHUD();
    this.controlIcon = this.hud.icon;

    // Update the shape and highlight grid squares
    this.refresh();

    // Enable highlighting for this template
    canvas.grid.addHighlightLayer(`Template.${this.id}`);
    this.highlightGrid();

    // Enable interactivity, only if the Tile has a true ID
    if ( this.id ) this.activateListeners();
    return this;
  }

  /* -------------------------------------------- */

  /** @override */
  destroy(options) {
    this.hud.destroy({children: true});
    canvas.grid.destroyHighlightLayer(`Template.${this.id}`);
    return super.destroy(options);
  }

  /* -------------------------------------------- */

  /**
   * Draw the HUD container which provides an interface for managing this template
   * @returns {ObjectHUD}
   * @private
   */
  _drawHUD() {
    if ( !this.hud.parent ) canvas.controls.hud.addChild(this.hud);
    this.hud.removeChildren();
    this.hud.icon = this.hud.addChild(this._drawControlIcon());
    this.hud.ruler = this.hud.addChild(this._drawRulerText());
    return this.hud;
  }

  /* -------------------------------------------- */

  /**
   * Draw the ControlIcon for the MeasuredTemplate
   * @return {ControlIcon}
   * @private
   */
  _drawControlIcon() {
    const size = Math.max(Math.round((canvas.dimensions.size * 0.5) / 20) * 20, 40);
    let icon = new ControlIcon({texture: CONFIG.controlIcons.template, size: size});
    icon.x -= (size * 0.5);
    icon.y -= (size * 0.5);
    return icon;
  }

  /* -------------------------------------------- */

  /**
   * Draw the Text label used for the MeasuredTemplate
   * @return {PreciseText}
   * @private
   */
  _drawRulerText() {
    const style = CONFIG.canvasTextStyle.clone();
    style.fontSize = Math.max(Math.round(canvas.dimensions.size * 0.36 * 12) / 12, 36);
    const text = new PreciseText(null, style);
    text.anchor.set(0, 1);
    return text;
  }

  /* -------------------------------------------- */

  /** @override */
  refresh() {
    let d = canvas.dimensions;
    this.position.set(this.data.x, this.data.y);

    // Extract and prepare data
    let {direction, distance, angle, width} = this.data;
    distance *= (d.size / d.distance);
    width *= (d.size / d.distance);
    direction = Math.toRadians(direction);

    // Create ray and bounding rectangle
    this.ray = Ray.fromAngle(this.data.x, this.data.y, direction, distance);

    // Get the Template shape
    switch ( this.data.t ) {
      case "circle":
        this.shape = this._getCircleShape(distance);
        break;
      case "cone":
        this.shape = this._getConeShape(direction, angle, distance);
        break;
      case "rect":
        this.shape = this._getRectShape(direction, distance);
        break;
      case "ray":
        this.shape = this._getRayShape(direction, distance, width);
    }

    // Draw the Template outline
    this.template.clear().lineStyle(this._borderThickness, this.borderColor, 0.75).beginFill(0x000000, 0.0);

    // Fill Color or Texture
    if ( this.texture ) this.template.beginTextureFill({
      texture: this.texture
    });
    else this.template.beginFill(0x000000, 0.0);

    // Draw the shape
    this.template.drawShape(this.shape);

    // Draw origin and destination points
    this.template.lineStyle(this._borderThickness, 0x000000)
      .beginFill(0x000000, 0.5)
      .drawCircle(0, 0, 6)
      .drawCircle(this.ray.dx, this.ray.dy, 6);

    // Update the HUD
    this.hud.icon.visible = this.layer._active;
    this.hud.icon.border.visible = this._hover;
    this._refreshRulerText();
    return this;
  }

  /* -------------------------------------------- */

  /**
   * Get a Circular area of effect given a radius of effect
   * @private
   */
  _getCircleShape(distance) {
    return new PIXI.Circle(0, 0, distance);
  }

  /* -------------------------------------------- */

  /**
   * Get a Conical area of effect given a direction, angle, and distance
   * @private
   */
  _getConeShape(direction, angle, distance) {
    angle = angle || 90;
    const coneType = game.settings.get("core", "coneTemplateType");

    // For round cones - approximate the shape with a ray every 3 degrees
    let angles;
    if ( coneType === "round" ) {
      const da = Math.min(angle, 3);
      angles = Array.fromRange(Math.floor(angle/da)).map(a => (angle/-2) + (a*da)).concat([angle/2]);
    }

    // For flat cones, direct point-to-point
    else {
      angles = [(angle/-2), (angle/2)];
      distance /= Math.cos(Math.toRadians(angle/2));
    }

    // Get the cone shape as a polygon
    const rays = angles.map(a => Ray.fromAngle(0, 0, direction + Math.toRadians(a), distance+1));
    const points = rays.reduce((arr, r) => {
      return arr.concat([r.B.x, r.B.y]);
    }, [0, 0]).concat([0, 0]);
    return new PIXI.Polygon(points);
  }

  /* -------------------------------------------- */

  /**
   * Get a Rectangular area of effect given a width and height
   * @private
   */
  _getRectShape(direction, distance) {
    let d = canvas.dimensions,
        r = Ray.fromAngle(0, 0, direction, distance),
        dx = Math.round(r.dx / (d.size / 2)) * (d.size / 2),
        dy = Math.round(r.dy / (d.size / 2)) * (d.size / 2);

    // Create rectangle shape and draw
    return new NormalizedRectangle(0, 0, dx + Math.sign(dx), dy + Math.sign(dy));
  }

  /* -------------------------------------------- */

  /**
   * Get a rotated Rectangular area of effect given a width, height, and direction
   * @private
   */
  _getRayShape(direction, distance, width) {
    let up = Ray.fromAngle(0, 0, direction - Math.toRadians(90), (width / 2)+1),
        down = Ray.fromAngle(0, 0, direction + Math.toRadians(90), (width / 2)+1),
        l1 = Ray.fromAngle(up.B.x, up.B.y, direction, distance+1),
        l2 = Ray.fromAngle(down.B.x, down.B.y, direction, distance+1);

    // Create Polygon shape and draw
    const points = [down.B.x, down.B.y, up.B.x, up.B.y, l1.B.x, l1.B.y, l2.B.x, l2.B.y, down.B.x, down.B.y];
    return new PIXI.Polygon(points);
  }

  /* -------------------------------------------- */

  /**
   * Update the displayed ruler tooltip text
   * @private
   */
  _refreshRulerText() {
    let text;
    let u = canvas.scene.data.gridUnits;
    if ( this.data.t === "rect" ) {
      let d = canvas.dimensions;
      let dx = Math.round(this.ray.dx) * (d.distance / d.size);
      let dy = Math.round(this.ray.dy) * (d.distance / d.size);
      let w = Math.round(dx * 10) / 10;
      let h = Math.round(dy * 10) / 10;
      text = `${w}${u} x ${h}${u}`;
    } else {
      let d = Math.round(this.data.distance * 10) / 10;
      text = `${d}${u}`;
    }
    this.hud.ruler.text = text;
    this.hud.ruler.position.set(this.ray.dx + 10, this.ray.dy + 5);
  }

  /* -------------------------------------------- */

  /**
   * Highlight the grid squares which should be shown under the area of effect
   */
  highlightGrid() {
    const grid = canvas.grid;
    const d = canvas.dimensions;
    const border = this.borderColor;
    const color = this.fillColor;

    // Only highlight for objects which have a defined shape
    if ( !this.id || !this.shape ) return;

    // Clear existing highlight
    const hl = grid.getHighlightLayer(`Template.${this.id}`);
    hl.clear();

    // If we are in gridless mode, highlight the shape directly
    if ( grid.type === CONST.GRID_TYPES.GRIDLESS ) {
      const shape = this.shape.clone();
      if ( "points" in shape ) {
        shape.points = shape.points.map((p, i) => {
          if ( i % 2 ) return this.y + p;
          else return this.x + p;
        });
      } else {
        shape.x += this.x;
        shape.y += this.y;
      }
      return grid.grid.highlightGridPosition(hl, {border, color, shape});
    }

    // Get number of rows and columns
    const [maxr, maxc] = grid.grid.getGridPositionFromPixels(d.width, d.height);
    let nr = Math.ceil(((this.data.distance * 1.5) / d.distance) / (d.size / grid.h));
    let nc = Math.ceil(((this.data.distance * 1.5) / d.distance) / (d.size / grid.w));
    nr = Math.min(nr, maxr);
    nc = Math.min(nc, maxc);

    // Get the offset of the template origin relative to the top-left grid space
    const [tx, ty] = canvas.grid.getTopLeft(this.data.x, this.data.y);
    const [row0, col0] = grid.grid.getGridPositionFromPixels(tx, ty);
    const hx = Math.ceil(canvas.grid.w / 2);
    const hy = Math.ceil(canvas.grid.h / 2);
    const isCenter = (this.data.x - tx === hx) && (this.data.y - ty === hy);

    // Identify grid coordinates covered by the template Graphics
    for (let r = -nr; r < nr; r++) {
      for (let c = -nc; c < nc; c++) {
        let [gx, gy] = canvas.grid.grid.getPixelsFromGridPosition(row0 + r, col0 + c);
        const testX = (gx+hx) - this.data.x;
        const testY = (gy+hy) - this.data.y;
        let contains = ((r === 0) && (c === 0) && isCenter ) || this.shape.contains(testX, testY);
        if ( !contains ) continue;
        grid.grid.highlightGridPosition(hl, {x: gx, y: gy, border, color});
      }
    }
  }

  /* -------------------------------------------- */
  /*  Methods                                     */
  /* -------------------------------------------- */

  /** @override */
  async rotate(angle, snap) {
    const direction = this._updateRotation({angle, snap});
    return this.document.update({direction});
  }

  /* -------------------------------------------- */
  /*  Interactivity                               */
  /* -------------------------------------------- */

  /** @override */
  _canControl(user, event) {
    return user.isGM || (user.id === this.data.user);
  }

  /** @override */
  _canConfigure(user, event) {
    return this._canControl(user, event);
  }

  /** @override */
  _canView(user, event) {
    return this._canControl(user, event);
  }

  /* -------------------------------------------- */
  /*  Socket Listeners and Handlers               */
  /* -------------------------------------------- */

  /** @override */
  _onUpdate(data, options, userId) {
    // If the texture was changed, we need to re-draw the whole thing
    const changed = new Set(Object.keys(data));
    if ( changed.has("texture") ) {
      return this.draw().then(() => {
        super._onUpdate(data, options, userId);
        this.highlightGrid();
      });
    }
    super._onUpdate(data, options, userId);
    this.highlightGrid();
  }
}

