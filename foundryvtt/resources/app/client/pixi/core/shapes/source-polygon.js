/**
 * @typedef {Object} PointSourcePolygonConfig
 * @property {string} [type]        The type of polygon being computed
 * @property {number} [angle=360]   The angle of emission, if limited
 * @property {number} [density]     The desired density of padding rays, a number per PI
 * @property {number} [radius]      A limited radius of the resulting polygon
 * @property {number} [rotation]    The direction of facing, required if the angle is limited
 * @property {boolean} [debug]      Display debugging visualization and logging for the polygon
 * @property {boolean} [walls]      Is this polygon constrained by any walls?
 * @property {PointSource} [source] The object (if any) that spawned this polygon.
 */

/**
 * An extension of the default PIXI.Polygon which is used to represent the line of sight for a point source.
 * @extends {PIXI.Polygon}
 */
class PointSourcePolygon extends PIXI.Polygon {

  /**
   * The origin point of the source polygon.
   * @type {Point}
   */
  origin;

  /**
   * The configuration of this polygon.
   * @type {PointSourcePolygonConfig}
   */
  config = {};

  /**
   * A cached array of SightRay objects used to compute the polygon.
   * @type {SightRay[]}
   */
  rays = [];

  /* -------------------------------------------- */

  /**
   * Compute the rectangular bounds for the Polygon.
   * @param {number[]} points     The initially provided array of coordinates
   * @returns {Rectangle}         The computed Rectangular bounds
   * @protected
   */
  _getBounds(points) {
    const bounds = {minX: Infinity, maxX: -Infinity, minY: Infinity, maxY: -Infinity};
    for ( let [i, p] of points.entries() ) {
      if ( i % 2 ) {
        if ( p < bounds.minX ) bounds.minX = p;
        if ( p > bounds.maxX ) bounds.maxX = p;
      } else {
        if ( p < bounds.minY ) bounds.minY = p;
        if ( p > bounds.maxY ) bounds.maxY = p;
      }
    }
    return new PIXI.Rectangle(bounds.minX, bounds.minY, bounds.maxX - bounds.minX, bounds.maxY - bounds.minY);
  }

  /* -------------------------------------------- */

  /**
   * Benchmark the performance of polygon computation for this source
   * @param {number} iterations   The number of test iterations to perform
   * @param {...any} args         Arguments passed to the compute method
   */
  static benchmark(iterations, ...args) {
    const f = () => this.create(...args);
    Object.defineProperty(f, "name", {value: `${this.name}.construct`, configurable: true});
    return foundry.utils.benchmark(f, iterations);
  }

  /* -------------------------------------------- */

  /**
   * Compute the polygon given a point origin and radius
   * @param {Point} origin                          The origin source point
   * @param {PointSourcePolygonConfig} [config={}]  Configuration options which customize the polygon computation
   * @returns {PointSourcePolygon}                  The computed polygon instance
   */
  static create(origin, config={}) {
    const poly = new this();
    poly.initialize(origin, config);
    return poly.compute();
  }

  /* -------------------------------------------- */

  /**
   * Compute the polygon using the origin and configuration options.
   * @returns {PointSourcePolygon}    The computed polygon
   */
  compute() {
    let t0 = performance.now();
    const {angle, debug, radius} = this.config;
    if ( (radius === 0) || (angle === 0) ) return this; // Skip zero-angle or zero-radius polygons

    // Delegate computation to the implementation
    this._compute();

    // Debugging and performance metrics
    if( debug ) {
      let t1 = performance.now();
      console.log(`Created ${this.constructor.name} in ${Math.round(t1 - t0)}ms`);
      this.visualize();
    }
    return this;
  }

  /**
   * Perform the implementation-specific computation
   * @protected
   */
  _compute() {
    throw new Error("Each subclass of PointSourcePolygon must define its own _compute method");
  }

  /* -------------------------------------------- */

  /**
   * Customize the provided configuration object for this polygon type.
   * @param {Point} origin                        The provided polygon origin
   * @param {PointSourcePolygonConfig} config     The provided configuration object
   */
  initialize(origin, config) {
    this.origin = origin;
    this.config = config;
    if ( CONFIG.debug.polygons ) this.config.debug = true;
  }

  /* -------------------------------------------- */

  /**
   * Visualize the polygon, displaying its computed area, rays, and collision points
   */
  visualize() {
    let dg = canvas.controls.debug;
    dg.clear();

    // Draw the resulting polygons
    dg.lineStyle(2, 0xFFFFFF, 1.0).beginFill(0xFFAA99, 0.25).drawShape(this).endFill();

    // Draw the rays
    dg.lineStyle(1, 0x00FF00, 1.0);
    for ( let r of this.rays ) {
      if ( !r.collisions.length ) continue;
      if ( !r.endpoint ) dg.lineStyle(1, 0x00AAFF, 1.0).moveTo(r.A.x, r.A.y).lineTo(r.B.x, r.B.y);
      else dg.lineStyle(1, 0x00FF00, 1.0).moveTo(r.A.x, r.A.y).lineTo(r.B.x, r.B.y);
    }

    // Draw target endpoints
    dg.lineStyle(1, 0x00FFFF, 1.0).beginFill(0x00FFFF, 0.5);
    for ( let r of this.rays ) {
      if ( r.endpoint ) dg.drawCircle(r.endpoint.x, r.endpoint.y, 4);
    }
    dg.endFill();

    // Draw collision points
    dg.lineStyle(1, 0xFF0000, 1.0)
    for ( let r of this.rays ) {
      for ( let [i, c] of r.collisions.entries() ) {
        if ( i === r.collisions.length-1 ) dg.beginFill(0xFF0000, 1.0).drawCircle(c.x, c.y, 3);
        else dg.endFill().drawCircle(c.x, c.y, 3);
      }
    }
    dg.endFill();
  }
}

/* -------------------------------------------- */

/**
 * Compare sight performance between different algorithms
 * @param {number} n      The number of iterations
 * @param {...any} args   Arguments passed to the polygon compute function
 */
async function benchmarkSight(n=1000, ...args) {
  await QuadtreeExpansionPolygon.benchmark(n, ...args);
  await RadialSweepPolygon.benchmark(n, ...args);
  await ClockwiseSweepPolygon.benchmark(n, ...args);
}
