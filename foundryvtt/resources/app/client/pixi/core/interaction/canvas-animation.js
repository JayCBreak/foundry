/**
 * @typedef {Object} CanvasAnimationData
 * @property {Function} fn                  The animation function being executed each frame
 * @property {PIXI.DisplayObject} context   The object context within which animation occurs
 * @property {Promise} promise              A Promise which resolves once the animation is complete
 * @property {Function} resolve             The resolution function, allowing animation to be ended early
 */

/**
 * A helper class providing utility methods for PIXI Canvas animation
 */
class CanvasAnimation {
  static get ticker() {
    return canvas.app.ticker;
  }

  /**
   * Track an object of active animations by name, context, and function
   * This allows a currently playing animation to be referenced and terminated
   * @type {Object<string, CanvasAnimationData>}
   */
  static animations = {};

  /* -------------------------------------------- */

  /**
   * Apply a linear animation from the current value of some attribute to a new value
   * Resolve a Promise once the animation has concluded and the attributes have reached their new target
   * @param {object[]} attributes   An array of attributes to animate. Structure of the Array is shown in the example
   *
   * @param {object} options        Additional options which customize the animation
   *
   * @param {PIXI.DisplayObject} [options.context]  An animation context to use which defines scope
   * @param {string} [options.name]                 Provide a unique animation name which may be referenced later
   * @param {number} [options.duration=1000]        The duration in milliseconds over which the animation should occur
   * @param {Function} [options.ontick]             A function which defines additional behaviors to apply every animation frame
   * @return {Promise}              A Promise which resolves once the linear animation has concluded
   *
   * @example
   * let animation = [
   *   {
   *     parent: token,
   *     attribute: x,
   *     to: 1000
   *   },
   *   {
   *     parent: token,
   *     attribute: y,
   *     to: 2000
   *   }
   * ];
   * CanvasAnimation.animateLinear(attributes, {duration:500, ontick: console.log("ticking")});
   */
  static async animateLinear(attributes, {context, name, duration = 1000, ontick} = {}) {

    // Prepare attributes
    attributes = attributes.map(a => {
      a.delta = a.to - a.parent[a.attribute];
      a.done = 0;
      a.remaining = Math.abs(a.delta);
      return a;
    }).filter(a => a.delta !== 0);

    // Register the request function and context
    context = context || canvas.stage;

    // Dispatch the animation request and return as a Promise
    return this._animatePromise(this._animateFrame, context, name, attributes, duration, ontick);
  }

  /* -------------------------------------------- */

  /**
   * Retrieve an animation currently in progress by its name
   * @param {string} name             The animation name to retrieve
   * @returns {CanvasAnimationData}   The animation data, or undefined
   */
  static getAnimation(name) {
    return this.animations[name];
  }

  /* -------------------------------------------- */

  /**
   * If an animation using a certain name already exists, terminate it
   * @param {string} name       The animation name to terminate
   */
  static terminateAnimation(name) {
    let animation = this.animations[name];
    if (animation) animation.resolve(false);
  }

  /* -------------------------------------------- */

  /**
   * Asynchronously animate a transition function and resolve a Promise once the animation has completed
   * @param {Function} fn         A suitable transition function. See PIXI.Ticker for details
   * @param {PIXI.Container} context The Canvas container providing scope for the transition
   * @param {string} name         Provide a unique animation name which may be referenced later
   * @param {object} attributes   The attributes being animated by the function
   * @param {number} duration     The duration in milliseconds over which the animation should occur
   * @param {Function} ontick     A function which defines additional behaviors to apply every animation frame
   * @return {Promise}            A Promise which resolves once the animation has completed
   * @private
   */
  static async _animatePromise(fn, context, name, attributes, duration, ontick) {
    if (name) this.terminateAnimation(name);
    let animate;

    // Create the animation promise
    const promise = new Promise((resolve, reject) => {
      animate = dt => fn(dt, resolve, reject, attributes, duration, ontick);
      this.ticker.add(animate, context);
      if (name) this.animations[name] = {fn: animate, context, resolve};
    })
    .catch(err => {
      console.error(err)
    })
    .finally(() => {
      this.ticker.remove(animate, context);
      const isCompleted = name && (this.animations[name]?.fn === animate);
      if ( isCompleted ) delete this.animations[name];
    });

    // Store the promise
    if ( name in this.animations ) this.animations[name].promise = promise;
    return promise;
  }

  /* -------------------------------------------- */

  /**
   * Generic ticker function to implement the animation.
   * This animation wrapper executes once per frame for the duration of the animation event.
   * Once the animated attributes have converged to their targets, it resolves the original Promise.
   * The user-provided ontick function runs each frame update to apply additional behaviors.
   * @private
   */
  static _animateFrame(deltaTime, resolve, reject, attributes, duration, ontick) {
    let complete = attributes.length === 0;
    let dt = (duration * PIXI.settings.TARGET_FPMS) / deltaTime;

    // Update each attribute
    try {
      for (let a of attributes) {
        let da = a.delta / dt;
        a.d = da;
        if (a.remaining < (Math.abs(da) * 1.25)) {
          a.parent[a.attribute] = a.to;
          a.done = a.delta;
          a.remaining = 0;
          complete = true;
        } else {
          a.parent[a.attribute] += da;
          a.done += da;
          a.remaining = Math.abs(a.delta) - Math.abs(a.done);
        }
      }
      if (ontick) ontick(dt, attributes);
    } catch (err) {
      reject(err);
    }

    // Resolve the original promise once the animation is complete
    if (complete) resolve(true);
  }
}
