/**
 * A tool for culling the renderable state of objects which are outside the current viewport.
 */
class ScreenCulling {
  constructor() {
    this.layers = canvas.layers.filter(l => l instanceof PlaceablesLayer);
  }

  /**
   * Toggle the renderable state of objects based on the current viewport rectangle
   * @param {Rectangle} rect
   */
  cull(rect) {
    for ( let l of this.layers ) {
      if ( !l.objects.visible ) continue;
      for ( let o of l.objects.children ) {
        const b = o.getBounds(false);
        o.renderable = this.intersects(rect, b);
      }
    }
  }

  /**
   * Test whether rectangular bounds intersect
   * @param {Rectangle} rect        The target rectangle (the screen)
   * @param {Rectangle} bounds      The reference rectangle (the object)
   * @returns {boolean}             Do they intersect?
   */
  intersects(rect, bounds) {
    return (rect.right >= bounds.left) && (rect.left <= bounds.right) && (rect.bottom >= bounds.top) && (rect.top <= bounds.bottom);
  }
}

/**
 * An experimental approach to culling using the quadtree mapping rather than the screen rectangle.
 * @ignore
 */
class QuadtreeCulling {
  constructor() {
    this.layers = canvas.layers.filter(l => l.constructor.layerOptions.quadtree);
  }

  cull(screen) {
    const rect = this._getRect(screen);
    for ( let l of this.layers ) {
      if ( !l.objects.visible ) continue;
      const visible = l.quadtree.getObjects(rect);
      for ( let o of l.objects.children ) {
        o.renderable = visible.has(o);
      }
    }
  }

  _getRect(screen) {
    const wt = canvas.stage.worldTransform;
    const p0 = wt.applyInverse(new PIXI.Point(screen.left, screen.top));
    const p1 = wt.applyInverse(new PIXI.Point(screen.right, screen.bottom));
    return new NormalizedRectangle(p0.x, p0.y, p1.x - p0.x, p1.y - p0.y);
  }
}

/**
 * Benchmark the performance of different culling methods
 * @ignore
 */
async function benchmarkCulling(n=10000) {
	CONFIG.Canvas.cullingBackend = null;
	await canvas.draw();
	const f0 = () => canvas.stage.render(canvas.app.renderer);
	Object.defineProperty(f0, "name", {value: "No Culling"});
	await foundry.utils.benchmark(f0, n);

	CONFIG.Canvas.cullingBackend = ScreenCulling;
	await canvas.draw();
	const f1 = () => canvas.stage.render(canvas.app.renderer);
	Object.defineProperty(f1, "name", {value: "Screen Culling"});
	await foundry.utils.benchmark(f1, n);

	CONFIG.Canvas.cullingBackend = QuadtreeCulling;
	await canvas.draw();
	const f2 = () => canvas.stage.render(canvas.app.renderer);
	Object.defineProperty(f2, "name", {value: "Quadtree Culling"});
	await foundry.utils.benchmark(f2, n);
}
