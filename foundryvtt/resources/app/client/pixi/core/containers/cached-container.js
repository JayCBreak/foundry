/**
 * A special type of PIXI.Container which draws its contents to a cached RenderTexture.
 * This is accomplished by overriding the Container#render method to draw to our own special RenderTexture.
 * @extends {PIXI.Container}
 */
class CachedContainer extends PIXI.Container {
  constructor() {
    super();
    const renderer = canvas.app?.renderer;

    /**
     * The RenderTexture that is the render destination for the contents of this Container
     * @type {PIXI.RenderTexture}
     */
    Object.defineProperty(this, "renderTexture", {
      value: PIXI.RenderTexture.create({
        width: renderer?.screen.width ?? window.innerWidth,
        height: renderer?.screen.height ?? window.innerHeight,
        resolution: renderer.resolution ?? PIXI.settings.RESOLUTION
      }),
      writable: false
    });

    // Listen for resize events
    this._onResize = () => this._resize(renderer)
    renderer.on("resize", this._onResize);
  }

  /**
   * An object which stores a reference to the normal renderer target and source frame.
   * We track this so we can restore them after rendering our cached texture.
   * @type {{sourceFrame: PIXI.Rectangle, renderTexture: PIXI.RenderTexture}}
   * @private
   */
  _backup = {
    renderTexture: undefined,
    sourceFrame: new PIXI.Rectangle()
  }

  /**
   * An RGBA array used to define the clear color of the RenderTexture
   * @type {number[]}
   */
  clearColor = [0,0,0,1];

  /**
   * Should our Container also be displayed on screen, in addition to being drawn to the cached RenderTexture?
   * @type {boolean}
   */
  displayed = false;

  /**
   * A bound Sprite which uses this container's render texture
   * @type {PIXI.Sprite}
   */
  get sprite() {
    if ( this._sprite ) return this._sprite;
    return this._sprite = new PIXI.Sprite(this.renderTexture);
  }

  /**
   * @private
   */
  _sprite;

  /* ---------------------------------------- */

  /** @inheritdoc */
  destroy(options) {
    if ( this._onResize ) canvas.app.renderer.off("resize", this._onResize);
    if ( this.renderTexture ) this.renderTexture.destroy(true);
    super.destroy(options);
  }

  /* ---------------------------------------- */

  /** @inheritdoc */
  render(renderer) {
    if ( !this.renderable ) return  // Skip updating the cached texture if the container is not renderable
    this._bind(renderer);           // Bind our cached texture
    super.render(renderer);         // Draw to our cached texture
    this._unbind(renderer);         // Restore the original target
    if ( this.displayed ) super.render(renderer); // Draw to the screen
  }

  /* ---------------------------------------- */

  /**
   * Bind our cached RenderTexture to the Renderer, replacing the original target.
   * @param {PIXI.Renderer} renderer      The active canvas renderer
   * @private
   */
  _bind(renderer) {

    // Get the RenderTexture to bind
    const tex = this.renderTexture;
    const rt = renderer.renderTexture;

    // Backup the current render target
    this._backup.renderTexture = rt.current;
    this._backup.sourceFrame.copyFrom(rt.sourceFrame);

    // Bind our texture to the renderer
    renderer.batch.flush();
    rt.bind(tex, undefined, undefined);
    rt.clear(this.clearColor);

    // Enable Filters which are applied to this Container to apply to our cached RenderTexture
    const fs = renderer.filter.defaultFilterStack;
    if ( fs.length > 1 ) {
      fs[fs.length - 1].renderTexture = tex;
    }
  }

  /* ---------------------------------------- */

  /**
   * Remove our cached RenderTexture from the Renderer, re-binding the original target.
   * @param {PIXI.Renderer} renderer      The active canvas renderer
   * @private
   */
  _unbind(renderer) {
    renderer.batch.flush();

    // Restore Filters to apply to the original RenderTexture
    const fs = renderer.filter.defaultFilterStack;
    if ( fs.length > 1 ) {
      fs[fs.length - 1].renderTexture = this._backup.renderTexture;
    }

    // Re-bind the original RenderTexture to the renderer
    renderer.renderTexture.bind(this._backup.renderTexture, this._backup.sourceFrame, undefined);
    this._backup.renderTexture = undefined;
  }

  /* ---------------------------------------- */

  /**
   * Resize the cached RenderTexture when the dimensions or resolution of the Renderer have changed.
   * @param {PIXI.Renderer} renderer
   * @protected
   */
  _resize(renderer) {
    const rt = this.renderTexture;
    const screen = renderer?.screen;
    if ( !rt || !screen ) return;
    if ( rt.baseTexture.resolution !== renderer.resolution ) rt.baseTexture.resolution = renderer.resolution;
    if ( (rt.width !== screen.width) || (rt.height !== screen.height) ) rt.resize(screen.width, screen.height);
    if ( this._sprite ) this._sprite._boundsID++;  // Inform PIXI that the internal frame needs to be updated
  }
}
