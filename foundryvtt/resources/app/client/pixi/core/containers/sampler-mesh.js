/**
 * A Mesh subclass used to render a texture with faster performance than a PIXI.Sprite.
 * @extends {PIXI.Mesh}
 */
class SamplerMesh extends PIXI.Mesh {

  /**
   * The basic quad geometry used for the Mesh
   * @type {PIXI.Geometry}
   */
  static QUAD = new PIXI.Geometry()
    .addAttribute('aVertexPosition', [0, 0, 1, 0, 1, 1, 0, 1], 2)
    .addAttribute('aUvs', [0, 0, 1, 0, 1, 1, 0, 1], 2)
    .addIndex([0, 1, 2, 0, 2, 3]);

  /* -------------------------------------------- */

  /**
   * Create a SamplerMesh using a provided RenderTexture
   * @param {PIXI.RenderTexture} texture      The texture to render using a Mesh
   * @returns {SamplerMesh}
   */
  static create(texture) {
    const screen = canvas.app.renderer.screen;
    const shader = BaseSamplerShader.create({
      sampler: texture,
      screenDimensions: canvas.screenDimensions
    });
    const state = new PIXI.State();
    return new this(this.QUAD, shader, state);
  }
}
