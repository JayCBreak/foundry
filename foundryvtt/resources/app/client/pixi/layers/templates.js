/**
 * This Canvas Layer provides a container for MeasuredTemplate objects.
 * @extends {PlaceablesLayer}
 * @see {@link MeasuredTemplate}
 */
class TemplateLayer extends PlaceablesLayer {

  /** @inheritdoc */
  static get layerOptions() {
    return foundry.utils.mergeObject(super.layerOptions, {
      name: "templates",
      canDragCreate: true,
      rotatableObjects: true,
      sortActiveTop: true,
      zIndex: 50
    });
  }

  /** @inheritdoc */
  static documentName = "MeasuredTemplate";

  /* -------------------------------------------- */
  /*  Methods                                     */
  /* -------------------------------------------- */

  /** @inheritdoc */
  activate() {
    super.activate();
    this.placeables.forEach(p => {
      p.hud.icon.visible = p.hud.ruler.visible = true;
    });
    return this;
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  deactivate() {
    super.deactivate();
    this.objects.visible = true;
    this.placeables.forEach(p => {
      p.hud.icon.visible = p.hud.ruler.visible = false;
    });
    return this;
  }

  /* -------------------------------------------- */

  /**
   * Register game settings used by the TemplatesLayer
   */
  static registerSettings() {
    game.settings.register("core", "coneTemplateType", {
      name: "TEMPLATE.ConeTypeSetting",
      hint: "TEMPLATE.ConeTypeSettingHint",
      scope: "world",
      config: true,
      default: "round",
      type: String,
      choices: {
        "flat": "TEMPLATE.ConeTypeFlat",
        "round": "TEMPLATE.ConeTypeRound"
      },
      onChange: () => canvas.templates?.placeables.filter(t => t.data.t === "cone").forEach(t => t.draw())
    });
  }

  /* -------------------------------------------- */
  /*  Event Listeners and Handlers                */
  /* -------------------------------------------- */

  /** @inheritdoc */
  async _onDragLeftStart(event) {
    await super._onDragLeftStart(event);

    // Create the new preview template
    const tool = game.activeTool;
    const origin = event.data.origin;

    // Create the template
    const data = {
      user: game.user.id,
      t: tool,
      x: origin.x,
      y: origin.y,
      distance: 1,
      direction: 0,
      fillColor: game.user.data.color || "#FF0000"
    };

    // Apply some type-specific defaults
    const defaults = CONFIG.MeasuredTemplate.defaults;
    if ( tool === "cone") data["angle"] = defaults.angle;
    else if ( tool === "ray" ) data["width"] = (defaults.width * canvas.dimensions.distance);

    // Create a preview template
    const doc = new MeasuredTemplateDocument(data, {parent: canvas.scene});
    const template = new MeasuredTemplate(doc);
    event.data.preview = this.preview.addChild(template);
    return template.draw();
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  _onDragLeftMove(event) {
    const { destination, createState, preview, origin } = event.data;
    if ( createState === 0 ) return;

    // Snap the destination to the grid
    event.data.destination = canvas.grid.getSnappedPosition(destination.x, destination.y, this.gridPrecision);

    // Compute the ray
    const ray = new Ray(origin, destination);
    const ratio = (canvas.dimensions.size / canvas.dimensions.distance);

    // Update the preview object
    preview.data.direction = Math.normalizeDegrees(Math.toDegrees(ray.angle));
    preview.data.distance = ray.distance / ratio;
    preview.refresh();

    // Confirm the creation state
    event.data.createState = 2;
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  _onMouseWheel(event) {

    // Determine whether we have a hovered template?
    const template = this._hover;
    if ( !template ) return;

    // Determine the incremental angle of rotation from event data
    let snap = event.shiftKey ? 15 : 5;
    let delta = snap * Math.sign(event.deltaY);
    return template.rotate(template.data.direction + delta, snap);
  }
}
