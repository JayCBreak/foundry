/**
 * An extension of PIXI.Transform.
 * This uses a different DisplayObject than the current parent as the reference for the worldTransform.
 * @extends {PIXI.Transform}
 */
class SynchronizedTransform extends PIXI.Transform {
  constructor(transform) {
    super();
    this.reference = transform;
  }

  /**
   * A list of attributes from the transform reference which should be synchronized
   * @type {string}
   */
  static synchronizedAttributes = [
    "localTransform", "position", "scale", "pivot", "skew", "_rotation",
    "_cx", "_sx", "_cy", "_sy", "_localID", "_currentLocalID"
  ];

  /**
   * A Transform instance which defines the reference point for the worldTransform
   * @type {PIXI.Transform}
   */
  get reference() {
    return this._reference;
  }
  set reference(value) {
    this._reference = value;
    this._syncLocalID = -1;
  }

  /** @override */
  updateTransform(parentTransform) {
    if ( this._localID !== this._currentLocalID ) this._reference._parentID = -1;
    else if ( this._localID !== this._syncLocalID ) this._parentID = -1;
    this._syncLocalID = this._localID;
    super.updateTransform(parentTransform);
  }

  /** @override */
  updateLocalTransform() {
    if (this._localID !== this._currentLocalID) {
      this._reference._parentID = -1;
      super.updateLocalTransform();
    }
  }
}
for ( let attr of SynchronizedTransform.synchronizedAttributes ) {
  Object.defineProperty(SynchronizedTransform.prototype, attr, {
    get() { return this._reference[attr]; },
    set(value) {
      if ( !this._reference ) return;
      this._reference[attr] = value;
    }
  });
}


/**
 * An extension of PIXI.Container used as the interface frame for a PlaceableObject on the ControlsLayer
 * @extends {PIXI.Container}
 *
 * @see {@link PlaceableObject}
 * @see {@link ControlsLayer}
 */
class ObjectHUD extends PIXI.Container {
  constructor(object) {
    super();

    /**
     * The object that this HUD container is linked to
     * @type {PIXI.DisplayObject}
     */
    this.object = object;

    /**
     * Use the linked object's transform matrix to easily synchronize position
     * @type {PIXI.Transform}
     */
    this.transform = new SynchronizedTransform(this.object.transform);
  }

  /** @override */
  get visible() {
    return this.object.visible;
  }
  set visible(value) {}

  /** @override */
  get renderable() {
    return this.object.renderable;
  }
  set renderable(value) {}

  /* -------------------------------------------- */

  /**
   * Display scrolling status text originating from this ObjectHUD container.
   * @param {string} content          The text content to display
   * @param {number} [anchor=0]       The original anchor point where the text first appears
   * @param {number} [direction=2]    The direction in which the text scrolls
   * @param {number} [duration=2000]  The duration of the scrolling effect in milliseconds
   * @param {number} [jitter=0]       An amount of randomization between 0 and 1 to apply to the initial position
   * @param {object} [textStyle={}]   Additional parameters of PIXI.TextStyle which are applied to the text
   * @returns {Promise<PreciseText|null>}   The created PreciseText object which is scrolling
   */
  async createScrollingText(content, {anchor=CONST.TEXT_ANCHOR_POINTS.CENTER, direction=CONST.TEXT_ANCHOR_POINTS.TOP, duration=2000, jitter=0, ...textStyle}={}) {
    if ( !this.visible || !this.renderable ) return;
    if ( game.settings.get("core", "scrollingStatusText") !== true ) return null;

    // Create text object
    const style = PreciseText.getTextStyle({anchor, ...textStyle});
    const text = new PreciseText(content, style);

    // Reference object dimensions
    const w = this.object.w;
    const h = this.object.h;
    const jx = (jitter ? (Math.random()-0.5) * jitter : 0) * w;
    const jy = (jitter ? (Math.random()-0.5) * jitter : 0) * h;

    // Configure anchor point
    switch ( anchor ) {
      case CONST.TEXT_ANCHOR_POINTS.CENTER:
        text.anchor.set(0.5, 0.5);
        text.position.set((w/2)+jx, (h/2)+jy);
        break;
      case CONST.TEXT_ANCHOR_POINTS.BOTTOM:
        text.anchor.set(0.5, 0);
        text.position.set((h/2)+jx, (h*0.75)+jy);
        break;
      case CONST.TEXT_ANCHOR_POINTS.TOP:
        text.anchor.set(0.5, 1);
        text.position.set((w/2)+jx, (h/4)+jy);
        break;
      case CONST.TEXT_ANCHOR_POINTS.LEFT:
        text.anchor.set(1, 0.5);
        text.position.set((w/4)+jx, (h/2)+jy);
        break;
      case CONST.TEXT_ANCHOR_POINTS.RIGHT:
        text.anchor.set(0, 0.5);
        text.position.set((w*0.75)+jx, (h/2)+jy);
        break;
    }

    // Configure animation distance
    let dx = 0;
    let dy = 0;
    switch ( direction ) {
      case CONST.TEXT_ANCHOR_POINTS.BOTTOM:
        dy = h; break;
      case CONST.TEXT_ANCHOR_POINTS.TOP:
        dy = -h; break;
      case CONST.TEXT_ANCHOR_POINTS.LEFT:
        dx = -w; break;
      case CONST.TEXT_ANCHOR_POINTS.RIGHT:
        dx = w; break;
    }

    // Begin animation
    // noinspection ES6MissingAwait
    this._animateScrollText(text, duration, dx, dy).then(() => {
      this.removeChild(text);
      text.destroy();
    });

    // Attach to the HUD
    return this.addChild(text);
  }

  /* -------------------------------------------- */

  /**
   * Orchestrate the animation of the scrolling text in this HUD
   * @param {PreciseText} text    The PrecisText instance to animate
   * @param {number} duration     A desired duration of animation
   * @param {number} dx           A horizontal distance to animate the text
   * @param {number} dy           A vertical distance to animate the text
   * @returns {Promise<void>}
   * @private
   */
  async _animateScrollText(text, duration, dx=0, dy=0) {
    const animationName = `${this.object.documentName}.${this.object.id}.${Math.trunc(Math.random() * 100000)}`;
    const small = 0.6;

    // Fade In
    text.alpha = 0;
    text.scale.set(small, small);
    const fadeIn = [
      {parent: text, attribute: "alpha", to: 1.0},
      {parent: text.scale, attribute: "x", to: 1.0},
      {parent: text.scale, attribute: "y", to: 1.0}
    ]
    await CanvasAnimation.animateLinear(fadeIn, {
      context: this,
      name: animationName,
      duration: duration / 4
    });

    // Scroll
    const scroll = [{parent: text, attribute: "alpha", to: 0.0}];
    if ( dx !== 0 ) scroll.push({parent: text, attribute: "x", to: text.position.x + dx});
    if ( dy !== 0 ) scroll.push({parent: text, attribute: "y", to: text.position.y + dy})
    await CanvasAnimation.animateLinear(scroll, {
      context: this,
      name: animationName,
      duration: duration * 0.75
    });
  }
}

