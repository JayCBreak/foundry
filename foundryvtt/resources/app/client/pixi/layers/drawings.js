/**
 * The DrawingsLayer subclass of PlaceablesLayer.
 * This layer implements a container for drawings which are rendered immediately above the BackgroundLayer. *
 * @extends {PlaceablesLayer}
 */
class DrawingsLayer extends PlaceablesLayer {

  /** @inheritdoc */
  static get layerOptions() {
    return foundry.utils.mergeObject(super.layerOptions, {
      name: "drawings",
      canDragCreate: true,
      controllableObjects: true,
      rotatableObjects: true,
      zIndex: 20
    });
  }

  /** @inheritdoc */
  static documentName = "Drawing";

  /**
   * The named game setting which persists default drawing configuration for the User
   * @type {string}
   */
  static DEFAULT_CONFIG_SETTING = "defaultDrawingConfig";

  /* -------------------------------------------- */
  /*  Properties                                  */
  /* -------------------------------------------- */

  /**
   * Use an adaptive precision depending on the size of the grid
   * @type {number}
   */
  get gridPrecision() {
    if ( canvas.scene.data.gridType === CONST.GRID_TYPES.GRIDLESS ) return 0;
    return canvas.dimensions.size >= 128 ? 16 : 8;
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  get hud() {
    return canvas.hud.drawing;
  }

  /* -------------------------------------------- */
  /*  Methods                                     */
  /* -------------------------------------------- */

  /**
   * Render a configuration sheet to configure the default Drawing settings
   */
  configureDefault() {
    const defaults = this._getNewDrawingData({x: 0, y: 0});
    const d = new DrawingDocument(defaults);
    new DrawingConfig(d, {configureDefault: true}).render(true);
  }

  /* -------------------------------------------- */

  /**
   * Override the deactivation behavior of this layer.
   * Placeables on this layer remain visible even when the layer is inactive.
   */
  deactivate() {
    super.deactivate();
    if (this.objects) this.objects.visible = true;
    return this;
  }

  /* -------------------------------------------- */

  /**
   * Get initial data for a new drawing.
   * Start with some global defaults, apply user default config, then apply mandatory overrides per tool.
   * @param {Object} origin     The initial coordinate
   * @return {Object}           The new drawing data
   */
  _getNewDrawingData(origin) {
    const tool = game.activeTool;

    // Get saved user defaults
    const defaults = game.settings.get("core", this.constructor.DEFAULT_CONFIG_SETTING) || {};
    const data = foundry.utils.mergeObject(defaults, {
      fillColor: game.user.color,
      strokeColor: game.user.color,
      fontFamily: CONFIG.defaultFontFamily
    }, {overwrite: false, inplace: false});

    // Mandatory additions
    data.x = Math.round(origin.x);
    data.y = Math.round(origin.y);
    data.author = game.user.id;

    // Tool-based settings
    switch ( tool ) {
      case "rect":
        data.type = CONST.DRAWING_TYPES.RECTANGLE;
        data.points = [];
        break;
      case "ellipse":
        data.type = CONST.DRAWING_TYPES.ELLIPSE;
        data.points = [];
        break;
      case "polygon":
        data.type = CONST.DRAWING_TYPES.POLYGON;
        data.points = [[origin.x - data.x, origin.y - data.y]];
        break;
      case "freehand":
        data.type = CONST.DRAWING_TYPES.FREEHAND;
        data.points = [[origin.x - data.x, origin.y - data.y]];
        data.bezierFactor = data.bezierFactor ?? 0.5;
        break;
      case "text":
        data.points = [];
        data.type = CONST.DRAWING_TYPES.TEXT;
        data.fillColor = "#FFFFFF";
        data.fillAlpha = 0.10;
        data.strokeColor = "#FFFFFF";
        data.text = data.text || "New Text";
    }
    return data;
  }

  /* -------------------------------------------- */
  /*  Event Listeners and Handlers                */
  /* -------------------------------------------- */

  /** @inheritdoc */
  _onClickLeft(event) {
    const {preview, createState, originalEvent} = event.data;

    // Continue polygon point placement
    if ( createState >= 1 && preview.isPolygon ) {
      let point = event.data.destination;
      if ( !originalEvent.shiftKey ) point = canvas.grid.getSnappedPosition(point.x, point.y, this.gridPrecision);
      preview._addPoint(point, false);
      preview._chain = true; // Note that we are now in chain mode
      return preview.refresh();
    }

    // Standard left-click handling
    super._onClickLeft(event);
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  _onClickLeft2(event) {
    const {createState, preview} = event.data;

    // Conclude polygon placement with double-click
    if ( createState >= 1 && preview.isPolygon ) {
      event.data.createState = 2;
      return this._onDragLeftDrop(event);
    }

    // Standard double-click handling
    super._onClickLeft2(event);
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  async _onDragLeftStart(event) {
    await super._onDragLeftStart(event);
    const document = new DrawingDocument(this._getNewDrawingData(event.data.origin), {parent: canvas.scene});
    const drawing = new Drawing(document);
    event.data.preview = this.preview.addChild(drawing);
    return drawing.draw();
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  _onDragLeftMove(event) {
    const {preview, createState} = event.data;
    if ( !preview || preview._destroyed ) return;
    if ( preview.parent === null ) { // In theory this should never happen, but rarely does
      this.preview.addChild(preview);
    }
    if (createState >= 1 ) {
      preview._onMouseDraw(event);
      if ( preview.data.type !== CONST.DRAWING_TYPES.POLYGON ) event.data.createState = 2;
    }
  }

  /* -------------------------------------------- */

  /**
   * Handling of mouse-up events which conclude a new object creation after dragging
   * @private
   */
  async _onDragLeftDrop(event) {
    const { createState, destination, origin, preview } = event.data;

    // Successful drawing completion
    if ( createState === 2 ) {
      const distance = Math.hypot(destination.x - origin.x, destination.y - origin.y);
      const minDistance = distance >= (canvas.dimensions.size / 8);
      const completePolygon = preview.isPolygon && (preview.data.points.length > 2);

      // Create a completed drawing
      if ( minDistance || completePolygon ) {
        event.data.createState = 0;
        const data = preview.data.toObject(false);

        // Set default text values
        if (data.type === CONST.DRAWING_TYPES.TEXT) {
          data.fillColor = null;
          data.fillAlpha = 0;
          data.strokeColor = null;
          data.strokeWidth = 0;
        }

        // Create the object
        preview._chain = false;
        const cls = getDocumentClass("Drawing");
        const createData = this.constructor.placeableClass.normalizeShape(data);
        const drawing = await cls.create(createData, {parent: canvas.scene});
        const o = drawing.object;
        o._creating = true;
        o._pendingText = "";
        if ( drawing.data.type !== CONST.DRAWING_TYPES.FREEHAND ) o.control({isNew: true});
      }

      // Cancel the preview
      return this._onDragLeftCancel(event);
    }

    // In-progress polygon
    if ( (createState === 1) && preview.isPolygon ) {
      event.data.originalEvent.preventDefault();
      if ( preview._chain ) return;
      return this._onClickLeft(event);
    }

    // Incomplete drawing
    return this._onDragLeftCancel(event);
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  _onDragLeftCancel(event) {
    const preview = this.preview.children?.[0] || null;
    if ( preview?._chain ) {
      preview._removePoint();
      preview.refresh();
      if (preview.data.points.length) return event.preventDefault();
    }
    super._onDragLeftCancel(event);
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  _onClickRight(event) {
    const preview = this.preview.children?.[0] || null;
    if ( preview ) return canvas.mouseInteractionManager._dragRight = false;
    super._onClickRight(event);
  }
}
