/**
 * A PlaceablesLayer designed for rendering the visual Scene for a specific vertical cross-section.
 * Each MapLayer contains a single background image as well as an arbitrary number of Tile objects.
 * @extends {PlaceablesLayer}
 */
class MapLayer extends PlaceablesLayer {
  constructor({bgPath, level=1}={}) {
    super();

    /**
     * The numeric Scene level to which this layer belongs
     * @type {number}
     */
    this.level = level;

    /**
     * The background source path
     * @type {string}
     */
    this.bgPath = bgPath;
  }

  /**
   * The layer background image
   * @type {PIXI.Sprite}
   */
  bg;

  /** @inheritdoc */
  static documentName = "Tile";

  /* -------------------------------------------- */
  /*  Layer Attributes                            */
  /* -------------------------------------------- */

  /** @inheritdoc */
  static get layerOptions() {
    return foundry.utils.mergeObject(super.layerOptions, {
      name: "background",
      zIndex: 0,
      controllableObjects: true,
      rotatableObjects: true
    });
  }

  /* -------------------------------------------- */

  /**
   * Return the base HTML image or video element which is used to generate the background Sprite.
   * @type {HTMLImageElement|HTMLVideoElement}
   */
  get bgSource() {
    if ( !this.bg || !this.bg.texture || !this.bg.texture.valid ) return null;
    const tex = this.bg.texture.baseTexture;
    return tex.resource.source;
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  get hud() {
    return canvas.hud.tile;
  }

  /* -------------------------------------------- */

  /**
   * Is the background texture used in this layer a video?
   * @type {boolean}
   */
  get isVideo() {
    let source = this.bgSource;
    return ( source && source.tagName === "VIDEO" );
  }

  /* -------------------------------------------- */

  /**
   * An array of Tile objects which are rendered within the objects container
   * @type {Tile[]}
   */
  get tiles() {
    return this.objects.children;
  }

  /* -------------------------------------------- */
  /*  Layer Methods                               */
  /* -------------------------------------------- */

  /** @inheritdoc */
  deactivate() {
    super.deactivate();
    this.objects.visible = true;
    return this;
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  async tearDown() {
    if ( this.isVideo ) this.bgSource.pause();
    this.bg = undefined;
    for ( let tile of this.tiles ) {
      if ( tile.isVideo ) {
        game.video.stop(tile.sourceElement);
      }
    }
    return super.tearDown();
  }

  /* -------------------------------------------- */
  /*  Layer Rendering                             */
  /* -------------------------------------------- */

  /** @inheritdoc */
  async draw() {
    await super.draw();

    // Draw the background image or video
    this.bg = this._drawBackground();

    // Enable playback for video backgrounds
    if ( this.isVideo ) {
      this.bgSource.loop = true;
      this.bgSource.volume = game.settings.get("core", "globalAmbientVolume");
      game.video.play(this.bgSource);
    }
    return this;
  }

  /* -------------------------------------------- */

  /**
   * Draw the background Sprite for the layer, aligning its dimensions with those configured for the canvas.
   * @returns {PIXI.Sprite}   The rendered Sprite, or undefined if no background is present
   * @private
   */
  _drawBackground() {

    // Get the background texture
    const tex = getTexture(this.bgPath);
    if ( !tex ) return undefined; // Require the texture to have already been loaded

    // Create the Sprite
    const d = canvas.dimensions;
    const bg = new PIXI.Sprite(tex);
    bg.position.set(d.paddingX - d.shiftX, d.paddingY - d.shiftY);
    bg.width = d.sceneWidth;
    bg.height = d.sceneHeight;

    // Add the Sprite to the container and return
    return this.addChildAt(bg, 0);
  }

  /* -------------------------------------------- */
  /*  Event Handlers                              */
  /* -------------------------------------------- */

  /** @inheritdoc */
  async _onDragLeftStart(event) {
    await super._onDragLeftStart(event);
    const tile = this.constructor.placeableClass.createPreview(event.data.origin);
    event.data.preview = this.preview.addChild(tile);
    this.preview._creating = false;
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  _onDragLeftMove(event) {
    const { destination, createState, preview, origin, originalEvent } = event.data;
    if ( createState === 0 ) return;

    // Determine the drag distance
    const dx = destination.x - origin.x;
    const dy = destination.y - origin.y;
    const dist = Math.min(Math.abs(dx), Math.abs(dy));

    // Update the preview object
    preview.data.width = (originalEvent.altKey ? dist * Math.sign(dx) : dx);
    preview.data.height = (originalEvent.altKey ? dist * Math.sign(dy) : dy);
    if ( !originalEvent.shiftKey ) {
      const half = canvas.dimensions.size / 2;
      preview.data.width = preview.data.width.toNearest(half);
      preview.data.height = preview.data.height.toNearest(half);
    }
    preview.refresh();

    // Confirm the creation state
    event.data.createState = 2;
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  _onDragLeftDrop(event) {
    const { createState, preview } = event.data;
    if ( createState !== 2 ) return;

    // Re-normalize the dropped shape
    const r = new NormalizedRectangle(preview.data.x, preview.data.y, preview.data.width, preview.data.height);
    preview.data.update(r);

    // Require a minimum created size
    if ( Math.hypot(r.width, r.height) < (canvas.dimensions.size / 2) ) return;

    // Render the preview sheet for confirmation
    preview.sheet.render(true, {preview: true});
    this.preview._creating = true;
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  _onDragLeftCancel(event) {
    if ( this.preview._creating ) return;
    return super._onDragLeftCancel(event);
  }

  /* -------------------------------------------- */

  /**
   * Handle drop events for Tile data on the Tiles Layer
   * @param {DragEvent} event     The concluding drag event
   * @param {object} data         The extracted Tile data
   * @private
   */
  async _onDropData(event, data) {
    if ( !data.img ) return;
    if ( !this._active ) this.activate();

    // Get the data for the tile to create
    const createData = await this._getDropData(event, data);

    // Validate that the drop position is in-bounds and snap to grid
    if ( !canvas.grid.hitArea.contains(createData.x, createData.y) ) return false;

    // Create the Tile Document
    const cls = getDocumentClass(this.constructor.documentName);
    return cls.create(createData, {parent: canvas.scene});
  }

  /* -------------------------------------------- */

  /**
   * Prepare the data object when a new Tile is dropped onto the canvas
   * @param {DragEvent} event     The concluding drag event
   * @param {object} data         The extracted Tile data
   * @returns {object}            The prepared data to create
   */
  async _getDropData(event, data) {

    // Determine the tile size
    const tex = await loadTexture(data.img);
    const ratio = canvas.dimensions.size / (data.tileSize || canvas.dimensions.size);
    data.width = tex.baseTexture.width * ratio;
    data.height = tex.baseTexture.height * ratio;

    // Determine the final position and snap to grid unless SHIFT is pressed
    data.x = data.x - (data.width / 2);
    data.y = data.y - (data.height / 2);
    if ( !event.shiftKey ) {
      const {x, y} = canvas.grid.getSnappedPosition(data.x, data.y);
      data.x = x;
      data.y = y;
    }

    // Create the tile as hidden if the ALT key is pressed
    if ( event.altKey ) data.hidden = true;
    return data;
  }
}

/**
 * An extension of the MapLayer that displays underfoot in the background of the Scene.
 * @extends {MapLayer}
 */
class BackgroundLayer extends MapLayer {

  /**
   * The outline of the scene.
   * Not rendered within the BackgroundLayer, but rather beneath it so it does not impact the PrimaryMesh texture.
   * @type {PIXI.Graphics}
   */
  outline;

  /* -------------------------------------------- */
  /*  Layer Methods                               */
  /* -------------------------------------------- */

  /**
   * Draw a background outline which emphasizes what portion of the canvas is playable space and what is buffer.
   * @param {PIXI.Graphics} outline     The outline graphics to use
   */
  drawOutline(outline) {
    outline.clear();
    const sd = canvas.scene.data;
    const displayCanvasBorder = sd.padding !== 0;
    const displaySceneOutline = !sd.img;
    if ( !(displayCanvasBorder || displaySceneOutline) ) return;
    if ( displayCanvasBorder ) outline.lineStyle({
      alignment: 1,
      alpha: 0.75,
      color: 0x000000,
      join: PIXI.LINE_JOIN.BEVEL,
      width: 4
    }).drawShape(canvas.dimensions.rect);
    if ( displaySceneOutline ) outline.lineStyle({
      alignment: 1,
      alpha: 0.25,
      color: 0x000000,
      join: PIXI.LINE_JOIN.BEVEL,
      width: 4
    }).beginFill(0xFFFFFF, 0.025).drawShape(canvas.dimensions.sceneRect).endFill();
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  getDocuments() {
    return this.documentCollection.filter(doc => !doc.data.overhead);
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  getZIndex() {
    return (this.level-1) * 1000;
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  storeHistory(type, data) {
    const [bg, fg] = data.partition(d => d.overhead);
    if ( fg.length ) canvas.foreground.storeHistory(type, fg);
    if ( bg.length ) super.storeHistory(type, bg);
  }
}

/**
 * An extension of the MapLayer that displays overhead in the foreground of the Scene.
 * @extends {MapLayer}
 */
class ForegroundLayer extends MapLayer {

  /** @inheritdoc */
  static get layerOptions() {
    return foundry.utils.mergeObject(super.layerOptions, {
      name: "foreground"
    });
  }

  /* -------------------------------------------- */

  /**
   * Get an array of overhead Tile objects which are roofs
   * @returns {PIXI.DisplayObject[]}
   */
  get roofs() {
    return this.placeables.filter(t => t.data.occlusion.mode === CONST.TILE_OCCLUSION_MODES.ROOF);
  }

  /* -------------------------------------------- */

  /**
   * Determine whether to display roofs
   * @type {boolean}
   */
  get displayRoofs() {
    const restrictVision = !game.user.isGM || (canvas.sight.sources.size !== 0);
    const mapLayerActive = canvas.foreground._active || canvas.background._active;
    return mapLayerActive || restrictVision;
  }

  /* -------------------------------------------- */
  /*  Layer Methods                               */
  /* -------------------------------------------- */

  /** @inheritdoc */
  async draw() {

    // Create a CachedContainer with the overhead tile occlusion mask
    this.occlusionMask = this._drawOcclusionMask();

    // Construct layer contents
    await super.draw();
    this.addChildAt(this.occlusionMask, 0);

    // Layer initialization
    this.initialize();

    // Warn about mismatched foreground size
    if ( this.bg && canvas.background.bg ) {
      const bgTex = canvas.background.bg.texture;
      const fgTex = this.bg.texture;
      if ((bgTex.width !== fgTex.width) || (bgTex.height !== fgTex.height)) {
        ui.notifications.warn("WARNING.ForegroundDimensionsMismatch", {localize: true});
      }
    }
  }

  /* -------------------------------------------- */

  /**
   * Draw the container used to cache the position of Token occlusion shapes to a RenderTexture
   * @returns {CachedContainer}
   * @private
   */
  _drawOcclusionMask() {
    const c = new CachedContainer();
    c.roofs = c.addChild(new PIXI.Container());
    c.roofs.blendMode = PIXI.BLEND_MODES.MAX_COLOR;
    c.tokens = c.addChild(new PIXI.Graphics());
    c.tokens.blendMode = PIXI.BLEND_MODES.MAX_COLOR;
    if ( canvas.performance.blur.enabled ) c.filters = [canvas.createBlurFilter()];
    return c;
  }

  /* -------------------------------------------- */

  /**
   * Perform one-time initialization actions which affect the foreground layer.
   * These actions presume and require that the layer has already been drawn.
   */
  initialize() {
    // Determine whether we require an occlusion mask cached texture
    this.occlusionMask.renderable = this.tiles.some(tile => {
      return tile.isRoof || tile.data.occlusion.mode === CONST.TILE_OCCLUSION_MODES.RADIAL;
    });
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  activate() {
    super.activate();
    canvas.perception.schedule({lighting: {refresh: true}, foreground: {refresh: true}});
    return this;
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  deactivate() {
    super.deactivate();
    canvas.perception.schedule({lighting: {refresh: true}, foreground: {refresh: true}});
    return this;
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  async tearDown() {
    this.occlusionMask.destroy();
    return super.tearDown();
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  getZIndex() {
    return super.getZIndex() + 200;
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  getDocuments() {
    return this.documentCollection.filter(doc => doc.data.overhead);
  }

  /* -------------------------------------------- */

  /**
   * Refresh the display of tiles on the Foreground Layer depending on Token occlusion.
   */
  refresh() {

    // Toggle the opacity of the container when the background tiles layer is active
    this.alpha = canvas.background._active ? 0.25 : 1.0;

    // Refresh the visibility of each tile based on current occlusion
    const displayRoofs = this.displayRoofs;
    for ( let t of this.tiles ) {
      if ( !t.tile || !t.occlusionFilter ) continue;

      // Update renderable state of the tile and its filter
      if ( t.occlusionTile ) t.occlusionTile.renderable = t.isRoof && (t.occluded || !this.displayRoofs);
      t.occlusionFilter.enabled = false;

      // Customize behavior by occlusion mode
      switch ( t.data.occlusion.mode ) {
        case CONST.TILE_OCCLUSION_MODES.NONE:
          t.tile.alpha = Math.min(t.data.hidden ? 0.5 : 1.0, t.data.alpha);
          break;
        case CONST.TILE_OCCLUSION_MODES.FADE:
        case CONST.TILE_OCCLUSION_MODES.ROOF:
          if (t.occluded) t.tile.alpha = t.data.occlusion.alpha;
          else if (!displayRoofs) t.tile.alpha = 0.25;
          else t.tile.alpha = Math.min(t.data.hidden ? 0.5 : 1.0, t.data.alpha);
          break;
        case CONST.TILE_OCCLUSION_MODES.RADIAL:
          t.occlusionFilter.uniforms.alphaOcclusion = t.data.occlusion.alpha;
          t.occlusionFilter.enabled = !t.data.hidden;
          t.tile.alpha = displayRoofs ? 1.0 : 0.25;
          break;
      }
    }
  }

  /* -------------------------------------------- */

  /**
   * Add a roof sprite to the occlusion roof mask container
   * @param {Tile} tile       The roof tile being added
   */
  addRoofSprite(tile) {

    // Checking that a roof sprite doesn't exist for this tile
    if ( tile.occlusionTile ) this.removeRoofSprite(tile);

    // Creating the roof sprite with the right tint
    const s = tile.getRoofSprite();
    if ( !s ) return;
    tile.occlusionTile = s;
    s.renderable = false;
    s.tint = 0x0000FF;
    s.blendMode = PIXI.BLEND_MODES.MAX_COLOR;
    this.occlusionMask.roofs.addChild(s);
  }

  /* -------------------------------------------- */

  /**
   * Remove a roof sprite from occlusion roof mask container
   * @param {Tile} tile       The roof tile being removed
   */
  removeRoofSprite(tile) {
    if ( !tile.occlusionTile ) return;
    this.occlusionMask.roofs.removeChild(tile.occlusionTile);
  }

  /* -------------------------------------------- */

  /**
   * Update occlusion for all tiles on the foreground layer
   */
  updateOcclusion() {
    const tokens = game.user.isGM ? canvas.tokens.controlled : canvas.tokens.ownedTokens;
    this._drawOcclusionShapes(tokens);
    for ( let tile of this.tiles ) {
      tile.updateOcclusion(tokens);
    }
  }

  /* -------------------------------------------- */

  /**
   * Draw the container which caches token-based occlusion shapes
   * @param {Token[]} tokens      The set of currently observed tokens
   * @private
   */
  _drawOcclusionShapes(tokens) {
    if (!this.tiles.length) return;
    const g = this.occlusionMask.tokens;
    g.clear().beginFill(0x00FF00, 1.0); // Draw radial occlusion using the green channel
    for (let token of tokens) {
      const c = token.center;
      const r = Math.max(token.w, token.h);
      g.drawCircle(c.x, c.y, r); // TODO - don't drawCircle every time, just move an existing circle with setPosition
    }
    g.endFill();
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  async _getDropData(event, data) {
    const toCreate = await super._getDropData(event, data);
    toCreate.overhead = true;
    return toCreate;
  }
}
