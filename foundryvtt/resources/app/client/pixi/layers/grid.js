/**
 * A CanvasLayer responsible for drawing a square grid
 * @extends {CanvasLayer}
 */
class GridLayer extends CanvasLayer {

  /**
   * The Grid container
   * @type {BaseGrid}
   */
  grid;

  /**
   * The Grid Highlight container
   * @type {PIXI.Container}
   */
  highlight;

  /**
   * Map named highlight layers
   * @type {Object<GridHighlight>}
   */
  highlightLayers = {};

  /* -------------------------------------------- */

  /** @inheritdoc */
  static get layerOptions() {
    return foundry.utils.mergeObject(super.layerOptions, {
      name: "grid",
      zIndex: 30
    });
  }

  /* -------------------------------------------- */

  /**
   * The grid type rendered in this Scene
   * @type {number}
   */
  get type() {
    return canvas.scene.data.gridType;
  }

  /**
   * A convenient reference to the pixel grid size used throughout this layer
   * @type {number}
   */
  get size() {
    return canvas.dimensions.size;
  }

  /**
   * Get grid unit width
   */
  get w() {
    return this.grid.w;
  }

  /**
   * Get grid unit height
   */
  get h() {
    return this.grid.h;
  }

  /**
   * A boolean flag for whether the current grid is hexagonal
   * @type {boolean}
   */
  get isHex() {
    const gt = CONST.GRID_TYPES;
    return [gt.HEXODDQ, gt.HEXEVENQ, gt.HEXODDR, gt.HEXEVENR].includes(this.type);
  }

  /* -------------------------------------------- */

  /**
   * Draw the grid
   * @param {Object} preview    Override settings used in place of those saved to the Scene data
   */
  async draw({type=null, dimensions=null, gridColor=null, gridAlpha=null}={}) {
    await super.draw();

    // Get grid data
    const gt = type !== null ? type : this.type;

    // Grid configuration
    let grid;
    let gridOptions = {
      dimensions: dimensions || canvas.dimensions,
      color: gridColor || canvas.scene.data.gridColor.replace("#", "0x") || "0x000000",
      alpha: gridAlpha ?? canvas.scene.data.gridAlpha,
      columns: [CONST.GRID_TYPES.HEXODDQ, CONST.GRID_TYPES.HEXEVENQ].includes(gt),
      even: [CONST.GRID_TYPES.HEXEVENR, CONST.GRID_TYPES.HEXEVENQ].includes(gt)
    };

    // Gridless
    if ( gt === CONST.GRID_TYPES.GRIDLESS ) grid = new BaseGrid(gridOptions);

    // Square grid
    else if ( gt === CONST.GRID_TYPES.SQUARE ) grid = new SquareGrid(gridOptions);

    // Hexagonal grid
    else grid = new HexagonalGrid(gridOptions);

    // Draw the highlight layer
    this.highlightLayers = {};
    this.highlight = this.addChild(new PIXI.Container());

    // Draw the grid
    this.grid = this.addChild(grid.draw());
    return this;
  }

  /* -------------------------------------------- */

  /**
   * Given a pair of coordinates (x1,y1), return the grid coordinates (x2,y2) which represent the snapped position
   * @param {number} x          The exact target location x
   * @param {number} y          The exact target location y
   * @param {number} [interval=1]  An interval of grid spaces at which to snap, default is 1. If the interval is zero, no snapping occurs.
   */
  getSnappedPosition(x, y, interval=1) {
    if ( interval === 0 ) return {x, y};
    return this.grid.getSnappedPosition(x, y, interval);
  }

  /* -------------------------------------------- */

  /**
   * Given a pair of coordinates (x, y) - return the top-left of the grid square which contains that point
   * @return {number[]}    An Array [x, y] of the top-left coordinate of the square which contains (x, y)
   */
  getTopLeft(x, y) {
    return this.grid.getTopLeft(x, y);
  }

  /* -------------------------------------------- */

  /**
   * Given a pair of coordinates (x, y), return the center of the grid square which contains that point
   * @return {number[]}    An Array [x, y] of the central point of the square which contains (x, y)
   */
  getCenter(x, y) {
    return this.grid.getCenter(x, y);
  }

  /* -------------------------------------------- */

  /**
   * @typedef {object} MeasureDistancesOptions
   * @property {boolean} [gridSpaces]  Return the distance in grid increments rather than the co-ordinate distance.
   */

  /**
   * Measure the distance between two point coordinates.
   * @param {{x: number, y: number}} origin    The origin point
   * @param {{x: number, y: number}} target    The target point
   * @param {MeasureDistancesOptions} options  Additional options which modify the measurement
   * @returns {number}                         The measured distance between these points
   *
   * @example
   * let distance = canvas.grid.measureDistance({x: 1000, y: 1000}, {x: 2000, y: 2000});
   */
  measureDistance(origin, target, options={}) {
    const ray = new Ray(origin, target);
    const segments = [{ray}];
    return this.grid.measureDistances(segments, options)[0];
  }

  /* -------------------------------------------- */

  /**
   * Measure the distance traveled over an array of distance segments.
   * @param {object[]} segments                An array of measured segments
   * @param {MeasureDistancesOptions} options  Additional options which modify the measurement
   */
  measureDistances(segments, options={}) {
    return this.grid.measureDistances(segments, options);
  }

  /* -------------------------------------------- */
  /*  Grid Highlighting Methods
  /* -------------------------------------------- */

  /**
   * Define a new Highlight graphic
   * @param {string} name     The name for the referenced highlight layer
   */
  addHighlightLayer(name) {
    const layer = this.highlightLayers[name];
    if ( !layer || layer._destroyed ) {
      this.highlightLayers[name] = this.highlight.addChild(new GridHighlight(name));
    }
    return this.highlightLayers[name];
  }

  /* -------------------------------------------- */

  /**
   * Clear a specific Highlight graphic
   * @param {string} name     The name for the referenced highlight layer
   */
  clearHighlightLayer(name) {
    const layer = this.highlightLayers[name];
    if ( layer ) layer.clear();
  }

  /* -------------------------------------------- */

  /**
   * Destroy a specific Highlight graphic
   * @param {string} name     The name for the referenced highlight layer
   */
  destroyHighlightLayer(name) {
    const layer = this.highlightLayers[name];
    if ( layer ) {
      this.highlight.removeChild(layer);
      layer.destroy();
    }
  }

  /* -------------------------------------------- */

  /**
   * Obtain the highlight layer graphic by name
   * @param {string} name     The name for the referenced highlight layer
   */
  getHighlightLayer(name) {
    return this.highlightLayers[name];
  }

  /* -------------------------------------------- */

  /**
   * Add highlighting for a specific grid position to a named highlight graphic
   * @param {string} name       The name for the referenced highlight layer
   * @param {object} options    Options for the grid position that should be highlighted
   */
  highlightPosition(name, options) {
    const layer = this.highlightLayers[name];
    if ( !layer ) return false;
    this.grid.highlightGridPosition(layer, options);
  }

  /* -------------------------------------------- */

  /**
   * Test if a specific row and column position is a neighboring location to another row and column coordinate
   * @param {number} r0     The original row position
   * @param {number} c0     The original column position
   * @param {number} r1     The candidate row position
   * @param {number} c1     The candidate column position
   */
  isNeighbor(r0, c0, r1, c1) {
    let neighbors = this.grid.getNeighbors(r0, c0);
    return neighbors.some(n => (n[0] === r1) && (n[1] === c1));
  }
}
