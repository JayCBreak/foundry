/**
 * The Sight Layer which implements dynamic vision, lighting, and fog of war
 * This layer uses an event-driven workflow to perform the minimal required calculation in response to changes.
 * @see {PointSource}
 * @extends {CanvasLayer}
 *
 * The container structure of this layer is as follows:
 *
 * @property {PIXI.Graphics} unexplored       The unexplored background which spans the entire canvas
 * @property {PIXI.Container} explored        The exploration container which tracks exploration progress
 * @property {PIXI.Container} revealed          A container of regions which have previously been revealed
 * @property {PIXI.Sprite} saved                The saved fog exploration texture
 * @property {PIXI.Container} pending           Pending exploration which has not yet been committed to the texture
 * @property {PIXI.Container} vision            The container of current vision exploration
 * @property {PIXI.Graphics} vision.base          Baseline provided vision
 * @property {PIXI.Container} vision.fov          Current light source field-of-view polygons
 * @property {PIXI.Graphics} vision.los           Current vision source line-of-sight polygons
 * @property {PIXI.Container} vision.roofs        Roof textures which should temporarily be revealed
 *
 * @example <caption>The sightRefresh hook</caption>
 * Hooks.on("sightRefresh", layer => {});
 */
class SightLayer extends CanvasLayer {
  constructor() {
    super();

    /**
     * The FogExploration document which applies to this canvas view
     * @type {FogExploration|null}
     */
    this.exploration = null;

    /**
     * A Collection of vision sources which are currently active within the rendered Scene.
     * @type {Collection<string,PointSource>}
     */
    this.sources = new foundry.utils.Collection();

    /**
     * A status flag for whether the layer initialization workflow has succeeded
     * @type {boolean}
     * @private
     */
    this._initialized = false;

    /**
     * A debounced function to save fog of war exploration once a stream of updates have stopped
     * @type {Function}
     */
    this.debounceSaveFog = foundry.utils.debounce(this.saveFog.bind(this), 3000);
  }

  /**
   * The current vision container which provides line-of-sight for vision sources and field-of-view of light sources.
   * @type {PIXI.Container}
   */
  vision;

  /**
   * The canonical line-of-sight polygon which defines current Token visibility.
   * @type {PIXI.Graphics}
   */
  los;

  /**
   * A cached container which creates a render texture used for the LOS mask.
   * @type {CachedContainer}
   */
  losCache;

  /**
   * Track whether we have pending fog updates which have not yet been saved to the database
   * @type {boolean}
   * @private
   */
  _fogUpdated = false;

  /**
   * The configured resolution used for the saved fog-of-war texture
   * @type {{resolution: number, width: number, height: number}}
   * @private
   */
  _fogResolution;

  /**
   * A pool of RenderTexture objects which can be cycled through to save fog exploration progress.
   * @type {PIXI.RenderTexture[]}
   * @private
   */
  _fogTextures = [];

  /**
   * Track whether there is a source of vision within the buffer region outside the primary scene canvas
   * @type {boolean}
   * @private
   */
  _inBuffer = false;

  /**
   * Define the threshold value for the number of distinct Wall endpoints.
   * Below this threshold, exact vision computation is used by casting a Ray at every endpoint.
   * Above this threshold, approximate vision computation is used by culling to only nearby endpoints.
   * @type {number}
   */
  static EXACT_VISION_THRESHOLD = 200;

  /**
   * Define the number of positions that are explored before a set of fog updates are pushed to the server.
   * @type {number}
   */
  static FOG_COMMIT_THRESHOLD = 10;

  /**
   * The maximum allowable fog of war texture size.
   * @type {number}
   */
  static MAXIMUM_FOW_TEXTURE_SIZE = 4096;

  /* -------------------------------------------- */

  /** @override */
  static get layerOptions() {
    return foundry.utils.mergeObject(super.layerOptions, {
      name: "sight",
      zIndex: 400
    });
  }

  /* -------------------------------------------- */

  /**
   * Does the currently viewed Scene support Token field of vision?
   * @type {boolean}
   */
  get tokenVision() {
    return canvas.scene.data.tokenVision;
  }

  /* -------------------------------------------- */

  /**
   * Does the currently viewed Scene support fog of war exploration?
   * @type {boolean}
   */
  get fogExploration() {
    return canvas.scene.data.fogExploration;
  }

  /* -------------------------------------------- */
  /*  Layer Initialization                        */
  /* -------------------------------------------- */

  /** @override */
  async tearDown() {

    // Save pending FOW exploration
    const wasDeleted = !game.scenes.has(canvas.scene?.id);
    if ( !wasDeleted ) {
      this.commitFog();
      if ( this._fogUpdated ) await this.saveFog();
    }

    // Clear sources
    this.exploration = null;
    this.sources.clear();
    this._initialized = false;

    // Destroy fog exploration textures
    for ( let t of this._fogTextures ) t.destroy(true);
    this._fogTextures = [];

    // Parent layer tear-down procedures
    return super.tearDown();
  }

  /* -------------------------------------------- */

  /**
   * Initialize fog of war - resetting it when switching scenes or re-drawing the canvas
   * @return {Promise<void>}
   */
  async initializeFog() {
    this._initialized = false;
    if ( this.tokenVision && !this.exploration ) await this.loadFog();
    this._initialized = true;
  }

  /* -------------------------------------------- */

  /**
   * Initialize all Token vision sources which are present on this layer
   */
  initializeSources() {

    // Clear the prior losCache
    this.losCache.removeChildren().forEach(c => c.destroy(true));

    // Update vision sources
    this.sources.clear();
    for ( let token of canvas.tokens.placeables ) {
      token.updateVisionSource({defer: true});
    }
  }

  /* -------------------------------------------- */

  /**
   * Update FoW unexplored and explored colors
   */
  updateFogExplorationColors() {
    if ( this.unexplored ) {
      const rect = canvas.dimensions.rect.clone().pad(CONFIG.Canvas.blurStrength + 2);
      this.unexplored.clear().beginFill(CONFIG.Canvas.unexploredColor, 1.0).drawShape(rect).endFill();
    }
    
    if ( this.revealed )
      this.revealed.fogColorFilter.uniforms.exploredColor = foundry.utils.hexToRGB(CONFIG.Canvas.exploredColor);
  }

  /* -------------------------------------------- */
  /*  Layer Rendering                             */
  /* -------------------------------------------- */

  /** @override */
  async draw() {
    await super.draw();
    this.removeChildren();

    // Internal stored variables
    const d = canvas.dimensions;
    this._fogResolution = this._configureFogResolution();

    // Create the cached masking sprite
    this._createCachedMask();

    // Create an initial vision container
    this._createVisionContainer();

    // Unexplored area is obscured by darkness. We need a larger rectangle so that the blur filter doesn't clip
    this.unexplored = this.addChild(new PIXI.LegacyGraphics());
    const rect = d.rect.clone().pad(CONFIG.Canvas.blurStrength+2);
    this.unexplored.beginFill(CONFIG.Canvas.unexploredColor, 1.0).drawShape(rect).endFill();

    // Exploration container
    this.explored = this.addChild(new PIXI.Container());
    this.explored.addChild(this.vision);
    if ( canvas.scene.data.padding !== 0 ) {
      this.explored.msk = this.addChild(new PIXI.LegacyGraphics());
      this.explored.msk.beginFill(0xFFFFFF, 1.0).drawShape(canvas.dimensions.sceneRect).endFill();
    } else this.explored.msk = null;
    this.explored.mask = this.explored.msk;

    // Past exploration updates
    this.revealed = this.explored.addChild(new PIXI.Container());
    this.saved = this.revealed.addChild(new PIXI.Sprite());
    this.saved.position.set(d.paddingX, d.paddingY);
    this.saved.width = this._fogResolution.width;
    this.saved.height = this._fogResolution.height;

    this.revealed.fogColorFilter = FogColorFilter.create({
      exploredColor: foundry.utils.hexToRGB(CONFIG.Canvas.exploredColor)
    });
    this.revealed.filters = [this.revealed.fogColorFilter];

    // Pending vision containers
    this.pending = this.revealed.addChild(new PIXI.Container());

    // Apply a multiply blend filter to the fog container
    this.filter = canvas.performance.blur.enabled ? canvas.createBlurFilter() : new PIXI.filters.AlphaFilter(1.0);
    this.filter.blendMode = PIXI.BLEND_MODES.MULTIPLY;
    this.filter.autoFit = false;
    this.filters = [this.filter];
    this.filterArea = canvas.app.screen;

    // Return the layer
    this.visible = false;
    return this;
  }

  /* -------------------------------------------- */

  /**
   * Create the cached container and sprite used to provide a LOS mask
   * @private
   */
  _createCachedMask() {
    const c = this.losCache = canvas.lighting.addChildAt(new CachedContainer(), 0);
    c.clearColor = [0, 0, 0, 0];
    c.filter = new PIXI.filters.BlurFilter(LightSource.BLUR_STRENGTH);
    c.filter.blendMode = PIXI.BLEND_MODES.NORMAL;
    c.filters = [c.filter];
  }

  /* -------------------------------------------- */

  /**
   * Construct a vision container that is used to render the current view position.
   * @return {PIXI.Container}
   * @private
   */
  _createVisionContainer() {
    const c = new PIXI.Container();
    c._explored = false;

    // Baseline visibility
    c.base = c.addChild(new PIXI.LegacyGraphics());

    // Field of view Container
    c.fov = c.addChild(new PIXI.Container());

    // Roof masks
    c.roofs = null;

    // Line of Sight masking Graphics
    c.los = c.addChild(new PIXI.LegacyGraphics());
    c.mask = c.los;

    // Clone LOS to the cached container
    this.losCache.removeChildren().forEach(c => c.destroy(true));
    this.losCache.addChild(c.los.clone());

    // Assign to the instance
    this.vision = c;
    this.los = c.los;
    return c;
  }

  /* -------------------------------------------- */

  /**
   * Update the display of the sight layer.
   * Organize sources into rendering queues and draw lighting containers for each source
   *
   * @param {boolean} [forceUpdateFog]  Always update the Fog exploration progress for this update
   * @param {boolean} [skipUpdateFog]   Never update the Fog exploration progress for this update
   */
  refresh({forceUpdateFog=false, skipUpdateFog=false}={}) {
    if ( !this._initialized ) return;
    if ( !this.tokenVision ) {
      this.visible = false;
      return this.restrictVisibility()
    }

    // Configuration variables
    const d = canvas.dimensions;
    const unrestrictedVisibility = canvas.lighting.globalLight;
    let commitFog = false;

    // Stage the prior vision container to be saved to the FOW texture
    const prior = this.explored.removeChild(this.vision);
    if ( prior._explored && !skipUpdateFog ) {
      this.pending.addChild(prior);
      commitFog = this.pending.children.length >= this.constructor.FOG_COMMIT_THRESHOLD;
    }
    else prior.destroy({children: true});

    // Create a new vision container for this frame
    const vision = this._createVisionContainer();
    this.explored.addChild(vision);

    // Draw standard vision sources
    let inBuffer = canvas.scene.data.padding === 0;

    // Unrestricted visibility, everything in LOS is visible
    if ( unrestrictedVisibility ) vision.base.beginFill(0xFFFFFF, 1.0).drawShape(d.rect).endFill();

    // Otherwise, provided minimum visibility for each vision source
    else {
      for ( let source of this.sources ) {
        vision.base.beginFill(0xFFFFFF, 1.0).drawCircle(source.x, source.y, d.size / 2);
      }
    }

    // Draw field-of-vision for lighting sources
    for ( let source of canvas.lighting.sources ) {
      if ( !this.sources.size || !source.active ) continue;
      const g = new PIXI.LegacyGraphics();
      g.beginFill(0xFFFFFF, 1.0).drawShape(source.los).endFill();
      vision.fov.addChild(g);
      if ( source.data.vision ) {  // Some ambient lights provide vision
        vision.los.beginFill(0xFFFFFF).drawShape(source.los).endFill();
      }
    }

    // Draw sight-based visibility for each vision source
    for ( let source of this.sources ) {
      source.active = true;
      if ( !inBuffer && !d.sceneRect.contains(source.x, source.y) ) inBuffer = true;
      if ( !unrestrictedVisibility && (source.radius > 0) ) {             // Token FOV radius
        vision.fov.addChild(source.drawSight());
      }
      vision.los.beginFill(0xFFFFFF).drawShape(source.los).endFill();     // Token LOS mask
      if ( !skipUpdateFog ) this.updateFog(source, forceUpdateFog);       // Update fog exploration
    }

    // Commit updates to the Fog of War texture
    if ( commitFog ) this.commitFog();

    // Alter visibility of the vision layer
    this.visible = this.sources.size || !game.user.isGM;

    // Apply a mask to the exploration container
    if ( this.explored.msk ) {
      const noMask = this.sources.size && inBuffer;
      this.explored.mask = noMask ? null : this.explored.msk;
      this.explored.msk.visible = !noMask;
    }

    // Restrict the visibility of other canvas objects
    this._inBuffer = inBuffer;
    this.restrictVisibility();
  }

  /* -------------------------------------------- */

  /**
   * Restrict the visibility of certain canvas assets (like Tokens or DoorControls) based on the visibility polygon
   * These assets should only be displayed if they are visible given the current player's field of view
   */
  restrictVisibility() {

    // Light Sources
    const ll = canvas.lighting;
    const lightMask = this.visible ? this.losCache.sprite : null;
    ll.illumination.primary.mask = ll.coloration.mask = ll.background.mask = lightMask;

    // Tokens
    for ( let t of canvas.tokens.placeables ) {
      t.visible = ( !this.tokenVision && !t.data.hidden ) || t.isVisible;
    }

    // Door Icons
    for ( let d of canvas.controls.doors.children ) {
      d.visible = !this.tokenVision || d.isVisible;
    }

    // Map Notes
    for ( let n of canvas.notes.placeables ) {
      n.visible = n.isVisible;
    }

    /**
     * A hook event that fires when the SightLayer has been refreshed.
     * @function sightRefresh
     * @memberof hookEvents
     * @param {SightLayer} sight The SightLayer
     */
    Hooks.callAll("sightRefresh", this);
  }

  /* -------------------------------------------- */

  /**
   * Test whether a point on the Canvas is visible based on the current vision and LOS polygons
   *
   * @param {Point} point           The point in space to test, an object with coordinates x and y.
   * @param {number} tolerance      A numeric radial offset which allows for a non-exact match. For example, if
   *                                tolerance is 2 then the test will pass if the point is within 2px of a vision
   *                                polygon.
   * @param {PIXI.DisplayObject} [object]   An optional reference to the object whose visibility is being tested
   *
   * @return {boolean}              Whether the point is currently visible.
   */
  testVisibility(point, {tolerance=2, object=null}={}) {
    const visionSources = this.sources;
    const lightSources = canvas.lighting.sources;
    const d = canvas.dimensions;
    if ( !visionSources.size ) return game.user.isGM;

    // Determine the array of offset points to test
    const t = tolerance;
    const offsets = t > 0 ? [[0, 0],[-t,-t],[-t,t],[t,t],[t,-t],[-t,0],[t,0],[0,-t],[0,t]] : [[0,0]];
    const points = offsets.map(o => new PIXI.Point(point.x + o[0], point.y + o[1]));

    // If the point is entirely inside the buffer region, it may be hidden from view
    if ( !this._inBuffer && !points.some(p => d.sceneRect.contains(p.x, p.y)) ) return false;

    // Check each point for one which provides both LOS and FOV membership
    return points.some(p => {
      let hasLOS = false;
      let hasFOV = false;
      let requireFOV = !canvas.lighting.globalLight;

      // Check vision sources
      for ( let source of visionSources.values() ) {
        if ( !source.active ) continue;               // The source may be currently inactive
        if ( !hasLOS || (!hasFOV && requireFOV) ) {   // Do we need to test for LOS?
          if ( source.los.contains(p.x, p.y) ) {
            hasLOS = true;
            if ( !hasFOV && requireFOV ) {            // Do we need to test for FOV?
              if ( source.fov.contains(p.x, p.y) ) hasFOV = true;
            }
          }
        }
        if ( hasLOS && (!requireFOV || hasFOV) ) {    // Did we satisfy all required conditions?
          return true;
        }
      }

      // Check light sources
      for ( let source of lightSources.values() ) {
        if ( !source.active ) continue;               // The source may be currently inactive
        if ( source.containsPoint(p) ) {
          if ( source.data.vision ) hasLOS = true;
          hasFOV = true;
        }
        if ( hasLOS && (!requireFOV || hasFOV) ) return true;
      }
      return false;
    });
  }

  /* -------------------------------------------- */
  /*  Fog of War Management                       */
  /* -------------------------------------------- */

  _getFogTexture() {
    if ( this._fogTextures.length ) {
      const tex = this._fogTextures.pop();
      if ( tex.valid ) return tex;
    }
    return PIXI.RenderTexture.create(this._fogResolution);
  }

  /* -------------------------------------------- */

  /**
   * Once a new Fog of War location is explored, composite the explored container with the current staging sprite
   * Save that staging Sprite as the rendered fog exploration and swap it out for a fresh staging texture
   * Do all this asynchronously, so it doesn't block token movement animation since this takes some extra time
   */
  commitFog() {
    if ( !this.pending.children.length ) return;
    if ( CONFIG.debug.fog ) console.debug("SightLayer | Committing fog exploration to render texture.");

    // Create a staging texture and render the entire fog container to it
    const d = canvas.dimensions;
    const tex = this._getFogTexture();
    const transform = new PIXI.Matrix(1, 0, 0, 1, -d.paddingX, -d.paddingY);

    // Disabling fogColorFilter to render only in black and white
    this.revealed.fogColorFilter.enabled = false;

    // Render the currently revealed vision to the texture
    canvas.app.renderer.render(this.revealed, tex, undefined, transform);

    // Enabling the filter now that render is done
    this.revealed.fogColorFilter.enabled = true;

    // Return reusable RenderTexture to the pool, destroy past exploration textures
    if ( this.saved.texture instanceof PIXI.RenderTexture ) this._fogTextures.push(this.saved.texture);
    else this.saved.texture.destroy();
    this.saved.texture = tex;

    // Clear the pending container
    this.pending.removeChildren().forEach(c => c.destroy(true));

    // Schedule saving the texture to the database
    this._fogUpdated = true;
    this.debounceSaveFog();
  }

  /* -------------------------------------------- */

  /**
   * Load existing fog of war data from local storage and populate the initial exploration sprite
   * @return {Promise<(PIXI.Texture|void)>}
   */
  async loadFog() {
    if ( CONFIG.debug.fog ) console.debug("SightLayer | Loading saved FogExploration for Scene.");

    // Remove the previous render texture if one exists
    if ( this.saved.texture?.valid ) {
      this._fogTextures.push(this.saved.texture);
      this.saved.texture = null;
    }

    // Take no further action if token vision is not enabled
    if ( !this.tokenVision ) return;

    // Load existing FOW exploration data or create a new placeholder
    const fogExplorationCls = getDocumentClass("FogExploration");
    this.exploration = await fogExplorationCls.get();

    // Create a brand new FogExploration document
    if ( !this.exploration ) {
      this.exploration = new fogExplorationCls();
      return this.saved.texture = PIXI.Texture.EMPTY;
    }

    // Extract and assign the fog data image
    const assign = (tex, resolve) => {
      this.saved.texture = tex;
      resolve(tex);
    }
    return await new Promise(resolve => {
      let tex = this.exploration.getTexture();
      if ( tex === null ) return assign(PIXI.Texture.EMPTY, resolve);
      if ( tex.baseTexture.valid ) return assign(tex, resolve);
      else tex.on("update", tex => assign(tex, resolve));
    });
  }

  /* -------------------------------------------- */

  /**
   * Dispatch a request to reset the fog of war exploration status for all users within this Scene.
   * Once the server has deleted existing FogExploration documents, the _onResetFog handler will re-draw the canvas.
   */
  async resetFog() {
    if ( CONFIG.debug.fog ) console.debug("SightLayer | Resetting fog of war exploration for Scene.");
    game.socket.emit("resetFog", canvas.scene.id, getDocumentClass("FogExploration")._onResetFog);
  }

  /* -------------------------------------------- */

  /**
   * Save Fog of War exploration data to a base64 string to the FogExploration document in the database.
   * Assumes that the fog exploration has already been rendered as fog.rendered.texture.
   */
  async saveFog() {
    if ( !this.tokenVision || !this.fogExploration || !this.exploration ) return;
    if ( !this._fogUpdated ) return;
    this._fogUpdated = false;
    if (CONFIG.debug.fog) console.debug("SightLayer | Saving exploration progress to FogExploration document.");

    // Use the existing rendered fog to create a Sprite and downsize to save with smaller footprint
    const d = canvas.dimensions;
    const fog = new PIXI.Sprite(this.saved.texture);

    // Determine whether a downscaling factor should be used
    const maxSize = this.constructor.MAXIMUM_FOW_TEXTURE_SIZE;
    const scale = Math.min(maxSize / d.sceneWidth, maxSize / d.sceneHeight);
    if ( scale < 1.0 ) fog.scale.set(scale, scale);

    // Add the fog to a temporary container to bound it's dimensions and export to base data
    const stage = new PIXI.Container();
    stage.addChild(fog);

    // Extract fog exploration to a base64 image
    const updateData = {
      explored: await ImageHelper.pixiToBase64(stage, "image/jpeg", 0.8),
      timestamp: Date.now()
    };

    // Create or update the FogExploration document
    if (!this.exploration.id) {
      this.exploration.data.update(updateData);
      this.exploration = await this.exploration.constructor.create(this.exploration.toJSON());
    }
    else await this.exploration.update(updateData);
  }

  /* -------------------------------------------- */

  /**
   * Update the fog layer when a player token reaches a board position which was not previously explored
   * @param {PointSource} source   The vision source for which the fog layer should update
   * @param {boolean} force             Force fog to be updated even if the location is already explored
   */
  updateFog(source, force=false) {
    if ( !this.fogExploration ) return;

    // If, for any reason, the fog exploration object does not exist. Create a fresh one
    if ( !this.exploration ) {
      const cls = getDocumentClass("FogExploration");
      this.exploration = new cls();
    }

    // Record the exploration point
    this.vision._explored = this.exploration.explore(source, force);
  }

  /* -------------------------------------------- */

  /**
   * Choose an adaptive fog rendering resolution which downscales the saved fog textures for larger dimension Scenes.
   * It is important that the width and height of the fog texture is evenly divisible by the downscaling resolution.
   * @return {{resolution: number, width: number, height: number}}
   * @private
   */
  _configureFogResolution() {
    const d = canvas.dimensions;
    let width = d.sceneWidth;
    let height = d.sceneHeight;
    const maxSize = this.constructor.MAXIMUM_FOW_TEXTURE_SIZE;

    // Adapt the fog texture resolution relative to some maximum size
    let resolution = 1.0;
    if ( (width >= height) && (width > maxSize) ) {
      resolution = maxSize / width;
    } else if ( height > maxSize ) {
      resolution = maxSize / height;
    }

    // Determine the fog texture dimensions that is evenly divisible by the scaled resolution
    return {
      resolution,
      width,
      height,
      mipmap: PIXI.MIPMAP_MODES.OFF,
      scaleMode: PIXI.SCALE_MODES.LINEAR,
      multisample: PIXI.MSAA_QUALITY.NONE
    }
  }

  /* -------------------------------------------- */

  /**
   * If fog of war data is reset from the server, re-draw the canvas
   * @return {Promise}
   */
  async _handleResetFog() {
    ui.notifications.info(`Fog of War exploration progress was reset for this Scene`);
    this.pending.removeChildren().forEach(c => c.destroy(true));
    this._fogUpdated = false;
    await canvas.draw();
  }
}
