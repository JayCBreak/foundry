/**
 * TODO: Document Me
 */
class SpecialEffect {
  constructor(parent, options) {
    this.parent = parent;
    this.options = mergeObject(this.constructor.effectOptions, options, {insertKeys: false});
    this.emitters = this.getParticleEmitters();

    /**
     * Use this flag as a way to pass a stop signal into the animation frame
     * @type {boolean}
     */
    this._stop = null;
  }

  static OPTION_TYPES = {
    VALUE: 1,
    CHECKBOX: 2,
    RANGE: 3,
    SELECT: 4
  };

  static DEFAULT_CONFIG = {
    "maxSpeed": 0,
    "noRotation": false,
    "blendMode": "normal",
    "emitterLifetime": -1,
    "pos": {
      "x": 0,
      "y": 0
    },
    "spawnType": "rect"
  };

  /* -------------------------------------------- */

  static get label() {
    return "Special Effect";
  }

  /* -------------------------------------------- */

  static get effectOptions() {
    return {
      density: {
        label: "Particle Density",
        type: this.OPTION_TYPES.RANGE,
        value: 0.5,
        min: 0.1,
        max: 5,
        step: 0.1
      }
    };
  }

  /* -------------------------------------------- */

  getParticleEmitters() {
    return [];
  }

  /* -------------------------------------------- */

  play(duration) {
    this._stop = null;
    for ( let e of this.emitters ) {
      this._startEmitter(e);
    }
  }

  /* -------------------------------------------- */

  stop() {
    this._stop = true;
    for ( let e of this.emitters ) {
      e.emit = false;
      e.cleanup();
    }
  }

  /* -------------------------------------------- */

  _startEmitter(emitter) {

    // Calculate the current time
    let elapsed = Date.now();

    // Update function every frame
    let update = () => {

      // Maybe stop
      if ( this._stop) return;

      // Update the next frame
      requestAnimationFrame(update);

      // Track the number of elapsed seconds since the previous frame update
      let now = Date.now();
      emitter.update((now - elapsed) * 0.001);
      elapsed = now;
    };

    // Start emitting
    emitter.emit = true;
    update();
	}
}
