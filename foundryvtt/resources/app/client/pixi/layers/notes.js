/**
 * The Notes Layer which contains Note canvas objects
 * @extends {PlaceablesLayer}
 */
class NotesLayer extends PlaceablesLayer {

  /** @inheritdoc */
  static get layerOptions() {
    return foundry.utils.mergeObject(super.layerOptions, {
      name: "notes",
      canDragCreate: false,
      sortActiveTop: true,
      zIndex: 60
    });
  }

  /** @inheritdoc */
  static documentName = "Note";

  /**
   * The named core setting which tracks the toggled visibility state of map notes
   * @type {string}
   */
  static TOGGLE_SETTING = "notesDisplayToggle";

  /* -------------------------------------------- */
  /*  Methods
  /* -------------------------------------------- */

  /** @inheritdoc */
  activate() {
    super.activate();
    if ( this.objects ) {
      this.placeables.forEach(p => p.controlIcon.visible = true);
    }
    return this;
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  deactivate() {
    super.deactivate();
    const isToggled = game.settings.get("core", this.constructor.TOGGLE_SETTING);
    if ( this.objects ) {
      this.objects.visible = isToggled;
      this.placeables.forEach(p => p.controlIcon.visible = isToggled);
    }
    this.interactiveChildren = isToggled;
    return this;
  }

  /* -------------------------------------------- */

  /**
   * Register game settings used by the NotesLayer
   */
  static registerSettings() {
    game.settings.register("core", this.TOGGLE_SETTING, {
      name: "Map Note Toggle",
      scope: "client",
      type: Boolean,
      config: false,
      default: false
    });
  }

  /* -------------------------------------------- */
  /*  Event Handlers                              */
  /* -------------------------------------------- */

  /** @inheritdoc */
  _onMouseDown(event) {}

  /* -------------------------------------------- */

  /**
   * Handle JournalEntry document drop data
   * @param {DragEvent} event   The drag drop event
   * @param {object} data       The dropped data transfer data
   * @protected
   */
  async _onDropData(event, data) {

    // Acquire Journal entry
    let entry = await JournalEntry.fromDropData(data);
    if ( entry.compendium ) {
      const journalData = game.journal.fromCompendium(entry);
      entry = await JournalEntry.implementation.create(journalData);
    }

    // Get the world-transformed drop position
    const coords = this._canvasCoordinatesFromDrop(event);
    if ( !coords ) return false;
    const noteData = {entryId: entry.id, x: coords[0], y: coords[1]};
    return this._createPreview(noteData, {top: event.clientY - 20, left: event.clientX + 40});
  }
}
