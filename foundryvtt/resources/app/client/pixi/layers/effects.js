/**
 * A CanvasLayer for displaying visual effects like weather, transitions, flashes, or more
 * @type {CanvasLayer}
 */
class WeatherLayer extends CanvasLayer {

  /**
   * The weather overlay container
   * @type {PIXI.Container}
   */
  weather;

  /**
   * The currently active weather effect
   * @type {SpecialEffect}
   */
  weatherEffect;

  /**
   * Track the set of particle Emitter instances which are active within this Scene.
   * @type {PIXI.particles.Emitter[]}
   */
  emitters = [];

  /**
   * An occlusion filter that prevents weather from being displayed in certain regions
   * @type {AbstractBaseMaskFilter}
   */
  weatherOcclusionFilter;

  /* -------------------------------------------- */

  /** @inheritdoc */
  static get layerOptions() {
    return foundry.utils.mergeObject(super.layerOptions, {
      name: "effects",
      zIndex: 700
    });
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  async tearDown() {
    if ( this.weatherEffect ) this.weatherEffect.stop();
    this.weather = this.weatherEffect = null;
    return super.tearDown();
  }

  /* -------------------------------------------- */

  /** @override */
  async draw() {
    this.weatherOcclusionFilter = InverseOcclusionMaskFilter.create({
      alphaOcclusion: 0,
      uMaskSampler: canvas.foreground.occlusionMask.renderTexture
    }, "b");
    this.drawWeather();
  }

  /* -------------------------------------------- */

  /**
   * Draw the weather container.
   * @returns {PIXI.Container|null}    The weather container, or null if no effect is present
   */
  drawWeather() {
    if ( this.weatherEffect )	this.weatherEffect.stop();
    const effect = CONFIG.weatherEffects[canvas.scene.data.weather];
    if ( !effect ) {
      this.weatherOcclusionFilter.enabled = false;
      return null;
    }

    // Create the effect and begin playback
    if ( !this.weather ) this.weather = this.addChild(new PIXI.Container());
    this.weatherEffect = new effect(this.weather);
    this.weatherEffect.play();

    // Apply occlusion filter
    this.weatherOcclusionFilter.enabled = true;
    this.weather.filters = [this.weatherOcclusionFilter];
    return this.weather;
  }
}
