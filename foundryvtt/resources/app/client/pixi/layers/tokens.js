/**
 * The Tokens Container
 * @type {PlaceablesLayer}
 */
class TokenLayer extends PlaceablesLayer {
  constructor() {
    super();

    /**
     * The current index position in the tab cycle
     * @type {number|null}
     * @private
     */
    this._tabIndex = null;

    /**
     * Remember the last drawn wildcard token image to avoid repetitions
     * @type {string}
     */
    this._lastWildcard = null;
  }


  /* -------------------------------------------- */

  /** @inheritdoc */
  static get layerOptions() {
    return foundry.utils.mergeObject(super.layerOptions, {
      name: "tokens",
      canDragCreate: false,
      controllableObjects: true,
      rotatableObjects: true,
      zIndex: 100
    });
  }

  /** @inheritdoc */
  static documentName = "Token";

  /* -------------------------------------------- */

  /** @inheritdoc */
  get gridPrecision() {
    return 1; // Snap tokens to top-left
  }

  /* -------------------------------------------- */
  /*  Properties
  /* -------------------------------------------- */

  /**
   * Token objects on this layer utilize the TokenHUD
   */
  get hud() {
    return canvas.hud.token;
  }

  /**
   * An Array of tokens which belong to actors which are owned
   * @type {Token[]}
   */
  get ownedTokens() {
    return this.placeables.filter(t => t.actor && t.actor.isOwner);
  }

  /* -------------------------------------------- */
  /*  Methods
  /* -------------------------------------------- */

  /** @override */
  async tearDown() {

    // Conclude token animation
    this.concludeAnimation();

    // Release tokens and destroy the layer
    return super.tearDown();
  }

  /* -------------------------------------------- */

  /** @override */
  activate() {
    super.activate();
    if ( canvas.controls ) canvas.controls.doors.visible = true;
    this._tabIndex = null;
    return this;
  }

  /* -------------------------------------------- */

  /** @override */
  deactivate() {
    super.deactivate();
    if ( this.objects ) this.objects.visible = true;
    if ( canvas.controls ) canvas.controls.doors.visible = false;
    return this;
  }

  /* -------------------------------------------- */

  /** @override */
  selectObjects({x, y, width, height, releaseOptions={}, controlOptions={}}={}) {
    controlOptions.releaseOthers = false;
    return super.selectObjects({x, y, width, height, releaseOptions, controlOptions});
  }

  /* -------------------------------------------- */

  /**
   * Target all Token instances which fall within a coordinate rectangle.
   *
   * @param {number} x                The top-left x-coordinate of the selection rectangle
   * @param {number} y                The top-left y-coordinate of the selection rectangle
   * @param {number} width            The width of the selection rectangle
   * @param {number} height           The height of the selection rectangle
   * @param {boolean} releaseOthers   Whether or not to release other targeted tokens
   * @return {number}                 The number of Token instances which were targeted.
   */
  targetObjects({x, y, width, height}, {releaseOthers=true}={}) {
    const user = game.user;

    // Get the set of targeted tokens
    const targets = this.placeables.filter(obj => {
      if ( !obj.visible ) return false;
      let c = obj.center;
      return Number.between(c.x, x, x+width) && Number.between(c.y, y, y+height);
    });

    // Maybe release other targets
    if ( releaseOthers ) {
      for ( let t of user.targets ) {
        if ( !targets.includes(t) ) t.setTarget(false, {releaseOthers: false, groupSelection: true});
      }
    }

    // Acquire targets for tokens which are not yet targeted
    targets.forEach(t => {
      if ( !user.targets.has(t) ) t.setTarget(true, {releaseOthers: false, groupSelection: true});
    });

    // Broadcast the target change
    user.broadcastActivity({targets: user.targets.ids});

    // Return the number of targeted tokens
    return user.targets.size;
  }

  /* -------------------------------------------- */

  /**
   * Cycle the controlled token by rotating through the list of Owned Tokens that are available within the Scene
   * Tokens are currently sorted in order of their TokenID
   *
   * @param {boolean} forwards  Which direction to cycle. A truthy value cycles forward, while a false value
   *                            cycles backwards.
   * @param {boolean} reset     Restart the cycle order back at the beginning?
   * @return {Token|null}       The Token object which was cycled to, or null
   */
  cycleTokens(forwards, reset) {
    let next = null;
    if ( reset ) this._tabIndex = null;
    const order = this._getCycleOrder();

    // If we are not tab cycling, try and jump to the currently controlled or impersonated token
    if ( this._tabIndex === null ) {
      this._tabIndex = 0;

      // Determine the ideal starting point based on controlled tokens or the primary character
      let current = this.controlled.length ? order.find(t => this.controlled.includes(t)) : null;
      if ( !current && game.user.character ) {
        const actorTokens = game.user.character.getActiveTokens();
        current = actorTokens.length ? order.find(t => actorTokens.includes(t)) : null;
      }
      current = current || order[this._tabIndex] || null;

      // Either start cycling, or cancel
      if ( !current ) return null;
      next = current;
    }

    // Otherwise, cycle forwards or backwards
    else {
      if ( forwards ) this._tabIndex = this._tabIndex < (order.length - 1) ? this._tabIndex + 1 : 0;
      else this._tabIndex = this._tabIndex > 0 ? this._tabIndex - 1 : order.length - 1;
      next = order[this._tabIndex];
      if ( !next ) return null;
    }

    // Pan to the token and control it (if possible)
    canvas.animatePan({x: next.center.x, y: next.center.y, duration: 250});
    next.control();
    return next;
  }

  /* -------------------------------------------- */

  /**
   * Add or remove the set of currently controlled Tokens from the active combat encounter
   * @param {boolean} state         The desired combat state which determines if each Token is added (true) or
   *                                removed (false)
   * @param {Combat|null} combat    A Combat encounter from which to add or remove the Token
   * @param {Token|null} [token]    A specific Token which is the origin of the group toggle request
   * @return {Promise<Combatant[]>} The Combatants added or removed
   */
  async toggleCombat(state=true, combat=null, {token=null}={}) {
    // Process each controlled token, as well as the reference token
    const tokens = this.controlled.filter(t => t.inCombat !== state);
    if ( token && !token._controlled && (token.inCombat !== state) ) tokens.push(token);

    // Reference the combat encounter displayed in the Sidebar if none was provided
    combat = combat ?? game.combats.viewed;
    if ( !combat ) {
      if ( game.user.isGM ) {
        const cls = getDocumentClass("Combat")
        combat = await cls.create({scene: canvas.scene.id, active: true}, {render: !state || !tokens.length});
      } else {
        ui.notifications.warn("COMBAT.NoneActive", {localize: true});
        return [];
      }
    }

    // Add tokens to the Combat encounter
    if ( state ) {
      const createData = tokens.map(t => {return {tokenId: t.id, sceneId: t.scene.id, actorId: t.data.actorId, hidden: t.data.hidden}});
      return combat.createEmbeddedDocuments("Combatant", createData);
    }

    // Remove Tokens from combat
    if ( !game.user.isGM ) return [];
    const tokenIds = new Set(tokens.map(t => t.id));
    const combatantIds = combat.combatants.reduce((ids, c) => {
      if (tokenIds.has(c.data.tokenId)) ids.push(c.id);
      return ids;
    }, []);
    return combat.deleteEmbeddedDocuments("Combatant", combatantIds);
  }

  /* -------------------------------------------- */

  /**
   * Get the tab cycle order for tokens by sorting observable tokens based on their distance from top-left.
   * @return {Token[]}
   * @private
   */
  _getCycleOrder() {
    const observable = this.placeables.filter(token => {
      if ( game.user.isGM ) return true;
      if ( !token.actor?.testUserPermission(game.user, "OBSERVER") ) return false;
      return !token.data.hidden;
    });
    observable.sort((a, b) => Math.hypot(a.x, a.y) - Math.hypot(b.x, b.y));
    return observable;
  }

  /* -------------------------------------------- */

  /**
   * Immediately conclude the animation of any/all tokens
   */
  concludeAnimation() {
    this.placeables.filter(t => t._movement).forEach(t => {
      let ray = t._movement;
      t._movement = null;
      t.stopAnimation();
      t.position.set(ray.B.x, ray.B.y);
    });
  }

  /* -------------------------------------------- */
  /*  Event Listeners and Handlers                */
  /* -------------------------------------------- */

  /**
   * Handle dropping of Actor data onto the Scene canvas
   * @private
   */
  async _onDropActorData(event, data) {

    // Ensure the user has permission to drop the actor and create a Token
    if ( !game.user.can("TOKEN_CREATE") ) {
      return ui.notifications.warn(`You do not have permission to create new Tokens!`);
    }

    // Acquire dropped data and import the actor
    let actor = await Actor.implementation.fromDropData(data);
    if ( !actor.isOwner ) {
      return ui.notifications.warn(`You do not have permission to create a new Token for the ${actor.name} Actor.`);
    }
    if ( actor.compendium ) {
      const actorData = game.actors.fromCompendium(actor);
      actor = await Actor.implementation.create(actorData);
    }

    // Prepare the Token data
    const td = await actor.getTokenData({x: data.x, y: data.y, hidden: event.altKey});

    // Bypass snapping
    if ( event.shiftKey ) td.update({
      x: td.x - (td.width * canvas.grid.w / 2),
      y: td.y - (td.height * canvas.grid.h / 2)
    });

    // Otherwise snap to nearest vertex, adjusting for large tokens
    else {
      const hw = canvas.grid.w/2;
      const hh = canvas.grid.h/2;
      td.update(canvas.grid.getSnappedPosition(td.x - (td.width*hw), td.y - (td.height*hh)));
    }

    // Validate the final position
    if ( !canvas.dimensions.rect.contains(td.x, td.y) ) return false;

    // Submit the Token creation request and activate the Tokens layer (if not already active)
    this.activate();
    const cls = getDocumentClass("Token");
    return cls.create(td, {parent: canvas.scene});
  }

  /* -------------------------------------------- */

  /** @inheritDoc */
  _onClickLeft(event) {
    let tool = game.activeTool;

    // If Control is being held, we always want the Tool to be Ruler
    if ( game.keyboard.isModifierActive(KeyboardManager.MODIFIER_KEYS.CONTROL) ) tool = "ruler";
    switch ( tool ) {
      // Clear targets if Left Click Release is set
      case "target":
        if ( game.settings.get("core", "leftClickRelease") ) game.user.updateTokenTargets([]);
        break;

      // Place Ruler waypoints
      case "ruler":
        return canvas.controls.ruler._onClickLeft(event);
        break;
    }

    // If we don't explicitly return from handling the tool, use the default behavior
    super._onClickLeft(event);
  }
}
