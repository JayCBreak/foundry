/**
 * Construct a hexagonal grid
 * @extends {BaseGrid}
 */
class HexagonalGrid extends BaseGrid {
  constructor(options) {
    super(options);
    this.columns = !!options.columns;
    this.even = !!options.even;

    // Grid width and height
    let s = options.dimensions.size;
    if ( this.columns ) {
      this.w = s;
      this.h = Math.sqrt(3) * 0.5 * s;
    } else {
      this.w = Math.sqrt(3) * 0.5 * s;
      this.h = s;
    }
  }

  /**
   * A matrix of x and y offsets which is multiplied by the width/height vector to get pointy-top polygon coordinates
   * @type {Array<number[]>}
   */
  static get pointyHexPoints() {
    return [[0, 0.25], [0.5, 0], [1, 0.25], [1, 0.75], [0.5, 1], [0, 0.75]];
  }

  /* -------------------------------------------- */

  /**
   * A matrix of x and y offsets which is multiplied by the width/height vector to get flat-top polygon coordinates
   * @type {Array<number[]>}
   */
  static get flatHexPoints() {
    return [[0, 0.5], [0.25, 0], [0.75, 0], [1, 0.5], [0.75, 1], [0.25, 1]];
  }

  /* -------------------------------------------- */

  /**
   * An array of the points which define a hexagon for this grid shape
   * @return {PointArray[]}
   */
  get hexPoints() {
    return this.options.columns ? this.constructor.flatHexPoints : this.constructor.pointyHexPoints;
  }

  /* -------------------------------------------- */
  /*  Grid Rendering
  /* -------------------------------------------- */

  /** @inheritdoc */
  draw({gridColor=null, gridAlpha=null}={}) {
    super.draw({gridColor, gridAlpha});
    const alpha = gridAlpha ?? this.options.alpha;
    if ( alpha === 0 ) return this;

    // Set dimensions
    let d = this.options.dimensions;
    this.width = d.width;
    this.height = d.height;

    // Draw grid polygons
    this.addChild(this._drawGrid({gridColor, gridAlpha}));
    return this;
  }

  /* -------------------------------------------- */

  /**
   * A convenience method for getting all the polygon points relative to a top-left [x,y] coordinate pair
   * @param {number} x    The top-left x-coordinate
   * @param {number} y    The top-right y-coordinate
   * @param {number} [w]  An optional polygon width
   * @param {number} [h]  An optional polygon height
   */
  getPolygon(x, y, w, h) {
    w = w ?? this.w;
    h = h ?? this.h;
    const points = this.hexPoints.concat([this.hexPoints[0]]); // Add the first point back
    return points.reduce((arr, p) => {
      return arr.concat([x + (w * p[0]), y + (h * p[1])]);
    }, []);
  }

  /* -------------------------------------------- */

  /**
   * Draw the grid lines.
   * @param {object} [preview]  Override settings used in place of those saved to the scene data.
   * @param {string|null} [preview.gridColor=null]  The grid color.
   * @param {number|null} [preview.gridAlpha=null]  The grid transparency.
   * @returns {Graphics}
   * @private
   */
  _drawGrid({gridColor=null, gridAlpha=null}={}) {
    const columns = this.options.columns;
    const color = gridColor ?? this.options.color;
    const alpha = gridAlpha ?? this.options.alpha;
    const ncols = Math.ceil(canvas.dimensions.width / this.w);
    const nrows = Math.ceil(canvas.dimensions.height / this.h);

    // Draw Grid graphic
    const grid = new PIXI.Graphics();
    grid.lineStyle(1, color, alpha);

    // Draw hex rows
    if ( columns ) this._drawColumns(grid, nrows, ncols);
    else this._drawRows(grid, nrows, ncols);
    return grid;
  }

  /* -------------------------------------------- */

  _drawRows(grid, nrows, ncols) {
    let shift = this.even ? 0 : 1;
    nrows /= 0.75;
    for ( let r=0; r<nrows; r++ ) {
      let sx = (r % 2) === shift ? 0 : -0.5;
      let y0 = r * this.h * 0.75;
      for ( let c=0; c<ncols; c++ ) {
        let x0 = (c+sx) * this.w;
        grid.drawPolygon(this.getPolygon(x0, y0));
      }
    }
  }

  /* -------------------------------------------- */

  _drawColumns(grid, nrows, ncols) {
    let shift = this.even ? 0 : 1;
    ncols /= 0.75;
    for ( let c=0; c<ncols; c++ ) {
      let sy = (c % 2) === shift ? 0 : -0.5;
      let x0 = c * this.w * 0.75;
      for ( let r=0; r<nrows; r++ ) {
        let y0 = (r+sy) * this.h;
        grid.drawPolygon(this.getPolygon(x0, y0));
      }
    }
  }

  /* -------------------------------------------- */
  /*  Grid Measurement Methods
  /* -------------------------------------------- */

  /** @override */
  getGridPositionFromPixels(x, y) {
    return this._getGridPositionFromPixels(x, y, "floor");
  }

  /* -------------------------------------------- */

  /**
   * Get the position in grid space from a pixel coordinate.
   * @param {number} x        The origin x-coordinate
   * @param {number} y        The origin y-coordinate
   * @param {string} method   The rounding method applied
   * @returns {number[]}      The row, column combination
   * @private
   */
  _getGridPositionFromPixels(x, y, method="floor") {
    let row, col;
    const fn = Math[method];
    if ( this.options.columns ) {
      col = fn(x / (this.w*0.75));
      const isEven = (col+1) % 2 === 0;
      row = fn((y / this.h) + (this.options.even === isEven ? 0.5 : 0));
    } else {
      row = fn(y / (this.h*0.75));
      const isEven = (row+1) % 2 === 0;
      col = fn((x / this.w) + (this.options.even === isEven ? 0.5 : 0))
    }
    return [row, col];
  }

  /* -------------------------------------------- */

  /** @override */
  getPixelsFromGridPosition(row, col) {
    let x, y;

    // Flat-topped hexes
    if (this.options.columns) {
      x = Math.ceil(col * (this.w * 0.75));
      const isEven = (col + 1) % 2 === 0;
      y = Math.ceil((row - (this.options.even === isEven ? 0.5 : 0)) * this.h);
    }

    // Pointy-topped hexes
    else {
      y = Math.ceil(row * (this.h * 0.75));
      const isEven = (row + 1) % 2 === 0;
      x = Math.ceil((col - (this.options.even === isEven ? 0.5 : 0)) * this.w);
    }
    return [x, y];
  }

  /* -------------------------------------------- */

  /** @override */
  getCenter(x, y) {
    let [x0, y0] = this.getTopLeft(x, y);
    return [x0 + (this.w/2), y0 + (this.h/2)];
  }

  /* -------------------------------------------- */

  /** @override */
  getSnappedPosition(x, y, interval=1) {

    // At precision 5, return the center or nearest vertex
    if ( interval === 5) {
      const w4 = this.w / 4;
      const h4 = this.h / 4;

      // Distance relative to center
      let [xc, yc] = this.getCenter(x, y);
      let dx = x - xc;
      let dy = y - yc;
      let ox = dx.between(-w4, w4) ? 0 : Math.sign(dx);
      let oy = dy.between(-h4, h4) ? 0 : Math.sign(dy);

      // Closest to the center
      if ( (ox === 0) && (oy === 0) ) return {x: xc, y: yc};

      // Closest vertex based on offset
      if ( this.options.columns && (ox === 0) ) ox = Math.sign(dx) ?? -1;
      if ( !this.options.columns && (oy === 0) ) oy = Math.sign(dy) ?? -1;
      return this._getClosestVertex(xc, yc, ox, oy);
    }

    // Start with the closest top-left grid position
    let [r0, c0] = this._getGridPositionFromPixels(x, y, "round");
    let [x0, y0] = this.getPixelsFromGridPosition(r0, c0);
    if ( interval === 1 ) return {x: x0, y: y0};

    // Round the remainder
    const dx = (x - x0).toNearest(this.w / interval);
    const dy = (y - y0).toNearest(this.h / interval);
    return {x: x0 + dx, y: y0 + dy};
  }

  /* -------------------------------------------- */

  _getClosestVertex(xc, yc, ox, oy) {
    const b = ox + (oy << 2); // Bit shift to make a unique reference
    const vertices = this.options.columns ?
      {"-1": 0, "-5": 1, "-3": 2, "1": 3, "5": 4, "3": 5} : // flat hex vertices
      {"-5": 0, "-4": 1, "-3": 2, "5": 3, "4": 4, "3": 5};  // pointy hex vertices
    const idx = vertices[b];
    const pt = this.hexPoints[idx];
    return {
      x: (xc - (this.w/2)) + (pt[0]*this.w),
      y: (yc - (this.h/2)) + (pt[1]*this.h)
    }
  }

  /* -------------------------------------------- */

  /** @override */
  shiftPosition(x, y, dx, dy) {
    let [row, col] = canvas.grid.grid.getGridPositionFromPixels(x, y);

    // Adjust diagonal moves for offset
    let isDiagonal = (dx !== 0) && (dy !== 0);
    if ( isDiagonal ) {

      // Column orientation
      if ( this.options.columns ) {
        let isEven = ((col+1) % 2 === 0) === this.options.even;
        if ( isEven && (dy > 0)) dy--;
        else if ( !isEven && (dy < 0)) dy++;
      }

      // Row orientation
      else {
        let isEven = ((row + 1) % 2 === 0) === this.options.even;
        if ( isEven && (dx > 0) ) dx--;
        else if ( !isEven && (dx < 0 ) ) dx++;
      }
    }
    return canvas.grid.grid.getPixelsFromGridPosition(row+dy, col+dx);
  }

  /* -------------------------------------------- */
  /*  Grid Highlighting
  /* -------------------------------------------- */

  /** @override */
  highlightGridPosition(layer, options={}) {
    const {x, y} = options;
    if ( !layer.highlight(x, y) ) return;
    options.shape = new PIXI.Polygon(this.getPolygon(x, y));
    return super.highlightGridPosition(layer, options);
  }

  /* -------------------------------------------- */

  /** @override */
  getNeighbors(row, col) {
    let offsets;

    // Column orientation
    if ( this.options.columns ) {
      let shift = ((col+1) % 2 === 0) === this.options.even;
      if ( shift ) offsets = [[-1, -1], [-1, 0], [-1, 1], [0, 1], [1, 0], [0, -1]];
      else offsets = [[0, -1], [-1, 0], [0, 1], [1, 1], [1, 0], [1, -1]];
    }

    // Row orientation
    else {
      let shift = ((row+1) % 2 === 0) === this.options.even;
      if ( shift ) offsets = [[0, -1], [-1, -1], [-1, 0], [0, 1], [1, 0], [1, -1]];
      else offsets = [[0, -1], [-1, 0], [-1, 1], [0, 1], [1, 1], [1, 0]];
    }
    return offsets.map(o => [row+o[0], col+o[1]]);
  }

  /* -------------------------------------------- */

  /** @override */
  measureDistances(segments, options={}) {
    if ( !options.gridSpaces ) return super.measureDistances(segments, options);
    return segments.map(s => {
      let r = s.ray;
      let [r0, c0] = this.getGridPositionFromPixels(r.A.x, r.A.y);
      let [r1, c1] = this.getGridPositionFromPixels(r.B.x, r.B.y);

      // Use cube conversion to measure distance
      let hex0 = this.offsetToCube(r0, c0);
      let hex1 = this.offsetToCube(r1, c1);
      let distance = HexagonalGrid.cubeDistance(hex0, hex1);
      return distance * canvas.dimensions.distance;
    });
  }

  /* -------------------------------------------- */
  /*  Helper Functions
  /* -------------------------------------------- */

  /**
   * Convert an offset coordinate (row, col) into a cube coordinate (q, r, s).
   * See https://www.redblobgames.com/grids/hexagons/ for reference
   * Source code available https://www.redblobgames.com/grids/hexagons/codegen/output/lib-functions.js
   * @param {number} row      The row number
   * @param {number} col      The column number
   * @return {{q: number, r: number, s: number}}
   */
  offsetToCube(row, col) {
    const offset = this.options.even ? 1 : -1;

    // Column orientation
    if ( this.options.columns ) {
      const q = col;
      const r = row - (col + (offset * (col & 1))) / 2;
      return {q, r, s: 0 - q - r};
    }

    // Row orientation
    else {
      const q = col - (row + (offset * (row & 1))) / 2;
      const r = row;
      return {q, r, s: 0 - q - r};
    }
  }

  /* -------------------------------------------- */

  /**
   * Convert a cube coordinate (q, r, s) into an offset coordinate (row, col).
   * See https://www.redblobgames.com/grids/hexagons/ for reference
   * Source code available https://www.redblobgames.com/grids/hexagons/codegen/output/lib-functions.js
   * @param {number} q      Cube coordinate 1
   * @param {number} r      Cube coordinate 2
   * @param {number} s      Cube coordinate 3
   * @return {{row: number, col: number}}
   */
  cubeToOffset(q, r, s) {
    const offset = this.options.even ? 1 : -1;

    // Column orientation
    if ( this.options.columns ) {
      const col = q;
      const row = r + (q + (offset * (q & 1))) / 2;
      return {row, col};
    }

    // Row orientation
    else {
      const row = r;
      const col = q + (r + (offset * (r & 1))) / 2;
      return {row, col}
    }
  }

  /* -------------------------------------------- */

  /**
   * Given a cursor position (x, y), obtain the cube coordinate hex (q, r, s) of the hex which contains it
   * http://justinpombrio.net/programming/2020/04/28/pixel-to-hex.html
   * @param {number} x    The x-coordinate in pixels
   * @param {number} y    The y-coordinate in pixels
   * @private
   */
  static pixelToCube(x, y) {
    const size = canvas.dimensions.size / 2;
    const cx = x / size;
    const cy = y / size;

    // Fractional hex coordinates, might not satisfy (fx + fy + fz = 0) due to rounding
    const fr = (2/3) * cx;
    const fq = ((-1/3) * cx) + ((1 / Math.sqrt(3)) * cy);
    const fs = ((-1/3) * cx) - ((1 / Math.sqrt(3)) * cy);

    // Convert to integer triangle coordinates
    const a = Math.ceil(fr - fq);
    const b = Math.ceil(fq - fs);
    const c = Math.ceil(fs - fr);

    // Convert back to cube coordinates
    return [
      Math.round((a - c) / 3),
      Math.round((c - b) / 3),
      Math.round((b - a) / 3)
    ];
  }

  /* -------------------------------------------- */

  /**
   * Measure the distance in hexagons between two cube coordinates
   * @private
   */
  static cubeDistance(a, b) {
    let diff = {q: a.q - b.q, r: a.r - b.r, s: a.s - b.s};
    return (Math.abs(diff.q) + Math.abs(diff.r) + Math.abs(diff.s)) / 2;
  }
}
