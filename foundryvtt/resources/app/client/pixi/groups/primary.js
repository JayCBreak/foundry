/**
 * A cached container group which renders the primary visible contents of a Scene.
 * @extends {CachedContainer}
 */
class PrimaryCanvasGroup extends CachedContainer {
  constructor() {
    super();

    // Create group layers
    this._createLayers();

    // Configure group attributes
    this.sortableChildren = true;
  }

  /**
   * The name of this canvas group
   * @type {string}
   */
  static groupName = "primary"

  /** @override */
  clearColor = [0, 0, 0, 0];

  /* -------------------------------------------- */

  /**
   * Create the member layers of the scene container
   * @private
   */
  _createLayers() {
    for ( let [name, config] of Object.entries(CONFIG.Canvas.layers) ) {
      if ( config.group !== this.constructor.groupName ) continue;
      const layer = new config.layerClass();
      Object.defineProperty(this, name, { value: layer, writable: false });
      this.addChild(layer);
    }
  }

  /* -------------------------------------------- */

  /** @override */
  render(renderer) {
    super.render(renderer);
    this.sprite._render(renderer); // Draw to the screen
  }
}
