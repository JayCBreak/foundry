/**
 * @typedef {Object} LightingTechnique
 * @property {number} id        The numeric identifier of the technique
 * @property {string} label     The localization string that labels the technique
 * @property {string} shader    The shader fragment when the technique is used
 */

/**
 * This class defines an interface which all adaptive lighting shaders extend.
 * @extends {AbstractBaseShader}
 * @interface
 */
class AdaptiveLightingShader extends AbstractBaseShader {

  /** @inheritdoc */
  static vertexShader = `
  precision mediump float;
  attribute vec2 aVertexPosition;
  attribute vec2 aUvs;
  uniform mat3 translationMatrix;
  uniform mat3 projectionMatrix;
  uniform vec2 screenDimensions;
  varying vec2 vUvs;
  varying vec2 vSamplerUvs;

  void main() {
    vec3 tPos = translationMatrix * vec3(aVertexPosition, 1.0);
    vUvs = aUvs;
    vSamplerUvs = tPos.xy / screenDimensions;
    gl_Position = vec4((projectionMatrix * tPos).xy, 0.0, 1.0);
  }`;

  /* -------------------------------------------- */
  /*  GLSL Helper Functions                       */
  /* -------------------------------------------- */

  /**
   * Useful constant values computed at compile time
   * @type {string}
   */
  static CONSTANTS = `
  const float PI = 3.14159265359;
  const float TWOPI = 2.0 * PI;
  const float INVTWOPI = 1.0 / TWOPI;
  const float INVTHREE = 1.0 / 3.0;
  const vec2 PIVOT = vec2(0.5);
  const vec3 BT709 = vec3(0.2126, 0.7152, 0.0722);
  const vec4 ALLONES = vec4(1.0);
  `;

  /**
   * The coloration technique shader fragment
   * @type {string}
   */
  static get ADAPTIVE_COLORATION() {
    let shader = `vec4 baseColor = texture2D(uBkgSampler, vSamplerUvs);\n`;
    for (let technique of Object.values(this.COLORATION_TECHNIQUES)) {
      let cond = `if (technique == ${technique.id})`;
      if (technique.id > 0) cond = "else " + cond;
      shader += `${cond} {${technique.shader}\n}\n`;
    }
    return shader;
  }

  /* -------------------------------------------- */

  /**
   * Fade easing to use with distance in interval [0,1]
   * @type {string}
   */
  static FADE(amp = 3, coef = 0.80) {
    return `float fade(in float dist) {
      float ampdist = dist;
      for (int i = 1; i < ${amp}; i++) {
        ampdist *= ampdist;
      }
      return 1.0 - (${coef.toFixed(2)} * ampdist * (${(amp + 1).toFixed(1)} - ${amp.toFixed(1)} * dist));
    }`;
  }

  /* -------------------------------------------- */

  /**
   * Fractional Brownian Motion for a given number of octaves
   * @type {string}
   */
  static FBM(octaves = 4, amp = 1.0) {
    return `float fbm(in vec2 uv) {
      float total = 0.0, amp = ${amp.toFixed(1)};
      for (int i = 0; i < ${octaves}; i++) {
        total += noise(uv) * amp;
        uv += uv;
        amp *= 0.5;
      }
      return total;
    }`;
  }

  /* -------------------------------------------- */

  /**
   * A conventional pseudo-random number generator with the "golden" numbers, based on uv position
   * @type {string}
   */
  static PRNG = `
  float random(in vec2 uv) { 
	  return fract(cos(dot(uv, vec2(12.9898, 4.1414))) * 43758.5453);
  }`;

  /* -------------------------------------------- */

  /**
   * A Vec3 pseudo-random generator, based on uv position
   * @type {string}
   */
  static PRNG3D = `
  vec3 random(in vec3 uv) {
    return vec3(fract(cos(dot(uv, vec3(12.9898,  234.1418,    152.01))) * 43758.5453),
                fract(sin(dot(uv, vec3(80.9898,  545.8937, 151515.12))) * 23411.1789),
                fract(cos(dot(uv, vec3(01.9898, 1568.5439,    154.78))) * 31256.8817));
  }`;

  /* -------------------------------------------- */

  /**
   * A conventional noise generator
   * @type {string}
   */
  static NOISE = `
  float noise(in vec2 uv) {
    const vec2 d = vec2(0.0, 1.0);
    vec2 b = floor(uv);
    vec2 f = smoothstep(vec2(0.), vec2(1.0), fract(uv));
    return mix(
      mix(random(b), random(b + d.yx), f.x), 
      mix(random(b + d.xy), random(b + d.yy), f.x), 
      f.y
    );
  }`;

  /* -------------------------------------------- */

  /**
   * Convert a Hue-Saturation-Brightness color to RGB - useful to convert polar coordinates to RGB
   * @type {string}
   */
  static HSB2RGB = `
  vec3 hsb2rgb(in vec3 c) {
    vec3 rgb = clamp(abs(mod(c.x*6.0+vec3(0.0,4.0,2.0), 6.0)-3.0)-1.0, 0.0, 1.0 );
    rgb = rgb*rgb*(3.0-2.0*rgb);
    return c.z * mix(vec3(1.0), rgb, c.y);
  }`;

  /* -------------------------------------------- */

  /**
   * Fast approximate perceived brightness computation
   * Using Digital ITU BT.709 : Exact luminance factors
   * @type {string}
   */
  static PERCEIVED_BRIGHTNESS = `
  float perceivedBrightness(in vec3 color) {
    return dot(color, BT709);
  }

  float perceivedBrightness(in vec4 color) {
    return mix(perceivedBrightness(color.rgb), 0.5, 1.0 - color.a);
  }

  float reversePerceivedBrightness(in vec3 color) {
    return 1.0 - dot(color, BT709);
  }

  float reversePerceivedBrightness(in vec4 color) {
    return 1.0 - mix(perceivedBrightness(color.rgb), 0.5, 1.0 - color.a);
  }
  `;

  /* -------------------------------------------- */

  /**
   * Switch between an inner and outer color, by comparing distance from center to ratio
   * Apply a strong gradient between the two areas if gradual uniform is set to true
   * @type {string}
   */
  static SWITCH_COLOR = `
  vec3 switchColor( in vec3 innerColor, in vec3 outerColor, in float dist ) {
    return mix(innerColor, outerColor, (gradual ? smoothstep(ratio * 0.80, clamp(ratio * 1.20, 0.0, 1.0), dist)
                                                : smoothstep(ratio * 0.95, clamp(ratio * 1.05, 0.0, 1.0), dist)));
  }`;

  /* -------------------------------------------- */

  /**
   * Transition between bright and dim colors, if requested
   * @type {string}
   */
  static TRANSITION = `
  finalColor = switchColor(colorBright, colorDim, dist) * alpha;
  `;

  /**
   * Constrain light to LOS
   * @type {string}
   */
  static CONSTRAIN_TO_LOS = `
  float a;
  // blue channel used for alpha factor
  if ( useFov ) a = texture2D(fovTexture, vUvs).b;
  else a = 1.0;

  if (darkness) gl_FragColor = vec4( mix( vec3(1.0), mix(vec3(1.0), finalColor, smoothstep(0.0, 0.001, 1.0 - dist)), a), 1.0);
  else gl_FragColor = vec4(finalColor, 1.0) * a * smoothstep(0.0, 0.025, 1.0 - dist);
  `;

  /**
   * Incorporate falloff if a gradual uniform is requested
   * @type {string}
   */
  static FALLOFF = `
  if (gradual && !darkness) finalColor *= fade(dist * dist);
  ${this.CONSTRAIN_TO_LOS}
  `;

  /**
   * Compute distance from the light center
   * @type {string}
   */
  static DISTANCE = `
  float dist = clamp(distance(vUvs, vec2(0.5)) * 2.0, 0.0, 1.0);
  `;

  /* -------------------------------------------- */
  /*  Coloration Techniques                       */
  /* -------------------------------------------- */

  /**
   * A mapping of available coloration techniques
   * @type {Object<string, LightingTechnique>}
   */
  static COLORATION_TECHNIQUES = {
    LEGACY: {
      id: 0,
      label: "LIGHT.LegacyColoration",
      shader: ""
    },
    LUMINANCE: {
      id: 1,
      label: "LIGHT.AdaptiveLuminance",
      shader: `
      float reflection = perceivedBrightness(baseColor);
      finalColor *= reflection;`
    },
    INTERNAL_HALO: {
      id: 2,
      label: "LIGHT.InternalHalo",
      shader: `
      float reflection = perceivedBrightness(baseColor);
      finalColor = switchColor(finalColor, finalColor * reflection, dist);`
    },
    EXTERNAL_HALO: {
      id: 3,
      label: "LIGHT.ExternalHalo",
      shader: `
      float reflection = perceivedBrightness(baseColor);
      finalColor = switchColor(finalColor * reflection, finalColor, dist);`
    },
    COLOR_BURN: {
      id: 4,
      label: "LIGHT.ColorBurn",
      shader: `
      float reflection = perceivedBrightness(baseColor);
      finalColor = (finalColor * (1.0 - sqrt(reflection))) / clamp(baseColor.rgb * 2.0, 0.001, 0.25);`
    },
    INTERNAL_BURN: {
      id: 5,
      label: "LIGHT.InternalBurn",
      shader: `
      float reflection = perceivedBrightness(baseColor);
      finalColor = switchColor((finalColor * (1.0 - sqrt(reflection))) / clamp(baseColor.rgb * 2.0, 0.001, 0.25), finalColor * reflection, dist);`
    },
    EXTERNAL_BURN: {
      id: 6,
      label: "LIGHT.ExternalBurn",
      shader: `
      float reflection = perceivedBrightness(baseColor);
      finalColor = switchColor(finalColor * reflection, (finalColor * (1.0 - sqrt(reflection))) / clamp(baseColor.rgb * 2.0, 0.001, 0.25), dist);`
    },
    LOW_ABSORPTION: {
      id: 7,
      label: "LIGHT.LowAbsorption",
      shader: `
      float reflection = perceivedBrightness(baseColor);
      reflection *= smoothstep(0.35, 0.75, reflection);
      finalColor *= reflection;`
    },
    HIGH_ABSORPTION: {
      id: 8,
      label: "LIGHT.HighAbsorption",
      shader: `
      float reflection = perceivedBrightness(baseColor);
      reflection *= smoothstep(0.55, 0.85, reflection);
      finalColor *= reflection;`
    },
    INVERT_ABSORPTION: {
      id: 9,
      label: "LIGHT.InvertAbsorption",
      shader: `
      float r = reversePerceivedBrightness(baseColor);
      finalColor *= (r * r * r * r * r);`
    }
  }
}

/* -------------------------------------------- */

/**
 * The default coloration shader used by standard rendering and animations
 * A fragment shader which creates a solid light source.
 * @implements {AdaptiveLightingShader}
 */
class AdaptiveBackgroundShader extends AdaptiveLightingShader {

  /**
   * Constrain light to LOS
   * @type {string}
   */
  static CONSTRAIN_TO_LOS = `
  float a;

  if ( useFov ) {
    // blue channel used for alpha factor
    a = texture2D(fovTexture, vUvs).b;
    a = (darkness ? clamp(a * 3.0, 0.0, 1.0) : a);
  } else a = 1.0;

  gl_FragColor = vec4(finalColor, 1.0) * a * smoothstep(0.0, 0.025, 1.0 - dist);
  `;

  /**
   * Color adjustments : exposure, contrast and shadows
   * @type {string}
   */
  static ADJUSTMENTS = `
  vec4 baseColor = texture2D(uBkgSampler, vSamplerUvs);
  vec3 changedColor = baseColor.rgb;

  // Computing contrasted color
  if (contrast != 0.0) {
    changedColor = (changedColor - 0.5) * (contrast + 1.0) + 0.5;
  }

  // Computing saturated color
  if (saturation != 0.0) {
    vec3 grey = vec3(perceivedBrightness(changedColor));
    changedColor = mix(grey, changedColor, 1.0 + saturation);
  }

  // Computing exposed color for background
  if (exposure > 0.0 && !darkness) {
    float halfExposure = exposure * 0.5;
    float finalExposure = halfExposure *
                          smoothstep(ratio * (gradual ? 1.06 : 1.02), ratio * (gradual ? 0.94 : 0.98), dist) +
                          halfExposure;
    changedColor *= (1.0 + finalExposure);
  }

  // Computing baseColor luminance factor
  if (shadows != 0.0) {
    float shadowing = mix(1.0, smoothstep(0.50, 0.80, perceivedBrightness(changedColor)), shadows);
  
    // Applying shadow factor
    finalColor = changedColor * shadowing;
  } else finalColor = changedColor;
  `;

  /**
   * Incorporate falloff if a gradual uniform is requested
   * @type {string}
   */
  static FALLOFF = `
  float a;
  vec4 finalColor4c = mix(vec4(0.0), vec4(finalColor, baseColor.a), 1.0 - smoothstep(0.75, 1.0, dist * dist * dist));
  
  // blue channel used for alpha
  if ( useFov ) a = texture2D(fovTexture, vUvs).b;
  else a = 1.0;

  gl_FragColor = finalColor4c * a;
  `;

  /**
   * Memory allocations for the Adaptive Background Shader
   * @type {string}
   */
  static SHADER_HEADER = `
  uniform bool darkness;
  uniform bool gradual;
  uniform bool useFov;
  uniform float contrast;
  uniform float shadows;
  uniform float exposure;
  uniform float saturation;
  uniform float alpha;
  uniform float ratio;
  uniform float time;
  uniform float reverseInverseGamma;
  uniform vec2 screenDimensions;
  uniform sampler2D fovTexture;
  uniform sampler2D uBkgSampler;
  varying vec2 vUvs;
  varying vec2 vSamplerUvs;
  ${this.CONSTANTS}
  ${this.SWITCH_COLOR}
  `;

  /** @inheritdoc */
  static fragmentShader = `
  ${this.SHADER_HEADER}
  ${this.FADE(3, 1.0)}
  ${this.PERCEIVED_BRIGHTNESS}

  void main() {
    ${this.DISTANCE}
    vec3 finalColor;
    ${this.ADJUSTMENTS}
    ${this.FALLOFF}
  }`;

  /** @inheritdoc */
  static defaultUniforms = {
    shadows: 0.0,
    contrast: 0.0,
    exposure: 0.0,  // [-1, 1]
    saturation: 0.0,
    alpha: 1.0,
    ratio: 0.5,
    time: 0,
    screenDimensions: [1, 1],
    uBkgSampler: 0,
    fovTexture: 0,
    darkness: false,
    gradual: false,
    useFov: true
  }

  /**
   * Flag whether the background shader is currently required.
   * If key uniforms are at their default values, we don't need to render the background container.
   * @type {boolean}
   */
  get isRequired() {
    const keys = ["contrast", "saturation", "shadows", "exposure"]
    return keys.some(k => this.uniforms[k] !== this._defaults[k]);
  }
}

/* -------------------------------------------- */

/**
 * The default coloration shader used by standard rendering and animations
 * A fragment shader which creates a solid light source.
 * @implements {AdaptiveLightingShader}
 */
class AdaptiveIlluminationShader extends AdaptiveLightingShader {

  /**
   * Constrain light to LOS
   * @type {string}
   */
  static CONSTRAIN_TO_LOS = `
  float a;
  if ( useFov ) {
    // blue channel used for alpha factor
    a = texture2D(fovTexture, vUvs).b;
    a = (darkness ? clamp(a * 3.0, 0.0, 1.0) : a);
  } else a = 1.0;

  if (darkness) gl_FragColor = vec4( mix( mix(colorBackground, vec3(1.0), 1.0 - a), finalColor, a), 1.0);
  else gl_FragColor = vec4(finalColor * a * smoothstep(0.0, 0.025, 1.0 - dist), 1.0);
  `;

  /**
   * Incorporate falloff if a gradual uniform is requested
   * @type {string}
   */
  static FALLOFF = `
  if (gradual) {
    if (darkness) {
      float f = smoothstep(0.7, 1.0, dist);
      finalColor = mix(finalColor, mix(colorDim, colorBackground, dist), f * f * f * f * f);
    } else finalColor = mix(colorBackground, finalColor, fade(dist * dist));
  }
  ${this.CONSTRAIN_TO_LOS}
  `;

  /**
   * Color adjustments : exposure, contrast and shadows
   * @type {string}
   */
  static ADJUSTMENTS = `
  vec4 baseColor = texture2D(uBkgSampler, vSamplerUvs);

  // Computing exposure with illumination
  if (exposure > 0.0 && !darkness) {
    // Diminishing exposure for illumination by a factor 2 (to reduce the "inflating radius" visual problem)
    float quartExposure = exposure * 0.25; 
    float finalExposure = quartExposure *
                          smoothstep(ratio * (gradual ? 1.06 : 1.02), ratio * (gradual ? 0.94 : 0.98), dist) +
                          quartExposure;
    finalColor *= (1.0 + finalExposure);
  }
  else if (exposure != 0.0) finalColor *= (1.0 + exposure);
  
  if (saturation != 0.0) {
    // Computing saturated color
    vec3 grey = vec3(perceivedBrightness(finalColor));

    // Creating saturated/desaturated color
    vec3 saturatedColor = mix(grey, finalColor, 1.0 + saturation);
    finalColor = saturatedColor;
  }

  // Computing illuminance shadow factor
  float shadowing = mix(1.0, smoothstep(0.50, 0.80, perceivedBrightness(baseColor.rgb)), shadows);
  
  // Applying shadow factor
  finalColor *= shadowing;
  `;

  /**
   * Memory allocations for the Adaptive Illumination Shader
   * @type {string}
   */
  static SHADER_HEADER = `
  uniform bool gradual;
  uniform bool darkness;
  uniform bool useFov;
  uniform float contrast;
  uniform float shadows;
  uniform float exposure;
  uniform float saturation;
  uniform float alpha;
  uniform float ratio;
  uniform float time;
  uniform float intensity;
  uniform vec2 screenDimensions;
  uniform vec3 colorBackground;
  uniform vec3 colorDim;
  uniform vec3 colorBright;
  uniform vec3 color;
  uniform sampler2D uBkgSampler;
  uniform sampler2D fovTexture;
  varying vec2 vUvs;
  varying vec2 vSamplerUvs;
  ${this.CONSTANTS}
  ${this.SWITCH_COLOR}
  `;

  /** @inheritdoc */
  static fragmentShader = `
  ${this.SHADER_HEADER}
  ${this.FADE(3, 1.0)}
  ${this.PERCEIVED_BRIGHTNESS}

  void main() {
    ${this.DISTANCE}
    vec3 finalColor;
    ${this.TRANSITION}
    ${this.ADJUSTMENTS}
    ${this.FALLOFF}
  }`;

  /** @inheritdoc */
  static defaultUniforms = {
    alpha: 1.0,
    ratio: 0.5,
    color: [0.9333333333333333, 0.9333333333333333, 0.9333333333333333],
    colorDim: [0.5, 0.5, 0.5],
    colorBright: [1.0, 1.0, 1.0],
    colorBackground: [1.0, 1.0, 1.0],
    darkness: false,
    exposure: 0.0,
    fovTexture: 0,
    gradual: false,
    intensity: 5,
    saturation: 0.0,
    screenDimensions: [1, 1],
    shadows: 0.0,
    time: 0,
    uBkgSampler: 0,
    useFov: true
  }

  /* -------------------------------------------- */

  /**
   * Determine the correct illumination penalty to apply for a given darkness level and luminosity
   * @param {number} darknessLevel      The current darkness level on [0,1]
   * @param {number} luminosity         The light source luminosity on [-1,1]
   * @returns {number}                  The amount of penalty to apply on [0,1]
   */
  getDarknessPenalty(darknessLevel, luminosity) {
    const l = Math.max(luminosity, 0);  // [0,1]
    return (darknessLevel/4) * (1-l); // [0, 0.25]
  }
}


/* -------------------------------------------- */


/**
 * The default coloration shader used by standard rendering and animations.
 * A fragment shader which creates a light source.
 * @implements {AdaptiveLightingShader}
 */
class AdaptiveColorationShader extends AdaptiveLightingShader {

  /**
   * Incorporate falloff if a falloff uniform is requested
   * @type {string}
   */
  static FALLOFF = `
  if (gradual && !darkness) finalColor *= fade(dist * dist);

  float a;
  if ( useFov ) {
    // blue channel used for alpha factor
    a = texture2D(fovTexture, vUvs).b;
    a = (darkness ? clamp(a * 3.0, 0.0, 1.0) : a);
  } else a = 1.0;

  if (darkness) gl_FragColor = vec4(mix(vec3(0.0), finalColor, a), a);
  else gl_FragColor = vec4(finalColor, 1.0) * a * smoothstep(0.0, 0.025, 1.0 - dist);
  `;

  /**
   * Color adjustments : exposure, contrast and shadows
   * @type {string}
   */
  static ADJUSTMENTS = `
  if (saturation != 0.0) {
    // Computing saturated color
    vec3 grey = vec3(perceivedBrightness(finalColor));

    // Creating saturated/desaturated color
    vec3 saturatedColor = mix(grey, finalColor, 1.0 + saturation);
    finalColor = saturatedColor;
  }

  // Computing shadow factor
  float reflection = perceivedBrightness(baseColor.rgb);
  float shadowing = mix(1.0, smoothstep(0.25, 0.35, reflection), shadows);
  
  // Applying shadow factor
  finalColor *= shadowing;
  `;
  /**
   * Memory allocations for the Adaptive Coloration Shader
   * @type {string}
   */
  static SHADER_HEADER = `
  uniform bool darkness;
  uniform bool gradual;
  uniform bool useFov;
  uniform int technique;
  uniform float ratio;
  uniform float shadows;
  uniform float saturation;
  uniform float time;
  uniform float intensity;
  uniform float alpha;
  uniform vec2 screenDimensions;
  uniform vec3 color;
  uniform sampler2D uBkgSampler;
  uniform sampler2D fovTexture;
  varying vec2 vUvs;
  varying vec2 vSamplerUvs;
  ${this.CONSTANTS}
  ${this.SWITCH_COLOR}
  `;

  /** @inheritdoc */
  static fragmentShader = `
  ${this.SHADER_HEADER}
  ${this.FADE(3, 1)}
  ${this.PERCEIVED_BRIGHTNESS}
  
  void main() {
    ${this.DISTANCE}
    vec3 finalColor = (darkness ? vec3(0.0) : color * alpha);
    ${this.ADAPTIVE_COLORATION}
    ${this.ADJUSTMENTS}
    ${this.FALLOFF}
  }`;

  /** @inheritdoc */
  static defaultUniforms = {
    technique: 1,
    ratio: 0.0,
    shadows: 0.0,
    saturation: 0.0,
    alpha: 1.0,
    color: [1.0, 1.0, 1.0],
    time: 0,
    intensity: 5,
    darkness: false,
    screenDimensions: [1, 1],
    uBkgSampler: 0,
    fovTexture: 0,
    gradual: true,
    useFov: true
  }
}

/* -------------------------------------------- */

/**
 * Allow coloring of illumination
 * @implements {AdaptiveIlluminationShader}
 * @author SecretFire
 */
class TorchIlluminationShader extends AdaptiveIlluminationShader {
  static fragmentShader = `
  ${this.SHADER_HEADER}
  ${this.FADE(3, 1.0)}
  ${this.PERCEIVED_BRIGHTNESS}

  void main() {
    ${this.DISTANCE}
    vec3 finalColor;
    ${this.TRANSITION}
    ${this.ADJUSTMENTS}
    ${this.FALLOFF}
  }`;
}

/* -------------------------------------------- */

/**
 * Torch animation coloration shader
 * @implements {AdaptiveColorationShader}
 * @author SecretFire
 */
class TorchColorationShader extends AdaptiveColorationShader {
  static fragmentShader = `
  ${this.SHADER_HEADER}
  ${this.FADE(3, 1.0)}
  ${this.PERCEIVED_BRIGHTNESS}
  uniform float brightnessPulse;

  float torchfade(in float dist, in float fadepower) {
    return pow(clamp(1.0 - dist, 0.0, 1.0), fadepower * 0.5);
  }

  void main() {
    ${this.DISTANCE}
    vec2 uv = (vUvs * 2.0) - 1.0;

    // creating the central waving "starlight"
    float angle = atan(uv.x, uv.y) * INVTWOPI;
    float flame = fract(angle * 64.0 + time);

    // compose the flame, put smooth gradient on all "edges"
    float df = pow(abs(max(flame, 1.0 - flame)), 0.05);

    // inner dancing flame distance
    float distf = dist / ratio * 0.75 / clamp(df, 0.1, 1.0);

    // maxing from coloration and the inner dancing flame
    vec3 finalColor = max(color * torchfade(dist , 1.8),
                          color * torchfade(distf, 1.2) * 1.5) * brightnessPulse * alpha;

    ${this.ADAPTIVE_COLORATION}
    ${this.ADJUSTMENTS}
    ${this.FALLOFF}
  }
  `;

  /** @inheritdoc */
  static defaultUniforms = Object.assign({}, super.defaultUniforms, {
    ratio: 0,
    brightnessPulse: 1
  });
}

/* -------------------------------------------- */

/**
 * Pulse animation illumination shader
 * @implements {AdaptiveIlluminationShader}
 * @author SecretFire
 */
class PulseIlluminationShader extends AdaptiveIlluminationShader {
  static fragmentShader = `
  ${this.SHADER_HEADER}
  ${this.FADE(3, 1.0)}
  ${this.PERCEIVED_BRIGHTNESS}

  void main() {
    ${this.DISTANCE}
    float fading = pow(abs(1.0 - dist * dist), 1.01 - ratio);

    vec3 finalColor;
    ${this.TRANSITION}
    finalColor *= fading;
    ${this.FALLOFF}
  }`;
}

/* -------------------------------------------- */

/**
 * Pulse animation coloration shader
 * @implements {AdaptiveColorationShader}
 * @author SecretFire
 */
class PulseColorationShader extends AdaptiveColorationShader {
  static fragmentShader = `
  ${this.SHADER_HEADER}
  uniform float pulse;

  ${this.FADE(3, 1.0)}
  ${this.PERCEIVED_BRIGHTNESS}

  float pfade(in float dist, in float pulse) {
      return pow(1.0 - dist, 1.25 - (pulse * 0.5));
  }
    
  void main() {
    ${this.DISTANCE}
    vec3 finalColor = color * pfade(dist, pulse) * alpha;
    ${this.ADAPTIVE_COLORATION}
    ${this.ADJUSTMENTS}
    ${this.FALLOFF}
  }`;

  /** @inheritdoc */
  static defaultUniforms = Object.assign({}, super.defaultUniforms, {
    pulse: 0
  });
}

/* -------------------------------------------- */

/**
 * Energy field animation coloration shader
 * @implements {AdaptiveColorationShader}
 * @author SecretFire
 */
class EnergyFieldColorationShader extends AdaptiveColorationShader {
  static fragmentShader = `    
  ${this.SHADER_HEADER}
  ${this.PRNG3D}
  ${this.FADE(3, 1.0)}
  ${this.PERCEIVED_BRIGHTNESS}

  // classic 3d voronoi (with some bug fixes)
  vec3 voronoi3d(const in vec3 x) {
    vec3 p = floor(x);
    vec3 f = fract(x);
    
    float id = 0.0;
    vec2 res = vec2(100.0);
    
    for (int k = -1; k <= 1; k++) {
      for (int j = -1; j <= 1; j++) {
        for (int i = -1; i <= 1; i++) {
          vec3 b = vec3(float(i), float(j), float(k));
          vec3 r = vec3(b) - f + random(p + b);
          
          float d = dot(r, r);
          float cond = max(sign(res.x - d), 0.0);
          float nCond = 1.0 - cond;
          float cond2 = nCond * max(sign(res.y - d), 0.0);
          float nCond2 = 1.0 - cond2;
    
          id = (dot(p + b, vec3(1.0, 67.0, 142.0)) * cond) + (id * nCond);
          res = vec2(d, res.x) * cond + res * nCond;
    
          res.y = cond2 * d + nCond2 * res.y;
        }
      }
    }
    // replaced abs(id) by pow( abs(id + 10.0), 0.01)
    // needed to remove artifacts in some specific configuration
    return vec3( sqrt(res), pow( abs(id + 10.0), 0.01) );
  }

  void main() {
    ${this.DISTANCE}
    vec2 uv = vUvs;
    
    // Hemispherize and scaling the uv
    float f = (1.0 - sqrt(1.0 - dist)) / dist;
    uv -= vec2(0.5);
    uv *= f * 4.0 * intensity;
    uv += vec2(0.5);
    
    // time and uv motion variables
    float t = time * 0.4;
    float uvx = cos(uv.x - t);
    float uvy = cos(uv.y + t);
    float uvxt = cos(uv.x + sin(t));
    float uvyt = sin(uv.y + cos(t));
    
    // creating the voronoi 3D sphere, applying motion
    vec3 c = voronoi3d(vec3(uv.x - uvx + uvyt, 
                            mix(uv.x, uv.y, 0.5) + uvxt - uvyt + uvx,
                            uv.y + uvxt - uvx));
    
    // abrupt fading
    float fade = pow(1.0 - dist, 0.20);

    // applying color and contrast, to create sharp black areas. 
    vec3 finalColor = c.x * c.x * c.x * color * alpha;

    ${this.ADAPTIVE_COLORATION}
    ${this.ADJUSTMENTS}
    ${this.CONSTRAIN_TO_LOS}
  }`;
}

/* -------------------------------------------- */

/**
 * Chroma animation coloration shader
 * @implements {AdaptiveColorationShader}
 * @author SecretFire
 */
class ChromaColorationShader extends AdaptiveColorationShader {
  static fragmentShader = `
  ${this.SHADER_HEADER}
  ${this.HSB2RGB}
  ${this.FADE(3, 1.0)}
  ${this.PERCEIVED_BRIGHTNESS}

  void main() {
    ${this.DISTANCE}
    vec3 finalColor = mix( color, 
                           hsb2rgb(vec3(time * 0.25, 1.0, 1.0)),
                           intensity * 0.1 ) * fade(dist) * alpha;

    ${this.ADAPTIVE_COLORATION}
    ${this.ADJUSTMENTS}
    ${this.CONSTRAIN_TO_LOS}
  }`;
}

/* -------------------------------------------- */

/**
 * Wave animation illumination shader
 * @implements {AdaptiveIlluminationShader}
 * @author SecretFire
 */
class WaveIlluminationShader extends AdaptiveIlluminationShader {
  static fragmentShader = `
  ${this.SHADER_HEADER}
  ${this.FADE(3, 1.0)}
  ${this.PERCEIVED_BRIGHTNESS}

  float wave(in float dist) {
    float sinWave = 0.5 * (sin(-time * 6.0 + dist * 10.0 * intensity) + 1.0);
    return 0.3 * sinWave + 0.8;
  }

  void main() {
    ${this.DISTANCE}
    vec3 finalColor;
    ${this.TRANSITION}
    finalColor *= wave(dist);
    ${this.ADJUSTMENTS}
    ${this.FALLOFF}
  }`;
}

/* -------------------------------------------- */

/**
 * Wave animation coloration shader
 * @implements {AdaptiveColorationShader}
 * @author SecretFire
 */
class WaveColorationShader extends AdaptiveColorationShader {
  static fragmentShader = `
  ${this.SHADER_HEADER}
  ${this.FADE(3, 1.0)}
  ${this.PERCEIVED_BRIGHTNESS}

  float wave(in float dist) {
    float sinWave = 0.5 * (sin(-time * 6.0 + dist * 10.0 * intensity) + 1.0);
    return 0.55 * sinWave + 0.8;
  }

  void main() {
    ${this.DISTANCE}
    vec3 finalColor = color * fade(dist) * wave(dist) * alpha;
    ${this.ADAPTIVE_COLORATION}
    ${this.ADJUSTMENTS}
    ${this.CONSTRAIN_TO_LOS}
  }`;
}

/* -------------------------------------------- */

/**
 * Bewitching Wave animation illumination shader
 * @implements {AdaptiveIlluminationShader}
 * @author SecretFire
 */
class BewitchingWaveIlluminationShader extends AdaptiveIlluminationShader {
  static fragmentShader = `
  ${this.SHADER_HEADER}
  ${this.FADE(3, 1.0)}
  ${this.PRNG}
  ${this.NOISE}
  ${this.FBM(4, 1.0)}
  ${this.PERCEIVED_BRIGHTNESS}

  // Transform UV
  vec2 transform(in vec2 uv, in float dist) {
    float t = time * 0.25;
    mat2 rotmat = mat2(cos(t), -sin(t), sin(t), cos(t));
    mat2 scalemat = mat2(2.5, 0.0, 0.0, 2.5);
    uv -= vec2(0.5); 
    uv *= rotmat * scalemat;
    uv += vec2(0.5);
    return uv;
  }

  float bwave(in float dist) {
    vec2 uv = transform(vUvs, dist);
    float motion = fbm(uv + time * 0.25);
    float distortion = mix(1.0, motion, clamp(1.0 - dist, 0.0, 1.0));
    float sinWave = 0.5 * (sin(-time * 6.0 + dist * 10.0 * intensity * distortion) + 1.0);
    return 0.3 * sinWave + 0.8;
  }

  void main() {
    ${this.DISTANCE}
    vec3 finalColor;
    ${this.TRANSITION}
    finalColor *= bwave(dist);
    ${this.ADJUSTMENTS}
    ${this.FALLOFF}
  }`;
}

/* -------------------------------------------- */

/**
 * Bewitching Wave animation coloration shader
 * @implements {AdaptiveColorationShader}
 * @author SecretFire
 */
class BewitchingWaveColorationShader extends AdaptiveColorationShader {
  static fragmentShader = `
  ${this.SHADER_HEADER}
  ${this.FADE(3, 1.0)}
  ${this.PRNG}
  ${this.NOISE}
  ${this.FBM(4, 1.0)}
  ${this.PERCEIVED_BRIGHTNESS}

  // Transform UV
  vec2 transform(in vec2 uv, in float dist) {
    float t = time * 0.25;
    mat2 rotmat = mat2(cos(t), -sin(t), sin(t), cos(t));
    mat2 scalemat = mat2(2.5, 0.0, 0.0, 2.5);
    uv -= vec2(0.5); 
    uv *= rotmat * scalemat;
    uv += vec2(0.5);
    return uv;
  }

  float bwave(in float dist) {
    vec2 uv = transform(vUvs, dist);
    float motion = fbm(uv + time * 0.25);
    float distortion = mix(1.0, motion, clamp(1.0 - dist, 0.0, 1.0));
    float sinWave = 0.5 * (sin(-time * 6.0 + dist * 10.0 * intensity * distortion) + 1.0);
    return 0.55 * sinWave + 0.8;
  }

  void main() {
    ${this.DISTANCE}
    vec3 finalColor = color * fade(dist) * bwave(dist) * alpha;
    ${this.ADAPTIVE_COLORATION}
    ${this.ADJUSTMENTS}
    ${this.CONSTRAIN_TO_LOS}
  }`;
}

/* -------------------------------------------- */

/**
 * Fog animation coloration shader
 * @extends {AdaptiveColorationShader}
 * @author SecretFire
 */
class FogColorationShader extends AdaptiveColorationShader {
  static fragmentShader = `
  ${this.SHADER_HEADER}
  ${this.PRNG}
  ${this.NOISE}
  ${this.FBM(4, 1.0)}
  ${this.FADE(5, 1.0)}
  ${this.PERCEIVED_BRIGHTNESS}

  vec3 fog() {
    // constructing the palette
    vec3 c1 = color * 0.60;
	  vec3 c2 = color * 0.95;
	  vec3 c3 = color * 0.50;
	  vec3 c4 = color * 0.75;
	  vec3 c5 = vec3(0.3);
	  vec3 c6 = color;

    // creating the deformation
    vec2 uv = vUvs;
	  vec2 p = uv.xy * 8.0;

    // time motion fbm and palette mixing
	  float q = fbm(p - time * 0.1);
	  vec2 r = vec2(fbm(p + q - time * 0.5 - p.x - p.y), 
                  fbm(p + q - time * 0.3));
	  vec3 c = clamp(mix(c1, 
                       c2, 
                   fbm(p + r)) + mix(c3, c4, r.x) 
                               - mix(c5, c6, r.y),
                   vec3(0.0), vec3(1.0));

    // returning the color
	  return c;
  }

  void main() {
    ${this.DISTANCE}
    float intens = intensity * 0.2;

    // applying fog
    vec3 finalColor = fog() * intens * fade(dist) * alpha;
    ${this.ADAPTIVE_COLORATION}
    ${this.ADJUSTMENTS}
	  ${this.CONSTRAIN_TO_LOS}
  }`;
}

/* -------------------------------------------- */

/**
 * Sunburst animation illumination shader
 * @extends {AdaptiveIlluminationShader}
 * @author SecretFire
 */
class SunburstIlluminationShader extends AdaptiveIlluminationShader {
  static fragmentShader = `
  ${this.SHADER_HEADER}
  ${this.FADE(4, 1.0)}
  ${this.PERCEIVED_BRIGHTNESS}

  // Smooth back and forth between a and b
  float cosTime(in float a, in float b) {
    return (a - b) * ((cos(time) + 1.0) * 0.5) + b;
  }

  // Create the sunburst effect
  vec3 sunBurst(in vec3 color, in vec2 uv, in float dist) {
    // Pulse calibration
    float intensityMod = 1.0 + (intensity * 0.05);
    float lpulse = cosTime(1.3 * intensityMod, 0.85 * intensityMod);
    
    // Compute angle
    float angle = atan(uv.x, uv.y) * INVTWOPI;
    
    // Creating the beams and the inner light
    float beam = fract(angle * 16.0 + time);
    float light = lpulse * pow(abs(1.0 - dist), 0.65);
    
    // Max agregation of the central light and the two gradient edges
    float sunburst = max(light, max(beam, 1.0 - beam));
        
    // Creating the effect : applying color and color correction. ultra saturate the entire output color.
    return color * pow(sunburst, 3.0);
  }

  void main() {
    ${this.DISTANCE}
    vec2 uv = (2.0 * vUvs) - 1.0;
    vec3 finalColor;
    finalColor = switchColor(colorBright, colorDim, dist) * alpha;
    ${this.ADJUSTMENTS}
    finalColor = sunBurst(finalColor, uv, dist);
    ${this.FALLOFF}
  }`;
}

/**
 * Sunburst animation coloration shader
 * @extends {AdaptiveColorationShader}
 * @author SecretFire
 */
class SunburstColorationShader extends AdaptiveColorationShader {
  static fragmentShader = `
  ${this.SHADER_HEADER}
  ${this.FADE(3, 1.0)}
  ${this.PERCEIVED_BRIGHTNESS}

  // Smooth back and forth between a and b
  float cosTime(in float a, in float b) {
    return (a - b) * ((cos(time) + 1.0) * 0.5) + b;
  }

  // Create a sun burst effect
  vec3 sunBurst(in vec2 uv, in float dist) {
    // pulse calibration
    float intensityMod = 1.0 + (intensity * 0.05);
    float lpulse = cosTime(1.1 * intensityMod, 0.85 * intensityMod);

    // compute angle
    float angle = atan(uv.x, uv.y) * INVTWOPI;
    
    // creating the beams and the inner light
    float beam = fract(angle * 16.0 + time);
    float light = lpulse * pow(abs(1.0 - dist), 0.65);
    
    // agregation of the central light and the two gradient edges to create the sunburst
    float sunburst = max(light, max(beam, 1.0 - beam));
        
    // creating the effect : applying color and color correction. saturate the entire output color.
    return color * pow(sunburst, 3.0);
  }

  void main() {
    ${this.DISTANCE}
    vec2 uvs = (2.0 * vUvs) - 1.0;
    vec3 finalColor = sunBurst(uvs, dist) * fade(dist) * alpha;
    ${this.ADAPTIVE_COLORATION}
    ${this.ADJUSTMENTS}
    ${this.CONSTRAIN_TO_LOS}
  }`;
}

/* -------------------------------------------- */

/**
 * Light dome animation coloration shader
 * @extends {AdaptiveColorationShader}
 * @author SecretFire
 */
class LightDomeColorationShader extends AdaptiveColorationShader {
  static fragmentShader = `
  ${this.SHADER_HEADER}
  ${this.PRNG}
  ${this.NOISE}
  ${this.FBM(2)}
  ${this.PERCEIVED_BRIGHTNESS}

  // Rotate and scale uv
  vec2 transform(in vec2 uv, in float dist) {
    float hspherize = (1.0 - sqrt(1.0 - dist)) / dist;
    float t = time * 0.02;
    mat2 rotmat = mat2(cos(t), -sin(t), sin(t), cos(t));
    mat2 scalemat = mat2(8.0 * intensity, 0.0, 0.0, 8.0 * intensity);
    uv -= PIVOT; 
    uv *= rotmat * scalemat * hspherize;
    uv += PIVOT;
    return uv;
  }
  
  vec3 ripples(in vec2 uv) {
    // creating the palette
    vec3 c1 = color * 0.550;
    vec3 c2 = color * 0.020;
    vec3 c3 = color * 0.3;
    vec3 c4 = color;
    vec3 c5 = color * 0.025;
    vec3 c6 = color * 0.200;

    vec2 p = uv + vec2(5.0);
    float q = 2.0 * fbm(p + time * 0.2);
    vec2 r = vec2(fbm(p + q + ( time  ) - p.x - p.y), fbm(p * 2.0 + ( time )));
    
    return clamp( mix( c1, c2, abs(fbm(p + r)) ) + mix( c3, c4, abs(r.x * r.x * r.x) ) - mix( c5, c6, abs(r.y * r.y)), vec3(0.0), vec3(1.0));
  }

  void main() {
    ${this.DISTANCE}
    
    // to hemispherize, rotate and magnify
    vec2 uv = transform(vUvs, dist);

    // abrupt fade
    float fade = pow(1.0 - dist, 0.25);
    vec3 finalColor = ripples(uv) * fade * alpha;
    ${this.ADAPTIVE_COLORATION}
    ${this.ADJUSTMENTS}
    ${this.CONSTRAIN_TO_LOS}
  }`;
}

/* -------------------------------------------- */


/**
 * Emanation animation coloration shader
 * @extends {AdaptiveColorationShader}
 * @author SecretFire
 */
class EmanationColorationShader extends AdaptiveColorationShader {
  static fragmentShader = `
  ${this.SHADER_HEADER}
  ${this.FADE(4, 1.0)}
  ${this.PERCEIVED_BRIGHTNESS}

  // Create an emanation composed of n beams, n = intensity
  vec3 beamsEmanation(in vec2 uv, in float dist) {
    float angle = atan(uv.x, uv.y) * INVTWOPI;

    // create the beams
    float beams = fract( angle * intensity + sin(dist * 10.0 - time));

    // compose the final beams with max, to get a nice gradient on EACH side of the beams.
    beams = max(beams, 1.0 - beams);

    // compute a strong color correction, to desaturate the space between the beams.
    //float colorburn = pow(beams, dist * 2.0);

    // creating the effect : applying color and color correction. saturate the entire output color.
    return smoothstep( 0.0, 1.0, beams * color);
  }

  void main() {
    // compute uvs and distance
    ${this.DISTANCE}
    vec2 uvs = (2.0 * vUvs) - 1.0;
    // apply beams emanation, fade and alpha
    vec3 finalColor = beamsEmanation(uvs, dist) * fade(dist) * alpha;
    ${this.ADAPTIVE_COLORATION}
    ${this.ADJUSTMENTS}
    ${this.CONSTRAIN_TO_LOS}
  }`;
}

/* -------------------------------------------- */

/**
 * Ghost light animation illumination shader
 * @extends {AdaptiveIlluminationShader}
 * @author SecretFire
 */
class GhostLightIlluminationShader extends AdaptiveIlluminationShader {
  static fragmentShader = `
  ${this.SHADER_HEADER}
  ${this.FADE(5, 1.0)}
  ${this.PERCEIVED_BRIGHTNESS}
  ${this.PRNG}
  ${this.NOISE}
  ${this.FBM(3, 1.0)}

  void main() {
    ${this.DISTANCE}
    
    // Creating distortion with vUvs and fbm
    float distortion1 = fbm(vec2( 
                        fbm(vUvs * 5.0 - time * 0.50), 
                        fbm((-vUvs - vec2(0.01)) * 5.0 + time * INVTHREE)));
    
    float distortion2 = fbm(vec2(
                        fbm(-vUvs * 5.0 - time * 0.50),
                        fbm((-vUvs + vec2(0.01)) * 5.0 + time * INVTHREE)));
    vec2 uv = vUvs;
      
    // time related var
    float t = time * 0.5;
    float tcos = 0.5 * (0.5 * (cos(t)+1.0)) + 0.25;

    vec3 finalColor;
    ${this.TRANSITION}
    finalColor *= mix( distortion1 * 1.5 * (intensity * 0.2),
                       distortion2 * 1.5 * (intensity * 0.2), tcos);
    ${this.ADJUSTMENTS}
    ${this.FALLOFF}
  }`;
}

/* -------------------------------------------- */

/**
 * Ghost light animation coloration shader
 * @extends {AdaptiveColorationShader}
 * @author SecretFire
 */
class GhostLightColorationShader extends AdaptiveColorationShader {
  static fragmentShader = `
  ${this.SHADER_HEADER}
  ${this.FADE(3, 1.0)}
  ${this.PRNG}
  ${this.NOISE}
  ${this.FBM(3, 1.0)}
  ${this.PERCEIVED_BRIGHTNESS}

  void main() {
    ${this.DISTANCE}
    
    // Creating distortion with vUvs and fbm
    float distortion1 = fbm(vec2( 
                        fbm(vUvs * 3.0 + time * 0.50), 
                        fbm((-vUvs + vec2(1.)) * 5.0 + time * INVTHREE)));
    
    float distortion2 = fbm(vec2(
                        fbm(-vUvs * 3.0 + time * 0.50),
                        fbm((-vUvs + vec2(1.)) * 5.0 - time * INVTHREE)));
    vec2 uv = vUvs;
      
    // time related var
    float t = time * 0.5;
    float tcos = 0.5 * (0.5 * (cos(t)+1.0)) + 0.25;
    float tsin = 0.5 * (0.5 * (sin(t)+1.0)) + 0.25;
    
    // Creating distortions with cos and sin : create fluidity
    uv -= PIVOT;
    uv *= tcos * distortion1;
    uv *= tsin * distortion2;
    uv *= fbm(vec2(time + distortion1, time + distortion2));
    uv += PIVOT;

    vec3 finalColor = distortion1 * distortion1 * 
                      distortion2 * distortion2 * 
                      color * pow(1.0 - dist, dist)
                      * alpha * mix( uv.x + distortion1 * 4.5 * (intensity * 0.2),
                                     uv.y + distortion2 * 4.5 * (intensity * 0.2), tcos);
    ${this.ADAPTIVE_COLORATION}
    ${this.ADJUSTMENTS}
    ${this.FALLOFF}
  }`;
}

/* -------------------------------------------- */

/**
 * Hexagonal dome animation coloration shader
 * @extends {AdaptiveColorationShader}
 * @author SecretFire
 */
class HexaDomeColorationShader extends AdaptiveColorationShader {
  static fragmentShader = `
  ${this.SHADER_HEADER}
  ${this.PERCEIVED_BRIGHTNESS}

  // rotate and scale uv
  vec2 transform(in vec2 uv, in float dist) {
    float hspherize = (1.0 - sqrt(1.0 - dist)) / dist;
    float t = -time * 0.20;
    float scale = 10.0 / (11.0 - intensity);
    float cost = cos(t);
    float sint = sin(t);

    mat2 rotmat = mat2(cost, -sint, sint, cost);
    mat2 scalemat = mat2(scale, 0.0, 0.0, scale);
    uv -= PIVOT; 
    uv *= rotmat * scalemat * hspherize;
    uv += PIVOT;
    return uv;
  }

  // Adapted classic hexa algorithm
  float hexDist(in vec2 uv) {
	  vec2 p = abs(uv);
    float c = dot(p, normalize(vec2(1.0, 1.73)));
    c = max(c, p.x);
    return c;
  }

  vec4 hexUvs(in vec2 uv) {
	  const vec2 r = vec2(1.0, 1.73);
    const vec2 h = r*0.5;
    
    vec2 a = mod(uv, r) - h;
    vec2 b = mod(uv - h, r) - h;
    vec2 gv = dot(a, a) < dot(b,b) ? a : b;
    
    float x = atan(gv.x, gv.y);
    float y = 0.55 - hexDist(gv);
    vec2 id = uv - gv;
    return vec4(x, y, id.x, id.y);
  }

  vec3 hexa(in vec2 uv) {
    float t = time;
    vec2 uv1 = uv + vec2(0.0, sin(uv.y) * 0.25);
    vec2 uv2 = 0.5 * uv1 + 0.5 * uv + vec2(0.55, 0);
    float a = 0.2;
    float c = 0.5;
    float s = -1.0;
    uv2 *= mat2(c, -s, s, c);

    vec3 col = color;
    float hexy = hexUvs(uv2 * 10.0).y;
    float hexa = smoothstep( 3.0 * (cos(t)) + 4.5, 12.0, hexy * 20.0) * 3.0;

    col *= mix(hexa, 1.0 - hexa, min(hexy, 1.0 - hexy));
    col += color * fract(smoothstep(1.0, 2.0, hexy * 20.0)) * 0.65;

    return col;
  }

  void main() {
    ${this.DISTANCE}

    // Rotate, magnify and hemispherize the uvs
    vec2 uv = transform(vUvs, dist);

    // Very abrupt fading
    float fade = pow(1.0 - dist, 0.18);
    
    // Hexaify the uv (hemisphere) and apply fade and alpha
    vec3 finalColor = hexa(uv) * fade * alpha;
    ${this.ADAPTIVE_COLORATION}
    ${this.ADJUSTMENTS}
    ${this.CONSTRAIN_TO_LOS}
  }`;
}

/* -------------------------------------------- */

/**
 * Roling mass illumination shader - intended primarily for darkness
 * @extends {AdaptiveIlluminationShader}
 * @author SecretFire
 */
class RoilingIlluminationShader extends AdaptiveIlluminationShader {
  static fragmentShader = `
  ${this.SHADER_HEADER}
  ${this.FADE(3, 1.0)}
  ${this.PERCEIVED_BRIGHTNESS}
  ${this.PRNG}
  ${this.NOISE}
  ${this.FBM(3)}

  void main() {
    // Classic distance
    ${this.DISTANCE}

    // Creating distortion with vUvs and fbm
    float distortion1 = fbm( vec2( 
                        fbm( vUvs * 2.5 + time * 0.5),
                        fbm( (-vUvs - vec2(0.01)) * 5.0 + time * INVTHREE)));
    
    float distortion2 = fbm( vec2(
                        fbm( -vUvs * 5.0 + time * 0.5),
                        fbm( (vUvs + vec2(0.01)) * 2.5 + time * INVTHREE)));
    
    // Timed values
    float t = -time * 0.5;
    float cost = cos(t);
    float sint = sin(t);
    
    // Rotation matrix
    mat2 rotmat = mat2(cost, -sint, sint, cost);
    vec2 uv = vUvs;

    // Applying rotation before distorting
    uv -= vec2(0.5);
    uv *= rotmat;
    uv += vec2(0.5);

    // Amplify distortions
    vec2 dstpivot = vec2( sin(min(distortion1 * 0.1, distortion2 * 0.1)),
                          cos(min(distortion1 * 0.1, distortion2 * 0.1)) ) * INVTHREE
                  - vec2( cos(max(distortion1 * 0.1, distortion2 * 0.1)),
                          sin(max(distortion1 * 0.1, distortion2 * 0.1)) ) * INVTHREE ;
    vec2 apivot = PIVOT - dstpivot;
    uv -= apivot;
    uv *= 1.13 + 1.33 * (cos(sqrt(max(distortion1, distortion2)) + 1.0) * 0.5);
    uv += apivot;

    // distorted distance
    float ddist = distance(uv, PIVOT) * 2.0;
    float alphaBright, alphaDim;

    // R'lyeh Ftagnh !
    float smooth = smoothstep(ratio * 0.95, ratio * 1.05, clamp(ddist, 0.0, 1.0));
    float inSmooth = min(smooth, 1.0 - smooth) * 2.0;
    
    // Creating the spooky membrane around the bright area
    vec3 membraneColor = vec3(1.0 - inSmooth);
   
    // Intensity modifier
    if (darkness) {
      alphaBright = 1.0 - pow(clamp(ratio - ddist, 0.0, 1.0), 0.75) * sqrt(2.0 - ddist);
      alphaDim =    1.0 - pow(clamp(1.0 - ddist, 0.0, 1.0), 0.65);
    } else {
      alphaBright = 1.0;
      alphaDim =    1.0;
    }

    float intensMod = (!darkness ? (11.0 - intensity) * 0.25 : 1.0);

    vec3 finalColor;
    if (!darkness && gradual && ratio > 0.0) {
      finalColor = mix(colorBright * intensMod * (darkness ? 1.0 : 1.5), 
                       colorDim * intensMod, 
                       smoothstep(ratio * 0.8, clamp(ratio * 0.95, 0.0, 1.0), clamp(ddist, 0.0, 1.0))) 
                       * min(alphaBright, alphaDim) * alpha;
    } else finalColor = mix(colorDim * intensMod, colorBright * intensMod, step(ddist, ratio)) * min(alphaBright, alphaDim) * alpha;
    
    finalColor *= membraneColor;
    ${this.ADJUSTMENTS}
    ${this.FALLOFF}
  }`;
}

/* -------------------------------------------- */

/**
 * Black Hole animation illumination shader
 * @extends {AdaptiveIlluminationShader}
 * @author SecretFire
 */
class BlackHoleIlluminationShader extends AdaptiveIlluminationShader {
  static fragmentShader = `
  ${this.SHADER_HEADER}
  ${this.FADE(3, 1.0)}
  ${this.PERCEIVED_BRIGHTNESS}

  // create an emanation composed of n beams, n = intensity
  vec3 beamsEmanation(in vec2 uv, in float dist, in vec3 pCol) {
    float angle = atan(uv.x, uv.y) * INVTWOPI;

    // Create the beams
    float beams = fract(angle * intensity + sin(dist * 30.0 - time));

    // Compose the final beams and reverse beams, to get a nice gradient on EACH side of the beams.
    beams = max(beams, 1.0 - beams);

    // Compute a darkness modifier.
    float darknessPower = (darkness ? pow(beams, 1.5) : 0.8);

    // Creating the effect : applying color and darkness power correction. saturate the entire output color.
    vec3 smoothie = smoothstep(0.2, 1.1 + (intensity * 0.1), beams * pCol * darknessPower);
    return ( darkness ? smoothie : pCol * (1.0 - beams) ) * intensity;
  }

  void main() {
    // compute uvs and distance
    ${this.DISTANCE}
    vec2 uvs = (2.0 * vUvs) - 1.0;
    
    vec3 pColorDim, pColorBright;
    if (darkness) {
      // palette of colors to give the darkness a disturbing purpleish tone
      pColorDim    = vec3(0.25, 0.10, 0.35);
      pColorBright = vec3(0.85, 0.80, 0.95);
    } else {
      pColorDim    = vec3(0.5);
      pColorBright = vec3(1.0);
    }
    
    // smooth mixing of the palette by distance from center and bright ratio
    vec3 finalColor;
    vec3 pCol = mix(pColorDim, pColorBright, smoothstep(ratio * 0.9, ratio * 1.1, (darkness ? dist : 1.0 - dist) ));
    if (darkness) {
      finalColor = min(colorDim, 
                       mix(colorBright, 
                           beamsEmanation(uvs, dist, pCol), 
                           1.0 - sqrt(1.0 - dist))) * alpha;
    } else {
      finalColor = mix(colorBright, 
                       mix(colorDim, 
                           beamsEmanation(uvs, dist, pCol), 
                           sqrt(dist)), (gradual ? smoothstep(ratio * 0.8, clamp(ratio * 1.2, 0.0, 1.0), dist) : step(1.0 - dist, ratio)) ) * alpha;      
    }
    //if (!darkness) finalColor = vec3(1.0) - finalColor;
    
    // Apply darker components of colorDim and mixed emanations/colorBright.
    ${this.ADJUSTMENTS}
    ${this.FALLOFF}
  }`;
}

/* -------------------------------------------- */

/**
 * Vortex animation coloration shader
 * @extends {AdaptiveColorationShader}
 * @author SecretFire
 */
class VortexColorationShader extends AdaptiveColorationShader {
  static fragmentShader = `
  ${this.SHADER_HEADER}
  ${this.PRNG}
  ${this.NOISE}
  ${this.FBM(4, 1.0)}
  ${this.FADE(3, 1.0)}
  ${this.PERCEIVED_BRIGHTNESS}

  vec2 vortex(in vec2 uv, in float dist, in float radius, in mat2 rotmat) {
    float intens = intensity * 0.2;
    vec2 uvs = uv - PIVOT;
    uv *= rotmat;
            
    if (dist < radius) {
      float sigma = (radius - dist) / radius;
      float theta = sigma * sigma * TWOPI * intens;
      float st = sin(theta);
      float ct = cos(theta);
      uvs = vec2(dot(uvs, vec2(ct, -st)), dot(uvs, vec2(st, ct)));
    }
    uvs += PIVOT;
    return uvs;
  }

  vec3 spice(in vec2 iuv, in mat2 rotmat) {
  
    // constructing the palette
    vec3 c1 = color * 0.55;
    vec3 c2 = color * 0.95;
    vec3 c3 = color * 0.45;
    vec3 c4 = color * 0.75;
    vec3 c5 = vec3(0.20);
    vec3 c6 = color * 1.2;

    // creating the deformation
    vec2 uv = iuv;
    uv -= PIVOT;
    uv *= rotmat;
	  vec2 p = uv.xy * 6.0;
    uv += PIVOT;

    // time motion fbm and palette mixing
	  float q = fbm(p + time);
	  vec2 r = vec2(fbm(p + q + time * 0.9 - p.x - p.y), 
                  fbm(p + q + time * 0.6));
	  vec3 c = mix(c1, 
                 c2, 
                 fbm(p + r)) + mix(c3, c4, r.x) 
                             - mix(c5, c6, r.y);
    // returning the color
	  return c;
  }

  void main() {
    ${this.DISTANCE}
    if (!darkness) {
      // Timed values
      float t = time * 0.5;
      float cost = cos(t);
      float sint = sin(t);

      // Rotation matrix
      mat2 vortexRotMat = mat2(cost, -sint, sint, cost);
      mat2 spiceRotMat = mat2(cost * 2.0, -sint * 2.0, sint * 2.0, cost * 2.0);

      // Creating vortex
      vec2 vuv = vortex(vUvs, dist, 1.0, vortexRotMat);

      // Applying spice
      vec3 finalColor = spice(vuv, spiceRotMat) * alpha;
      ${this.ADAPTIVE_COLORATION}
      ${this.ADJUSTMENTS}
      ${this.FALLOFF}
    } else {
      vec3 finalColor = vec3(0.0);
      ${this.FALLOFF}
    }
  }`;
}

/* -------------------------------------------- */

/**
 * Vortex animation coloration shader
 * @extends {AdaptiveColorationShader}
 * @author SecretFire
 */
class VortexIlluminationShader extends AdaptiveIlluminationShader {
  static fragmentShader = `
  ${this.SHADER_HEADER}
  ${this.PRNG}
  ${this.NOISE}
  ${this.FBM(4, 1.0)}
  ${this.FADE(3, 1.0)}
  ${this.PERCEIVED_BRIGHTNESS}

  vec2 vortex(in vec2 uv, in float dist, in float radius, in float angle, in mat2 rotmat) {
    vec2 uvs = uv - PIVOT;
    uv *= rotmat;
            
    if (dist < radius) {
      float sigma = (radius - dist) / radius;
      float theta = sigma * sigma * angle;
      float st = sin(theta);
      float ct = cos(theta);
      uvs = vec2(dot(uvs, vec2(ct, -st)), dot(uvs, vec2(st, ct)));
    }
    uvs += PIVOT;
    return uvs;
  }

  vec3 spice(in vec2 iuv, in mat2 rotmat) {
    // constructing the palette
    vec3 c1 = vec3(0.20);
	  vec3 c2 = vec3(0.80);
	  vec3 c3 = vec3(0.15);
	  vec3 c4 = vec3(0.85);
	  vec3 c5 = c3;
	  vec3 c6 = vec3(0.9);

    // creating the deformation
    vec2 uv = iuv;
    uv -= PIVOT;
    uv *= rotmat;
	  vec2 p = uv.xy * 6.0;
    uv += PIVOT;

    // time motion fbm and palette mixing
	  float q = fbm(p + time);
	  vec2 r = vec2(fbm(p + q + time * 0.9 - p.x - p.y), fbm(p + q + time * 0.6));
                  
    // Mix the final color
	  return mix(c1, c2, fbm(p + r)) + mix(c3, c4, r.x) - mix(c5, c6, r.y);
  }

  vec3 convertToDarknessColors(in vec3 col, in float dist) {
    float intens = intensity * 0.20;
    float lum = (col.r * 2.0 + col.g * 3.0 + col.b) * 0.5 * INVTHREE;
    float colorMod = smoothstep(ratio * 0.99, ratio * 1.01, dist);
    return mix(colorDim, colorBright * colorMod, 1.0 - smoothstep( 0.80, 1.00, lum)) *
                smoothstep( 0.25 * intens, 0.85 * intens, lum);
  }

  void main() {
    ${this.DISTANCE}
    vec3 finalColor;

    if (darkness) {
      // Timed values
      float t = time * 0.5;
      float cost = cos(t) * 2.0;
      float sint = sin(t) * 2.0;

      // Rotation matrix
      mat2 rotmatrix = mat2(cost, -sint, sint, cost);

      // Creating vortex
      vec2 svuv = vortex(vUvs, dist, 1.0, 6.24, rotmatrix);
      vec2 nvuv = vortex(vUvs, dist, 1.0, 2.12, rotmatrix);

      // Applying spice
      vec3 normalSpice = spice(nvuv, rotmatrix);
      finalColor = convertToDarknessColors( max(normalSpice, spice(svuv, rotmatrix)), dist );
    } else {
      ${this.TRANSITION}
    }
    ${this.ADJUSTMENTS}
    ${this.FALLOFF}
  }`;
}

/* -------------------------------------------- */

/**
 * Swirling rainbow animation coloration shader
 * @implements {AdaptiveColorationShader}
 * @author SecretFire
 */
class SwirlingRainbowColorationShader extends AdaptiveColorationShader {
  static fragmentShader = `
  ${this.SHADER_HEADER}
  ${this.HSB2RGB}
  ${this.FADE(3, 1.0)}
  ${this.PERCEIVED_BRIGHTNESS}

  void main() {
    ${this.DISTANCE}

    float intens = intensity * 0.1;
    vec2 nuv = vUvs * 2.0 - 1.0;
    vec2 puv = vec2(atan(nuv.x, nuv.y) * INVTWOPI + 0.5, length(nuv));
    vec3 rainbow = hsb2rgb(vec3(puv.x + puv.y - time * 0.2, 1.0, 1.0));
    vec3 finalColor = mix(color, rainbow, smoothstep(0.0, 1.5 - intens, dist))
                      * (1.0 - dist * dist * dist);
    
    ${this.ADAPTIVE_COLORATION}
    ${this.ADJUSTMENTS}
    ${this.FALLOFF}
  }`;
}

/* -------------------------------------------- */

/**
 * Radial rainbow animation coloration shader
 * @implements {AdaptiveColorationShader}
 * @author SecretFire
 */
class RadialRainbowColorationShader extends AdaptiveColorationShader {
  static fragmentShader = `
  ${this.SHADER_HEADER}
  ${this.HSB2RGB}
  ${this.FADE(3, 1.0)}
  ${this.PERCEIVED_BRIGHTNESS}

  void main() {
    ${this.DISTANCE}

    float intens = intensity * 0.1;
    vec2 nuv = vUvs * 2.0 - 1.0;
    vec2 puv = vec2(atan(nuv.x, nuv.y) * INVTWOPI + 0.5, length(nuv)); 
    vec3 rainbow = hsb2rgb(vec3(puv.y - time * 0.2, 1.0, 1.0));
    vec3 finalColor = mix(color, rainbow, smoothstep(0.0, 1.5 - intens, dist))
                      * (1.0 - dist * dist * dist);
    
    ${this.ADAPTIVE_COLORATION}
    ${this.ADJUSTMENTS}
    ${this.FALLOFF}
  }`;
}

/* -------------------------------------------- */

/**
 * Fairy light animation coloration shader
 * @extends {AdaptiveColorationShader}
 * @author SecretFire
 */
class FairyLightColorationShader extends AdaptiveColorationShader {
  static fragmentShader = `
  ${this.SHADER_HEADER}
  ${this.HSB2RGB}
  ${this.FADE(3, 1.0)}
  ${this.PRNG}
  ${this.NOISE}
  ${this.FBM(3, 1.0)}
  ${this.PERCEIVED_BRIGHTNESS}

  void main() {
    ${this.DISTANCE}
    
    // Creating distortion with vUvs and fbm
    float distortion1 = fbm(vec2( 
                        fbm(vUvs * 3.0 + time * 0.50), 
                        fbm((-vUvs + vec2(1.)) * 5.0 + time * INVTHREE)));
    
    float distortion2 = fbm(vec2(
                        fbm(-vUvs * 3.0 + time * 0.50),
                        fbm((-vUvs + vec2(1.)) * 5.0 - time * INVTHREE)));
    vec2 uv = vUvs;
      
    // time related var
    float t = time * 0.5;
    float tcos = 0.5 * (0.5 * (cos(t)+1.0)) + 0.25;
    float tsin = 0.5 * (0.5 * (sin(t)+1.0)) + 0.25;
    
    // Creating distortions with cos and sin : create fluidity
    uv -= PIVOT;
    uv *= tcos * distortion1;
    uv *= tsin * distortion2;
    uv *= fbm(vec2(time + distortion1, time + distortion2));
    uv += PIVOT;

    // Creating the rainbow
    float intens = intensity * 0.1;
    vec2 nuv = vUvs * 2.0 - 1.0;
    vec2 puv = vec2(atan(nuv.x, nuv.y) * INVTWOPI + 0.5, length(nuv));
    vec3 rainbow = hsb2rgb(vec3(puv.x + puv.y - time * 0.2, 1.0, 1.0));
    vec3 mixedColor = mix(color, rainbow, smoothstep(0.0, 1.5 - intens, dist));

    vec3 finalColor = distortion1 * distortion1 * 
                      distortion2 * distortion2 * 
                      mixedColor * alpha * (1.0 - dist * dist * dist) *
                      mix( uv.x + distortion1 * 4.5 * (intensity * 0.4),
                           uv.y + distortion2 * 4.5 * (intensity * 0.4), tcos);
    ${this.ADAPTIVE_COLORATION}
    ${this.ADJUSTMENTS}
    ${this.FALLOFF}
  }`;
}

/* -------------------------------------------- */

/**
 * Fairy light animation illumination shader
 * @extends {AdaptiveIlluminationShader}
 * @author SecretFire
 */
class FairyLightIlluminationShader extends AdaptiveIlluminationShader {
  static fragmentShader = `
  ${this.SHADER_HEADER}
  ${this.FADE(5, 1.0)}
  ${this.PERCEIVED_BRIGHTNESS}
  ${this.PRNG}
  ${this.NOISE}
  ${this.FBM(3, 1.0)}

  void main() {
    ${this.DISTANCE}
    
    // Creating distortion with vUvs and fbm
    float distortion1 = fbm(vec2( 
                        fbm(vUvs * 3.0 - time * 0.50), 
                        fbm((-vUvs + vec2(1.)) * 5.0 + time * INVTHREE)));
    
    float distortion2 = fbm(vec2(
                        fbm(-vUvs * 3.0 - time * 0.50),
                        fbm((-vUvs + vec2(1.)) * 5.0 - time * INVTHREE)));
      
    // linear interpolation motion
    float motionWave = 0.5 * (0.5 * (cos(time * 0.5) + 1.0)) + 0.25;

    vec3 finalColor;
    ${this.TRANSITION}
    finalColor *= mix(distortion1, distortion2, motionWave);
    ${this.ADJUSTMENTS}
    ${this.FALLOFF}
  }`;
}
