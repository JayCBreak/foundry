/**
 * This class defines an interface for masked custom filters
 * @extends {PIXI.Filter}
 * @interface
 */
class AbstractBaseMaskFilter extends PIXI.Filter {

  /**
   * The default vertex shader used by all instances of AbstractBaseMaskFilter
   * @type {string}
   */
  static vertexShader = `
  attribute vec2 aVertexPosition;

  uniform mat3 projectionMatrix;
  uniform vec2 screenDimensions;
  uniform vec4 inputSize;
  uniform vec4 outputFrame;

  varying vec2 vTextureCoord;
  varying vec2 vMaskTextureCoord;

  vec4 filterVertexPosition( void ) {
      vec2 position = aVertexPosition * max(outputFrame.zw, vec2(0.)) + outputFrame.xy;
      return vec4((projectionMatrix * vec3(position, 1.0)).xy, 0., 1.);
  }

  // getting normalized coord for the tile texture
  vec2 filterTextureCoord( void ) {
      return aVertexPosition * (outputFrame.zw * inputSize.zw);
  }

  // getting normalized coord for a screen sized mask render texture
  vec2 filterMaskTextureCoord( in vec2 textureCoord ) {
    return (textureCoord * inputSize.xy + outputFrame.xy) / screenDimensions;
  }

  void main() {
    vTextureCoord = filterTextureCoord();
    vMaskTextureCoord = filterMaskTextureCoord(vTextureCoord);
    gl_Position = filterVertexPosition();
  }`;

  /**
   * The fragment shader which renders this filter.
   * A subclass of AbstractBaseMaskFilter must implement the fragmentShader(channel) static field.
   * @type {Function}
   */
  static fragmentShader = null;

  /**
   * A factory method for creating the filter using its defined default values
   * @param {object} [defaultUniforms]    Initial uniforms provided to the filter
   * @param {string} [channel=r]          A color channel to target for masking.
   * @return {AbstractBaseMaskFilter}
   */
  static create(defaultUniforms={}, channel="r") {
    defaultUniforms.screenDimensions = [1, 1];
    return new this(this.vertexShader, this.fragmentShader(channel), defaultUniforms);
  }

  /** @override */
  apply(filterManager, input, output, clear, currentState) {
    const screen = filterManager.renderer.screen ?? null;
    if (screen) this.uniforms.screenDimensions = [screen.width, screen.height];
    filterManager.applyFilter(this, input, output, clear);
  }
}

/* -------------------------------------------- */

/**
 * A filter used to control channels intensity using an externally provided mask texture.
 * The mask channel used must be provided at filter creation.
 * Contributed by SecretFire#4843
 * @extends {AbstractBaseMaskFilter}
 */
class InverseOcclusionMaskFilter extends AbstractBaseMaskFilter {
  static fragmentShader(channel) {
    return `
    precision mediump float;
    varying vec2 vTextureCoord;
    varying vec2 vMaskTextureCoord;
    uniform sampler2D uSampler;
    uniform sampler2D uMaskSampler;
    uniform float alphaOcclusion;
    uniform float alpha;
    void main() {
      vec4 tex = texture2D(uMaskSampler, vMaskTextureCoord);
      float mask = 1.0 - tex.${channel} + alphaOcclusion * tex.${channel};
      float calpha = tex.${channel} + alpha * (1.0 - tex.${channel});
      gl_FragColor = texture2D(uSampler, vTextureCoord) * mask * calpha;
    }`;
  };
  static create(defaultUniforms = {}, channel = "r") {
    defaultUniforms.alpha = 1.0;
    return super.create(defaultUniforms, channel);
  }
}

/* -------------------------------------------- */


/**
 * An abstract filter which provides a framework for reusable definition
 * @extends {PIXI.Filter}
 */
class AbstractFilter extends PIXI.Filter {

  /**
   * The default uniforms used by the filter
   * @type {object}
   */
  static defaultUniforms = {};

  /**
   * The fragment shader which renders this filter.
   * @type {string}
   */
  static fragmentShader = undefined;

  /**
   * The vertex shader which renders this filter.
   * @type {string}
   */
  static vertexShader = undefined;

  /**
   * A factory method for creating the filter using its defined default values
   * @return {AbstractFilter}
   */
  static create(uniforms = {}) {
    uniforms = Object.assign({}, this.defaultUniforms, uniforms);
    return new this(this.vertexShader, this.fragmentShader, uniforms);
  }

  /**
   * Always target the resolution of the render texture or renderer
   * @type {number}
   */
  get resolution() {
    const renderer = canvas.app.renderer;
    const renderTextureSystem = renderer.renderTexture;
    if (renderTextureSystem.current) {
      return renderTextureSystem.current.resolution;
    }
    return renderer.resolution;
  }
  set resolution(value) {}

  /**
   * Always target the MSAA level of the render texture or renderer
   * @returns {PIXI.MSAA_QUALITY}
   */
  get multisample() {
    const renderer = canvas.app.renderer;
    const renderTextureSystem = renderer.renderTexture;
    if (renderTextureSystem.current) {
      return renderTextureSystem.current.multisample;
    }
    return renderer.multisample;
  }
  set multisample(value) { }
}

/* -------------------------------------------- */

/**
 * A filter which forces all non-transparent pixels to a specific color and transparency.
 * @extends {AbstractFilter}
 */
class ForceColorFilter extends AbstractFilter {
  static defaultUniforms = {
    color: [1, 1, 1],
    alpha: 1.0
  }
  static fragmentShader = `
  varying vec2 vTextureCoord;
  uniform sampler2D uSampler;
  uniform vec3 color;
  uniform float alpha;
  
  void main() {
    vec4 tex = texture2D(uSampler, vTextureCoord);
    if ( tex.a > 0.0 ) gl_FragColor = vec4(color * alpha, 1.0);
    else gl_FragColor = vec4(0.0);
  }`;
}

/* -------------------------------------------- */

/**
 * A filter which is rendering explored color according to FoW intensity. 
 * @extends {AbstractFilter}
 */
class FogColorFilter extends AbstractFilter {
  static defaultUniforms = {
    exploredColor: [1, 1, 1]
  }
  static fragmentShader = `
  varying vec2 vTextureCoord;
  uniform sampler2D uSampler;
  uniform vec3 exploredColor;
  
  void main() {
    vec3 tex = texture2D(uSampler, vTextureCoord).rgb;
    float a = step(0.15, max(tex.r, max(tex.g, tex.b)));
    gl_FragColor = vec4(exploredColor, 1.0) * a;
  }`;
}

/* -------------------------------------------- */

/**
 * This filter turns pixels with an alpha channel < alphaThreshold in transparent pixels
 * Then, optionally, it can turn the result in the chosen color (default: pure white).
 * The alpha [threshold,1] is re-mapped to [0,1] with an hermite interpolation slope to prevent pixelation.
 * @extends {PIXI.Filter}
 */
class RoofMaskFilter extends AbstractFilter {
  static defaultUniforms = {
    alphaThreshold: 0.75,
    turnToColor: false,
    color: [1, 1, 1]
  }
  static fragmentShader = `
  precision mediump float;
  varying vec2 vTextureCoord;
  uniform sampler2D uSampler;
  uniform float alphaThreshold;
  uniform bool turnToColor;
  uniform vec3 color;

  void main(void) {
    vec4 tex = texture2D(uSampler, vTextureCoord);
    float zapper = smoothstep(alphaThreshold, 1.0, tex.a);
    if (turnToColor) tex = vec4(color, 1.0);
    gl_FragColor = tex * zapper;
  }`;
}


/* -------------------------------------------- */


/**
 * A filter which implements an inner or outer glow around the source texture.
 * Incorporated from https://github.com/pixijs/filters/tree/main/filters/glow
 * @license MIT
 * @extends AbstractFilter
 */
class GlowFilter extends AbstractFilter {

  /** @inheritdoc */
  static defaultUniforms = {
    distance: 10,
    innerStrength: 0,
    glowColor: [1, 1, 1, 1],
    quality: 0.1,
  }

  /** @inheritdoc */
  static fragmentShader(quality, distance) { return `
  precision mediump float;
  varying vec2 vTextureCoord;
  varying vec4 vColor;

  uniform sampler2D uSampler;
  uniform float innerStrength;
  uniform vec4 glowColor;
  uniform vec4 inputSize;
  uniform vec4 inputClamp;

  const float PI = 3.14159265358979323846264;
  const float DIST = ${distance.toFixed(0)}.0;
  const float ANGLE_STEP_SIZE = min(${(1 / quality / distance).toFixed(7)}, PI * 2.0);
  const float ANGLE_STEP_NUM = ceil(PI * 2.0 / ANGLE_STEP_SIZE);
  const float MAX_TOTAL_ALPHA = ANGLE_STEP_NUM * DIST * (DIST + 1.0) / 2.0;

  float getClip(in vec2 uv) {
    return step(3.5,
     step(inputClamp.x, uv.x) +
     step(inputClamp.y, uv.y) +
     step(uv.x, inputClamp.z) +
     step(uv.y, inputClamp.w));
  }

  void main(void) {
    vec2 px = inputSize.zw;
    float totalAlpha = 0.0;
    vec2 direction;
    vec2 displaced;
    vec4 curColor;

    for (float angle = 0.0; angle < PI * 2.0; angle += ANGLE_STEP_SIZE) {
     direction = vec2(cos(angle), sin(angle)) * px;
     for (float curDistance = 0.0; curDistance < DIST; curDistance++) {
       displaced = vTextureCoord + direction * (curDistance + 1.0);
       curColor = texture2D(uSampler, displaced) * getClip(displaced);
       totalAlpha += (DIST - curDistance) * (curColor.a);
     }
    }

    curColor = texture2D(uSampler, vTextureCoord);
    float alphaRatio = (totalAlpha / MAX_TOTAL_ALPHA);
    float innerGlowAlpha = (1.0 - alphaRatio) * innerStrength * curColor.a;
    float innerGlowStrength = min(1.0, innerGlowAlpha);
    gl_FragColor = mix(curColor, glowColor, innerGlowStrength);
  }`};

  /** @inheritdoc */
  static vertexShader = `
  precision mediump float;
  attribute vec2 aVertexPosition;
  uniform mat3 projectionMatrix;
  uniform vec4 inputSize;
  uniform vec4 outputFrame;
  varying vec2 vTextureCoord;

  void main(void) {
      vec2 position = aVertexPosition * max(outputFrame.zw, vec2(0.0)) + outputFrame.xy;
      gl_Position = vec4((projectionMatrix * vec3(position, 1.0)).xy, 0.0, 1.0);
      vTextureCoord = aVertexPosition * (outputFrame.zw * inputSize.zw);
  }`;


  /** @inheritdoc */
  static create(uniforms = {}) {
    uniforms = Object.assign({}, this.defaultUniforms, uniforms);
    return new this(this.vertexShader, this.fragmentShader(uniforms.quality, uniforms.distance), uniforms);
  }
}