/**
 * The legacy polygon computation algorithm using "Quadtree Expansion".
 * This algorithm expands the reference frame outwards from the polygon origin, testing nearest walls first.
 *
 * This algorithm has been outmoded by ClockwiseSweepPolygon and can be deleted before V9 stable
 * @deprecated since v9p1
 * @ignore
 *
 * @extends {PointSourcePolygon}
 */
class QuadtreeExpansionPolygon extends PointSourcePolygon {

  /** @inheritdoc */
  static create(origin, {angle=360, debug=false, density=6, radius, rotation=0, type="sight"}={}) {
    const poly = new this();
    poly.origin = origin;
    poly.config = {angle, debug, density, radius, rotation, type};
    return poly.compute();
  }

  /** @inheritdoc */
  _compute() {
    const origin = this.origin;
    const {angle, debug, density, radius, rotation, type} = this.config;
    const quadtree = canvas.walls.quadtree;
    let {x, y} = origin;
    let d = canvas.dimensions;
    const r = radius ?? 0;
    const distance = Math.max(r, d.maxR);

    // Determine the direction of facing, the angle of vision, and the angles of boundary rays
    const limitAngle = angle.between(0, 360, false);
    const aMin = limitAngle ? Math.normalizeRadians(Math.toRadians(rotation + 90 - (angle / 2))) : -Math.PI;
    const aMax = limitAngle ? aMin + Math.toRadians(angle) : Math.PI;

    // For high wall count maps, restrict to a subset of endpoints using quadtree bounds
    // Target wall endpoints within the vision radius or within 10 grid units, whichever is larger
    let endpoints = canvas.walls.endpoints;
    let bounds = null;
    if ( endpoints.length > SightLayer.EXACT_VISION_THRESHOLD ) {
      const rb2 = Math.max(d.size * 10, r);
      bounds = new NormalizedRectangle(origin.x - rb2, origin.y - rb2, (2 * rb2), (2 * rb2));
      let walls = quadtree.getObjects(bounds);
      endpoints = WallsLayer.getUniqueEndpoints(walls, {bounds, type});
    }

    // Cast sight rays at target endpoints using the full unrestricted line-of-sight distance
    const rays = this._castRays(x, y, distance, {density, endpoints, limitAngle, aMin, aMax});
    const rayQueue = new Set(rays);

    // Record which Rays appear in each Quadtree quadrant
    const quadMap = new Map();
    for ( let r of rays ) {
      r._cs = null;
      r._c = null;
      const nodes = quadtree.getLeafNodes(r.bounds);
      for ( let n of nodes ) {
        let s = quadMap.get(n);
        if ( !s ) {
          s = new Set();
          quadMap.set(n, s);
        }
        s.add(r);
      }
    }

    // Identify the closest quadtree vertex as the origin point for progressive testing
    const quadSize = Math.max(d.sceneWidth, d.sceneHeight) / (quadtree.maxDepth * 2);
    const originNode = quadtree.getLeafNodes({x: origin.x, y: origin.y, width: 0, height: 0})[0];
    const ob = originNode.bounds;
    const testFrame = new PIXI.Rectangle((origin.x - ob.x) < (ob.width / 2) ? ob.x : ob.x + ob.width,
      (origin.y - ob.y) < (ob.height / 2) ? ob.y : ob.y + ob.height, 0, 0).pad(quadSize);

    // Iterate until we have matched all rays or run out of quadtree nodes
    const nodeQueue = new Set(quadtree.getLeafNodes(testFrame));
    const testedNodes = new Set();
    while ( rayQueue.size && nodeQueue.size ) {

      // Check every ray+wall collision for each quadrant in the batch
      const nodes = Array.from(nodeQueue);
      for (let node of nodes) {
        const rays = quadMap.get(node) || [];

        // Iterate over each wall which appears in the quadrant
        for (let obj of node.objects) {
          const w = obj.t;
          let wt = w.data[type];

          // Coerce interior walls beneath roofs as vision blocking for sight polygons
          const isInterior = (type === "sight") && (w.roof?.occluded === false);
          if ( isInterior ) wt = CONST.WALL_SENSE_TYPES.NORMAL;

          // Ignore walls and open doors that don't block senses
          else if ( wt === CONST.WALL_SENSE_TYPES.NONE ) continue;
          else if ((w.data.door > CONST.WALL_DOOR_TYPES.NONE) && (w.data.ds === CONST.WALL_DOOR_STATES.OPEN)) continue;

          // Iterate over rays
          for (let r of rays) {
            if ( r._c || !w.canRayIntersect(r) ) continue;

            // Test collision for the ray
            const x = this._testWall(r, w);
            if ( !x ) continue;

            // Flag the collision
            r._cs = r._cs || new Map();
            const pt = (Math.round(x.x) << 16) + Math.round(x.y); // 32 bit integer, x[16]y[16]
            const c = r._cs.get(pt);
            if ( c ) {
              c.type = Math.min(wt, c.type);
              for ( let n of obj.n ) c.nodes.push(n);
            }
            else {
              x.type = wt;
              x.nodes = Array.from(obj.n);
              r._cs.set(pt, x);
            }
          }
        }

        // Mark this node as tested
        testedNodes.add(node);
        nodeQueue.delete(node);
      }

      // After completing a batch of quadrants, test rays that were hit for the closest collision
      for ( let r of rayQueue ) {
        if ( !r._cs ) continue;
        const closest = this._getClosestCollision([...r._cs.values()]);
        if ( closest && closest.nodes.every(n => testedNodes.has(n)) ) {
          rayQueue.delete(r);
          r._c = closest;
          r.collisions = [closest];
        }
      }

      // If all rays have been hit we are done, otherwise expand the test frame
      if ( !rayQueue.size ) break;
      while ( (nodeQueue.size === 0) && (testedNodes.size < quadMap.size) ) {
        testFrame.pad(quadSize);
        for ( let a of quadtree.getLeafNodes(testFrame)) {
          if (!testedNodes.has(a)) nodeQueue.add(a);
        }
      }
    }

    // Construct visibility polygons
    const losPoints = [];
    for ( let r of rays ) {
      const c = r._c || { x: r.B.x, y: r.B.y, t0: 1, t1: 0};
      losPoints.push(c.x, c.y);
    }
    this.points = losPoints;
  }

  /* -------------------------------------------- */
  /*  Helper Methods                              */
  /* -------------------------------------------- */

  /**
   * A helper method responsible for casting rays at wall endpoints.
   * Rays are restricted by limiting angles.
   *
   * @param {number} x                  The origin x-coordinate
   * @param {number} y                  The origin y-coordinate
   * @param {number} distance           The ray distance
   * @param {number} density            The desired radial density
   * @param {PointArray[]} endpoints    An array of endpoints to target
   * @param {boolean} limitAngle        Whether the rays should be cast subject to a limited angle of emission
   * @param {number} aMin               The minimum bounding angle
   * @param {number} aMax               The maximum bounding angle
   *
   * @returns {Ray[]}                   An array of Ray objects
   */
  _castRays(x, y, distance, {density=4, endpoints, limitAngle=false, aMin, aMax}={}) {
    const rOffset = 0.02;

    // Enforce that all rays increase in angle from minimum towards maximum
    const rMin = limitAngle ? SightRay.fromAngle(x, y, aMin, distance) : null;
    const rMax = limitAngle ? SightRay.fromAngle(x, y, aMax, distance) : null;

    // Define de-duping casting function
    const cast = (ray, filter=false) => {
      const a = ray.angle.toNearest(rOffset);
      if ( filter && angles.has(a) ) return;
      rays.push(ray);
      angles.add(a);
    };

    // Track rays and unique emission angles
    const angles = new Set();
    const rays = [];

    // First prioritize rays which are cast directly at wall endpoints
    for ( let e of endpoints ) {
      let angle = Math.atan2(e[1]-y, e[0]-x);
      if ( limitAngle ) {
        angle = this._normalizeAngle(aMin, angle);
        if ( !angle.between(aMin, aMax) ) continue;
      }
      const ray = SightRay.fromAngle(x, y, angle, distance);
      cast(ray, false);
    }

    // Next cast rays at any non-duplicate offset angles
    const nr = rays.length;
    for ( let i=0; i<nr; i++ ) {
      const r = rays[i];
      cast(r.shiftAngle(rOffset), true);
      cast(r.shiftAngle(-rOffset), true);
    }

    // Add additional limiting and central rays
    if ( limitAngle ) {
      const aCenter = aMin + ((aMax - aMin) / 2) + Math.PI;
      const rCenter = SightRay.fromAngle(x, y, aCenter, 0);
      rCenter._isCenter = true;
      cast(rMin, true);
      cast(rCenter, true);
      cast(rMax, true);
    }

    // Add additional approximate rays to reach a desired radial density
    if ( !!density ) {
      const rDensity = Math.toRadians(density);
      const nFill = Math.ceil((aMax - aMin) / rDensity);
      for ( let a of Array.fromRange(nFill) ) {
        cast(SightRay.fromAngle(x, y, aMin + (a * rDensity), distance), true);
      }
    }

    // Sort rays counter-clockwise (increasing radians)
    rays.sort((r1, r2) => r1.angle - r2.angle);
    return rays;
  }

  /* -------------------------------------------- */

  /**
   * Test a single Ray against a single Wall
   * @param {Ray} ray                 The Ray being tested
   * @param {Wall} wall               The Wall against which to test
   * @return {RayIntersection|null}   A RayIntersection if a collision occurred, or null
   */
  _testWall(ray, wall) {
    let i = ray.intersectSegment(wall.data.c);
    return ( i && i.t0 > 0 ) ? i : null;
  }

  /* -------------------------------------------- */

  /**
   * Identify the closest collision point from an array of collisions
   * @param {RayIntersection[]} collisions  An array of intersection points
   * @return {RayIntersection|null}         The closest blocking intersection or null if no collision occurred
   */
  _getClosestCollision(collisions) {
    if ( !collisions.length ) return null;
    collisions.sort((a, b) => a.t0 - b.t0);
    let closest = ( collisions[0].type === CONST.WALL_SENSE_TYPES.LIMITED ) ? collisions[1] : collisions[0];
    return closest || null;
  }

  /* -------------------------------------------- */

  /**
   * Normalize an angle to ensure it is baselined to be the smallest angle that is greater than a minimum.
   * @param {number} aMin       The lower-bound minimum angle
   * @param {number} angle      The angle to adjust
   * @return {number}           The adjusted angle which is greater than or equal to aMin.
   * @private
   */
  _normalizeAngle(aMin, angle) {
    while ( angle < aMin ) {
      angle += (2*Math.PI);
    }
    return angle;
  }
}
