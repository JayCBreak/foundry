/**
 * A helper class which manages the refresh workflow for perception layers on the canvas.
 * This controls the logic which batches multiple requested updates to minimize the amount of work required.
 * A singleton instance is available as canvas#perception.
 * @see {Canvas#perception}
 */
class PerceptionManager {
  constructor() {
    this._reset();
  }

  /**
   * The number of milliseconds by which to throttle non-immediate refreshes
   * @type {number}
   * @private
   */
  _throttleMS

  /**
   * An internal tracker for the last time that a perception refresh was executed
   * @type {number}
   * @private
   */
  _refreshTime;

  /**
   * An internal tracker for the window timeout that applies a debounce to the refresh
   * @type {number}
   * @private
   */
  _timeout;

  /**
   * Cache a reference to the canvas scene to avoid attempting scheduled refreshes after the scene is changed
   * @type {string}
   * @private
   */
  _scene;

  /**
   * The default values of update parameters.
   * When a refresh occurs, the staged parameters are reset to these initial values.
   * @type {object}
   */
  static DEFAULTS = {
    lighting: {
      initialize: false,
      refresh: false
    },
    sight: {
      initialize: false,
      refresh: false,
      skipUpdateFog: false,
      forceUpdateFog: false
    },
    sounds: {
      initialize: false,
      refresh: false,
      fade: false
    },
    foreground: {
      refresh: false
    }
  }

  /**
   * The configured parameters for the next refresh.
   * @type {object}
   */
  params = {}

  /* -------------------------------------------- */

  /**
   * Cancel any pending perception refresh.
   */
  cancel() {
    if ( this._timeout ) window.clearTimeout(this._timeout);
    this._timeout = undefined;
    this._refreshTime = undefined;
  }

  /* -------------------------------------------- */

  /**
   * Schedule a perception update with requested parameters.
   * @param {Object} options
   */
  schedule(options={}) {
    this._set(options);
    this._update(false);
  }

  /* -------------------------------------------- */

  /**
   * Perform an immediate perception update.
   */
  update(options={}) {
    this._set(options);
    this._update(true);
  }

  /* -------------------------------------------- */

  /**
   * A helper function to perform an immediate initialization plus incremental refresh.
   */
  initialize() {
    return this.update({
      lighting: {initialize: true, refresh: true},
      sight: {initialize: true, refresh: true},
      sounds: {initialize: true, refresh: true},
      foreground: {refresh: true},
    });
  }

  /* -------------------------------------------- */

  /**
   * A helper function to perform an incremental refresh only.
   */
  refresh() {
    return this.update({
      lighting: {refresh: true},
      sight: {refresh: true},
      sounds: {refresh: true},
      foreground: {refresh: true},
    });
  }

  /* -------------------------------------------- */
  /*  Internal Helpers                            */
  /* -------------------------------------------- */

  /**
   * Set option flags which configure the next perception update
   * @param {object} options
   * @private
   */
  _set(options) {
    for ( let [layer, params] of Object.entries(options) ) {
      for ( let [k, v] of Object.entries(params) ) {
        this.params[layer][k] = Boolean(this.params[layer][k] + v);
      }
    }
    if ( this._throttleMS === undefined ) this._throttleMS = Math.round(1000 / (canvas.app.ticker.maxFPS || 60));
  }

  /* -------------------------------------------- */

  /**
   * Perform the perception update workflow
   * @param {boolean} immediate     Perform the workflow immediately, otherwise it is throttled
   * @private
   */
  _update(immediate=false) {

    // Apply both throttle and debounce
    const t = Date.now();
    if ( !immediate ) {
      const d = this._refreshTime ? t - this._refreshTime : 0;
      if ( !this._timeout ) {
        this._timeout = window.setTimeout(() => this._update(), this._throttleMS - d);
        this._refreshTime = t;
        return;
      }
      else if ( d < this._throttleMS ) return;
    }
    this.cancel();

    // When an update occurs, immediately reset refresh parameters
    const p = this.params;
    this._reset();
    if ( !canvas.ready ) return;

    // Update roof occlusion states based on token positions
    if ( p.foreground.refresh ) canvas.foreground.updateOcclusion();

    // Initialize perception sources for each layer
    if ( p.lighting.initialize ) canvas.lighting.initializeSources();
    if ( p.sight.initialize ) canvas.sight.initializeSources();
    if ( p.sounds.initialize ) canvas.sounds.initializeSources();

    // Refresh the display of roof tiles
    if ( p.foreground.refresh ) canvas.foreground.refresh();

    // Next refresh lighting to establish the coloration channels for the Scene
    if ( p.lighting.refresh ) canvas.lighting.refresh();

    // Next refresh vision and fog of war
    if ( p.sight.refresh ) canvas.sight.refresh({
      skipUpdateFog: p.sight.skipUpdateFog,
      forceUpdateFog: p.sight.forceUpdateFog
    });

    // Lastly update the playback of ambient sounds
    if ( p.sounds.refresh ) canvas.sounds.refresh({fade: p.sounds.fade ? 250 : 0});
  }

  /* -------------------------------------------- */

  /**
   * Reset the values of a pending refresh back to their default states.
   */
  _reset() {
    this.params = foundry.utils.deepClone(this.constructor.DEFAULTS);
  }
}
