/**
 * The virtual tabletop environment is implemented using a WebGL powered HTML 5 canvas using the powerful PIXI.js
 * library. The canvas is comprised of an ordered sequence of layers which define rendering groups and collections of
 * objects that are drawn on the canvas itself.
 *
 * @see {@link CanvasLayer} An abstract class for all Canvas layers.
 * @see {@link PlaceablesLayer} An abstract class for Canvas Layers which contain Placeable Objects.
 * @see {@link PlaceableObject} An abstract class for objects which are placed into the Scene and drawn on the canvas.
 *
 * @property {PrimaryCanvasGroup} primary
 * @property {EffectsCanvasGroup} effects
 * @property {InterfaceCanvasGroup} interface
 *
 * @property {BackgroundLayer} background
 * @property {DrawingsLayer} drawings
 * @property {GridLayer} grid
 * @property {WallsLayer} walls
 * @property {TemplateLayer} templates
 * @property {NotesLayer} notes
 * @property {TokenLayer} tokens
 * @property {LightingLayer} lighting
 * @property {ForegroundLayer} foreground
 * @property {SoundsLayer} sounds
 * @property {SightLayer} sight
 * @property {WeatherLayer} weather
 * @property {ControlsLayer} controls
 *
 * @example <caption>Example Canvas commands</caption>
 * canvas.ready; // Is the canvas ready for use?
 * canvas.scene; // The currently viewed Scene document.
 * canvas.dimensions; // The dimensions of the current Scene.
 * canvas.draw(); // Completely re-draw the game canvas (this is usually unnecessary).
 * canvas.pan(x, y, zoom); // Pan the canvas to new coordinates and scale.
 * canvas.recenter(); // Re-center the canvas on the currently controlled Token.
 */
class Canvas {
  constructor() {

    /**
     * A reference to the currently displayed Scene document, or null if the Canvas is currently blank.
     * @type {Scene|null}
     */
    this.scene = null;

    /**
     * The current pixel dimensions of the displayed Scene, or null if the Canvas is blank.
     * @type {null|object}
     */
    this.dimensions = null;

    /**
     * A reference to the HeadsUpDisplay container which overlays HTML rendering on top of this Canvas.
     * @type {HeadsUpDisplay|null}
     */
    this.hud = null;

    /**
     * An Array of pending canvas operations which should trigger on the next re-paint
     * @type {object[]}
     */
    this.pendingOperations = [];

    /**
     * A perception manager interface for batching lighting, sight, and sound updates
     * @type {PerceptionManager}
     */
    this.perception = new PerceptionManager();

    /**
     * A flag for whether the game Canvas is ready to be used. False if the canvas is not yet drawn, true otherwise.
     * @type {boolean}
     */
    this.ready = false;

    /**
     * A flag to indicate whether a new Scene is currently being drawn.
     * @type {boolean}
     */
    this.loading = false;

    /**
     * A flag for whether the game Canvas is initialized and ready for drawing.
     * @type {boolean}
     */
    this.initialized = false;

    /**
     * A promise that resolves when the canvas is first initialized and ready.
     * @type {Promise<void>|null}
     */
    this.initializing = null;

    /**
     * Track the timestamp of the last stage zoom operation
     * @type {number}
     * @private
     */
    this._zoomTime = 0;

    /**
     * Track the last automatic pan time to throttle
     * @type {number}
     * @private
     */
    this._panTime = 0;

    /**
     * An object of data which is temporarily cached to be reloaded after the canvas is drawn
     * @type {Object}
     * @private
     */
    this._reload = {};

    /**
     * A Set of unique pending operation names to ensure operations are only performed once
     * @type {Set<string>}
     */
    this._pendingOperationNames = new Set();
  }

  /**
   * The pixel radius of blur distance that should be applied for the current zoom level
   * @type {number}
   */
  blurDistance = 0;

  /**
   * An array of blur filter instances which are modified by the zoom level and the "soft shadows" setting
   * @type {PIXI.filters.BlurFilter[]}
   */
  blurFilters = [];

  /**
   * A reference to the MouseInteractionManager that is currently controlling pointer-based interaction, or null.
   * @type {MouseInteractionManager|null}
   */
  currentMouseManager = null;

  /**
   * Record framerate performance data
   * @type {{average: number, last: number, values: number[], element: HTMLElement, fn: Function, render: number}}
   */
  fps = {
    values: [],
    average: 0,
    render: 0,
    element: document.getElementById("fps")
  }

  /**
   * The singleton interaction manager instance which handles mouse interaction on the Canvas.
   * @type {MouseInteractionManager}
   */
  mouseInteractionManager;

  /**
   * The renderer screen dimensions.
   * @type {number[]}
   */
  screenDimensions = [0, 0];

  /* -------------------------------------------- */

  /**
   * Initialize the Canvas by creating the HTML element and PIXI application.
   * This step should only ever be performed once per client session.
   * Subsequent requests to reset the canvas should go through Canvas#draw
   */
  initialize() {

    // If the game canvas is disabled by "no canvas" mode, we don't need to initialize anything
    if ( game.settings.get("core", "noCanvas") ) return;

    // Verify that WebGL is available
    if ( !PIXI.utils.isWebGLSupported() ) {
      const err = new Error(game.i18n.localize("ERROR.NoWebGL"));
      ui.notifications.error(err.message, {permanent: true});
      throw err;
    }
    PIXI.settings.PREFER_ENV = PIXI.ENV.WEBGL2;

    // Replace the placeholder "board" template with a canvas
    const board = document.getElementById("board");
    const canvas = document.createElement("canvas");
    canvas.id = "board";
    canvas.style.display = "none";
    board.replaceWith(canvas);

    // Configure canvas settings
    const resolution = game.settings.get("core", "disableResolutionScaling") ? 1 : window.devicePixelRatio;
    const canvasConfig = {
      view: canvas,
      width: window.innerWidth,
      height: window.innerHeight,
      transparent: false,
      resolution: resolution,
      autoDensity: true,
      backgroundColor: null,
      antialias: false,  // Not needed because we use SmoothGraphics
      powerPreference: "high-performance" // Prefer high performance GPU for devices with dual graphics cards
    };

    /**
     * A hook event that fires immediately prior to PIXI Application construction with the configuration parameters.
     * @function canvasConfig
     * @memberof hookEvents
     * @param {object} canvasConfig   Canvas configuration parameters that will be used
     */
    Hooks.callAll("canvasConfig", canvasConfig);

    // Create the PIXI Application
    this.app = new PIXI.Application(canvasConfig);
    this.app.renderer.plugins.interaction.moveWhenInside = true;
    this._displayPerformanceWarnings();

    // Define the Stage
    this.stage = this.app.stage;
    Object.defineProperty(this.stage.constructor, 'name', {value: `CanvasStage`, writable: false});

    // Register custom blend modes
    for ( let [k,v] of Object.entries(BLEND_MODES) ) {
      PIXI.BLEND_MODES[k] = this.app.renderer.state.blendModes.push(v) - 1;
    }

    // Activate drop handling
    this._dragDrop = new DragDrop({ callbacks: { drop: this._onDrop.bind(this) } }).bind(canvas);

    // Create Canvas groups and interface elements
    this.hud = new HeadsUpDisplay();
    this.outline = this.stage.addChildAt(new PIXI.Graphics(), 0);
    this.msk = this.stage.addChild(new PIXI.LegacyGraphics());
    this._createGroups();

    // Update state flags
    this.scene = null;
    this.initialized = true;
    this.ready = false;
  }

  /* -------------------------------------------- */

  /**
   * Display warnings for known performance issues which may occur due to the user's hardware or browser configuration
   * @private
   */
  _displayPerformanceWarnings() {
    const context = this.app.renderer.context;
    const gl = context.gl;
    try {
      const rendererInfo = SupportDetails.getWebGLRendererInfo(gl);
      if ( /swiftshader/i.test(rendererInfo) ) {
        ui.notifications.warn("ERROR.NoHardwareAcceleration", {localize: true, permanent: true});
      }
    } catch(err) {
      ui.notifications.warn("ERROR.RendererNotDetected", {localize: true});
    }

    // Verify that WebGL2 is being used
    if (context.webGLVersion !== 2 ) {
      const err = new Error(game.i18n.localize("ERROR.NoWebGL2"));
      ui.notifications.warn(err.message, {permanent: true});
    }
  }

  /* -------------------------------------------- */
  /*  Properties and Attributes
  /* -------------------------------------------- */

  /**
   * The id of the currently displayed Scene.
   * @type {string|null}
   */
  get id() {
    return this.scene?.id || null;
  }

  /* -------------------------------------------- */

  /**
   * A mapping of named CanvasLayer classes which defines the layers which comprise the Scene.
   * @type {Object<string, CanvasLayer>}
   */
  static get layers() {
    return CONFIG.Canvas.layers;
  }

  /* -------------------------------------------- */

  /**
   * An Array of all CanvasLayer instances which are active on the Canvas board
   * @type {CanvasLayer[]}
   */
  get layers() {
    return Object.keys(this.constructor.layers).map(k => this[k]);
  }

  /* -------------------------------------------- */

  /**
   * Return a reference to the active Canvas Layer
   * @type {CanvasLayer}
   */
  get activeLayer() {
    for ( let name of Object.keys(this.constructor.layers) ) {
      const layer = this[name];
      if ( layer?._active ) return layer;
    }
    return null;
  }

  /* -------------------------------------------- */
  /*  Rendering
  /* -------------------------------------------- */

  /**
   * Initialize the group containers of the game Canvas.
   * @private
   */
  _createGroups() {

    // Create Groups
    for ( let [name, config] of Object.entries(CONFIG.Canvas.groups) ) {
      const group = new config.groupClass();
      Object.defineProperty(this, name, { value: group, writable: false} );
      this.stage.addChild(group);
    }

    // Reference Layers
    for ( let [name, config] of Object.entries(CONFIG.Canvas.layers) ) {
      const group = this[config.group];
      Object.defineProperty(this, name, { value: group[name], writable: false} );
    }
  }

  /* -------------------------------------------- */

  /**
   * When re-drawing the canvas, first tear down or discontinue some existing processes
   * @return {Promise<void>}
   */
  async tearDown() {
    this.stage.visible = false;

    // Track current data which should be restored on draw
    this._reload = {
      scene: this.scene.id,
      layer: this.activeLayer?.options.name,
      controlledTokens: Object.keys(this.tokens._controlled),
      targetedTokens: Array.from(game.user.targets).map(t => t.id)
    };

    // Cancel framerate tracking
    this.deactivateFPSMeter();

    // Cancel culling
    this.app.renderer.off("prerender");

    // Cancel perception refresh
    this.perception.cancel();

    // Deactivate every layer before teardown
    for ( let l of this.layers.reverse() ) {
      l.deactivate();
    }

    // Tear down every layer
    for ( let l of this.layers.reverse() ) {
      await l.tearDown();
    }

    // Discard shared filters
    this.blurFilters = [];
  }

  /* -------------------------------------------- */

  /**
   * Draw the game canvas.
   * @param {Scene} [scene]         A specific Scene document to render on the Canvas
   * @return {Promise<Canvas>}      A Promise which resolves once the Canvas is fully drawn
   */
  async draw(scene) {

    // If the canvas had not yet been initialized, we have done something out of order
    if ( !this.initialized ) {
      throw new Error("You may not call Canvas#draw before Canvas#initialize");
    }

    // Reference the active scene
    scene = scene ?? game.scenes.current ?? null;
    const wasReady = this.ready;
    this.ready = false;
    this.stage.visible = false;
    this.loading = true;

    try {
      // Tear down any existing scene
      if ( wasReady ) await this.tearDown();

      // Confirm there is an active scene
      this.scene = scene;
      if ( this.scene === null ) {
        console.log(`${vtt} | Skipping game canvas - no active scene.`);
        canvas.app.view.style.display = "none";
        ui.controls.render();
        return this;
      } else if ( !(scene instanceof Scene) ) {
        throw new Error("You must provide a Scene document to draw the VTT canvas.")
      }

      // Configure Scene variables
      this.dimensions = this.constructor.getDimensions(scene.data);
      canvas.app.view.style.display = "block";
      document.documentElement.style.setProperty("--gridSize", this.dimensions.size+"px");

      // Configure performance settings
      this.performance = this._configurePerformanceMode();
      PIXI.settings.MIPMAP_TEXTURES = PIXI.MIPMAP_MODES[this.performance.mipmap];
      PIXI.settings.FILTER_RESOLUTION = canvas.app.renderer.resolution;
      this.blurDistance = this.performance.blur.enabled ? CONFIG.Canvas.blurStrength : 0;
      this.app.ticker.maxFPS = this.performance.fps;
      PIXI.Ticker.shared.maxFPS = this.performance.fps;
      PIXI.Ticker.system.maxFPS = this.performance.fps;

      // Call initialization hooks
      console.log(`${vtt} | Drawing game canvas for scene ${this.scene.name}`);
      /**
       * A hook event that fires when the Canvas is initialized.
       * @function canvasInit
       * @memberof hookEvents
       * @param {Canvas} canvas The canvas
       */
      Hooks.callAll('canvasInit', this);

      // Configure primary canvas stage
      this.stage.position.set(window.innerWidth / 2, window.innerHeight / 2);
      this.stage.hitArea = new PIXI.Rectangle(0, 0, this.dimensions.width, this.dimensions.height);
      this.stage.interactive = true;
      this.stage.sortableChildren = true;

      // Temporary workaround until "Scene Levels" are developed
      this.background.bgPath = this.scene.data.img;
      this.foreground.bgPath = this.scene.data.foreground;

      // Load required textures
      await TextureLoader.loadSceneTextures(this.scene);

      // Set scene background color and canvas outline
      this.setBackgroundColor(scene.data.backgroundColor);
      this.background.drawOutline(this.outline);

      // Draw layers
      for ( let l of this.layers ) {
        try {
          await l.draw();
        } catch(err) {
          Hooks.onError("Canvas#draw", err, {
            msg: `Canvas drawing failed for the ${l.name}`,
            log: "error",
            notify: "error",
            layer: l
          });
        }
      }

      // Mask primary and effects layers by the overall canvas
      this.msk.clear().beginFill(0xFFFFFF, 1.0).drawShape(this.dimensions.rect).endFill();
      this.primary.mask = this.msk;
      this.effects.mask = this.msk;

      // Initialize starting conditions
      this.ready = true;
      await this._initialize();
      this._addListeners();

      /**
       * A hook event that fires when the Canvas is ready.
       * @function canvasReady
       * @memberof hookEvents
       * @param {Canvas} canvas The canvas
       */
      Hooks.call("canvasReady", this);
      this._reload = {};

      // Perform a final resize to ensure the rendered dimensions are correct
      this._onResize();
      this.stage.visible = true;
    } finally {
      this.loading = false;
    }

    return this;
  }

  /* -------------------------------------------- */

  /**
   * Get the value of a GL parameter
   * @param {string} parameter The GL parameter to retrieve
   * @returns {any}            The returned value type depends of the parameter to retrieve
   */
  getGLParameter(parameter) {
    const gl = this.app.renderer.context.gl;
    return gl.getParameter(gl[parameter]);
  }

  /* -------------------------------------------- */

  /**
   * Get the canvas active dimensions based on the size of the scene's map.
   * We expand the image size by a factor of 1.5 and round to the nearest 2x grid size.
   * The rounding accomplishes that the padding buffer around the map always contains whole grid spaces.
   * @see {@link documents.BaseScene.getDimensions}
   * @param {object} dimensions     The scene dimensions data being established
   */
  static getDimensions(dimensions) {
    dimensions.size = dimensions.grid;
    const d = Scene.getDimensions(dimensions);
    d.rect = new PIXI.Rectangle(0, 0, d.width, d.height);
    d.sceneRect = new PIXI.Rectangle(d.paddingX-d.shiftX, d.paddingY-d.shiftY, d.sceneWidth, d.sceneHeight);
    d.maxR = Math.hypot(d.width, d.height);
    return d;
  }

  /* -------------------------------------------- */

  /**
   * Configure performance settings for hte canvas application based on the selected performance mode
   * @returns {object}
   * @private
   */
  _configurePerformanceMode() {
    const modes = CONST.CANVAS_PERFORMANCE_MODES;

    // Get client settings
    let mode = game.settings.get("core", "performanceMode");
    const fps = game.settings.get("core", "maxFPS");
    const mip = game.settings.get("core", "mipmap");

    // Get GPU information
    const gl = this.app.renderer.context.gl;
    const maxTextureSize = gl.getParameter(gl.MAX_TEXTURE_SIZE);

    // Configure default performance mode if one is not set
    if ( !Number.isFinite(mode) || (mode === -1) ) {
      if ( maxTextureSize <= Math.pow(2, 12) ) mode = CONST.CANVAS_PERFORMANCE_MODES.LOW;
      else if ( maxTextureSize <= Math.pow(2, 13) ) mode = CONST.CANVAS_PERFORMANCE_MODES.MED;
      else if ( maxTextureSize <= Math.pow(2, 14) ) mode = CONST.CANVAS_PERFORMANCE_MODES.HIGH;
      game.settings.storage.get("client").setItem("core.performanceMode", String(mode));
    }

    // Construct performance settings object
    const settings = {
      mode: mode,
      blur: {
        enabled: false,
        illumination: false
      },
      mipmap: mip ? "ON" : "OFF",
      msaa: false,
      fps: Math.clamped(fps, 0, 60),
      tokenAnimation: true,
      lightAnimation: true,
      textures: {
        enabled: false,
        maxSize: maxTextureSize,
        p2Steps: 2,
        p2StepsMax: 3
      }
    }

    // Low settings
    if ( mode >= modes.LOW ) {
      settings.tokenAnimation = false;
      settings.lightAnimation = false;
    }

    // Medium settings
    if ( mode >= modes.MED ) {
      settings.blur.enabled = true;
      settings.textures.enabled = true;
      settings.textures.p2Steps = 3;
    }

    // High settings
    if ( mode >= modes.HIGH ) {
      settings.blur.illumination = true;
      settings.textures.p2Steps = 2;
    }

    // Max settings
    if ( mode === modes.MAX ) {
      settings.textures.p2Steps = 1;
      if ( settings.fps === 60 ) settings.fps = 0;
    }
    return settings;
  }

  /* -------------------------------------------- */

  /**
   * Once the canvas is drawn, initialize control, visibility, and audio states
   * @return {Promise<void>}
   */
  async _initialize() {

    // Clear the set of targeted Tokens for the current user
    game.user.targets.clear();

    // Render the HUD layer
    this.hud.render(true);

    // Initialize starting layer conditions
    await this.sight.initializeFog();  // Sight layer starting conditions
    canvas.walls.initialize();         // Wall endpoints and interior walls
    this.perception.initialize();      // Vision, lighting, sound, overhead tiles

    // Initialize canvas conditions
    this._initializeCanvasPosition();
    this._initializeCanvasLayer();
    this._initializeTokenControl();

    // If the player has no vision tokens in a visibility-restricted scene, display a warning on a slight delay
    if ( this.sight.tokenVision && !game.user.isGM && !this.sight.sources.size ) {
      ui.notifications.warn("TOKEN.WarningNoVision", {localize: true});
    }

    // Broadcast user presence in the Scene
    game.user.broadcastActivity({sceneId: this.scene.id});
  }

  /* -------------------------------------------- */

  /**
   * Initialize the starting view of the canvas stage
   * If we are re-drawing a scene which was previously rendered, restore the prior view position
   * Otherwise set the view to the top-left corner of the scene at standard scale
   * @private
   */
  _initializeCanvasPosition() {

    // If we are re-drawing a Scene that was already visited, use it's cached view position
    let position = this.scene._viewPosition;

    // Use a saved position, or determine the default view based on the scene size
    if ( foundry.utils.isObjectEmpty(position) ) {
      const initial = this.scene.data.initial;
      const r = this.dimensions.sceneRect;
      position = initial ?? {
        x: r.x + this.stage.position.x,
        y: r.y + this.stage.position.y,
        scale: 1
      }
    }

    // Set initial blur strength
    this.updateBlur(position.scale);

    // Pan to the initial view
    this.pan(position);
  }

  /* -------------------------------------------- */

  /**
   * Initialize a CanvasLayer in the activation state
   * @private
   */
  _initializeCanvasLayer() {
    const layer = this[this._reload.layer] ?? this.tokens;
    layer.activate();
  }

  /* -------------------------------------------- */

  /**
   * Initialize a token or set of tokens which should be controlled.
   * Restore controlled and targeted tokens from before the re-draw.
   * @private
   */
  _initializeTokenControl() {
    let isReload = this._reload.scene === this.scene.id;
    let panToken = null;

    // Initial tokens based on reload data
    let controlledTokens = isReload ? this._reload.controlledTokens.map(id => canvas.tokens.get(id)) : [];
    if ( !isReload && !game.user.isGM ) {

      // Initial tokens based on primary character
      controlledTokens = game.user.character?.getActiveTokens() || [];

      // Based on owned actors
      if (!controlledTokens.length) {
        controlledTokens = canvas.tokens.placeables.filter(t => t.actor?.testUserPermission(game.user, "OWNER"));
      }

      // Based on observed actors
      if (!controlledTokens.length) {
        const observed = canvas.tokens.placeables.filter(t => t.actor?.testUserPermission(game.user, "OBSERVER"));
        panToken = observed.shift() || null;
      }
    }

    // Initialize Token Control
    for ( let token of controlledTokens ) {
      if ( !panToken ) panToken = token;
      token.control({releaseOthers: false});
    }

    // Initialize Token targets
    const targetedTokens = isReload ? this._reload.targetedTokens : [];
    for ( let id of targetedTokens ) {
      const token = canvas.tokens.get(id);
      token.setTarget(true, {releaseOthers: false, groupSelection: true});
    }

    // Pan camera to controlled token
    if ( panToken && !isReload ) this.pan({x: panToken.center.x, y: panToken.center.y, duration: 250});
  }

  /* -------------------------------------------- */

  /**
   * Given an embedded object name, get the canvas layer for that object
   * @param {string} embeddedName
   * @returns {PlaceablesLayer|null}
   */
  getLayerByEmbeddedName(embeddedName) {
    return {
      AmbientLight: this.lighting,
      AmbientSound: this.sounds,
      Drawing: this.drawings,
      Note: this.notes,
      MeasuredTemplate: this.templates,
      Tile: this.background, // Tiles may also appear in the foreground, but background is default
      Token: this.tokens,
      Wall: this.walls
    }[embeddedName] || null;
  }

  /* -------------------------------------------- */
  /*  Methods                                     */
  /* -------------------------------------------- */

  /**
   * Activate a specific CanvasLayer by its canonical name
   * @param {string} layerName      The named layer to activate
   */
  activateLayer(layerName) {
    this[layerName].activate();
  }

  /* -------------------------------------------- */

  /**
   * Activate framerate tracking by adding an HTML element to the display and refreshing it every frame.
   */
  activateFPSMeter() {
    this.deactivateFPSMeter();
    if ( !this.ready ) return;
    // Show element
    this.fps.element.style.display = "block";
    // Activate ticker
    this.app.ticker.add(this._measureFPS, this, PIXI.UPDATE_PRIORITY.LOW);
  }

  /* -------------------------------------------- */

  /**
   * Deactivate framerate tracking by canceling ticker updates and removing the HTML element.
   */
  deactivateFPSMeter() {
    // Deactivate ticker
    this.app.ticker.remove(this._measureFPS, this);
    // Hide element
    this.fps.element.style.display = "none";
  }

  /* -------------------------------------------- */

  /**
   * Measure average framerate per second over the past 30 frames
   * @private
   */
  _measureFPS() {
    const lastTime = this.app.ticker.lastTime;
 
    // Push fps values every frame
    this.fps.values.push(1000 / this.app.ticker.elapsedMS);
    if ( this.fps.values.length > 60 ) this.fps.values.shift();
    
    // Do some computations and rendering occasionally
    if ( (lastTime - this.fps.render) < 250 ) return;
    if ( !this.fps.element ) return;

    // Compute average fps
    const total = this.fps.values.reduce((fps, total) => total + fps, 0);
    this.fps.average = (total / this.fps.values.length);

    // Render it
    this.fps.element.innerHTML = `<label>FPS:</label> <span>${this.fps.average.toFixed(2)}</span>`;
    this.fps.render = lastTime;
  }

  /* -------------------------------------------- */

  /**
   * Pan the canvas to a certain {x,y} coordinate and a certain zoom level
   * @param {number|null} x      The x-coordinate of the pan destination
   * @param {number|null} y      The y-coordinate of the pan destination
   * @param {number|null} scale  The zoom level (max of CONFIG.Canvas.maxZoom) of the action
   */
  pan({x=null, y=null, scale=null}={}) {

    // Constrain the resulting canvas view
    const constrained = this._constrainView({x, y, scale});
    const scaleChange = constrained.scale !== this.stage.scale.x;

    // Set the pivot point
    this.stage.pivot.set(constrained.x, constrained.y);

    // Set the zoom level
    if ( scaleChange ) {
      this.stage.scale.set(constrained.scale, constrained.scale);
      this.updateBlur(constrained.scale);
    }

    // Update the scene tracked position
    canvas.scene._viewPosition = constrained;

    /**
     * A hook event that fires when the Canvas is panned.
     * @function canvasPan
     * @memberof hookEvents
     * @param {Canvas} canvas          The canvas
     * @param {object} transform       The applied translate/transform
     * @param {number} transform.x     The constrained x-coordinate of the pan
     * @param {number} transform.y     The constrained y-coordinate of the pan
     * @param {number} transform.scale The constrained zoom level of the pan
     */
    Hooks.callAll("canvasPan", this, constrained);

    // Align the HUD
    this.hud.align();
  }

  /* -------------------------------------------- */

  /**
   * Animate panning the canvas to a certain destination coordinate and zoom scale
   * Customize the animation speed with additional options
   * Returns a Promise which is resolved once the animation has completed
   *
   * @param {object} view         The desired view parameters
   * @param {number} [view.x]            The destination x-coordinate
   * @param {number} [view.y]            The destination y-coordinate
   * @param {number} [view.scale]        The destination zoom scale
   * @param {number} [view.duration=250] The total duration of the animation in milliseconds; used if speed is not set
   * @param {number} [view.speed]        The speed of animation in pixels per second; overrides duration if set
   * @returns {Promise}           A Promise which resolves once the animation has been completed
   */
  async animatePan({x, y, scale, duration=250, speed}={}) {

    // Determine the animation duration to reach the target
    if ( speed ) {
      let ray = new Ray(this.stage.pivot, {x, y});
      duration = Math.round(ray.distance * 1000 / speed);
    }

    // Constrain the resulting dimensions and construct animation attributes
    const constrained = this._constrainView({x, y, scale});
    const attributes = [
      { parent: this.stage.pivot, attribute: 'x', to: constrained.x },
      { parent: this.stage.pivot, attribute: 'y', to: constrained.y },
      { parent: this.stage.scale, attribute: 'x', to: constrained.scale },
      { parent: this.stage.scale, attribute: 'y', to: constrained.scale },
    ].filter(a => a.to !== undefined);

    // Trigger the animation function
    const animation = await CanvasAnimation.animateLinear(attributes, {
      name: "canvas.animatePan",
      duration: duration,
      ontick: () => {
        this.hud.align();
        const stage = this.stage;
        Hooks.callAll("canvasPan", this, {x: stage.pivot.x, y: stage.pivot.y, scale: stage.scale.x});
      }
    });

    // Record final values
    this.updateBlur(constrained.scale);
    canvas.scene._viewPosition = constrained;
    return animation;
  }

  /* -------------------------------------------- */

  /**
   * Recenter the canvas
   * Otherwise, pan the stage to put the top-left corner of the map in the top-left corner of the window
   * @returns {Promise<void>}     A Promise which resolves once the animation has been completed
   */
  async recenter(coordinates) {
    if ( coordinates ) this.pan(coordinates);
    const r = this.dimensions.sceneRect;
    return this.animatePan({
      x: r.x + (window.innerWidth / 2),
      y: r.y + (window.innerHeight / 2),
      duration: 250
    });
  }

  /* -------------------------------------------- */

  /**
   * Highlight objects on any layers which are visible
   * @param {boolean} active
   */
  highlightObjects(active) {
    if ( !this.ready ) return;

    // Highlight placeable objects on any layers which are visible
    for ( let layer of this.layers ) {
      if ( !layer.objects || !layer.interactiveChildren ) continue;

      // Take note of the currently hovered object, if any
      const currentHover = layer._hover;

      // Modify the hover state of objects
      for ( let o of layer.placeables ) {
        if ( !o.visible || (o === currentHover) || !o.can(game.user, "hover") ) continue;

        // Mock the PIXI.InteractionEvent
        const event = new PIXI.InteractionEvent();
        event.type = active ? "mouseover" : "mouseout";
        event.currentTarget = event.target = o;

        // Call the onHover behavior
        if ( active ) o._onHoverIn(event, {hoverOutOthers: false});
        else o._onHoverOut(event);
      }

      // Re-reference the pre-existing hover target
      layer._hover = currentHover;
    }
  }

  /* -------------------------------------------- */

  /**
   * Get the constrained zoom scale parameter which is allowed by the maxZoom parameter
   * @param {number} x              The requested x-coordinate
   * @param {number} y              The requested y-coordinate
   * @param {number} scale          The requested scale
   * @return {{x, y, scale}}        The allowed scale
   * @private
   */
  _constrainView({x, y, scale}) {
    const d = canvas.dimensions;

    // Constrain the maximum zoom level
    if ( Number.isNumeric(scale) && (scale !== this.stage.scale.x) ) {
      const max = CONFIG.Canvas.maxZoom;
      const ratio = Math.max(d.width / window.innerWidth, d.height / window.innerHeight, max);
      scale = Math.round(Math.clamped(scale, 1 / ratio, max) * 100) / 100;
    } else {
      scale = this.stage.scale.x;
    }

    // Constrain the pivot point using the new scale
    if ( Number.isNumeric(x) && x !== this.stage.pivot.x ) {
      const padw = 0.4 * (window.innerWidth / scale);
      x = Math.clamped(x, -padw, d.width + padw);
    }
    else x = this.stage.pivot.x;
    if ( Number.isNumeric(y) && y !== this.stage.pivot.y ) {
      const padh = 0.4 * (window.innerHeight / scale);
      y = Math.clamped(y, -padh, d.height + padh);
    }
    else y = this.stage.pivot.y;

    // Return the constrained view dimensions
    return {x, y, scale};
  }

  /* -------------------------------------------- */

  /**
   * Create a BlurFilter instance and register it to the array for updates when the zoom level changes.
   * @returns {PIXI.filters.BlurFilter}
   */
  createBlurFilter() {
    const f = new PIXI.filters.BlurFilter(this.blurDistance);
    this.blurFilters.push(f);
    return f;
  }

  /* -------------------------------------------- */

  /**
   * Update the blur strength depending on the scale of the canvas stage.
   * This number is zero if "soft shadows" are disabled
   * @param {number} [scale]
   * @private
   */
  updateBlur(scale) {
    scale = scale || this.stage.scale.x;
    if ( !this.performance.blur.enabled ) return; // Blur is disabled entirely
    this.blurDistance = Math.ceil(Math.clamped(scale * CONFIG.Canvas.blurStrength, 0, CONFIG.Canvas.blurStrength));
    for ( let f of this.blurFilters ) {
      f.blur = this.blurDistance;
    }
  }

  /* -------------------------------------------- */

  /**
   * Sets the background color.
   * @param {string} color  The color to set the canvas background to.
   */
  setBackgroundColor(color) {
    color = foundry.utils.colorStringToHex(color);
    this.app.renderer.backgroundColor = this.backgroundColor = color;
  }

  /* -------------------------------------------- */
  /* Event Handlers
  /* -------------------------------------------- */

  /**
   * Attach event listeners to the game canvas to handle click and interaction events
   * @private
   */
  _addListeners() {

    // Remove all existing listeners
    this.stage.removeAllListeners();

    // Define callback functions for mouse interaction events
    const callbacks = {
      clickLeft: this._onClickLeft.bind(this),
      clickLeft2: this._onClickLeft2.bind(this),
      clickRight: this._onClickRight.bind(this),
      clickRight2: null,
      dragLeftStart: this._onDragLeftStart.bind(this),
      dragLeftMove: this._onDragLeftMove.bind(this),
      dragLeftDrop: this._onDragLeftDrop.bind(this),
      dragLeftCancel: this._onDragLeftCancel.bind(this),
      dragRightStart: null,
      dragRightMove: this._onDragRightMove.bind(this),
      dragRightDrop: this._onDragRightDrop.bind(this),
      dragRightCancel: null
    };

    // Create and activate the interaction manager
    const permissions = { clickRight2: false };
    const mgr = new MouseInteractionManager(this.stage, this.stage, permissions, callbacks);
    this.mouseInteractionManager = mgr.activate();

    // Debug average FPS
    if ( game.settings.get("core", "fpsMeter") ) this.activateFPSMeter();

    // Add a listener for cursor movement
    this.stage.on("mousemove", this._onMouseMove.bind(this));

    // Cull the visibility of objects
    const cullingBackend = CONFIG.Canvas.cullingBackend;
    if ( cullingBackend !== null ) {
      console.log(`${vtt} | Initializing Canvas culling using ${cullingBackend.name} backend`);
      const cull = new cullingBackend();
      this.app.renderer.on("prerender", () => {
        cull.cull(this.app.renderer.screen);
      });
    }
  }

  /* -------------------------------------------- */

  /**
   * Handle normal mouse movement.
   * Throttle cursor position updates to 100ms intervals
   * @param {PIXI.InteractionEvent} event
   * @private
   */
  _onMouseMove(event) {
    const now = Date.now();
    if ( (now - event.data["_cursorTime"]) < 100 ) return;
    event.data["_cursorTime"] = now;
    canvas.controls._onMouseMove(event)
    canvas.sounds._onMouseMove(event);
  }

  /* -------------------------------------------- */

  /**
   * Handle left mouse-click events occurring on the Canvas stage or its active Layer.
   * @see {MouseInteractionManager#_handleClickLeft}
   * @param {PIXI.InteractionEvent} event
   * @private
   */
  _onClickLeft(event) {
    const layer = this.activeLayer;
    if ( layer instanceof PlaceablesLayer ) layer._onClickLeft(event);
  }

  /* -------------------------------------------- */

  /**
   * Handle double left-click events occurring on the Canvas stage.
   * @see {MouseInteractionManager#_handleClickLeft2}
   * @param {PIXI.InteractionEvent} event
   */
  _onClickLeft2(event) {
    const layer = this.activeLayer;
    if ( layer instanceof PlaceablesLayer ) layer._onClickLeft2(event);
  }

  /* -------------------------------------------- */

  /**
   * Handle the beginning of a left-mouse drag workflow on the Canvas stage or its active Layer.
   * @see {MouseInteractionManager#_handleDragStart}
   * @param {PIXI.InteractionEvent} event
   * @private
   */
  _onDragLeftStart(event) {

    // Extract event data
    const layer = this.activeLayer;
    const isRuler = game.activeTool === "ruler";
    const isCtrlRuler = game.keyboard.isModifierActive(KeyboardManager.MODIFIER_KEYS.CONTROL) && (layer instanceof TokenLayer);

    // Begin ruler measurement
    if ( isRuler || isCtrlRuler ) return this.controls.ruler._onDragStart(event);

    // Activate select rectangle
    const isSelect = ["select", "target"].includes(game.activeTool);
    if ( isSelect ) {
      canvas.controls.select.active = true;
      return;
    }

    // Dispatch the event to the active layer
    if ( layer instanceof PlaceablesLayer ) layer._onDragLeftStart(event);
  }

  /* -------------------------------------------- */

  /**
   * Handle mouse movement events occurring on the Canvas stage or it's active layer
   * @see {MouseInteractionManager#_handleDragMove}
   * @param {PIXI.InteractionEvent} event
   * @private
   */
  _onDragLeftMove(event) {
    const layer = this.activeLayer;

    // Pan the canvas if the drag event approaches the edge
    this._onDragCanvasPan(event.data.originalEvent);

    // Continue a Ruler measurement
    const ruler = this.controls.ruler;
    if ( ruler._state > 0 ) return ruler._onMouseMove(event);

    // Continue a select event
    const isSelect = ["select", "target"].includes(game.activeTool);
    if ( isSelect && canvas.controls.select.active ) return this._onDragSelect(event);

    // Dispatch the event to the active layer
    if ( layer instanceof PlaceablesLayer ) layer._onDragLeftMove(event);
  }

  /* -------------------------------------------- */

  /**
   * Handle the conclusion of a left-mouse drag workflow when the mouse button is released.
   * @see {MouseInteractionManager#_handleDragDrop}
   * @param {PIXI.InteractionEvent} event
   * @private
   */
  _onDragLeftDrop(event) {

    // Extract event data
    const {coords, originalEvent} = event.data;
    const tool = game.activeTool;
    const layer = canvas.activeLayer;

    // Conclude a measurement event if we aren't holding the CTRL key
    const ruler = canvas.controls.ruler;
    if ( ruler.active ) {
      if ( game.keyboard.isModifierActive(KeyboardManager.MODIFIER_KEYS.CONTROL) ) originalEvent.preventDefault();
      return ruler._onMouseUp(event);
    }

    // Conclude a select event
    const isSelect = ["select", "target"].includes(tool);
    if ( isSelect && canvas.controls.select.active ) {
      canvas.controls.select.clear();
      canvas.controls.select.active = false;
      if ( tool === "select" ) return layer.selectObjects(coords);
      if ( tool === "target" ) return layer.targetObjects(coords, {releaseOthers: !originalEvent.shiftKey});
    }

    // Dispatch the event to the active layer
    if ( layer instanceof PlaceablesLayer ) layer._onDragLeftDrop(event);
  }

  /* -------------------------------------------- */

  /**
   * Handle the cancellation of a left-mouse drag workflow
   * @see {MouseInteractionManager#_handleDragCancel}
   * @param {PointerEvent} event
   * @private
   */
  _onDragLeftCancel(event) {
    const layer = canvas.activeLayer;
    const tool = game.activeTool;

    // Don't cancel ruler measurement
    const ruler = canvas.controls.ruler;
    if ( ruler.active ) return event.preventDefault();

    // Clear selection
    const isSelect = ["select", "target"].includes(tool);
    if ( isSelect ) return canvas.controls.select.clear();

    // Dispatch the event to the active layer
    if ( layer instanceof PlaceablesLayer ) layer._onDragLeftCancel(event);
  }

  /* -------------------------------------------- */

  /**
   * Handle right mouse-click events occurring on the Canvas stage or it's active layer
   * @see {MouseInteractionManager#_handleClickRight}
   * @param {PIXI.InteractionEvent} event
   * @private
   */
  _onClickRight(event) {
    const ruler = canvas.controls.ruler;
    if ( ruler.active ) return ruler._onClickRight(event);

    // Dispatch to the active layer
    const layer = this.activeLayer;
    if ( layer instanceof PlaceablesLayer ) layer._onClickRight(event);
  }

  /* -------------------------------------------- */

  /**
   * Handle right-mouse drag events occuring on the Canvas stage or an active Layer
   * @see {MouseInteractionManager#_handleDragMove}
   * @param {PIXI.InteractionEvent} event
   * @private
   */
  _onDragRightMove(event) {

    // Extract event data
    const DRAG_SPEED_MODIFIER = 0.8;
    const {cursorTime, origin, destination} = event.data;
    const dx = destination.x - origin.x;
    const dy = destination.y - origin.y;

    // Update the client's cursor position every 100ms
    const now = Date.now();
    if ( (now - (cursorTime || 0)) > 100 ) {
      if ( this.controls ) this.controls._onMouseMove(event, destination);
      event.data.cursorTime = now;
    }

    // Pan the canvas
    this.pan({
      x: canvas.stage.pivot.x - (dx * DRAG_SPEED_MODIFIER),
      y: canvas.stage.pivot.y - (dy * DRAG_SPEED_MODIFIER)
    });

    // Reset Token tab cycling
    this.tokens._tabIndex = null;
  }


  /* -------------------------------------------- */

  /**
   * Handle the conclusion of a right-mouse drag workflow the Canvas stage.
   * @see {MouseInteractionManager#_handleDragDrop}
   * @param {PIXI.InteractionEvent} event
   * @private
   */
  _onDragRightDrop(event) {}

  /* -------------------------------------------- */

  /**
   * Determine selection coordinate rectangle during a mouse-drag workflow
   * @param {PIXI.InteractionEvent} event
   * @private
   */
  _onDragSelect(event) {

    // Extract event data
    const {origin, destination} = event.data;

    // Determine rectangle coordinates
    let coords = {
      x: Math.min(origin.x, destination.x),
      y: Math.min(origin.y, destination.y),
      width: Math.abs(destination.x - origin.x),
      height: Math.abs(destination.y - origin.y)
    };

    // Draw the select rectangle
    canvas.controls.drawSelect(coords);
    event.data.coords = coords;
  }

  /* -------------------------------------------- */

  /**
   * Pan the canvas view when the cursor position gets close to the edge of the frame
   * @param {MouseEvent} event    The originating mouse movement event
   */
  _onDragCanvasPan(event) {

    // Throttle panning by 200ms
    const now = Date.now();
    if ( now - (this._panTime || 0) <= 200 ) return;
    this._panTime = now;

    // Shift by 3 grid spaces at a time
    const {x, y} = event;
    const pad = 50;
    const shift = (this.dimensions.size * 3) / this.stage.scale.x;

    // Shift horizontally
    let dx = 0;
    if ( x < pad ) dx = -shift;
    else if ( x > window.innerWidth - pad ) dx = shift;

    // Shift vertically
    let dy = 0;
    if ( y < pad ) dy = -shift;
    else if ( y > window.innerHeight - pad ) dy = shift;

    // Enact panning
    if ( dx || dy ) return this.animatePan({x: this.stage.pivot.x + dx, y: this.stage.pivot.y + dy, duration: 200});
  }

  /* -------------------------------------------- */
  /*  Other Event Handlers                        */
  /* -------------------------------------------- */

  /**
   * Handle window resizing with the dimensions of the window viewport change
   * @param {Event} event     The Window resize event
   * @private
   */
  _onResize(event=null) {
    if ( !this.ready ) return false;

    // Resize the renderer to the current screen dimensions
    this.app.renderer.resize(window.innerWidth, window.innerHeight);

    // Record the dimensions that were resized to (may be rounded, etc..)
    const w = this.screenDimensions[0] = this.app.renderer.screen.width;
    const h = this.screenDimensions[1] = this.app.renderer.screen.height;

    // Update the canvas position
    this.stage.position.set(w/2, h/2);
    this.pan(this.stage.pivot);
  }

  /* -------------------------------------------- */

  /**
   * Handle mousewheel events which adjust the scale of the canvas
   * @param {WheelEvent} event    The mousewheel event that zooms the canvas
   * @private
   */
  _onMouseWheel(event) {
    let dz = ( event.deltaY < 0 ) ? 1.05 : 0.95;
    this.pan({scale: dz * canvas.stage.scale.x});
  }

  /* -------------------------------------------- */

  /**
   * Event handler for the drop portion of a drag-and-drop event.
   * @param {DragEvent}   The drag event being dropped onto the canvas
   * @private
   */
  _onDrop(event) {
    event.preventDefault();
    const data = TextEditor.getDragEventData(event);
    if ( !data.type ) return;

    // Acquire the cursor position transformed to Canvas coordinates
    const [x, y] = [event.clientX, event.clientY];
    const t = this.stage.worldTransform;
    data.x = (x - t.tx) / canvas.stage.scale.x;
    data.y = (y - t.ty) / canvas.stage.scale.y;

    /**
     * A hook event that fires when some useful data is dropped onto the
     * Canvas.
     * @function dropCanvasData
     * @memberof hookEvents
     * @param {Canvas} canvas The Canvas
     * @param {object} data   The data that has been dropped onto the Canvas
     */
    const allowed = Hooks.call("dropCanvasData", this, data);
    if ( allowed === false ) return;

    // Handle different data types
    switch ( data.type ) {
      case "Actor":
        return canvas.tokens._onDropActorData(event, data);
      case "JournalEntry":
        return canvas.notes._onDropData(event, data);
      case "Macro":
        return game.user.assignHotbarMacro(null, Number(data.slot));
      case "PlaylistSound":
        return canvas.sounds._onDropData(event, data);
      case "Tile":
        if ( canvas.activeLayer instanceof MapLayer ) {
          return canvas.activeLayer._onDropData(event, data);
        }
    }
  }

  /* -------------------------------------------- */
  /*  Pending Operations                          */
  /* -------------------------------------------- */

  /**
   * Add a pending canvas operation that should fire once the socket handling workflow concludes.
   * This registers operations by a unique string name into a queue - avoiding repeating the same work multiple times.
   * This is especially helpful for multi-object updates to avoid costly and redundant refresh operations.
   * @param {string} name     A unique name for the pending operation, conventionally Class.method
   * @param {Function} fn     The unbound function to execute later
   * @param {*} scope         The scope to which the method should be bound when called
   * @param {...*} args       Arbitrary arguments to pass to the method when called
   */
  addPendingOperation(name, fn, scope, args) {
    if ( this._pendingOperationNames.has(name) ) return;
    this._pendingOperationNames.add(name);
    this.pendingOperations.push([fn, scope, args]);
  }

  /* -------------------------------------------- */

  /**
   * Fire all pending functions that are registered in the pending operations queue and empty it.
   */
  triggerPendingOperations() {
    for ( let [fn, scope, args] of this.pendingOperations ) {
      if ( !fn ) continue;
      args = args || [];
      fn = fn.call(scope, ...args);
    }
    this.pendingOperations = [];
    this._pendingOperationNames.clear();
  }

  /* -------------------------------------------- */
  /*  Deprecations                                */
  /* -------------------------------------------- */

  /**
   * Get a reference to the a specific CanvasLayer by it's name.
   * @param {string} layerName    The name of the canvas layer to get
   * @return {CanvasLayer}
   * @deprecated since v9, will be deleted in v10
   * @ignore
   */
  getLayer(layerName) {
    console.warn("The Canvas#getLayer method is deprecated without replacement. Callers should instead reference Canvas attributes directly.");
    return {
      BackgroundLayer: this.background,
      DrawingsLayer: this.drawings,
      GridLayer: this.grid,
      TemplateLayer: this.templates,
      TokenLayer: this.tokens,
      WallsLayer: this.walls,
      LightingLayer: this.lighting,
      WeatherLayer: this.weather,
      SightLayer: this.sight,
      SoundsLayer: this.sounds,
      NotesLayer: this.notes,
      ControlsLayer: this.controls
    }
  }
}
