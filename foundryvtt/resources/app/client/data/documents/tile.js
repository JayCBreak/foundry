/**
 * The client-side Tile document which extends the common BaseTile model.
 * Each Tile document contains TileData which defines its data schema.
 *
 * @extends abstract.Document
 * @extends abstract.BaseTile
 * @extends ClientDocumentMixin
 *
 * @see {@link data.TileData}                 The Tile data schema
 * @see {@link documents.Scene}               The Scene document type which contains Tile embedded documents
 * @see {@link applications.TileConfig}       The Tile configuration application
 *
 * @param {data.TileData} [data={}]           Initial data provided to construct the Tile document
 * @param {Scene} parent            The parent Scene document to which this Tile belongs
 */
class TileDocument extends CanvasDocumentMixin(foundry.documents.BaseTile) {

  /** @inheritdoc */
  prepareDerivedData() {
    super.prepareDerivedData();
    if ( !this.parent?.data ) return;
    const d = this.parent.dimensions;
    const minX = d.size - Math.abs(this.data.width);
    const minY = d.size - Math.abs(this.data.height);
    const maxX = d.width - d.size;
    const maxY = d.height - d.size;
    this.data.x = Math.clamped(this.data.x.toNearest(0.1), minX, maxX);
    this.data.y = Math.clamped(this.data.y.toNearest(0.1), minY, maxY);
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  get layer() {
    return this.data.overhead ? canvas.foreground : canvas.background;
  }
}
