/**
 * The client-side AmbientSound document which extends the common BaseAmbientSound model.
 * Each AmbientSound document contains AmbientSoundData which defines its data schema.
 *
 * @extends abstract.Document
 * @extends abstract.BaseAmbientSound
 * @extends ClientDocumentMixin
 *
 * @see {@link data.AmbientSoundData}             The AmbientSound data schema
 * @see {@link documents.Scene}                   The Scene document type which contains AmbientSound embedded documents
 * @see {@link applications.AmbientSoundConfig}   The AmbientSound configuration application
 *
 * @param {data.AmbientSoundData} [data={}]       Initial data provided to construct the AmbientSound document
 * @param {Scene} parent                The parent Scene document to which this AmbientSound belongs
 */
class AmbientSoundDocument extends CanvasDocumentMixin(foundry.documents.BaseAmbientSound) {}
