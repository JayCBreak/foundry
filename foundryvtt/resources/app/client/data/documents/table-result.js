/**
 * The client-side TableResult document which extends the common BaseTableResult model.
 * Each TableResult belongs to the results collection of a RollTable document.
 * Each TableResult contains a TableResultData object which provides its source data.
 *
 * @extends abstract.Document
 * @extends abstract.BaseTableResult
 * @extends ClientDocumentMixin
 *
 * @see {@link data.TableResultData}        The TableResult data schema
 * @see {@link documents.RollTable}         The RollTable document which contains TableResult embedded documents
 *
 * @param {TableResultData} [data={}]  Initial data provided to construct the TableResult document
 * @param {RollTable} parent      The parent RollTable document to which this result belongs
 */
class TableResult extends ClientDocumentMixin(foundry.documents.BaseTableResult) {

  /**
   * A path reference to the icon image used to represent this result
   */
  get icon() {
    return this.data.img || CONFIG.RollTable.resultIcon;
  }

  /**
   * Prepare a string representation for the result which (if possible) will be a dynamic link or otherwise plain text
   * @return {string}         The text to display
   */
  getChatText() {
    const d = this.data;
    let text = d.text;
    if ( d.type === CONST.TABLE_RESULT_TYPES.DOCUMENT ) {
      text = `@${d.collection}[${d.resultId}]{${d.text}}`;
    }
    else if ( d.type === CONST.TABLE_RESULT_TYPES.COMPENDIUM ) {
      text = `@Compendium[${d.collection}.${d.resultId}]{${d.text}}`;
    }
    return text;
  }
}
