/**
 * The client-side Scene document which extends the common BaseScene model.
 * Each Scene document contains SceneData which defines its data schema.
 *
 * @extends abstract.Document
 * @extends abstract.Document
 * @extends abstract.BaseScene
 * @extends ClientDocumentMixin
 *
 * @see {@link data.SceneData}              The Scene data schema
 * @see {@link documents.Scenes}            The world-level collection of Scene documents
 * @see {@link applications.SceneConfig}    The Scene configuration application
 *
 * @param {data.SceneData} [data={}]        Initial data provided to construct the Scene document
 */
class Scene extends ClientDocumentMixin(foundry.documents.BaseScene) {
  constructor(data, context) {
    super(data, context);

    /**
     * Determine the canvas dimensions this Scene would occupy, if rendered
     * @type {object}
     */
    this.dimensions = this.dimensions || {};

    /**
     * Track whether the scene is the active view
     * @type {boolean}
     */
    this._view = this.data.active;

    /**
     * Track the viewed position of each scene (while in memory only, not persisted)
     * When switching back to a previously viewed scene, we can automatically pan to the previous position.
     * @type {{x: number, y: number, scale: number}}
     */
    this._viewPosition = {};
  }

	/* -------------------------------------------- */
  /*  Scene Properties                            */
	/* -------------------------------------------- */

  /**
   * A convenience accessor for whether the Scene is currently active
   * @type {boolean}
   */
  get active() {
	  return this.data.active;
  }

	/* -------------------------------------------- */

  /**
   * A convenience accessor for the background image of the Scene
   * @type {string}
   */
	get img() {
	  return this.data.img;
  }

  /* -------------------------------------------- */

  /**
   * Provide a thumbnail image path used to represent this document.
   * @type {string}
   */
  get thumbnail() {
    return this.data.thumb;
  }

	/* -------------------------------------------- */

  /**
   * A convenience accessor for whether the Scene is currently viewed
   * @type {boolean}
   */
  get isView() {
	  return this._view;
  }

	/* -------------------------------------------- */

  /**
   * A reference to the JournalEntry document associated with this Scene, or null
   * @return {JournalEntry|null}
   */
  get journal() {
    return this.data.journal ? game.journal.get(this.data.journal) : null;
  }

	/* -------------------------------------------- */

  /**
   * A reference to the Playlist document for this Scene, or null
   * @type {Playlist|null}
   */
  get playlist() {
    return this.data.playlist ? game.playlists.get(this.data.playlist) : null;
  }

	/* -------------------------------------------- */

  /**
   * A reference to the PlaylistSound document which should automatically play for this Scene, if any
   * @type {PlaylistSound|null}
   */
  get playlistSound() {
    const playlist = this.playlist;
    if ( !playlist ) return null;
    return playlist.sounds.get(this.data.playlistSound) || null;
  }

	/* -------------------------------------------- */
  /*  Scene Methods                               */
	/* -------------------------------------------- */

  /**
   * Set this scene as currently active
   * @return {Promise<Scene>}  A Promise which resolves to the current scene once it has been successfully activated
   */
	async activate() {
	  if ( this.active ) return this;
	  return this.update({active: true});
  }

	/* -------------------------------------------- */

  /**
   * Set this scene as the current view
   * @return {Promise<Scene>}
   */
	async view() {

	  // Do not switch if the loader is still running
    if ( canvas.loading ) {
      return ui.notifications.warn(`You cannot switch Scenes until resources finish loading for your current view.`);
    }

    // Switch the viewed scene
    for ( let scene of game.scenes ) {
      scene._view = scene.id === this.id
    }

    // Notify the user in no-canvas mode
    if ( game.settings.get("core", "noCanvas") ) {
      ui.notifications.info(game.i18n.format("INFO.SceneViewCanvasDisabled", {
        name: this.data.navName ? this.data.navName : this.name
      }));
    }

    // Re-draw the canvas if the view is different
    if ( canvas.initialized && (canvas.id !== this.id) ) {
      console.log(`Foundry VTT | Viewing Scene ${this.name}`);
      await canvas.draw(this);
    }

    // Render apps for the collection
    this.collection.render();
    ui.combat.initialize();
    return this;
  }

	/* -------------------------------------------- */

  /** @override */
  clone(createData={}, options={}) {
    createData["active"] = false;
    createData["navigation"] = false;
    if ( !foundry.data.validators.isBase64Image(createData.thumb) ) delete createData.thumb;
    if ( !options.save ) return super.clone(createData, options);
    return this.createThumbnail().then(data => {
      createData.thumb = data.thumb;
      return super.clone(createData, options);
    });
  }

	/* -------------------------------------------- */

  /** @inheritdoc */
  prepareBaseData() {
    super.prepareBaseData();
    this.dimensions = Canvas.getDimensions(this.data);
  }

	/* -------------------------------------------- */
  /*  Event Handlers                              */
	/* -------------------------------------------- */

  /** @override */
  async _preCreate(data, options, user) {
    await super._preCreate(data, options, user);

    // Set a Scene as active if none currently are
    if ( !("active" in data) && !game.scenes.active ) this.data.update({active: true});

    // Base64 the thumbnail for compendium Scenes
    if ( canvas.ready && this.compendium ) {
      const t = await this.createThumbnail({img: data.img});
      this.data.update({thumb: t.thumb});
    }

    // Trigger Playlist Updates
    if ( this.data.active ) return game.playlists._onChangeScene(this, data);
  }

	/* -------------------------------------------- */

  /** @override */
  _onCreate(data, options, userId) {
    super._onCreate(data, options, userId);
    if ( data.active === true ) this._onActivate(true);
  }

	/* -------------------------------------------- */

  /** @override */
  async _preUpdate(data, options, user) {
    await super._preUpdate(data, options, user);
    const audioChange = ("active" in data) || (this.data.active && ["playlist", "playlistSound"].some(k => k in data));
    if ( audioChange ) return game.playlists._onChangeScene(this, data);
  }

	/* -------------------------------------------- */

  /** @override */
  _onUpdate(data, options, userId) {
    super._onUpdate(data, options, userId);
    const changed = new Set(Object.keys(data).filter(k => k !== "_id"));

    // If the Scene became active, go through the full activation procedure
    if ( changed.has("active") ) this._onActivate(data.active);

    // If the Thumbnail was updated, bust the image cache
    if ( changed.has("thumb") && this.data.thumb ) {
      this.data.thumb = this.data.thumb.split("?")[0] + `?${Date.now()}`;
    }

    // If the scene is already active, maybe re-draw the canvas
    if ( canvas.scene === this ) {
      const redraw = [
        "img", "foreground", "shiftX", "shiftY", "width", "height", "padding",    // Scene Dimensions
        "gridType", "grid", "gridDistance", "gridUnits",                          // Grid Configuration
        "drawings", "lights", "sounds", "templates", "tiles", "tokens", "walls",  // Placeable Objects
        "weather"                                                                 // Ambience
      ];
      if ( redraw.some(k => changed.has(k)) ) return canvas.draw();
      if ( changed.has("backgroundColor") ) canvas.setBackgroundColor(data.backgroundColor);
      if ( ["gridColor", "gridAlpha"].some(k => changed.has(k)) ) canvas.grid.draw();

      // Modify vision conditions
      const perceptionAttrs = ["globalLight", "globalLightThreshold", "tokenVision", "fogExploration"];
      if ( perceptionAttrs.some(k => changed.has(k)) ) canvas.perception.initialize();

      // Progress darkness level
      if ( changed.has("darkness") && options.animateDarkness ) {
        return canvas.lighting.animateDarkness(data.darkness, {
          duration: typeof options.animateDarkness === "number" ? options.animateDarkness : undefined
        });
      }

      if ( ["darkness", "backgroundColor"].some(k => changed.has(k)) ) canvas.lighting.refresh(data);
    }
  }

	/* -------------------------------------------- */

  /** @inheritdoc */
  async _preDelete(options, user) {
    await super._preDelete(options, user);
    if ( this.data.active ) game.playlists._onChangeScene(this, {active: false});
  }

	/* -------------------------------------------- */

  /** @override */
  _onDelete(options, userId) {
    super._onDelete(options, userId);
    if ( canvas.scene?.id === this.id ) canvas.draw(null);
  }

	/* -------------------------------------------- */

  /**
   * Handle Scene activation workflow if the active state is changed to true
   * @param {boolean} active    Is the scene now active?
   * @protected
   */
  _onActivate(active) {

    // Deactivate other scenes
    for ( let s of game.scenes ) {
      if ( s.data.active && (s !== this) ) {
        s.data.update({active: false});
        s._initialize();
      }
    }

    // Update the Canvas display
    if ( canvas.initialized && !active ) return canvas.draw(null);
    return this.view();
  }

	/* -------------------------------------------- */

  /** @inheritdoc */
  _preCreateEmbeddedDocuments(embeddedName, result, options, userId) {
    super._preCreateEmbeddedDocuments(embeddedName, result, options, userId);
    if ( (userId === game.userId) && this.isView && !options.isUndo ) {
      const layer = canvas.getLayerByEmbeddedName(embeddedName);
      layer?.storeHistory("create", result);
    }
  }

	/* -------------------------------------------- */

  /** @inheritdoc */
  _onCreateEmbeddedDocuments(...args) {
    super._onCreateEmbeddedDocuments(...args);
    if ( this.isView ) canvas.triggerPendingOperations();
  }

	/* -------------------------------------------- */

  /** @inheritdoc */
  _preUpdateEmbeddedDocuments(embeddedName, result, options, userId) {
    super._preUpdateEmbeddedDocuments(embeddedName, result, options, userId);
    if ( (userId === game.userId) && this.isView && !options.isUndo ) {
      const layer = canvas.getLayerByEmbeddedName(embeddedName);
      const updatedIds = new Set(result.map(r => r._id));
      const originals = this.getEmbeddedCollection(embeddedName).reduce((arr, d) => {
        if ( updatedIds.has(d.id) ) arr.push(d.toJSON());
        return arr;
      }, []);
      layer?.storeHistory("update", originals);
    }
  }

	/* -------------------------------------------- */

  /** @override */
  _onUpdateEmbeddedDocuments(...args) {
    super._onUpdateEmbeddedDocuments(...args);
    if ( this.isView ) canvas.triggerPendingOperations();
  }

	/* -------------------------------------------- */

  /** @inheritdoc */
  _preDeleteEmbeddedDocuments(embeddedName, result, options, userId) {
    super._preDeleteEmbeddedDocuments(embeddedName, result, options, userId);
    if ( (userId === game.userId) && this.isView && !options.isUndo ) {
      const layer = canvas.getLayerByEmbeddedName(embeddedName);
      const originals = this.getEmbeddedCollection(embeddedName).reduce((arr, d) => {
        if ( result.includes(d.id) ) arr.push(d.toJSON());
        return arr;
      }, []);
      layer?.storeHistory("delete", originals);
    }
  }

	/* -------------------------------------------- */

  /** @inheritdoc */
  _onDeleteEmbeddedDocuments(...args) {
    super._onDeleteEmbeddedDocuments(...args);
    if ( this.isView ) canvas.triggerPendingOperations();
  }

  /* -------------------------------------------- */
  /*  Importing and Exporting                     */
  /* -------------------------------------------- */

  /** @inheritdoc */
  toCompendium(pack, options={}) {
    const data = super.toCompendium(pack, options);
    if ( options.clearState ) delete data.fogReset;
    if ( options.clearSort ) {
      delete data.navigation;
      delete data.navOrder;
    }
    return data;
  }

  /* -------------------------------------------- */

  /**
   * Create a 300px by 100px thumbnail image for this scene background
   * @param {string} [string|null]  A background image to use for thumbnail creation, otherwise the current scene
   *                          background is used.
   * @param {number} [width]        The desired thumbnail width. Default is 300px
   * @param {number} [height]       The desired thumbnail height. Default is 100px;
   * @return {Promise<object>}      The created thumbnail data.
   */
  async createThumbnail({img, width=300, height=100, format, quality}={}) {
    if ( game.settings.get("core", "noCanvas") ) throw new Error(game.i18n.localize("SCENES.GenerateThumbNoCanvas"));
    const newImage = img !== undefined;
    img = img ?? this.data.img;
    const tiles = this.tiles.filter(t => t.data.img && !t.data.hidden).sort((a, b) => a.data.z - b.data.z);

    // Load required textures to create the thumbnail
    const toLoad = tiles.map(t => t.data.img);
    if ( img ) toLoad.push(img);
    if ( this.data.foreground ) toLoad.push(this.data.foreground);
    await TextureLoader.loader.load(toLoad);

    // First load the background texture to get dimensions
    const dims = this.toObject();
    const backgroundTexture = img ? getTexture(img) : null;
    if ( newImage && backgroundTexture ) {
      dims.width = backgroundTexture.width;
      dims.height = backgroundTexture.height;
    }
    const d = Canvas.getDimensions(dims);

    // Create a container and add a transparent graphic to enforce the size
    const baseContainer = new PIXI.Container();
    const sceneRectangle = new PIXI.Rectangle(0, 0, d.sceneWidth, d.sceneHeight);
    const baseGraphics = baseContainer.addChild(new PIXI.LegacyGraphics())
    baseGraphics.beginFill(0xFFFFFF, 1.0).drawShape(sceneRectangle).endFill();
    baseContainer.mask = baseGraphics;

    // Tile drawing function
    const drawTile = async (container, tile) => {
      const t = new Tile(tile);
      container.addChild(t);
      await t.draw();
      t.x -= d.sceneRect.x; // remove padding and offset
      t.y -= d.sceneRect.y;
    }

    // Background container
    if ( backgroundTexture ) {
      const bg = baseContainer.addChild(new PIXI.Container());
      bg.addChild(new PIXI.Sprite(backgroundTexture));
      bg.width = d.sceneWidth
      bg.height = d.sceneHeight
    }

    // Background Tiles
    const backgroundTiles = tiles.filter(t => !t.data.overhead);
    if ( backgroundTiles.length ) {
      const backgroundTileContainer = baseContainer.addChild(new PIXI.Container());
      for ( let t of backgroundTiles ) {
        await drawTile(backgroundTileContainer, t);
      }
    }

    // Foreground container
    if ( this.data.foreground ) {
      const fgTex = getTexture(this.data.foreground);
      const fg = baseContainer.addChild(new PIXI.Container());
      fg.addChild(new PIXI.Sprite(fgTex));
      fg.width = d.sceneWidth
      fg.height = d.sceneHeight
    }

    // Foreground Tiles
    const foregroundTiles = tiles.filter(t => t.data.overhead);
    if ( foregroundTiles.length ) {
      const foregroundTileContainer = baseContainer.addChild(new PIXI.Container());
      for ( let t of foregroundTiles ) {
        await drawTile(foregroundTileContainer, t);
      }
    }

    // Render the container to a thumbnail
    return ImageHelper.createThumbnail(baseContainer, {width, height, format, quality});
  }
}
