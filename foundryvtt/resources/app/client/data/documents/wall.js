/**
 * The client-side Wall document which extends the common BaseWall model.
 * Each Wall document contains WallData which defines its data schema.
 *
 * @extends abstract.Document
 * @extends abstract.BaseWall
 * @extends ClientDocumentMixin
 *
 * @see {@link data.WallData}                 The Wall data schema
 * @see {@link documents.Scene}               The Scene document type which contains Wall embedded documents
 * @see {@link applications.WallConfig}       The Wall configuration application
 *
 * @param {data.WallData} [data={}]           Initial data provided to construct the Wall document
 * @param {Scene} parent            The parent Scene document to which this Wall belongs
 */
class WallDocument extends CanvasDocumentMixin(foundry.documents.BaseWall) {}
