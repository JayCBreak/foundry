/**
 * The client-side Drawing document which extends the common BaseDrawing model.
 * Each Drawing document contains DrawingData which defines its data schema.
 *
 * @extends abstract.Document
 * @extends abstract.BaseDrawing
 * @extends ClientDocumentMixin
 *
 * @see {@link data.DrawingData}              The Drawing data schema
 * @see {@link documents.Scene}               The Scene document type which contains Drawing embedded documents
 * @see {@link applications.DrawingConfig}    The Drawing configuration application
 *
 * @param {data.DrawingData} [data={}]        Initial data provided to construct the Drawing document
 * @param {Scene} parent            The parent Scene document to which this Drawing belongs
 */
class DrawingDocument extends CanvasDocumentMixin(foundry.documents.BaseDrawing) {

  /* -------------------------------------------- */
  /*  Properties                                  */
  /* -------------------------------------------- */

  /**
   * A reference to the User who created the Drawing document.
   * @type {User}
   */
  get author() {
    return game.users.get(this.data.author);
  }
}
