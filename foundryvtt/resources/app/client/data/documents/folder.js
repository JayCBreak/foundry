/**
 * The client-side Folder document which extends the common BaseFolder model.
 * Each Folder document contains FolderData which defines its data schema.
 *
 * @extends abstract.Document
 * @extends abstract.Document
 * @extends abstract.BaseFolder
 * @extends ClientDocumentMixin
 *
 * @see {@link data.FolderData}              The Folder data schema
 * @see {@link documents.Folders}            The world-level collection of Folder documents
 * @see {@link embedded.FolderSound}         The FolderSound embedded document within a parent Folder
 * @see {@link applications.FolderConfig}    The Folder configuration application
 *
 * @param {data.FolderData} [data={}]       Initial data provided to construct the Folder document
 */
class Folder extends ClientDocumentMixin(foundry.documents.BaseFolder) {

  /**
   * The depth of this folder in its sidebar tree
   * @type {number}
   */
  depth;

  /* -------------------------------------------- */
  /*  Properties                                  */
  /* -------------------------------------------- */

  /**
   * Return an array of the Document instances which are contained within this Folder.
   * @type {Document[]}
   */
  get contents() {
    return this.documentCollection.filter(d => d.data.folder === this.id);
  }

  /* -------------------------------------------- */

  /**
   * Return whether the folder is displayed in the sidebar to the current user
   * @type {boolean}
   */
  get displayed() {
    return game.user.isGM || !!this.content.length || this.children.some(c => c.displayed);
  }

  /* -------------------------------------------- */

  /**
   * Return a reference to the Document type which is contained within this Folder.
   * @returns {Function}
   */
  get documentClass() {
    return CONFIG[this.data.type].documentClass;
  }

  /* -------------------------------------------- */

  /**
   * Return a reference to the WorldCollection instance which provides Documents to this Folder.
   * @returns {WorldCollection}
   */
  get documentCollection() {
    return game.collections.get(this.data.type);
  }

  /* -------------------------------------------- */

  /**
   * Return whether the folder is currently expanded within the sidebar interface.
   * @type {boolean}
   */
  get expanded() {
    return game.folders._expanded[this.id] || false;
  }

  /* -------------------------------------------- */

  /**
   * A reference to the parent Folder if one is set, otherwise null.
   * @type {Folder|null}
   */
  get parentFolder() {
    return this.collection.get(this.data.parent);
  }

  /* -------------------------------------------- */
  /*  Methods                                     */
  /* -------------------------------------------- */

  /**
   * Present a Dialog form to create a new Folder.
   * @see ClientDocumentMixin.createDialog
   * @param {object} data              Initial data with which to populate the creation form
   * @param {object} [context={}]      Additional context options or dialog positioning options
   * @return {Promise<Folder|null>}    A Promise which resolves to the created Folder, or null if the dialog was
   *                                   closed.
   */
  static async createDialog(data={}, options={}) {
    const label = game.i18n.localize(this.metadata.label);
    const folder = new Folder(foundry.utils.mergeObject({
      name: game.i18n.format("DOCUMENT.New", {type: label}),
      sorting: "a",
    }, data));
    return new Promise(resolve => {
      options.resolve = resolve;
      new FolderConfig(folder, options).render(true);
    });
  }

  /* -------------------------------------------- */

  /**
   * Export all Documents contained in this Folder to a given Compendium pack.
   * Optionally update existing Documents within the Pack by name, otherwise append all new entries.
   * @param {CompendiumCollection} pack       A Compendium pack to which the documents will be exported
   * @param {object} [options]                Additional options which customize how content is exported. See {@link ClientDocumentMixin#toCompendium}
   * @param {boolean} [options.updateByName=false]    Update existing entries in the Compendium pack, matching by name
   * @return {Promise<CompendiumCollection>}  The updated Compendium Collection instance
   */
  async exportToCompendium(pack, options={}) {
    const updateByName = options.updateByName ?? false;
    const index = await pack.getIndex();
    const documents = this.contents;
    ui.notifications.info(game.i18n.format("FOLDER.Exporting", {
      n: documents.length,
      type: this.type,
      compendium: pack.collection
    }));

    // Classify creations and updates
    const creations = [];
    const updates = [];
    for ( let d of this.contents ) {
      const data = d.toCompendium(pack, options);
      let existing = updateByName ? index.find(i => i.name === d.name) : index.find(i => i._id === d.id);
      if (existing) {
        if ( this.type === "Scene" ) {
          const thumb = await d.createThumbnail({img: data.img});
          data.thumb = thumb.thumb;
        }
        data._id = existing._id;
        updates.push(data);
      }
      else creations.push(data);
      console.log(`Prepared ${d.name} for export to ${pack.collection}`);
    }

    // Create new Documents
    const cls = pack.documentClass;
    if ( creations.length ) await cls.createDocuments(creations, {
      pack: pack.collection,
      keepId: options.keepId
    });

    // Update existing Documents
    if ( updates.length ) await cls.updateDocuments(updates, {
      pack: pack.collection,
      diff: false,
      recursive: false,
      render: false
    });

    // Re-render the pack
    ui.notifications.info(game.i18n.format("FOLDER.ExportDone", {type: this.type, compendium: pack.collection}));
    pack.render(false);
    return pack;
  }

  /* -------------------------------------------- */

  /**
   * Provide a dialog form that allows for exporting the contents of a Folder into an eligible Compendium pack.
   * @param {string} pack       A pack ID to set as the default choice in the select input
   * @param {object} options    Additional options passed to the Dialog.prompt method
   * @return {Promise<void>}    A Promise which resolves or rejects once the dialog has been submitted or closed
   */
  async exportDialog(pack, options={}) {

    // Get eligible pack destinations
    const packs = game.packs.filter(p => (p.documentName === this.type) && !p.locked);
    if ( !packs.length ) {
      return ui.notifications.warn(game.i18n.format("FOLDER.ExportWarningNone", {type: this.type}));
    }

    // Render the HTML form
    const html = await renderTemplate("templates/sidebar/apps/folder-export.html", {
      packs: packs.reduce((obj, p) => {
        obj[p.collection] = p.title;
        return obj;
      }, {}),
      pack: options.pack ?? null,
      merge: options.merge ?? true,
      keepId: options.keepId ?? true
    });

    // Display it as a dialog prompt
    return Dialog.prompt({
      title: game.i18n.localize("FOLDER.ExportTitle") + `: ${this.name}`,
      content: html,
      label: game.i18n.localize("FOLDER.ExportTitle"),
      callback: html => {
        const form = html[0].querySelector("form");
        const pack = game.packs.get(form.pack.value);
        return this.exportToCompendium(pack, {
          updateByName: form.merge.checked,
          keepId: form.keepId.checked
        });
      },
      rejectClose: false,
      options
    });
  }

  /* -------------------------------------------- */

  /**
   * Get the Folder documents which are sub-folders of the current folder, either direct children or recursively.
   * @param {boolean} [recursive=false] Identify child folders recursively, if false only direct children are returned
   * @returns {Folder[]}  An array of Folder documents which are subfolders of this one
   */
  getSubfolders(recursive=false) {
    let subfolders = game.folders.filter(f => f.data.parent === this.id);
    if ( recursive && subfolders.length ) {
      for ( let f of subfolders ) {
        const children = f.getSubfolders(true);
        subfolders = subfolders.concat(children);
      }
    }
    return subfolders;
  }

  /* -------------------------------------------- */
  /*  Event Handlers                              */
  /* -------------------------------------------- */

  /** @inheritdoc */
  _onDelete(options, userId) {
    const parentId = this.data.parent || null;
    const db = CONFIG.DatabaseBackend;
    const {deleteSubfolders, deleteContents} = options;

    // Delete or move sub-Folders
    const deleteFolderIds = [];
    for ( let f of this.getSubfolders() ) {
      if ( deleteSubfolders ) deleteFolderIds.push(f.id);
      else f.data.update({parent: parentId});
    }
    if ( deleteFolderIds.length ) {
      db._handleDeleteDocuments({
        request: { type: "Folder", options: { deleteSubfolders, deleteContents, render: false } },
        result: deleteFolderIds,
        userId
      });
    }

    // Delete or move contained Documents
    const deleteDocumentIds = [];
    for ( let d of this.documentCollection ) {
      if ( d.data.folder !== this.id ) continue;
      if ( deleteContents ) deleteDocumentIds.push(d.id);
      else d.data.update({folder: parentId});
    }
    if ( deleteDocumentIds.length ) {
      db._handleDeleteDocuments({
        request: { type: this.data.type, options: { render: false } },
        result: deleteDocumentIds,
        userId
      });
    }
    return super._onDelete(options, userId);
  }
}
