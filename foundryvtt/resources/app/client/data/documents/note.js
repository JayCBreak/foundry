/**
 * The client-side Note document which extends the common BaseNote model.
 * Each Note document contains NoteData which defines its data schema.
 *
 * @extends abstract.Document
 * @extends abstract.BaseNote
 * @extends ClientDocumentMixin
 *
 * @see {@link data.NoteData}                 The Note data schema
 * @see {@link documents.Scene}               The Scene document type which contains Note embedded documents
 * @see {@link applications.NoteConfig}       The Note configuration application
 *
 * @param {data.NoteData} [data={}]           Initial data provided to construct the Note document
 * @param {Scene} parent            The parent Scene document to which this Note belongs
 */
class NoteDocument extends CanvasDocumentMixin(foundry.documents.BaseNote) {

  /* -------------------------------------------- */
  /*  Properties                                  */
  /* -------------------------------------------- */

  /**
   * The associated JournalEntry which is referenced by this Note
   * @type {JournalEntry}
   */
  get entry() {
    return game.journal.get(this.data.entryId);
  }

  /* -------------------------------------------- */

  /**
   * The text label used to annotate this Note
   * @type {string}
   */
  get label() {
    return this.data.text || this.entry?.name || "Unknown";
  }
}
