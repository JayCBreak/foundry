/**
 * The client-side Card document which extends the common BaseCard model.
 * Each Card document contains CardData which defines its data schema.
 *
 * @extends abstract.Document
 * @extends abstract.BaseCard
 * @extends ClientDocumentMixin
 *
 * @see {@link data.CardData}                      The Card data schema
 * @see {@link documents.Cards}                    The Cards document type which contains Card embedded documents
 *
 * @param {data.CardData} [data={}]                Initial data provided to construct a Card document
 */
class Card extends ClientDocumentMixin(foundry.documents.BaseCard) {

  /**
   * The card back.
   * This reference is cached and lazily evaluated to retrieve an image and name from the source deck.
   * @type {data.CardFaceData}
   */
  get back() {
    if ( this._back ) return this._back;
    const source = this.isHome ? this.parent : this.source;
    const b = this._back = this.data.back;
    b.img = b.img || source.data.img;
    b.name = b.name || game.i18n.format("CARD.Unknown", {source: source.data.name});
    return b;
  }

  /**
   * The current card face
   * @type {data.CardFaceData|null}
   */
  get face() {
    if ( this.data.face === null ) return null;
    const n = Math.clamped(this.data.face, 0, this.data.faces.length-1);
    return this.data.faces[n] || null;
  }

  /**
   * The image used to depict the back of this card
   * @type {string}
   */
  get backImg() {
    return this.back.img || foundry.data.CardFaceData.DEFAULT_ICON;
  }

  /**
   * The image of the currently displayed card face or back
   * @type {string}
   */
  get img() {
    return this.face?.img || this.back.img || foundry.data.CardFaceData.DEFAULT_ICON;
  }

  /**
   * The name of the current card face, or the name of the card itself
   * @type {string}
   */
  get name() {
    return this.face?.name ?? this.back?.name ?? this.data.name;
  }

  /* -------------------------------------------- */

  /**
   * A reference to the source Cards document which defines this Card.
   * @type {Cards|null}
   */
  get source() {
    if ( this.parent?.type === "deck" ) return this.parent;
    return this.data.origin ? game.cards.get(this.data.origin) : null;
  }

  /* -------------------------------------------- */

  /**
   * A convenience property for whether or not the Card is within its source Cards stack. Cards in decks are always
   * considered home.
   * @type {boolean}
   */
  get isHome() {
    return (this.parent?.type === "deck") || (this.data.origin === this.parent?.id);
  }

  /**
   * Whether or not to display the face of this card?
   * @type {boolean}
   */
  get showFace() {
    return this.data.face !== null;
  }

  /**
   * Does this Card have a next face available to flip to?
   * @type {boolean}
   */
  get hasNextFace() {
    return (this.data.face === null) || (this.data.face < this.data.faces.length - 1);
  }

  /**
   * Does this Card have a previous face available to flip to?
   * @type {boolean}
   */
  get hasPreviousFace() {
    return this.data.face !== null;
  }

  /* -------------------------------------------- */
  /*  Core Methods                                */
  /* -------------------------------------------- */

  /** @override */
  prepareDerivedData() {
    super.prepareDerivedData();
    this._back = undefined;
  }

  /* -------------------------------------------- */
  /*  API Methods                                 */
  /* -------------------------------------------- */

  /**
   * Flip this card to some other face. A specific face may be requested, otherwise:
   * If the card currently displays a face the card is flipped to the back.
   * If the card currently displays the back it is flipped to the first face.
   * @param {number|null} [face]      A specific face to flip the card to
   * @returns {Promise<Card>}         A reference to this card after the flip operation is complete
   */
  async flip(face) {

    // Flip to an explicit face
    if ( Number.isNumeric(face) || (face === null) ) return this.update({face});

    // Otherwise flip to default
    return this.update({face: this.data.face === null ? 0 : null});
  }

  /* -------------------------------------------- */

  /**
   * Pass this Card to some other Cards document.
   * @param {Cards} to                A new Cards document this card should be passed to
   * @param {object} [options={}]     Options which modify the pass operation
   * @param {object} [updateData={}]  Modifications to make to the Card as part of the pass operation,
   *                                  for example the displayed face
   * @returns {Promise<Card>}         A reference to this card after the it has been passed to another parent document
   */
  async pass(to, {updateData={}, ...options}={}) {
    const created = await this.parent.pass(to, [this.id], {updateData, action: "pass", ...options});
    return created[0];
  }

  /* -------------------------------------------- */

  /**
   * Play a specific card to some other Cards document.
   * This method is currently a more semantic alias for Card#pass.
   * @alias Card#pass
   * @returns {Promise<Card>}
   */
  async play(to, {updateData={}, ...options}={}) {
    const created = await this.parent.pass(to, [this.id], {updateData, action: "play", ...options});
    return created[0];
  }

  /* -------------------------------------------- */

  /**
   * Discard a specific card to some other Cards document.
   * This method is currently a more semantic alias for Card#pass.
   * @alias Card#pass
   * @returns {Promise<Card>}
   */
  async discard(to, {updateData={}, ...options}={}) {
    const created = await this.parent.pass(to, [this.id], {updateData, action: "discard", ...options});
    return created[0];
  }

  /* -------------------------------------------- */

  /**
   * Reset this Card to its original Cards parent.
   * @param {object} [options={}]   Options which modify the reset operation
   * @returns {Promise<Card>}       A reference to the reset card belonging to its original parent
   */
  async reset(options={}) {

    // Mark the original card as no longer drawn
    const original = this.isHome ? this : this.source?.cards.get(this.id);
    if ( original ) await original.update({drawn: false});

    // Delete this card if it's not the original
    if ( !this.isHome ) await this.delete();
    return original;
  }

  /* -------------------------------------------- */

  /**
   * Create a chat message which displays this Card.
   * @param {object} [messageData={}] Additional data which becomes part of the created ChatMessageData
   * @param {object} [options={}]     Options which modify the message creation operation
   * @returns {Promise<ChatMessage>}  The created chat message
   */
  async toMessage(messageData={}, options={}) {
    messageData = foundry.utils.mergeObject({
      content: `<div class="card-draw flexrow">
        <img class="card-face" src="${this.img}"/>
        <h4 class="card-name">${this.name}</h4>
      </div>`
    }, messageData);
    return ChatMessage.create(messageData, options);
  }
}
