/**
 * The client-side FogExploration document which extends the common BaseFogExploration model.
 * Each FogExploration document contains FogExplorationData which defines its data schema.
 *
 * @extends abstract.Document
 * @extends abstract.Document
 * @extends abstract.BaseFogExploration
 * @extends ClientDocumentMixin
 *
 * @see {@link data.FogExplorationData}              The FogExploration data schema
 *
 * @param {data.FogExplorationData} [data={}]        Initial data provided to construct the FogExploration document
 */
class FogExploration extends ClientDocumentMixin(foundry.documents.BaseFogExploration) {
  constructor(data={}, context) {
    data.user = data.user || game.user.id;
    data.scene = data.scene || canvas.scene?.id || null;
    super(data, context);
  }

  /* -------------------------------------------- */

  /**
   * Explore fog of war for a new point source position.
   * @param {PointSource} source    The candidate source of exploration
   * @param {boolean} [force=false] Force the position to be re-explored
   * @returns {boolean}             Is the source position newly explored?
   */
  explore(source, force=false) {
    const r = canvas.lighting.globalLight ? canvas.dimensions.maxR : source.radius;
    if ( r < 0 ) return false;
    const coords = canvas.grid.getCenter(source.x, source.y).map(Math.round).join("_");
    const position = this.data.positions[coords];

    // Check whether the position has already been explored
    let explored = position && (position.limit !== true) && (position.radius >= r);
    if ( explored && !force ) return false;

    // Update explored positions
    if ( CONFIG.debug.fog ) console.debug("SightLayer | Updating fog exploration for new explored position.");
    this.data.update({
      positions: {
        [coords]: {radius: r, limit: source.limited}
      }
    });
    return true;
  }

  /* -------------------------------------------- */

  /**
   * Obtain the fog of war exploration progress for a specific Scene and User.
   * @returns {FogExploration|null}
   */
  static async get({scene, user}={}, options={}) {
    const collection = game.collections.get("FogExploration");
    const sceneId = (scene || canvas.scene)?.id || null;
    const userId = (user || game.user)?.id;
    if ( !sceneId || !userId ) return null;
    if ( !(game.user.isGM || (userId === game.user.id)) ) {
      throw new Error("You do not have permission to access the FogExploration object of another user");
    }

    // Return cached exploration
    let exploration = collection.find(x => (x.data.user === userId) && (x.data.scene === sceneId) );
    if ( exploration ) return exploration;

    // Return persisted exploration
    const response = await this.database.get(this, {
      query: { scene: sceneId, user: userId },
      options: options
    });
    exploration = response.length ? response.shift() : null;
    if ( exploration ) collection.set(exploration.id, exploration);
    return exploration;
  }

  /* -------------------------------------------- */

  /**
   * Transform the explored base64 data into a PIXI.Texture object
   * @returns {PIXI.Texture|null}
   */
  getTexture() {
    if ( this.data.explored === null ) return null;
    const bt = new PIXI.BaseTexture(this.data.explored);
    return new PIXI.Texture(bt);
  }

  /* -------------------------------------------- */

  /**
   * Open Socket listeners which transact JournalEntry data
   */
  static _activateSocketListeners(socket) {
    socket.on("resetFog", this._onResetFog.bind(this));
  }

  /* -------------------------------------------- */

  /**
   * Handle a request from the server to reset fog of war for a particular scene.
   * @param {string} sceneId
   * @private
   */
  static _onResetFog(sceneId) {
    const fogs = game.collections.get("FogExploration");
    for ( let fog of fogs ) {
      if ( fog.data.scene === sceneId ) fogs.delete(fog.id);
    }
    if ( sceneId !== canvas.scene.id ) return;
    return canvas.sight._handleResetFog();
  }
}
