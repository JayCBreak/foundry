/**
 * The client-side ActiveEffect document which extends the common BaseActiveEffect model.
 * Each ActiveEffect belongs to the effects collection of its parent Document.
 * Each ActiveEffect contains a ActiveEffectData object which provides its source data.
 *
 * @extends abstract.Document
 * @extends abstract.BaseActiveEffect
 * @extends ClientDocumentMixin
 *
 * @see {@link data.ActiveEffectData}               The ActiveEffect data schema
 * @see {@link documents.Actor}                     The Actor document which contains ActiveEffect embedded documents
 * @see {@link documents.Item}                      The Item document which contains ActiveEffect embedded documents
 *
 * @param {ActiveEffectData} [data={}]    Initial data provided to construct the ActiveEffect document
 * @param {Actor|documents.Item} parent   The parent document to which this ActiveEffect belongs
 */
class ActiveEffect extends ClientDocumentMixin(foundry.documents.BaseActiveEffect) {
  constructor(data, context) {
    super(data, context);

    /**
     * A cached reference to the source name to avoid recurring database lookups
     * @type {string|null}
     */
    this._sourceName = null;

    /**
     * A cached reference to the ActiveEffectConfig instance which configures this effect
     * @type {ActiveEffectConfig|null}
     */
    this._sheet = null;
  }

  /* -------------------------------------------- */
  /*  Properties                                  */
  /* -------------------------------------------- */

  /**
   * Is there some system logic that makes this active effect ineligible for application?
   * @type {boolean}
   */
  get isSuppressed() {
    return false;
  };

  /* --------------------------------------------- */

  /**
   * Summarize the active effect duration
   * @type {{type: string, duration: number|null, remaining: number|null, label: string}}
   */
  get duration() {
    const d = this.data.duration;

    // Time-based duration
    if ( Number.isNumeric(d.seconds) ) {
      const start = (d.startTime || game.time.worldTime);
      const elapsed = game.time.worldTime - start;
      const remaining = d.seconds - elapsed;
      return {
        type: "seconds",
        duration: d.seconds,
        remaining: remaining,
        label: `${remaining} Seconds`
      };
    }

    // Turn-based duration
    else if ( d.rounds || d.turns ) {

      // Determine the current combat duration
      const cbt = game.combat;
      const c = {round: cbt?.round ?? 0, turn: cbt?.turn ?? 0, nTurns: cbt?.turns.length ?? 1};
      const current = this._getCombatTime(c.round, c.turn);
      const duration = this._getCombatTime(d.rounds, d.turns);
      const start = this._getCombatTime(d.startRound, d.startTurn, c.nTurns);

      // If the effect has not started yet display the full duration
      if ( current <= start ) {
        return {
          type: "turns",
          duration: duration,
          remaining: duration,
          label: this._getDurationLabel(d.rounds, d.turns)
        }
      }

      // Some number of remaining rounds and turns (possibly zero)
      const remaining = Math.max(((start + duration) - current).toNearest(0.01), 0);
      const remainingRounds = Math.floor(remaining);
      const remainingTurns = Math.min(((remaining - remainingRounds) * 100).toNearest(0.01), c.nTurns-1);
      return {
        type: "turns",
        duration: duration,
        remaining: remaining,
        label: this._getDurationLabel(remainingRounds, remainingTurns)
      }
    }

    // No duration
    else return {
      type: "none",
      duration: null,
      remaining: null,
      label: game.i18n.localize("None")
    }
  }

  /* -------------------------------------------- */

  /**
   * Format a round+turn combination as a decimal
   * @param {number} round    The round number
   * @param {number} turn     The turn number
   * @param {number} [nTurns] The maximum number of turns in the encounter
   * @returns {number}        The decimal representation
   * @private
   */
  _getCombatTime(round, turn, nTurns) {
    if ( nTurns !== undefined ) turn = Math.min(turn, nTurns)
    round = Math.max(round, 0);
    turn = Math.max(turn, 0);
    return (round || 0) + ((turn || 0) / 100);
  }

  /* -------------------------------------------- */

  /**
   * Format a number of rounds and turns into a human-readable duration label
   * @param {number} rounds   The number of rounds
   * @param {number} turns    The number of turns
   * @returns {string}        The formatted label
   * @private
   */
  _getDurationLabel(rounds, turns) {
    const parts = [];
    if ( rounds > 0 ) parts.push(`${rounds} ${game.i18n.localize(rounds === 1 ? "COMBAT.Round": "COMBAT.Rounds")}`);
    if ( turns > 0 ) parts.push(`${turns} ${game.i18n.localize(turns === 1 ? "COMBAT.Turn": "COMBAT.Turns")}`);
    if (( rounds + turns ) === 0 ) parts.push(game.i18n.localize("None"));
    return parts.filterJoin(", ");
  }

  /* -------------------------------------------- */

  /**
   * Describe whether the ActiveEffect has a temporary duration based on combat turns or rounds.
   * @type {boolean}
   */
  get isTemporary() {
    const duration = this.data.duration.seconds ?? (this.data.duration.rounds || this.data.duration.turns) ?? 0;
    return (duration > 0) || this.getFlag("core", "statusId");
  }

  /* -------------------------------------------- */

  /**
   * A cached property for obtaining the source name
   * @type {string}
   */
  get sourceName() {
    if ( this._sourceName === null ) this._getSourceName();
    return this._sourceName ?? "Unknown";
  }

  /* -------------------------------------------- */
  /*  Methods                                     */
  /* -------------------------------------------- */

  /**
   * Apply this ActiveEffect to a provided Actor.
   * @param {Actor} actor                   The Actor to whom this effect should be applied
   * @param {data.EffectChangeData} change  The change data being applied
   * @return {*}                            The resulting applied value
   */
  apply(actor, change) {
    const modes = CONST.ACTIVE_EFFECT_MODES;
    switch ( change.mode ) {
      case modes.ADD:
        return this._applyAdd(actor, change);
      case modes.MULTIPLY:
        return this._applyMultiply(actor, change);
      case modes.OVERRIDE:
        return this._applyOverride(actor, change);
      case modes.UPGRADE:
      case modes.DOWNGRADE:
        return this._applyUpgrade(actor, change);
      default:
        return this._applyCustom(actor, change);
    }
  }

  /* -------------------------------------------- */

  /**
   * Apply an ActiveEffect that uses an ADD application mode.
   * The way that effects are added depends on the data type of the current value.
   *
   * If the current value is null, the change value is assigned directly.
   * If the current type is a string, the change value is concatenated.
   * If the current type is a number, the change value is cast to numeric and added.
   * If the current type is an array, the change value is appended to the existing array if it matches in type.
   *
   * @param {Actor} actor                   The Actor to whom this effect should be applied
   * @param {data.EffectChangeData} change  The change data being applied
   * @return {*}                            The resulting applied value
   * @private
   */
  _applyAdd(actor, change) {
    const {key, value} = change;
    const current = foundry.utils.getProperty(actor.data, key) ?? null;
    const ct = foundry.utils.getType(current);
    let update = null;

    // Handle different types of the current data
    switch ( ct ) {
      case "null":
        update = value;
        break;
      case "string":
        update = current + String(value);
        break;
      case "number":
        const n = Number.fromString(value);
        if ( !isNaN(n) ) update = current + n;
        break;
      case "Array":
        const at = foundry.utils.getType(current[0]);
        if ( !current.length || (foundry.utils.getType(value) === at) ) update = current.concat([value]);
    }
    if ( update !== null ) foundry.utils.setProperty(actor.data, key, update);
    return update;
  }

  /* -------------------------------------------- */

  /**
   * Apply an ActiveEffect that uses a MULTIPLY application mode.
   * Changes which MULTIPLY must be numeric to allow for multiplication.
   * @param {Actor} actor                   The Actor to whom this effect should be applied
   * @param {data.EffectChangeData} change  The change data being applied
   * @return {*}                            The resulting applied value
   * @private
   */
  _applyMultiply(actor, change) {
    const {key, value} = change;
    const current = foundry.utils.getProperty(actor.data, key);
    const n = Number.fromString(value);
    if ( (typeof(current) !== "number") || isNaN(n) ) return null;
    const update = current * n;
    foundry.utils.setProperty(actor.data, key, update);
    return update;
  }

  /* -------------------------------------------- */

  /**
   * Apply an ActiveEffect that uses an OVERRIDE application mode.
   * Numeric data is overridden by numbers, while other data types are overridden by any value
   * @param {Actor} actor                   The Actor to whom this effect should be applied
   * @param {data.EffectChangeData} change  The change data being applied
   * @return {*}                            The resulting applied value
   * @private
   */
  _applyOverride(actor, change) {
    const {key, value} = change;
    const current = foundry.utils.getProperty(actor.data, key);
    let update = value;
    switch ( foundry.utils.getType(current) ) {
      case "number":
        update = Number.fromString(value);
        if ( isNaN(update) ) return null;
        break;
      case "string":
        update = String(value);
        break;
      case "Array":
        if (!(value instanceof Array)) return null;
        break;
    }
    foundry.utils.setProperty(actor.data, key, update);
    return update;
  }

  /* -------------------------------------------- */

  /**
   * Apply an ActiveEffect that uses an UPGRADE, or DOWNGRADE application mode.
   * Changes which UPGRADE or DOWNGRADE must be numeric to allow for comparison.
   * @param {Actor} actor                   The Actor to whom this effect should be applied
   * @param {data.EffectChangeData} change  The change data being applied
   * @return {*}                            The resulting applied value
   * @private
   */
  _applyUpgrade(actor, change) {
    let {key, value, mode} = change;
    const current = foundry.utils.getProperty(actor.data, key);
    value = Number.fromString(value);
    if ( (typeof(current) !== "number") || isNaN(value) ) return null;
    if ( (mode === CONST.ACTIVE_EFFECT_MODES.UPGRADE) && (current >= value) ) return null;
    if ( (mode === CONST.ACTIVE_EFFECT_MODES.DOWNGRADE) && (current <= value) ) return null;
    foundry.utils.setProperty(actor.data, key, value);
    return value;
  }

  /* -------------------------------------------- */

  /**
   * Apply an ActiveEffect that uses a CUSTOM application mode.
   * @param {Actor} actor                   The Actor to whom this effect should be applied
   * @param {data.EffectChangeData} change  The change data being applied
   * @return {*}                            The resulting applied value
   * @private
   */
  _applyCustom(actor, change) {
    const preHook = foundry.utils.getProperty(actor.data, change.key);
    /**
     * A hook event that fires when a custom active effect is applied.
     * @function applyActiveEffect
     * @memberof hookEvents
     * @param {Actor} actor                  The actor the active effect is being applied to
     * @param {data.EffectChangeData} change The change data being applied
     */
    Hooks.call("applyActiveEffect", actor, change);
    const postHook = foundry.utils.getProperty(actor.data, change.key);
    return postHook !== preHook ? postHook : null;
  }

  /* -------------------------------------------- */

  /**
   * Get the name of the source of the Active Effect
   * @type {string}
   */
  async _getSourceName() {
    if ( this._sourceName ) return this._sourceName;
    if ( !this.data.origin ) return this._sourceName = game.i18n.localize("None");
    const source = await fromUuid(this.data.origin);
    return this._sourceName = source?.name ?? "Unknown";
  }

  /* -------------------------------------------- */
  /*  Event Handlers                              */
  /* -------------------------------------------- */

  /** @inheritdoc */
  async _preCreate(data, options, user) {
    await super._preCreate(data, options, user);

    // Set initial duration data for Actor-owned effects
    if ( this.parent instanceof Actor ) {
      const updates = {duration: {startTime: game.time.worldTime}, transfer: false};
      if ( game.combat ) {
        updates.duration.startRound = game.combat.round;
        updates.duration.startTurn = game.combat.turn ?? 0;
      }
      this.data.update(updates);
    }
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  _onCreate(data, options, userId) {
    super._onCreate(data, options, userId);
    if ( !this.data.disabled ) this._displayScrollingStatus(true);
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  _onUpdate(data, options, userId) {
    super._onUpdate(data, options, userId);
    if ( "disabled" in data ) this._displayScrollingStatus(!data.disabled);
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  _onDelete(options, userId) {
    super._onDelete(options, userId);
    if ( !this.data.disabled ) this._displayScrollingStatus(false);
  }

  /* -------------------------------------------- */

  /**
   * Display changes to active effects as scrolling Token status text.
   * @private
   */
  _displayScrollingStatus(enabled) {
    if ( !(this.parent instanceof Actor) || this.disabled || this.isSuppressed ) return;
    if ( !(this.data.flags.core?.statusId || this.data.changes.length) ) return;
    const actor = this.parent;
    const tokens = actor.isToken ? [actor.token?.object] : actor.getActiveTokens(true);
    const label = `${enabled ? "+" : "-"}(${this.data.label})`;
    for ( let t of tokens ) {
      if ( !t?.hud ) continue;
      t.hud.createScrollingText(label, {
        anchor: CONST.TEXT_ANCHOR_POINTS.CENTER,
        direction: enabled ? CONST.TEXT_ANCHOR_POINTS.TOP : CONST.TEXT_ANCHOR_POINTS.BOTTOM,
        fontSize: 28,
        stroke: 0x000000,
        strokeThickness: 4,
        jitter: 0.25
      });
    }
  }
}
