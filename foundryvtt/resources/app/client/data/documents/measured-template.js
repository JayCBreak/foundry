/**
 * The client-side MeasuredTemplate document which extends the common BaseMeasuredTemplate model.
 * Each MeasuredTemplate document contains MeasuredTemplateData which defines its data schema.
 *
 * @extends abstract.Document
 * @extends abstract.BaseMeasuredTemplate
 * @extends ClientDocumentMixin
 *
 * @see {@link data.MeasuredTemplateData}              The MeasuredTemplate data schema
 * @see {@link documents.Scene}               The Scene document type which contains MeasuredTemplate embedded documents
 * @see {@link applications.MeasuredTemplateConfig}    The MeasuredTemplate configuration application
 *
 * @param {data.MeasuredTemplateData} [data={}]        Initial data provided to construct the MeasuredTemplate document
 * @param {Scene} parent            The parent Scene document to which this MeasuredTemplate belongs
 */
class MeasuredTemplateDocument extends CanvasDocumentMixin(foundry.documents.BaseMeasuredTemplate) {

  /* -------------------------------------------- */
  /*  Properties                                  */
  /* -------------------------------------------- */

  /**
   * A reference to the User who created the MeasuredTemplate document.
   * @type {User}
   */
  get author() {
    return game.users.get(this.data.user);
  }
}
