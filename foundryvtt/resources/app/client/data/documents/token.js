/**
 * The client-side Token document which extends the common BaseToken model.
 * Each Token document contains TokenData which defines its data schema.
 *
 * @extends abstract.Document
 * @extends abstract.BaseToken
 * @extends ClientDocumentMixin
 *
 * @see {@link data.TokenData}                The Token data schema
 * @see {@link documents.Scene}               The Scene document type which contains Token embedded documents
 * @see {@link applications.TokenConfig}      The Token configuration application
 *
 * @param {data.TokenData} [data={}]          Initial data provided to construct the Token document
 * @param {Scene} parent            The parent Scene document to which this Token belongs
 */
class TokenDocument extends CanvasDocumentMixin(foundry.documents.BaseToken) {
  constructor(data, context) {
    super(data, context);

    /**
     * A cached reference to the Actor document that this Token modifies.
     * This may be a "synthetic" unlinked Token Actor which does not exist in the World.
     * @type {Actor|null}
     */
    this._actor = context.actor || null;
  }

  /* -------------------------------------------- */
  /*  Properties                                  */
  /* -------------------------------------------- */

  /**
   * A lazily evaluated reference to the Actor this Token modifies.
   * If actorLink is true, then the document is the primary Actor document.
   * Otherwise the Actor document is a synthetic (ephemeral) document constructed using the Token's actorData.
   * @returns {Actor|null}
   */
  get actor() {
    if ( !this._actor ) {
      this._actor = this.getActor();
    }
    return this._actor;
  }

  /* -------------------------------------------- */

  /**
   * An indicator for whether or not the current User has full control over this Token document.
   * @type {boolean}
   */
  get isOwner() {
    if ( game.user.isGM ) return true;
    return this.actor?.isOwner ?? false;
  }

  /* -------------------------------------------- */

  /**
   * A convenient reference for whether this TokenDocument is linked to the Actor it represents, or is a synthetic copy
   * @type {boolean}
   */
  get isLinked() {
    return this.data.actorLink;
  }

  /* -------------------------------------------- */

  /**
   * Return a reference to a Combatant that represents this Token, if one is present in the current encounter.
   * @type {Combatant|null}
   */
  get combatant() {
    return game.combat?.getCombatantByToken(this.id) || null;
  }

  /* -------------------------------------------- */

  /**
   * An indicator for whether or not this Token is currently involved in the active combat encounter.
   * @type {boolean}
   */
  get inCombat() {
    return !!this.combatant;
  }

  /* -------------------------------------------- */

  /**
   * Is the Token currently hidden from player view?
   * @type {boolean}
   */
  get hidden() {
    return this.data.hidden;
  }

  /* -------------------------------------------- */
  /*  Methods                                     */
  /* -------------------------------------------- */

  /** @inheritdoc */
  clone(data={}, options={}) {
    const cloned = super.clone(data, options);
    cloned._actor = this._actor;
    return cloned;
  }

  /* -------------------------------------------- */

  /**
   * Create a synthetic Actor using a provided Token instance
   * If the Token data is linked, return the true Actor document
   * If the Token data is not linked, create a synthetic Actor using the Token's actorData override
   * @return {Actor}
   */
  getActor() {
    const baseActor = game.actors.get(this.data.actorId);
    if ( !baseActor ) return null;
    if ( !this.id || this.isLinked ) return baseActor;

    // Create a synthetic token Actor
    const overrideData = foundry.utils.mergeObject(baseActor.toObject(), this.data.actorData);
    const cls = getDocumentClass("Actor");
    return new cls(overrideData, {parent: this});
  }

  /* -------------------------------------------- */

  /**
   * A helper method to retrieve the underlying data behind one of the Token's attribute bars
   * @param {string} barName        The named bar to retrieve the attribute for
   * @param {string} alternative    An alternative attribute path to get instead of the default one
   * @return {object|null}          The attribute displayed on the Token bar, if any
   */
  getBarAttribute(barName, {alternative}={}) {
    const attr = alternative || this.data[barName]?.attribute;
    if ( !attr || !this.actor ) return null;
    let data = foundry.utils.getProperty(this.actor.data.data, attr);
    if ( (data === null) || (data === undefined) ) return null;
    const model = game.system.model.Actor[this.actor.type];

    // Single values
    if ( Number.isNumeric(data) ) {
      return {
        type: "value",
        attribute: attr,
        value: Number(data),
        editable: foundry.utils.hasProperty(model, attr)
      }
    }

    // Attribute objects
    else if ( ("value" in data) && ("max" in data) ) {
      return {
        type: "bar",
        attribute: attr,
        value: parseInt(data.value || 0),
        max: parseInt(data.max || 0),
        editable: foundry.utils.hasProperty(model, `${attr}.value`)
      }
    }

    // Otherwise null
    return null;
  }

  /* -------------------------------------------- */

  /**
   * A helper function to toggle a status effect which includes an Active Effect template
   * @param {{id: string, label: string, icon: string}} effectData The Active Effect data, including statusId
   * @param {object} [options]                                     Options to configure application of the Active Effect
   * @param {boolean} [options.overlay=false]                      Should the Active Effect icon be displayed as an
   *                                                               overlay on the token?
   * @param {boolean} [options.active]                             Force a certain active state for the effect.
   * @return {Promise<boolean>}                                    Whether the Active Effect is now on or off
   */
  async toggleActiveEffect(effectData, {overlay=false, active}={}) {
    if ( !this.actor || !effectData.id ) return false;

    // Remove an existing effect
    const existing = this.actor.effects.find(e => e.getFlag("core", "statusId") === effectData.id);
    const state = active ?? !existing;
    if ( !state && existing ) await existing.delete();

    // Add a new effect
    else if ( state ) {
      const createData = foundry.utils.deepClone(effectData);
      createData.label = game.i18n.localize(effectData.label);
      createData["flags.core.statusId"] = effectData.id;
      if ( overlay ) createData["flags.core.overlay"] = true;
      delete createData.id;
      const cls = getDocumentClass("ActiveEffect");
      await cls.create(createData, {parent: this.actor});
    }
    return state;
  }

  /* -------------------------------------------- */
  /*  Actor Data Operations                       */
  /* -------------------------------------------- */

  /**
   * Redirect updates to a synthetic Token Actor to instead update the tokenData override object.
   * Once an attribute in the Token has been overridden, it must always remain overridden.
   *
   * @param {object} update       The provided differential update data which should update the Token Actor
   * @param {object} options      Provided options which modify the update request
   * @returns {Promise<Actor[]>}  The updated un-linked Actor instance
   */
  async modifyActorDocument(update, options) {
    delete update._id;
    const delta = foundry.utils.diffObject(this.actor.toJSON(), foundry.utils.expandObject(update));
    await this.update({actorData: delta}, options);
    return [this.actor];
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  getEmbeddedCollection(embeddedName) {
    if ( this.isLinked ) return super.getEmbeddedCollection(embeddedName);
    switch ( embeddedName ) {
      case "Item":
        return this.actor.items;
      case "ActiveEffect":
        return this.actor.effects;
    }
  }

  /* -------------------------------------------- */

  /**
   * Redirect creation of Documents within a synthetic Token Actor to instead update the tokenData override object.
   * @param {string} embeddedName   The named embedded Document type being modified
   * @param {object[]} data         The provided initial data with which to create the embedded Documents
   * @param {object} options        Provided options which modify the creation request
   * @returns {Promise<Document[]>} The created Embedded Document instances
   */
  async createActorEmbeddedDocuments(embeddedName, data, options) {

    // Get the current embedded collection data
    const cls = getDocumentClass(embeddedName);
    const collection = this.actor.getEmbeddedCollection(embeddedName);
    const collectionData = collection.toObject();

    // Apply proposed creations to the collection data
    const hookData = []; // an array of created data
    for ( let d of data ) {
      if ( d instanceof foundry.abstract.DocumentData ) d = d.toObject();
      d = foundry.utils.expandObject(d);
      if ( !d._id || !options.keepId ) d._id = foundry.utils.randomID(16);
      collectionData.push(d);
      hookData.push(d);
    }

    // Perform a TokenDocument update, replacing the entire embedded collection in actorData
    options.action = "create";
    options.embedded = {embeddedName, hookData};
    await this.update({
      actorData: {
        [cls.metadata.collection]: collectionData
      }
    }, options);
    return hookData.map(d => this.actor.getEmbeddedDocument(embeddedName, d._id));
  }

  /* -------------------------------------------- */

  /**
   * Redirect updating of Documents within a synthetic Token Actor to instead update the tokenData override object.
   * @param {string} embeddedName   The named embedded Document type being modified
   * @param {object[]} updates      The provided differential data with which to update the embedded Documents
   * @param {object} options        Provided options which modify the update request
   * @returns {Promise<Document[]>} The updated Embedded Document instances
   */
  async updateActorEmbeddedDocuments(embeddedName, updates, options) {

    // Get the current embedded collection data
    const cls = getDocumentClass(embeddedName);
    const collection = this.actor.getEmbeddedCollection(embeddedName);
    const collectionData = collection.toObject();

    // Apply proposed updates to the collection data
    const hookData = {}; // a mapping of changes
    for ( let update of updates ) {
      const current = collectionData.find(x => x._id === update._id);
      if ( !current ) continue;
      if ( options.diff ) {
        update = foundry.utils.diffObject(current, foundry.utils.expandObject(update));
        if ( foundry.utils.isObjectEmpty(update) ) continue;
        update._id = current._id;
      }
      hookData[update._id] = update;
      foundry.utils.mergeObject(current, update);
    }

    // Perform a TokenDocument update, replacing the entire embedded collection in actorData
    if ( !Object.values(hookData).length ) return [];
    options.action = "update";
    options.embedded = {embeddedName, hookData};
    await this.update({
      actorData: {
        [cls.metadata.collection]: collectionData
      }
    }, options);
    return Object.keys(hookData).map(id => this.actor.getEmbeddedDocument(embeddedName, id));
  }

  /* -------------------------------------------- */

  /**
   * Redirect deletion of Documents within a synthetic Token Actor to instead update the tokenData override object.
   * @param {string} embeddedName   The named embedded Document type being deleted
   * @param {string[]} ids          The IDs of Documents to delete
   * @param {object} options        Provided options which modify the deletion request
   * @returns {Promise<Document[]>} The deleted Embedded Document instances
   */
  async deleteActorEmbeddedDocuments(embeddedName, ids, options) {
    const cls = getDocumentClass(embeddedName);
    const collection = this.actor.getEmbeddedCollection(embeddedName);

    // Remove proposed deletions from the collection
    const collectionData = collection.toObject();
    const deleted = [];
    const hookData = []; // an array of deleted ids
    for ( let id of ids ) {
      const doc = collection.get(id);
      if ( !doc ) continue;
      deleted.push(doc);
      hookData.push(id);
      collectionData.findSplice(d => d._id === id);
    }

    // Perform a TokenDocument update, replacing the entire embedded collection in actorData
    options.action = "delete";
    options.embedded = {embeddedName, hookData};
    await this.update({
      actorData: {
        [cls.metadata.collection]: collectionData
      }
    }, options);
    return deleted;
  }

  /* -------------------------------------------- */
  /*  Event Handlers                              */
  /* -------------------------------------------- */

  /** @inheritdoc */
  async _preUpdate(data, options, user) {
    await super._preUpdate(data, options, user);
    if ( "width" in data ) data.width = Math.max((data.width || 1).toNearest(0.5), 0.5);
    if ( "height" in data ) data.height = Math.max((data.height || 1).toNearest(0.5), 0.5);
    if ( ("actorData" in data) && !this.isLinked ) {
      await this._preUpdateTokenActor(data.actorData, options, user);
    }
  }

  /* -------------------------------------------- */

  /**
   * When the Actor data overrides change for an un-linked Token Actor, simulate the pre-update process.
   * @returns {Promise<void>}
   * @private
   */
  async _preUpdateTokenActor(data, options, user) {
    const embeddedKeys = new Set(["_id"]);

    // Simulate modification of embedded documents
    if ( options.embedded ) {
      const {embeddedName, hookData} = options.embedded;
      const cls = getDocumentClass(embeddedName);
      const documents = data[cls.metadata.collection];
      embeddedKeys.add(cls.metadata.collection);
      const result = [];

      // Handle different embedded operations
      switch (options.action) {
        case "create":
          for ( const d of hookData ) {
            const createData = foundry.utils.deepClone(d);
            const doc = new cls(d, {parent: this.actor});
            await doc._preCreate(d, options, user);
            const allowed = options.noHook || Hooks.call(`preCreate${embeddedName}`, doc, createData, options, user.id);
            if ( allowed === false ) {
              documents.findSplice(toCreate => toCreate._id === d._id);
              console.debug(`${vtt} | ${embeddedName} creation prevented by preCreate hook`);
            }
            else result.push(d);
          }
          this.actor._preCreateEmbeddedDocuments(embeddedName, result, options, user.id);
          break;

        case "update":
          for ( const [i, d] of documents.entries() ) {
            const update = hookData[d._id];
            if ( !update ) continue;
            const doc = this.actor.getEmbeddedDocument(embeddedName, d._id);
            await doc._preUpdate(update, options, user);
            const allowed = options.noHook || Hooks.call(`preUpdate${embeddedName}`, doc, update, options, user.id);
            if ( allowed === false ) {
              documents[i] = doc.toObject();
              console.debug(`${vtt} | ${embeddedName} update prevented by preUpdate hook`);
            }
            else result.push(update);
          }
          this.actor._preUpdateEmbeddedDocuments(embeddedName, result, options, user.id);
          break;

        case "delete":
          for ( const id of hookData ) {
            const doc = this.actor.getEmbeddedDocument(embeddedName, id);
            await doc._preDelete(options, user);
            const allowed = options.noHook || Hooks.call(`preDelete${embeddedName}`, doc, options, user.id);
            if ( allowed === false ) {
              documents.push(doc.toObject());
              console.debug(`${vtt} | ${embeddedName} deletion prevented by preDelete hook`);
            }
            else result.push(id);
          }
          this.actor._preDeleteEmbeddedDocuments(embeddedName, result, options, user.id);
          break;
      }
    }

    // Simulate updates to the Actor itself
    if ( Object.keys(data).some(k => !embeddedKeys.has(k)) ) {
      await this.actor._preUpdate(data, options, user);
      Hooks.callAll("preUpdateActor", this.actor, data, options, user.id);
    }
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  _onUpdate(data, options, userId) {

    // If the Actor association has changed, replace the cached Token actor
    if (("actorId" in data) || ("actorLink" in data)) {
      this._actor = this.getActor();
    }

    // If the Actor data override changed, simulate updating the synthetic Actor
    if (("actorData" in data) && !this.isLinked ) {
      this._onUpdateTokenActor(data.actorData, options, userId);
    }

    // Post-update the Token itself
    return super._onUpdate(data, options, userId);
  }

  /* -------------------------------------------- */

  /**
   * When the base Actor for a TokenDocument changes, we may need to update its Actor instance
   * @private
   */
  _onUpdateBaseActor(update={}, options) {

    // Update synthetic Actor data
    if ( !this.isLinked ) {
      update = foundry.utils.mergeObject(update, this.data.actorData, {
        insertKeys: false,
        insertValues: false,
        inplace: false
      });
      this.actor.data.update(update, options);
      this.actor.prepareData();
      this.actor.sheet.render(false);
    }

    // Update tracked Combat resource
    const c = this.combatant;
    if ( c && foundry.utils.hasProperty(update.data || {}, game.combat.settings.resource) ) {
      c.updateResource();
      ui.combat.render();
    }

    // Trigger redraws on the token
    if ( this.parent.isView ) {
      this.object.drawBars();
      if ( "effects" in update ) this.object.drawEffects();
    }
  }

  /* -------------------------------------------- */

  /**
   * When the Actor data overrides change for an un-linked Token Actor, simulate the post-update process.
   * @private
   */
  _onUpdateTokenActor(data, options, userId) {
    const embeddedKeys = new Set(["_id"]);

    // Obtain references to any embedded documents which will be deleted
    let deletedDocuments = [];
    if ( options.embedded && (options.action === "delete") ) {
      const {embeddedName, hookData} = options.embedded;
      const collection = this.actor.getEmbeddedCollection(embeddedName);
      deletedDocuments = hookData.map(id => collection.get(id));
    }

    // Update the Token Actor data
    if ( options.embedded ) this.actor.data.update(data, {recursive: false});
    else {
      // We have to handle embedded collections specially to be consistent with the server's source of truth about them.
      for ( const k of Object.keys(data) ) {
        const schema = this.actor.data.schema[k];
        if ( schema?.isCollection ) {
          this.actor.data.update({[k]: this.data.actorData[k]}, {recursive: false});
          delete data[k];
        }
      }
      this.actor.data.update(data, {recursive: true});
    }
    this.actor.prepareData();

    // Simulate modification of embedded documents
    if ( options.embedded ) {
      const {embeddedName, hookData} = options.embedded;
      const cls = Actor.metadata.embedded[embeddedName];
      const changes = data[cls.metadata.collection];
      const collection = this.actor.getEmbeddedCollection(embeddedName);
      embeddedKeys.add(cls.metadata.collection);
      const result = [];

      switch (options.action) {
        case "create":
          const created = [];
          for ( const d of hookData ) {
            result.push(d);
            const doc = collection.get(d._id);
            created.push(doc);
            doc._onCreate(d, options, userId);
            Hooks.callAll(`create${embeddedName}`, doc, options, userId);
          }
          this.actor._onCreateEmbeddedDocuments(embeddedName, created, result, options, userId);
          break;

        case "update":
          const documents = [];
          for ( let d of changes ) {
            const update = hookData[d._id];
            if ( !update ) continue;
            result.push(update);
            const doc = collection.get(d._id);
            documents.push(doc);
            doc._onUpdate(update, options, userId);
            Hooks.callAll(`update${embeddedName}`, doc, update, options, userId);
          }
          this.actor._onUpdateEmbeddedDocuments(embeddedName, documents, result, options, userId);
          break;

        case "delete":
          for ( let doc of deletedDocuments ) {
            doc._onDelete(options, userId);
            Hooks.callAll(`delete${embeddedName}`, doc, options, userId);
          }
          this.actor._onDeleteEmbeddedDocuments(embeddedName, deletedDocuments, hookData, options, userId);
          break;
      }
    }

    // Update tracked Combat resource
    const c = this.combatant;
    if ( c && foundry.utils.hasProperty(data.data || {}, game.combat.settings.resource) ) {
      c.updateResource();
      ui.combat.render();
    }

    // Simulate updates to the Actor itself
    if ( Object.keys(data).some(k => !embeddedKeys.has(k)) ) {
      this.actor._onUpdate(data, options, userId);
      Hooks.callAll("updateActor", this.actor, data, options, userId);
    }
  }

  /* -------------------------------------------- */

  /**
   * Get an Array of attribute choices which could be tracked for Actors in the Combat Tracker
   * @return {object}
   */
  static getTrackedAttributes(data, _path=[]) {
    if ( !data ) {
      data = {};
      for ( let model of Object.values(game.system.model.Actor) ) {
        foundry.utils.mergeObject(data, model);
      }
    }

    // Track the path and record found attributes
    const attributes = {
      "bar": [],
      "value": []
    };

    // Recursively explore the object
    for ( let [k, v] of Object.entries(data) ) {
      let p  = _path.concat([k]);

      // Check objects for both a "value" and a "max"
      if ( v instanceof Object ) {
        const isBar = ("value" in v) && ("max" in v);
        if ( isBar ) attributes.bar.push(p);
        else {
          const inner = this.getTrackedAttributes(data[k], p);
          attributes.bar.push(...inner.bar);
          attributes.value.push(...inner.value);
        }
      }

      // Otherwise identify values which are numeric or null
      else if ( Number.isNumeric(v) || (v === null) ) {
        attributes.value.push(p);
      }
    }
    return attributes;
  }

  /* -------------------------------------------- */

  /**
   * Inspect the Actor data model and identify the set of attributes which could be used for a Token Bar
   * @return {object}
   */
  static getTrackedAttributeChoices(attributes) {
    attributes = attributes || this.getTrackedAttributes();
    attributes.bar = attributes.bar.map(v => v.join("."));
    attributes.bar.sort((a, b) => a.localeCompare(b));
    attributes.value = attributes.value.map(v => v.join("."));
    attributes.value.sort((a, b) => a.localeCompare(b));
    return {
      [game.i18n.localize("TOKEN.BarAttributes")]: attributes.bar,
      [game.i18n.localize("TOKEN.BarValues")]: attributes.value
    }
  }
}


/**
 * An extended Document definition used specifically
 * This ensures that the PrototypeTokenData schema is used instead of the standard TokenData.
 * This exists specifically for prototype Token configuration in the TokenConfig app and should not be used otherwise.
 * @extends {TokenDocument}
 * @private
 */
class PrototypeTokenDocument extends TokenDocument {
  static get schema() {
    return foundry.data.PrototypeTokenData;
  }

  /** @inheritdoc */
  async update(data={}, context={}) {
    await this.actor.update({token: data}, context);
    this.data.update(this.actor.data.token.toObject());
    return this;
  }
}
