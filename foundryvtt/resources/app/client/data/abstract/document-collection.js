/**
 * An abstract subclass of the Collection container which defines a collection of Document instances.
 * @extends {Collection}
 * @interface
 *
 * @param {object[]} data      An array of data objects from which to create document instances
 */
class DocumentCollection extends foundry.utils.Collection {
  constructor() {
    super();

    /**
     * An Array of application references which will be automatically updated when the collection content changes
     * @type {Application[]}
     */
    this.apps = [];
  }

  /* -------------------------------------------- */
  /*  Collection Properties                       */
  /* -------------------------------------------- */

  /**
   * The Collection class name
   * @type {string}
   */
  get name() {
    return this.constructor.name;
  }

  /**
   * A reference to the Document class definition which is contained within this DocumentCollection.
   * @type {Function}
   */
  get documentClass() {
    return getDocumentClass(this.documentName);
  }

  /**
   * A reference to the named Document class which is contained within this DocumentCollection.
   * @type {string}
   */
  get documentName() {
    throw new Error("A subclass of DocumentCollection must implement the documentName getter");
  }

  /* -------------------------------------------- */
  /*  Collection Methods                          */
  /* -------------------------------------------- */

  /** @inheritdoc */
  set(id, document) {
    const cls = this.documentClass;
    if (!(document instanceof cls)) {
      throw new Error(`You may only push instances of ${cls.documentName} to the ${this.name} collection`);
    }
    return super.set(document.id, document);
  }

  /* -------------------------------------------- */

  /**
   * Render any Applications associated with this DocumentCollection.
   */
  render(force, options) {
    for (let a of this.apps) a.render(force, options);
  }

  /* -------------------------------------------- */
  /*  Database Operations                         */
  /* -------------------------------------------- */

  /**
   * Update all objects in this DocumentCollection with a provided transformation.
   * Conditionally filter to only apply to Entities which match a certain condition.
   * @param {Function|object} transformation    An object of data or function to apply to all matched objects
   * @param {Function|null}  condition          A function which tests whether to target each object
   * @param {object} [options]                  Additional options passed to Document.update
   * @return {Promise<Document[]>}              An array of updated data once the operation is complete
   */
  async updateAll(transformation, condition=null, options={}) {
    const hasTransformer = transformation instanceof Function;
    if ( !hasTransformer && (foundry.utils.getType(transformation) !== "Object") ) {
      throw new Error("You must provide a data object or transformation function");
    }
    const hasCondition = condition instanceof Function;
    const updates = [];
    for ( let doc of this ) {
      if ( hasCondition && !condition(doc) ) continue;
      const update = hasTransformer ? transformation(doc) : foundry.utils.deepClone(transformation);
      update._id = doc.id;
      updates.push(update);
    }
    return this.documentClass.updateDocuments(updates, options);
  }

  /* -------------------------------------------- */
  /*  Event Handlers                              */
  /* -------------------------------------------- */

  /**
   * Preliminary actions taken before a set of Documents in this Collection are created.
   * @param {object[]} result       An Array of created data objects
   * @param {object} options        Options which modified the creation operation
   * @param {string} userId         The ID of the User who triggered the operation
   * @protected
   */
  _preCreateDocuments(result, options, userId) {}

  /* -------------------------------------------- */

  /**
   * Follow-up actions taken after a set of Documents in this Collection are created.
   * @param {Document[]} documents  An Array of created Documents
   * @param {object[]} result       An Array of created data objects
   * @param {object} options        Options which modified the creation operation
   * @param {string} userId         The ID of the User who triggered the operation
   * @protected
   */
  _onCreateDocuments(documents, result, options, userId) {
    if ( options.render !== false ) this.render(false, this._getRenderContext("create", documents, result));
  }

  /* -------------------------------------------- */

  /**
   * Preliminary actions taken before a set of Documents in this Collection are updated.
   * @param {object[]} result       An Array of incremental data objects
   * @param {object} options        Options which modified the update operation
   * @param {string} userId         The ID of the User who triggered the operation
   * @protected
   */
  _preUpdateDocuments(result, options, userId) {}

  /* -------------------------------------------- */

  /**
   * Follow-up actions taken after a set of Documents in this Collection are updated.
   * @param {Document[]} documents  An Array of updated Documents
   * @param {object[]} result       An Array of incremental data objects
   * @param {object} options        Options which modified the update operation
   * @param {string} userId         The ID of the User who triggered the operation
   * @protected
   */
  _onUpdateDocuments(documents, result, options, userId) {
    if ( options.render !== false ) this.render(false, this._getRenderContext("update", documents, result));
  }

  /* -------------------------------------------- */

  /**
   * Preliminary actions taken before a set of Documents in this Collection are deleted.
   * @param {string[]} result       An Array of document IDs being deleted
   * @param {object} options        Options which modified the deletion operation
   * @param {string} userId         The ID of the User who triggered the operation
   * @protected
   */
  _preDeleteDocuments(result, options, userId) {}

  /* -------------------------------------------- */

  /**
   * Follow-up actions taken after a set of Documents in this Collection are deleted.
   * @param {Document[]} documents  An Array of deleted Documents
   * @param {string[]} result       An Array of document IDs being deleted
   * @param {object} options        Options which modified the deletion operation
   * @param {string} userId         The ID of the User who triggered the operation
   * @protected
   */
  _onDeleteDocuments(documents, result, options, userId) {
    if ( options.render !== false ) this.render(false, this._getRenderContext("delete", documents, result));
  }

  /* -------------------------------------------- */

  /**
   * Generate the render context information provided for CRUD operations.
   * @param {string} action           The CRUD operation.
   * @param {Document[]} documents    The documents being operated on.
   * @param {object[]|string[]} data  An array of creation or update objects, or an array of document IDs, depending on
   *                                  the operation.
   * @returns {{action: string, documentType: string, documents: Document[], data: object[]|string[]}}
   * @private
   */
  _getRenderContext(action, documents, data) {
    const documentType = this.documentName;
    const context = {action, documentType, documents, data};

    Object.defineProperty(context, "entities", {
      get() {
        console.warn("The 'entities' render context is deprecated. Please use 'documents' instead.");
        return documents;
      }
    });

    Object.defineProperty(context, "entityType", {
      get() {
        console.warn("The 'entityType' render context is deprecated. Please use 'documentType' instead.");
        return documentType;
      }
    });

    return context;
  }
}
