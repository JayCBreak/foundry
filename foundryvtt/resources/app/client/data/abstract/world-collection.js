/**
 * A collection of world-level Document objects with a singleton instance per primary Document type.
 * Each primary Document type has an associated subclass of WorldCollection which contains them.
 * @extends {DocumentCollection}
 * @abstract
 * @see {Game#collections}
 *
 * @param {object[]} data      An array of data objects from which to create Document instances
 */
class WorldCollection extends DocumentCollection {
  constructor(data=[]) {
    super();

    /**
     * The source data array from which the Documents in the WorldCollection are created
     * @type {object[]}
     * @private
     */
    Object.defineProperty(this, "_source", {
      value: data,
      writable: false
    });

    // Initialize data
    this._initialize();
  }

  /* -------------------------------------------- */

  /**
   * Initialize the WorldCollection object by constructing its contained Document instances
   * @private
   */
  _initialize() {
    this.clear();
    for ( let d of this._source ) {
      let doc;
      try {
        doc = new this.documentClass(d);
        super.set(doc.id, doc);
      } catch(err) {
        Hooks.onError("WorldCollection#_initialize", err, {
          msg: `Failed to initialized ${this.documentName} [${d._id}]`,
          log: "error",
          id: d._id
        });
      }
    }
  }

  /* -------------------------------------------- */
  /*  Collection Properties                       */
  /* -------------------------------------------- */

  /** @inheritdoc */
  get documentName() {
    return this.constructor.documentName;
  }

  /* -------------------------------------------- */

  /**
   * The base Document type which is contained within this WorldCollection
   * @type {string|null}
   */
  static documentName = null;

  /* -------------------------------------------- */

  /**
   * Return a reference to the SidebarDirectory application for this WorldCollection.
   * @type {SidebarDirectory}
   */
  get directory() {
    const doc = getDocumentClass(this.constructor.documentName);
    return ui[doc.metadata.collection];
  }

  /* -------------------------------------------- */

  /**
   * Return a reference to the singleton instance of this WorldCollection, or null if it has not yet been created.
   * @type {WorldCollection}
   */
  static get instance() {
    return game.collections.get(this.documentName);
  }

  /* -------------------------------------------- */
  /*  Collection Methods                          */
  /* -------------------------------------------- */

  /** @inheritdoc */
  set(id, document) {
    super.set(id, document);
    this._source.push(document.toJSON());
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  delete(id) {
    super.delete(id);
    this._source.findSplice(e => e._id === id);
  }

  /* -------------------------------------------- */

  /**
   * Import a Document from a Compendium collection, adding it to the current World.
   * @param {CompendiumCollection} pack The CompendiumCollection instance from which to import
   * @param {string} id             The ID of the compendium entry to import
   * @param {Object} [updateData]   Optional additional data used to modify the imported Document before it is created
   * @param {Object} [options]      Optional arguments passed to the {@link WorldCollection#fromCompendium} and {@link Document.create} methods
   * @return {Promise<Document>}    The imported Document instance
   */
  async importFromCompendium(pack, id, updateData={}, options={}) {
    const cls = this.documentClass;
    if (pack.documentName !== cls.documentName) {
      throw new Error(`The ${pack.documentName} Document type provided by Compendium ${pack.collection} is incorrect for this Collection`);
    }

    // Prepare the source data from which to create the Document
    const document = await pack.getDocument(id);
    const sourceData = this.fromCompendium(document, options);
    const createData = foundry.utils.mergeObject(sourceData, updateData);

    // Create the Document
    console.log(`${vtt} | Importing ${cls.documentName} ${document.name} from ${pack.collection}`);
    this.directory.activate();
    return this.documentClass.create(createData, options);
  }

  /* -------------------------------------------- */

  /**
   * Apply data transformations when importing a Document from a Compendium pack
   * @param {Document|object} document    The source Document, or a plain data object
   * @param {object} [options]            Additional options which modify how the document is imported
   * @param {boolean} [options.addFlags=false]        Add flags which track the import source
   * @param {boolean} [options.clearSort=true]        Clear the currently assigned folder and sort order
   * @param {boolean} [options.clearPermissions=true] Clear document permissions
   * @param {boolean} [options.keepId=false]          Retain the Document id from the source Compendium
   * @return {object}                     The processed data ready for world Document creation
   */
  fromCompendium(document, {addFlags=true, clearSort=true, clearPermissions=true, keepId=false}={}) {

    // Prepare the data structure
    let data = document;
    if (document instanceof foundry.abstract.Document) {
      data = document.toObject();
      if (!data.flags.core?.sourceId && addFlags) foundry.utils.setProperty(data, "flags.core.sourceId", document.uuid);
    }

    // Eliminate certain fields
    if (!keepId) delete data["_id"];
    if (clearSort) {
      delete data["folder"];
      delete data["sort"];
    }
    if (clearPermissions && ("permission" in data)) {
      data.permission = {
        default: CONST.DOCUMENT_PERMISSION_LEVELS.NONE,
        [game.user.id]: CONST.DOCUMENT_PERMISSION_LEVELS.OWNER
      };
    }
    return data;
  }

  /* -------------------------------------------- */
  /*  Sheet Registration Methods                  */
  /* -------------------------------------------- */

  /**
   * Register a Document sheet class as a candidate which can be used to display Documents of a given type.
   * See {@link DocumentSheetConfig.registerSheet} for details.
   * @static
   * @see DocumentSheetConfig.registerSheet
   *
   * @example <caption>Register a new ActorSheet subclass for use with certain Actor types.</caption>
   * Actors.registerSheet("dnd5e", ActorSheet5eCharacter, { types: ["character], makeDefault: true });
   */
  static registerSheet(...args) {
    DocumentSheetConfig.registerSheet(getDocumentClass(this.documentName), ...args);
  }

  /* -------------------------------------------- */

  /**
   * Unregister a Document sheet class, removing it from the list of available sheet Applications to use.
   * See {@link DocumentSheetConfig.unregisterSheet} for detauls.
   * @static
   * @see DocumentSheetConfig.unregisterSheet
   *
   * @example <caption>Deregister the default ActorSheet subclass to replace it with others.</caption>
   * Actors.unregisterSheet("core", ActorSheet);
   */
  static unregisterSheet(...args) {
    DocumentSheetConfig.unregisterSheet(getDocumentClass(this.documentName), ...args);
  }

  /* -------------------------------------------- */

  /**
   * Return an array of currently registered sheet classes for this Document type.
   * @static
   * @type {DocumentSheet[]}
   */
  static get registeredSheets() {
    const sheets = new Set();
    for ( let t of Object.values(CONFIG[this.documentName].sheetClasses) ) {
      for ( let s of Object.values(t) ) {
        sheets.add(s.cls);
      }
    }
    return Array.from(sheets);
  }
}
