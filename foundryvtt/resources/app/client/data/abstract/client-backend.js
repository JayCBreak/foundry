/**
 * The client-side database backend implementation which handles Document modification operations.
 * @extends {abstract.DatabaseBackend}
 * @implements {abstract.DatabaseBackend}
 */
class ClientDatabaseBackend extends foundry.abstract.DatabaseBackend {

  /* -------------------------------------------- */
  /*  Socket Workflows                            */
  /* -------------------------------------------- */

  /**
   * Activate the Socket event listeners used to receive responses from events which modify database documents
   * @param {Socket} socket   The active game socket
   */
  activateSocketListeners(socket) {

    // Document Operations
    socket.on("modifyDocument", response => {
      const { request } = response;
      const isEmbedded = CONST.DOCUMENT_TYPES.includes(request.parentType);
      switch ( request.action ) {
        case "create":
          if ( isEmbedded ) return this._handleCreateEmbeddedDocuments(response);
          else return this._handleCreateDocuments(response);
        case "update":
          if ( isEmbedded ) return this._handleUpdateEmbeddedDocuments(response);
          else return this._handleUpdateDocuments(response);
        case "delete":
          if ( isEmbedded ) return this._handleDeleteEmbeddedDocuments(response);
          else return this._handleDeleteDocuments(response);
        default:
          throw new Error(`Invalid Document modification action ${request.action} provided`);
      }
    });
  }

  /* -------------------------------------------- */
  /*  Get Operations                              */
  /* -------------------------------------------- */

  /** @inheritdoc */
  async _getDocuments(documentClass, {query, options, pack}, user) {
    const type = documentClass.documentName;

    // Dispatch the request
    const response = await SocketInterface.dispatch("modifyDocument", {
      type: type,
      action: "get",
      query: query,
      options: options,
      pack: pack
    });

    // Return the index only
    if ( options.index ) return response.result;

    // Create Document objects
    return response.result.map(data => {
      return new documentClass(data, {pack});
    });
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  async _getEmbeddedDocuments(documentClass, parent, {query, options, pack}, user) {
    throw new Error("Get operations for embedded Documents are currently un-supported");
  }

  /* -------------------------------------------- */
  /*  Create Operations                           */
  /* -------------------------------------------- */

  /** @inheritdoc */
  async _createDocuments(documentClass, {data, options, pack}, user) {
    const toCreate = await this._preCreateDocumentArray(documentClass, {data, options, pack, user});
    if ( !toCreate.length || options.temporary ) return toCreate;
    const response = await SocketInterface.dispatch("modifyDocument", {
      type: documentClass.documentName,
      action: "create",
      data: toCreate,
      options: options,
      pack: pack
    });
    return this._handleCreateDocuments(response);
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  async _createEmbeddedDocuments(documentClass, parent, {data, options, pack}, user) {

    // Special Case
    if ( parent.parent instanceof TokenDocument ) {
      return parent.parent.createActorEmbeddedDocuments(documentClass.documentName, data, options);
    }
    if ( parent.parent ) {
      throw new Error(`Managing embedded Documents which are not direct descendants of a primary Document is un-supported at this time.`);
    }

    // Standard Case
    const toCreate = await this._preCreateDocumentArray(documentClass, {data, options, pack, parent, user});
    if ( !toCreate.length || options.temporary ) return toCreate;
    const response = await SocketInterface.dispatch("modifyDocument", {
      action: "create",
      type: documentClass.documentName,
      parentType: parent.documentName,
      parentId: parent.id,
      data: toCreate,
      options: options,
      pack: pack
    });
    return this._handleCreateEmbeddedDocuments(response);
  }

  /* -------------------------------------------- */

  /**
   * Perform a standardized pre-creation workflow for all Document types. For internal use only.
   * @private
   */
  async _preCreateDocumentArray(documentClass, {data, options, pack, parent, user}) {
    user = user || game.user;
    const type = documentClass.documentName;
    const toCreate = [];
    for ( let d of data ) {

      // Handle DocumentData or expand flattened objects
      if ( d instanceof foundry.abstract.DocumentData ) d = d.toObject();
      else if ( Object.keys(d).some(k => k.indexOf(".") !== -1) ) d = foundry.utils.expandObject(d);
      const createData = foundry.utils.deepClone(d);

      // Perform pre-creation operations
      let doc;
      try {
        doc = new documentClass(d, {parent, pack});
      } catch(err) {
        Hooks.onError("ClientDatabaseBackend#_preCreateDocumentArray", err, {id: d._id, log: "error", notify: "error"});
        continue;
      }
      await doc._preCreate(createData, options, user);

      /**
       * A hook event that fires for every Document type before execution of a creation workflow. Substitute the
       * Document name in the hook event to target a specific Document type, for example "preCreateActor". This hook
       * only fires for the client who is initiating the creation request.
       *
       * The hook provides the pending document instance which will be used for the Document creation. Hooked functions
       * may modify that data or prevent the workflow entirely by explicitly returning false.
       *
       * @function preCreateDocument
       * @memberof hookEvents
       * @param {Document} document     The pending document which is requested for creation
       * @param {object} data           The initial data object provided to the document creation request
       * @param {object} options        Additional options which modify the creation request
       * @param {string} userId         The ID of the requesting user, always game.user.id
       * @return {boolean|void}         Explicitly return false to prevent creation of this Document
       */
      const allowed = options.noHook || Hooks.call(`preCreate${type}`, doc, createData, options, user.id);
      if ( allowed === false ) {
        console.debug(`${vtt} | ${type} creation prevented by preCreate hook`);
        continue;
      }
      toCreate.push(doc);
    }
    return toCreate;
  }

  /* -------------------------------------------- */

  /**
   * Handle a SocketResponse from the server when one or multiple documents were created
   * @param {SocketResponse} response               The provided Socket response
   * @param {SocketRequest} [response.request]      The initial socket request
   * @param {object[]} [response.result]            An Array of created data objects
   * @param {string} [response.userId]              The id of the requesting User
   * @return {Document[]}                           An Array of created Document instances
   * @private
   */
  _handleCreateDocuments({request, result=[], userId}) {
    const { type, options, pack } = request;

    // Pre-operation collection actions
    const collection = pack ? game.packs.get(pack) : game.collections.get(type);
    collection._preCreateDocuments(result, options, userId);

    // Perform creations and execute callbacks
    const callbacks = this._postCreateDocumentCallbacks(type, collection, result, {options, userId, pack});
    const documents = callbacks.map(fn => fn());

    // Post-operation collection actions
    collection._onCreateDocuments(documents, result, options, userId);
    this._logOperation("Created", type, documents, {level: "info", pack});
    return documents;
  }

  /* -------------------------------------------- */

  /**
   * Handle a SocketResponse from the server when one or multiple documents were created
   * @param {SocketResponse} response               The provided Socket response
   * @param {SocketRequest} [response.request]      The initial socket request
   * @param {object[]} [response.result]            An Array of created data objects
   * @param {string} [response.userId]              The id of the requesting User
   * @return {Document[]}                           An Array of created Document instances
   * @private
   */
  _handleCreateEmbeddedDocuments({request, result=[], userId}) {
    const {type, parentType, parentId, options, pack} = request;
    const parentCollection = pack ? game.packs.get(pack) : game.collections.get(parentType);
    const parent = parentCollection.get(parentId, {strict: !pack});
    if ( !parent || !result.length ) return [];

    // Pre-operation parent actions
    const collection = parent.getEmbeddedCollection(type);
    parent._preCreateEmbeddedDocuments(type, result, options, userId);

    // Perform creations and execute callbacks
    const callbacks = this._postCreateDocumentCallbacks(type, collection, result, {options, userId, parent, pack});
    parent.prepareData();
    const documents = callbacks.map(fn => fn());

    // Perform follow-up operations for the parent Document
    parent._onCreateEmbeddedDocuments(type, documents, result, options, userId);
    this._logOperation("Created", type, documents, {level: "info", parent, pack});
    return documents;
  }

  /* -------------------------------------------- */

  /**
   * Perform a standardized post-creation workflow for all Document types. For internal use only.
   * @return {Function[]}   An array of callback operations to perform once every Document is created
   * @private
   */
  _postCreateDocumentCallbacks(type, collection, result, {options, userId, parent, pack}) {
    const cls = getDocumentClass(type);
    const callback = (doc, data) => {
      doc._onCreate(data, options, userId);
      /**
       * A hook event that fires for every embedded Document type after conclusion of a creation workflow.
       * Substitute the Document name in the hook event to target a specific type, for example "createToken".
       * This hook fires for all connected clients after the creation has been processed.
       *
       * @function createDocument
       * @memberof hookEvents
       * @param {Document} document     The new Document instance which has been created
       * @param {object} options        Additional options which modified the creation request
       * @param {string} userId         The ID of the User who triggered the creation workflow
       */
      Hooks.callAll(`create${type}`, doc, options, userId);
      return doc;
    }
    return result.map(data => {
      const doc = new cls(data, {parent, pack});
      collection.set(doc.id, doc);
      return callback.bind(this, doc, data);
    });
  }

  /* -------------------------------------------- */
  /*  Update Operations                           */
  /* -------------------------------------------- */

  /** @inheritdoc */
  async _updateDocuments(documentClass, {updates, options, pack}, user) {
    const collection = pack ? game.packs.get(pack) : game.collections.get(documentClass.documentName);
    const toUpdate = await this._preUpdateDocumentArray(collection, {updates, options, user});
    if ( !toUpdate.length ) return [];
    const response = await SocketInterface.dispatch("modifyDocument", {
      type: documentClass.documentName,
      action: "update",
      updates: toUpdate,
      options: options,
      pack: pack
    });
    return this._handleUpdateDocuments(response);
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  async _updateEmbeddedDocuments(documentClass, parent, {updates, options, pack}, user) {

    // Special Cases
    if ( (parent instanceof TokenDocument) && (updates.length === 1) ) {
      return parent.modifyActorDocument(updates[0], options);
    }
    if ( parent.parent instanceof TokenDocument ) {
      return parent.parent.updateActorEmbeddedDocuments(documentClass.documentName, updates, options);
    }
    if ( parent.parent ) {
      throw new Error(`Managing embedded Documents which are not direct descendants of a primary Document is un-supported at this time.`);
    }

    // Normal case
    const collection = parent.getEmbeddedCollection(documentClass.documentName);
    const toUpdate = await this._preUpdateDocumentArray(collection, {updates, options, user});
    if ( !toUpdate.length ) return [];
    const response = await SocketInterface.dispatch("modifyDocument", {
      action: "update",
      type: documentClass.documentName,
      parentType: parent.documentName,
      parentId: parent.id,
      updates: toUpdate,
      options: options,
      pack: pack
    });
    return this._handleUpdateEmbeddedDocuments(response);
  }

  /* -------------------------------------------- */

  /**
   * Perform a standardized pre-update workflow for all Document types. For internal use only.
   * @private
   */
  async _preUpdateDocumentArray(collection, {updates, options, user}) {
    user = user || game.user;
    const toUpdate = [];
    if ( collection instanceof CompendiumCollection ) {
      const updateIds = updates.reduce((arr, u) => {
        if ( u._id && !collection.has(u._id) ) arr.push(u._id);
        return arr;
      }, []);
      await collection.getDocuments({_id: {$in: updateIds}});
    }
    for ( let update of updates ) {

      // Get the document being updated
      if ( !update._id ) throw new Error(`You must provide an _id for every object in the update data Array.`);
      const doc = collection.get(update._id, {strict: true});

      // Validate the changes against current data
      if ( options.diff ) {
        update = foundry.utils.diffObject(doc.data._source, foundry.utils.expandObject(update));
        if ( foundry.utils.isObjectEmpty(update) ) continue;
        update._id = doc.id;
      }

      // Validate the changes and perform pre-update operations
      try {
        doc.data.validate({changes: update, clean: true, strict: true});
      } catch(err) {
        ui.notifications.error(err.message.split("] ").pop());
        Hooks.onError("ClientDatabaseBackend#_preUpdateDocumentArray", err, {id: doc.id, log: "error"});
        continue;
      }
      await doc._preUpdate(update, options, user);

      /**
       * A hook event that fires for every Document type before execution of an update workflow. Substitute the Document
       * name in the hook event to target a specific Document type, for example "preUpdateActor". This hook only fires
       * for the client who is initiating the update request.
       *
       * The hook provides the differential data which will be used to update the Document. Hooked functions may modify
       * that data or prevent the workflow entirely by explicitly returning false.
       *
       * @function preUpdateDocument
       * @memberof hookEvents
       * @param {Document} document     The Document instance being updated
       * @param {object} change         Differential data that will be used to update the document
       * @param {object} options        Additional options which modify the update request
       * @param {string} userId         The ID of the requesting user, always game.user.id
       * @return {boolean|void}         Explicitly return false to prevent update of this Document
       */
      const allowed = options.noHook || Hooks.call(`preUpdate${doc.documentName}`, doc, update, options, user.id);
      if ( allowed === false ) {
        console.debug(`${vtt} | ${doc.documentName} update prevented by preUpdate hook`);
        continue;
      }
      toUpdate.push(update);
    }
    return toUpdate;
  }

  /* -------------------------------------------- */

  /**
   * Handle a SocketResponse from the server when one or multiple documents were updated
   * @param {SocketResponse} response               The provided Socket response
   * @param {SocketRequest} [response.request]      The initial socket request
   * @param {object[]} [response.result]            An Array of incremental data objects
   * @param {string} [response.userId]              The id of the requesting User
   * @return {Document[]}                           An Array of updated Document instances
   * @private
   */
  _handleUpdateDocuments({request, result=[], userId}={}) {
    const { type, options, pack } = request;
    const collection = pack ? game.packs.get(pack) : game.collections.get(type);

    // Pre-operation collection actions
    collection._preUpdateDocuments(result, options, userId);

    // Perform updates and execute callbacks
    const callbacks = this._postUpdateDocumentCallbacks(collection, result, {options, userId});
    const documents = callbacks.map(fn => fn());

    // Post-operation collection actions
    collection._onUpdateDocuments(documents, result, options, userId);
    if ( CONFIG.debug.documents ) this._logOperation("Updated", type, documents, {level: "debug", pack});
    return documents;
  }

  /* -------------------------------------------- */

  /**
   * Handle a SocketResponse from the server when embedded Documents are updated in a parent Document.
   * @param {SocketResponse} response               The provided Socket response
   * @param {SocketRequest} [response.request]      The initial socket request
   * @param {object[]} [response.result]            An Array of incremental data objects
   * @param {string} [response.userId]              The id of the requesting User
   * @return {Document[]}                           An Array of updated Document instances
   * @private
   */
  _handleUpdateEmbeddedDocuments({request, result=[], userId}) {
    const { type, parentType, parentId, options, pack } = request;
    const parentCollection = pack ? game.packs.get(pack) : game.collections.get(parentType);
    const parent = parentCollection.get(parentId, {strict: !pack});
    if ( !parent || !result.length ) return [];

    // Pre-operation parent actions
    const collection = parent.getEmbeddedCollection(type);
    parent._preUpdateEmbeddedDocuments(type, result, options, userId);

    // Perform updates and execute callbacks
    const callbacks = this._postUpdateDocumentCallbacks(collection, result, {options, userId});
    parent.prepareData();
    const documents = callbacks.map(fn => fn());

    // Perform follow-up operations for the parent Document
    parent._onUpdateEmbeddedDocuments(type, documents, result, options, userId);
    if ( CONFIG.debug.documents ) this._logOperation("Updated", type, documents, {level: "debug", parent, pack});
    return documents;
  }

  /* -------------------------------------------- */

  /**
   * Perform a standardized post-update workflow for all Document types. For internal use only.
   * @return {Function[]}   An array of callback operations to perform after every Document is updated
   * @private
   */
  _postUpdateDocumentCallbacks(collection, result, {options, userId}) {
    const callback = (doc, change) => {
      doc._onUpdate(change, options, userId);
      /**
       * A hook event that fires for every Document type after conclusion of an update workflow.
       * Substitute the Document name in the hook event to target a specific Document type, for example "updateActor".
       * This hook fires for all connected clients after the update has been processed.
       *
       * @function updateDocument
       * @memberof hookEvents
       * @param {Document} document     The existing Document which was updated
       * @param {object} change         Differential data that was used used to update the document
       * @param {object} options        Additional options which modified the update request
       * @param {string} userId         The ID of the User who triggered the update workflow
       */
      Hooks.callAll(`update${doc.documentName}`, doc, change, options, userId);
      return doc;
    }
    const callbacks = [];
    for ( let change of result ) {
      const doc = collection.get(change._id, {strict: false});
      if ( !doc ) continue;
      doc.data.update(change, options);
      doc.prepareData();
      callbacks.push(callback.bind(this, doc, change));
    }
    return callbacks;
  }

  /* -------------------------------------------- */
  /*  Delete Operations                           */
  /* -------------------------------------------- */

  /** @inheritdoc */
  async _deleteDocuments(documentClass, {ids, options, pack}, user) {
    user = user || game.user;
    const collection = pack ? game.packs.get(pack) : game.collections.get(documentClass.documentName);
    if ( options.deleteAll ) ids = pack ? collection.index.keys() : collection.keys();
    const toDelete = await this._preDeleteDocumentArray(collection, {ids, options, user});
    if ( !toDelete.length ) return [];
    const response = await SocketInterface.dispatch("modifyDocument", {
      type: documentClass.documentName,
      action: "delete",
      ids: toDelete,
      options: options,
      pack: pack
    });
    return this._handleDeleteDocuments(response);
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  async _deleteEmbeddedDocuments(documentClass, parent, {ids, options, pack}, user) {

    // Special Cases
    if ( parent.parent instanceof TokenDocument ) {
      return parent.parent.deleteActorEmbeddedDocuments(documentClass.documentName, ids, options);
    }
    if ( parent.parent ) {
      throw new Error(`Managing embedded Documents which are not direct descendants of a primary Document is un-supported at this time.`);
    }

    // Normal case
    const collection = parent.getEmbeddedCollection(documentClass.documentName);
    const deleteIds = options.deleteAll ? collection.keys() : ids;
    const toDelete = await this._preDeleteDocumentArray(collection, {ids: deleteIds, options, user});
    if ( !toDelete.length ) return [];
    const response = await SocketInterface.dispatch("modifyDocument", {
      action: "delete",
      type: documentClass.documentName,
      parentType: parent.documentName,
      parentId: parent.id,
      ids: toDelete,
      options: options,
      pack: pack
    });
    return this._handleDeleteEmbeddedDocuments(response);
  }

  /* -------------------------------------------- */

  /**
   * Perform a standardized pre-delete workflow for all Document types. For internal use only.
   * @private
   */
  async _preDeleteDocumentArray(collection, {ids, options, user}) {
    user = user || game.user;
    const toDelete = [];
    if ( collection instanceof CompendiumCollection ) {
      await collection.getDocuments({_id: {$in: ids.filter(id => !collection.has(id))}});
    }
    for ( let id of ids ) {
      const doc = collection.get(id, {strict: true});
      await doc._preDelete(options, user);
      /**
       * A hook event that fires for every Document type before execution of a deletion workflow. Substitute the
       * Document name in the hook event to target a specific Document type, for example "preDeleteActor". This hook
       * only fires for the client who is initiating the update request.
       *
       * The hook provides the Document instance which is requested for deletion. Hooked functions may prevent the
       * workflow entirely by explicitly returning false.
       *
       * @function preDeleteDocument
       * @memberof hookEvents
       * @param {Document} document     The Document instance being deleted
       * @param {object} options        Additional options which modify the deletion request
       * @param {string} userId         The ID of the requesting user, always game.user.id
       * @return {boolean|void}         Explicitly return false to prevent deletion of this Document
       */
      const allowed = options.noHook || Hooks.call(`preDelete${doc.documentName}`, doc, options, user.id);
      if ( allowed === false ) {
        console.debug(`${vtt} | ${doc.documentName} deletion prevented by preDelete hook`);
        continue;
      }
      toDelete.push(id);
    }
    return toDelete;
  }

  /* -------------------------------------------- */

  /**
   * Handle a SocketResponse from the server where Documents are deleted.
   * @param {SocketResponse} response               The provided Socket response
   * @param {SocketRequest} [response.request]      The initial socket request
   * @param {string[]} [response.result]            An Array of deleted Document ids
   * @param {string} [response.userId]              The id of the requesting User
   * @return {Document[]}                           An Array of deleted Document instances
   * @private
   */
  _handleDeleteDocuments({request, result=[], userId}={}) {
    const {type, options, pack} = request;
    const collection = pack ? game.packs.get(pack) : game.collections.get(type);
    result = options.deleteAll ? Array.from(collection.keys()) : result;

    // Pre-operation collection actions
    collection._preDeleteDocuments(result, options, userId);

    // Perform deletions and execute callbacks
    const callbacks = this._postDeleteDocumentCallbacks(collection, result, {options, userId});
    const documents = callbacks.map(fn => fn());

    // Post-operation collection actions
    collection._onDeleteDocuments(documents, result, options, userId);
    this._logOperation("Deleted", type, documents, {level: "info", pack});
    return documents;
  }

  /* -------------------------------------------- */

  /**
   * Handle a SocketResponse from the server when embedded Documents are deleted from a parent Document.
   * @param {SocketResponse} response               The provided Socket response
   * @param {SocketRequest} [response.request]      The initial socket request
   * @param {string[]} [response.result]            An Array of deleted Document ids
   * @param {string} [response.userId]              The id of the requesting User
   * @return {Document[]}                           An Array of deleted Document instances
   * @private
   */
  _handleDeleteEmbeddedDocuments({request, result=[], userId}) {
    const { type, parentType, parentId, options, pack } = request;
    const parentCollection = pack ? game.packs.get(pack) : game.collections.get(parentType);
    const parent = parentCollection.get(parentId, {strict: !pack});
    if ( !parent || !result.length ) return [];

    // Pre-operation parent actions
    const collection = parent.getEmbeddedCollection(type);
    parent._preDeleteEmbeddedDocuments(type, result, options, userId);

    // Perform updates and execute callbacks
    const callbacks = this._postDeleteDocumentCallbacks(collection, result, {options, userId});
    parent.prepareData();
    const documents = callbacks.map(fn => fn());

    // Perform follow-up operations for the parent Document
    parent._onDeleteEmbeddedDocuments(type, documents, result, options, userId);
    this._logOperation("Deleted", type, documents, {level: "info", parent, pack});
    return documents;
  }

  /* -------------------------------------------- */

  /**
   * Perform a standardized post-deletion workflow for all Document types. For internal use only.
   * @return {Function[]}   An array of callback operations to perform after every Document is deleted
   * @private
   */
  _postDeleteDocumentCallbacks(collection, result, {options, userId}) {
    const callback = doc => {
      doc._onDelete(options, userId);
      /**
       * A hook event that fires for every Document type after conclusion of an deletion workflow.
       * Substitute the Document name in the hook event to target a specific Document type, for example "deleteActor".
       * This hook fires for all connected clients after the deletion has been processed.
       *
       * @function deleteDocument
       * @memberof hookEvents
       * @param {Document} document     The existing Document which was deleted
       * @param {object} options        Additional options which modified the deletion request
       * @param {string} userId         The ID of the User who triggered the deletion workflow
       */
      Hooks.callAll(`delete${doc.documentName}`, doc, options, userId);
      return doc;
    }
    const callbacks = [];
    for ( let id of result ) {
      const doc = collection.get(id, {strict: false});
      if ( !doc ) continue;
      collection.delete(id);
      callbacks.push(callback.bind(this, doc));
    }
    return callbacks;
  }

  /* -------------------------------------------- */
  /*  Helper Methods                              */
  /* -------------------------------------------- */

  /** @inheritdoc */
  getFlagScopes() {
    let scopes = ["core", game.system.id];
    scopes = scopes.concat(Array.from(game.modules.keys()));
    scopes.push("world");
    return scopes;
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  getCompendiumScopes() {
    return Array.from(game.packs.keys());
  }
}
