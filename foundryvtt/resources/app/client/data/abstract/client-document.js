/**
 * A mixin which extends each Document definition with specialized client-side behaviors.
 * This mixin defines the client-side interface for database operations and common document behaviors.
 * @mixin
 * @augments abstract.Document
 */
const ClientDocumentMixin = Base => class extends Base {
  constructor(data, context) {
    super(data, context);
  }

  /**
   * A collection of Application instances which should be re-rendered whenever this document is updated.
   * The keys of this object are the application ids and the values are Application instances. Each
   * Application in this object will have its render method called by {@link Document#render}.
   * @type {Object<Application>}
   * @see {@link Document#render}
   * @memberof ClientDocumentMixin#
   */
  apps = {};

  /**
   * A cached reference to the FormApplication instance used to configure this Document.
   * @type {FormApplication|null}
   * @private
   */
  _sheet = null;

  /**
   * @see abstract.Document#_initialize
   * @memberof ClientDocumentMixin#
   */
  _initialize() {
    super._initialize();
    try {
      this.prepareData();
    } catch(err) {
      Hooks.onError("ClientDocumentMixin#_initialize", err, {
        msg: `Failed initial data preparation for ${this.documentName} [${this.id}]`,
        log: "error",
        id: this.id
      });
    }
  }

	/* -------------------------------------------- */
  /*  Properties                                  */
	/* -------------------------------------------- */

  /**
   * Return a reference to the parent Collection instance which contains this Document.
   * @type {Collection}
   * @memberof ClientDocumentMixin#
   */
  get collection() {
    if ( this.isEmbedded ) return this.parent[this.constructor.metadata.collection];
    else return CONFIG[this.documentName].collection.instance;
  }

	/* -------------------------------------------- */

  /**
   * A reference to the Compendium Collection which contains this Document, if any, otherwise undefined.
   * @type {CompendiumCollection}
   * @name ClientDocumentMixin#compendium
   */
  get compendium() {
	  return game.packs.get(this.pack);
  }

	/* -------------------------------------------- */

  /**
   * Return a reference to the Folder to which this Document belongs, if any.
   * @type {Folder|null}
   * @memberof ClientDocumentMixin#
   *
   * @example <caption>A Document may belong to a Folder</caption>
   * let folder = game.folders.contents[0];
   * let actor = await Actor.create({name: "New Actor", folder: folder.id});
   * console.log(actor.data.folder); // folder.id;
   * console.log(actor.folder); // folder;
   */
  get folder() {
    if ( !this.data.folder ) return null;
    return game.folders.get(this.data.folder);
  }

	/* -------------------------------------------- */

  /**
   * A boolean indicator for whether or not the current game User has ownership rights for this Document.
   * Different Document types may have more specialized rules for what constitutes ownership.
   * @type {boolean}
   * @memberof ClientDocumentMixin#
   */
  get isOwner() {
    return this.testUserPermission(game.user, "OWNER");
  }

  /* -------------------------------------------- */

  /**
   * Test whether this Document is owned by any non-Gamemaster User.
   * @type {boolean}
   * @memberof ClientDocumentMixin#
   */
  get hasPlayerOwner() {
    for ( let u of game.users ) {
      if ( u.isGM ) continue;
      if ( this.testUserPermission(u, "OWNER") ) return true;
    }
    return false;
  }

  /* ---------------------------------------- */

  /**
   * A boolean indicator for whether the current game User has exactly LIMITED visibility (and no greater).
   * @type {boolean}
   * @memberof ClientDocumentMixin#
   */
  get limited() {
    return this.testUserPermission(game.user, "LIMITED", {exact: true});
  }

  /* -------------------------------------------- */

  /**
   * Return a string which creates a dynamic link to this Document instance.
   * @return {string}
   * @memberof ClientDocumentMixin#
   */
  get link() {
    const id = this.isEmbedded ? this.uuid : this.id;
    if ( this.pack ) return `@Compendium[${this.pack}.${id}]{${this.name}}`;
    return `@${this.documentName}[${id}]{${this.name}}`;
  }

  /* ---------------------------------------- */

  /**
   * Return the permission level that the current game User has over this Document.
   * See the CONST.DOCUMENT_PERMISSION_LEVELS object for an enumeration of these levels.
   * @type {number}
   * @memberof ClientDocumentMixin#
   *
   * @example
   * game.user.id; // "dkasjkkj23kjf"
   * actor.data.permission; // {default: 1, "dkasjkkj23kjf": 2};
   * actor.permission; // 2
   */
	get permission() {
	  if ( game.user.isGM ) return CONST.DOCUMENT_PERMISSION_LEVELS.OWNER;
	  if ( this.isEmbedded ) return this.parent.permission;
	  return this.getUserLevel(game.user);
  }

	/* -------------------------------------------- */

  /**
   * Lazily obtain a FormApplication instance used to configure this Document, or null if no sheet is available.
   * @type {FormApplication|null}
   * @memberof ClientDocumentMixin#
   */
  get sheet() {
    if ( !this._sheet ) {
      const cls = this._getSheetClass();
      if ( !cls ) return null;
      this._sheet = new cls(this, {editable: this.isOwner});
    }
    return this._sheet;
  }

  /* -------------------------------------------- */

  /**
   * A Universally Unique Identifier (uuid) for this Document instance.
   * @type {string}
   * @memberof ClientDocumentMixin#
   */
  get uuid() {
    let parts = [this.documentName, this.id];
    if ( this.parent ) parts = [this.parent.uuid].concat(parts);
    else if ( this.pack ) parts = ["Compendium", this.pack].concat(parts.slice(1));
    return parts.join(".");
  }

  /* -------------------------------------------- */

  /**
   * A boolean indicator for whether or not the current game User has at least limited visibility for this Document.
   * Different Document types may have more specialized rules for what determines visibility.
   * @type {boolean}
   * @memberof ClientDocumentMixin#
   */
  get visible() {
    if ( this.isEmbedded ) return this.parent.visible;
    return this.testUserPermission(game.user, "LIMITED");
  }

	/* -------------------------------------------- */
  /*  Methods                                     */
	/* -------------------------------------------- */

  /**
   * Obtain the FormApplication class constructor which should be used to configure this Document.
   * @returns {Function|null}
   * @private
   */
  _getSheetClass() {
    const cfg = CONFIG[this.documentName];
    const type = this.data.type || CONST.BASE_DOCUMENT_TYPE;
    const sheets = cfg.sheetClasses[type] || {};
    const override = this.getFlag("core", "sheetClass");
    if ( sheets[override] ) return sheets[override].cls;
    const classes = Object.values(sheets);
    const usesSheetClass = ![Actor, Item, Cards, Card].some(cls => this instanceof cls);
    if ( usesSheetClass && cfg.sheetClass && (classes.length < 2) ) {
      console.warn('Setting CONFIG[documentName].sheetClass is deprecated, please use '
        + 'DocumentSheetConfig.registerSheet(Document, "scope", SheetClass, {makeDefault: true}) instead.');
      return cfg.sheetClass;
    }
    if ( !classes.length ) return null;
    return (classes.find(s => s.default) ?? classes.pop()).cls;
  }

  /* -------------------------------------------- */

  /**
   * Prepare data for the Document.
   * Begin by resetting the prepared data back to its source state.
   * Next prepare any embedded Documents and compute any derived data elements.
   * @memberof ClientDocumentMixin#
   */
  prepareData() {
    this.data.reset();
    this.prepareBaseData();
    this.prepareEmbeddedDocuments();
    this.prepareDerivedData();
  }

  /* -------------------------------------------- */

  /**
   * Prepare data related to this Document itself, before any embedded Documents or derived data is computed.
   * @memberof ClientDocumentMixin#
   */
  prepareBaseData() {}

  /* -------------------------------------------- */

  /**
   * Prepare all embedded Document instances which exist within this primary Document.
   * @memberof ClientDocumentMixin#
   */
  prepareEmbeddedDocuments() {
    const embeddedTypes = this.constructor.metadata.embedded || {};
    for ( let cls of Object.values(embeddedTypes) ) {
      const collection = cls.metadata.collection;
      for ( let e of this[collection] ) {
        e.prepareData();
      }
    }
  }

  /* -------------------------------------------- */

  /**
   * Apply transformations or derivations to the values of the source data object.
   * Compute data fields whose values are not stored to the database.
   * @memberof ClientDocumentMixin#
   */
  prepareDerivedData() {}

  /* -------------------------------------------- */

  /**
   * Render all of the Application instances which are connected to this document by calling their respective
   * @see Application#render
   * @param {boolean} [force=false]     Force rendering
   * @param {object} [context={}]       Optional context
   * @memberof ClientDocumentMixin#
   */
  render(force=false, context={}) {
    for ( let app of Object.values(this.apps) ) {
      app.render(force, context);
    }
  }

  /* -------------------------------------------- */

  /**
   * Determine the sort order for this Document by positioning it relative a target sibling.
   * See SortingHelper.performIntegerSort for more details
   * @param {object} [options]          Sorting options provided to SortingHelper.performIntegerSort
   * @returns {Promise<Document>}       The Document after it has been re-sorted
   * @memberof ClientDocumentMixin#
   */
  async sortRelative({target=null, siblings=[], sortKey="sort", sortBefore=true, updateData={}}={}) {
    const sorting = SortingHelpers.performIntegerSort(this, {target, siblings, sortKey, sortBefore});
    const updates = [];
    for ( let s of sorting ) {
      const doc = s.target;
      const update = foundry.utils.mergeObject(updateData, s.update, {inplace: false});
      update._id = doc.id;
      if ( doc.sheet && doc.sheet.rendered ) await doc.sheet.submit({updateData: update});
      else updates.push(update);
    }
    if ( updates.length ) await this.constructor.updateDocuments(updates, {parent: this.parent, pack: this.pack});
    return this;
  }

  /* -------------------------------------------- */
  /*  Event Handlers                              */
  /* -------------------------------------------- */

  /**
   * @see abstract.Document#_onCreate
   * @memberof ClientDocumentMixin#
   */
	_onCreate(data, options, userId) {
    if ( options.renderSheet && (userId === game.user.id) ) {
      if ( this.sheet ) this.sheet.render(true, {
        action: "create",
        data: data
      });
    }
  }

  /* -------------------------------------------- */

  /**
   * @see abstract.Document#_onUpdate
   * @memberof ClientDocumentMixin#
   */
	_onUpdate(data, options, userId) {

    // Re-render associated applications
    if (options.render !== false) {
      this.render(false, {
        action: "update",
        data: data
      });
    }

    // Update Compendium index
    if ( this.pack && !this.isEmbedded ) {
      this.compendium.indexDocument(this);
    }
  }

  /* -------------------------------------------- */

  /**
   * @see abstract.Document#_onDelete
   * @memberof ClientDocumentMixin#
   */
  _onDelete(options, userId) {
    Object.values(this.apps).forEach(a => a.close({submit: false}));
  }

	/* -------------------------------------------- */

  /**
   * Preliminary actions taken before a set of embedded Documents in this parent Document are created.
   * @param {string} embeddedName   The name of the embedded Document type
   * @param {object[]} result       An Array of created data objects
   * @param {object} options        Options which modified the creation operation
   * @param {string} userId         The ID of the User who triggered the operation
   * @memberof ClientDocumentMixin#
   */
  _preCreateEmbeddedDocuments(embeddedName, result, options, userId) {}

	/* -------------------------------------------- */

  /**
   * Follow-up actions taken after a set of embedded Documents in this parent Document are created.
   * @param {string} embeddedName   The name of the embedded Document type
   * @param {Document[]} documents  An Array of created Documents
   * @param {object[]} result       An Array of created data objects
   * @param {object} options        Options which modified the creation operation
   * @param {string} userId         The ID of the User who triggered the operation
   * @memberof ClientDocumentMixin#
   */
  _onCreateEmbeddedDocuments(embeddedName, documents, result, options, userId) {
    if ( options.render === false ) return;
    this.render(false, {renderContext: `create${embeddedName}`});
  }

	/* -------------------------------------------- */

  /**
   * Preliminary actions taken before a set of embedded Documents in this parent Document are updated.
   * @param {string} embeddedName   The name of the embedded Document type
   * @param {object[]} result       An Array of incremental data objects
   * @param {object} options        Options which modified the update operation
   * @param {string} userId         The ID of the User who triggered the operation
   * @memberof ClientDocumentMixin#
   */
  _preUpdateEmbeddedDocuments(embeddedName, result, options, userId) {}

	/* -------------------------------------------- */

  /**
   * Follow-up actions taken after a set of embedded Documents in this parent Document are updated.
   * @param {string} embeddedName   The name of the embedded Document type
   * @param {Document[]} documents  An Array of updated Documents
   * @param {object[]} result       An Array of incremental data objects
   * @param {object} options        Options which modified the update operation
   * @param {string} userId         The ID of the User who triggered the operation
   * @memberof ClientDocumentMixin#
   */
  _onUpdateEmbeddedDocuments(embeddedName, documents, result, options, userId) {
    if ( options.render === false ) return;
    this.render(false, {renderContext: `update${embeddedName}`});
  }

	/* -------------------------------------------- */

  /**
   * Preliminary actions taken before a set of embedded Documents in this parent Document are deleted.
   * @param {string} embeddedName   The name of the embedded Document type
   * @param {object[]} result       An Array of document IDs being deleted
   * @param {object} options        Options which modified the deletion operation
   * @param {string} userId         The ID of the User who triggered the operation
   * @memberof ClientDocumentMixin#
   */
  _preDeleteEmbeddedDocuments(embeddedName, result, options, userId) {}

	/* -------------------------------------------- */

  /**
   * Follow-up actions taken after a set of embedded Documents in this parent Document are deleted.
   * @param {string} embeddedName   The name of the embedded Document type
   * @param {Document[]} documents  An Array of deleted Documents
   * @param {object[]} result       An Array of document IDs being deleted
   * @param {object} options        Options which modified the deletion operation
   * @param {string} userId         The ID of the User who triggered the operation
   * @memberof ClientDocumentMixin#
   */
  _onDeleteEmbeddedDocuments(embeddedName, documents, result, options, userId) {
    if ( options.render === false ) return;
    this.render(false, {renderContext: `delete${embeddedName}`});
  }

  /* -------------------------------------------- */
  /*  Importing and Exporting                     */
  /* -------------------------------------------- */

  /**
   * Present a Dialog form to create a new Document of this type.
   * Choose a name and a type from a select menu of types.
   * @param {object} data              Initial data with which to populate the creation form
   * @param {object} [context={}]      Additional context options or dialog positioning options
   * @return {Promise<Document|null>}  A Promise which resolves to the created Document, or null if the dialog was
   *                                   closed.
   * @memberof ClientDocumentMixin
   */
  static async createDialog(data={}, {parent=null, pack=null, ...options}={}) {

    // Collect data
    const documentName = this.metadata.name;
    const types = game.system.documentTypes[documentName];
    const folders = parent ? [] : game.folders.filter(f => (f.data.type === documentName) && f.displayed);
    const label = game.i18n.localize(this.metadata.label);
    const title = game.i18n.format("DOCUMENT.Create", {type: label});

    // Render the document creation form
    const html = await renderTemplate(`templates/sidebar/document-create.html`, {
      name: data.name || game.i18n.format("DOCUMENT.New", {type: label}),
      folder: data.folder,
      folders: folders,
      hasFolders: folders.length >= 1,
      type: data.type || types[0],
      types: types.reduce((obj, t) => {
        const label = CONFIG[documentName]?.typeLabels?.[t] ?? t;
        obj[t] = game.i18n.has(label) ? game.i18n.localize(label) : t;
        return obj;
      }, {}),
      hasTypes: types.length > 1
    });

    // Render the confirmation dialog window
    return Dialog.prompt({
      title: title,
      content: html,
      label: title,
      callback: html => {
        const form = html[0].querySelector("form");
        const fd = new FormDataExtended(form);
        foundry.utils.mergeObject(data, fd.toObject(), {inplace: true});
        if ( !data.folder ) delete data["folder"];
        if ( types.length === 1 ) data.type = types[0];
        return this.create(data, {parent, pack, renderSheet: true});
      },
      rejectClose: false,
      options: options
    });
  }

  /* -------------------------------------------- */

  /**
   * Present a Dialog form to confirm deletion of this Document.
   * @param {object} [options]    Positioning and sizing options for the resulting dialog
   * @return {Promise<Document>}  A Promise which resolves to the deleted Document
   */
  async deleteDialog(options={}) {
    const type = game.i18n.localize(this.constructor.metadata.label);
    return Dialog.confirm({
      title: `${game.i18n.format("DOCUMENT.Delete", {type})}: ${this.name}`,
      content: `<h4>${game.i18n.localize("AreYouSure")}</h4><p>${game.i18n.format("SIDEBAR.DeleteWarning", {type})}</p>`,
      yes: this.delete.bind(this),
      options: options
    });
  }

  /* -------------------------------------------- */

  /**
   * Export document data to a JSON file which can be saved by the client and later imported into a different session.
   * @param {object} [options]      Additional options passed to the {@link ClientDocumentMixin#toCompendium} method
   * @memberof ClientDocumentMixin#
   */
  exportToJSON(options) {
    const data = this.toCompendium(null, options);
    data.flags["exportSource"] = {
      world: game.world.id,
      system: game.system.id,
      coreVersion: game.version,
      systemVersion: game.system.data.version
    };
    const filename = `fvtt-${this.documentName}-${this.name.slugify()}.json`;
    saveDataToFile(JSON.stringify(data, null, 2), "text/json", filename);
  }

  /* -------------------------------------------- */

  /**
   * A helper function to handle obtaining the relevant Document from dropped data provided via a DataTransfer event.
   * The dropped data could have:
   * 1. A compendium pack and entry id
   * 2. A World Document _id
   * 3. A data object explicitly provided
   * @memberof ClientDocumentMixin
   *
   * @param {object} data   The data object extracted from a DataTransfer event
   * @param {object} [options={}]   Additional options which configure data retrieval
   * @param {boolean} [options.importWorld=false]   Import the provided document data into the World, if it is not already a World-level Document reference
   * @return {Promise<Document|null>}    The Document data that should be handled by the drop handler
   */
  static async fromDropData(data, {importWorld=false}={}) {
    if ( data.type !== this.documentName ) return null;
    const collection = CONFIG[this.documentName].collection.instance;
    let document = null;

    // Case 1 - Data explicitly provided
    if (data.data) {
      document = importWorld ? await this.create(data.data) : new this(data.data);
    }

    // Case 2 - Import from a Compendium pack
    else if (data.pack) {
      const pack = game.packs.get(data.pack);
      if (pack.documentName !== this.documentName) return null;
      document = importWorld ? await collection.importFromCompendium(pack, data.id) : await pack.getDocument(data.id);
    }

    // Case 3 - Import from World document
    else document = collection.get(data.id);

    // Flag the source GUID
    if ( document && !document.getFlag("core", "sourceId") ) {
      document.data.update({"flags.core.sourceId": document.uuid});
      document.prepareData();
    }
    return document;
  }

  /* -------------------------------------------- */

  /**
   * Update this Document using a provided JSON string.
   * @param {string} json           JSON data string
   * @return {Promise<Document>}    The updated Document
   * @memberof ClientDocumentMixin#
   */
  async importFromJSON(json) {
    let data = JSON.parse(json);
    data._id = this.id;
    data = this.collection.fromCompendium(data, {addFlags: false, keepId: true});
    const d = new this.constructor.schema(data);

    // Preserve certain fields
    const {folder, sort, permission} = this.data;
    this.data.update(foundry.utils.mergeObject(d.toObject(), {folder, sort, permission}), {recursive: false});
    return this.update(this.toObject(), {diff: false, recursive: false}).then(doc => {
      ui.notifications.info(game.i18n.format("DOCUMENT.Imported", {document: this.documentName, name: this.data.name}));
      return doc;
    });
  }

  /* -------------------------------------------- */

  /**
   * Render an import dialog for updating the data related to this Document through an exported JSON file
   * @return {Promise<void>}
   * @memberof ClientDocumentMixin#
   */
  async importFromJSONDialog() {
    new Dialog({
      title: `Import Data: ${this.name}`,
      content: await renderTemplate("templates/apps/import-data.html",
          {
            hint1: game.i18n.format("DOCUMENT.ImportDataHint1", {document: this.documentName}),
            hint2: game.i18n.format("DOCUMENT.ImportDataHint2", {name: this.name})
          }),
      buttons: {
        import: {
          icon: '<i class="fas fa-file-import"></i>',
          label: "Import",
          callback: html => {
            const form = html.find("form")[0];
            if ( !form.data.files.length ) return ui.notifications.error("You did not upload a data file!");
            readTextFromFile(form.data.files[0]).then(json => this.importFromJSON(json));
          }
        },
        no: {
          icon: '<i class="fas fa-times"></i>',
          label: "Cancel"
        }
      },
      default: "import"
    }, {
      width: 400
    }).render(true);
  }

  /* -------------------------------------------- */

  /**
   * Transform the Document data to be stored in a Compendium pack.
   * Remove any features of the data which are world-specific.
   * @param {CompendiumCollection} [pack]   A specific pack being exported to
   * @param {object} [options]              Additional options which modify how the document is converted
   * @param {boolean} [options.clearFlags=false]      Clear the flags object
   * @param {boolean} [options.clearSort=true]        Clear the currently assigned folder and sort order
   * @param {boolean} [options.clearPermissions=true] Clear document permissions
   * @param {boolean} [options.clearState=true]       Clear fields which store document state
   * @param {boolean} [options.keepId=false]          Retain the current Document id
   * @return {object}                       A data object of cleaned data suitable for compendium import
   * @memberof ClientDocumentMixin#
   */
  toCompendium(pack, {clearSort=true, clearFlags=false, clearPermissions=true, clearState=true, keepId=false}={}) {
    const data = this.toObject();
    if ( !keepId ) delete data["_id"];
    if ( clearSort ) {
      delete data["folder"];
      delete data["sort"];
    }
    if ( clearFlags ) delete data["flags"];
    if ( clearPermissions ) delete data["permission"];
    if ( clearState ) delete data["active"];
    return data;
  }

  /* ---------------------------------------- */
  /*  Deprecations                            */
  /* ---------------------------------------- */

  /**
   * @deprecated since v9 - Use prepareEmbeddedDocuments instead.
   * @ignore
   */
  prepareEmbeddedEntities() {
    console.warn("Document#prepareEmbeddedEntities is deprecated. Please use Document#prepareEmbeddedDocuments instead.");
    this.prepareEmbeddedDocuments();
  }
};
