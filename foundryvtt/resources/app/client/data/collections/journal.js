/**
 * The singleton collection of JournalEntry documents which exist within the active World.
 * This Collection is accessible within the Game object as game.journal.
 * @extends {WorldCollection}
 *
 * @see {@link JournalEntry} The JournalEntry document
 * @see {@link JournalDirectory} The JournalDirectory sidebar directory
 */
class Journal extends WorldCollection {

  /** @override */
  static documentName = "JournalEntry";

  /* -------------------------------------------- */
  /*  Socket Listeners and Handlers               */
  /* -------------------------------------------- */

  /**
   * Open Socket listeners which transact JournalEntry data
   */
  static _activateSocketListeners(socket) {
    socket.on("showEntry", this._showEntry.bind(this));
    socket.on("shareImage", ImagePopout._handleShareImage);
  }

  /* -------------------------------------------- */

  /**
   * Handle a received request to show a JournalEntry to the current client
   * @param {string} entryId      The ID of the journal entry to display for other players
   * @param {string} mode         The JournalEntry mode to display
   * @param {boolean} force       Display the entry to all players regardless of normal permissions
   * @private
   */
  static async _showEntry(entryId, mode="text", force=true) {
    let entry = await fromUuid(entryId);
    if ( entry.documentName !== "JournalEntry" ) return;
    if ( !force && !entry.visible ) return;

    // Don't show an entry that has no content
    if ( mode === "image" && !entry.data.img ) return;
    else if ( mode === "text" && !entry.data.content ) return;

    // Show the sheet with the appropriate mode
    entry.sheet.render(true, {sheetMode: mode});
  }
}
