/**
 * The singleton collection of FogExploration documents which exist within the active World.
 * @extends {WorldCollection}
 * @see {@link FogExploration} The FogExploration document
 */
class FogExplorations extends WorldCollection {
  static documentName = "FogExploration";
}
