/**
 * A helper class to provide common functionality for working with Image objects
 */
class ImageHelper {

  /**
   * Create thumbnail preview for a provided image path.
   * @param {string|PIXI.DisplayObject} src   The URL or display object of the texture to render to a thumbnail
   * @param {object} options    Additional named options passed to the compositeCanvasTexture function
   * @return {Promise<object>}  The parsed and converted thumbnail data
   */
  static async createThumbnail(src, {width, height, tx, ty, center, format, quality}) {
    if ( !src ) return null;

    // Load the texture and create a Sprite
    let object = src;
    if ( !(src instanceof PIXI.DisplayObject) ) {
      const texture = await loadTexture(src);
      object = PIXI.Sprite.from(texture);
    }

    // Reduce to the smaller thumbnail texture
    if ( !canvas.ready && canvas.initializing ) await canvas.initializing;
    const reduced = this.compositeCanvasTexture(object, {width, height, tx, ty, center});
    const thumb = this.textureToImage(reduced, {format, quality});
    reduced.destroy(true);

    // Return the image data
    return { src, texture: reduced, thumb, width: object.width, height: object.height };
  }

  /* -------------------------------------------- */

  /**
   * Composite a canvas object by rendering it to a single texture
   *
   * @param {PIXI.DisplayObject} object   The object to render to a texture
   * @param {number} [width]              The desired width of the output texture
   * @param {number} [height]             The desired height of the output texture
   * @param {number} [tx]                 A horizontal translation to apply to the object
   * @param {number} [ty]                 A vertical translation to apply to the object
   * @param {boolean} [center]            Center the texture in the rendered frame?
   *
   * @return {PIXI.Texture}               The composite Texture object
   */
  static compositeCanvasTexture(object, {width, height, tx=0, ty=0, center=true}={}) {
    if ( !canvas.app?.renderer ) throw new Error("Unable to compose texture because there is no game canvas");
    width = width ?? object.width;
    height = height ?? object.height;

    // Downscale the object to the desired thumbnail size
    const currentRatio = object.width / object.height;
    const targetRatio = width / height;
    const s = currentRatio > targetRatio ? (height / object.height) : (width / object.width);

    // Define a transform matrix
    const transform = PIXI.Matrix.IDENTITY.clone();
    transform.scale(s, s);

    // Translate position
    if ( center ) {
      tx = (width - (object.width * s)) / 2;
      ty = (height - (object.height * s)) / 2;
    } else {
      tx *= s;
      ty *= s;
    }
    transform.translate(tx, ty);

    // Create and render a texture with the desired dimensions
    const texture = PIXI.RenderTexture.create({
      width: width,
      height: height,
      scaleMode: PIXI.SCALE_MODES.LINEAR,
      resolution: 2
    });
    canvas.app.renderer.render(object, texture, undefined, transform);
    return texture;
  }

  /* -------------------------------------------- */

  /**
   * Extract a texture to a base64 PNG string
   * @param {PIXI.Texture} texture      The texture object to extract
   * @param {string} [format]           Image format, e.g. "image/jpeg" or "image/webp".
   * @param {number} [quality]          JPEG or WEBP compression from 0 to 1. Default is 0.92.
   * @return {string}                   A base64 png string of the texture
   */
  static textureToImage(texture, {format, quality}={}) {
    const s = new PIXI.Sprite(texture);
    return canvas.app.renderer.extract.base64(s, format, quality);
  }

  /* -------------------------------------------- */

  /**
   * Asynchronously convert a DisplayObject container to base64 using Canvas#toBlob and FileReader
   * @param {PIXI.DisplayObject} target     A PIXI display object to convert
   * @param {string} type                   The requested mime type of the output, default is image/png
   * @param {number} quality                A number between 0 and 1 for image quality if image/jpeg or image/webp
   * @returns {Promise<string>}             A processed base64 string
   */
  static async pixiToBase64(target, type, quality) {
    const extracted = canvas.app.renderer.extract.canvas(target);
    return new Promise((resolve, reject) => {
      extracted.toBlob(blob => {
        const reader = new FileReader();
        reader.onload = () => resolve(reader.result);
        reader.onerror = reject;
        reader.readAsDataURL(blob);
      }, type, quality);
    })
  }
}
