/**
 * A directory listing of hook events which occur in the Foundry VTT framework
 * @namespace hookEvents
 */

/**
 * A simple event framework used throughout Foundry Virtual Tabletop.
 * When key actions or events occur, a "hook" is defined where user-defined callback functions can execute.
 * This class manages the registration and execution of hooked callback functions.
 */
class Hooks {

  /**
   * Register a callback handler which should be triggered when a hook is triggered.
   *
   * @param {string} hook   The unique name of the hooked event
   * @param {Function} fn   The callback function which should be triggered when the hook event occurs
   * @return {number}       An ID number of the hooked function which can be used to turn off the hook later
   */
  static on(hook, fn) {
    console.debug(`${vtt} | Registered callback for ${hook} hook`);
    const id = this._id++;
    this._hooks[hook] = this._hooks[hook] || [];
    this._hooks[hook].push(fn);
    this._ids[id] = fn;
    return id;
  }

  /* -------------------------------------------- */

  /**
   * Register a callback handler for an event which is only triggered once the first time the event occurs.
   * After a "once" hook is triggered the hook is automatically removed.
   *
   * @param {string} hook   The unique name of the hooked event
   * @param {Function} fn   The callback function which should be triggered when the hook event occurs
   * @return {number}       An ID number of the hooked function which can be used to turn off the hook later
   */
  static once(hook, fn) {
    this._once.push(fn);
    return this.on(hook, fn);
  }

  /* -------------------------------------------- */

  /**
   * Unregister a callback handler for a particular hook event
   *
   * @param {string} hook           The unique name of the hooked event
   * @param {Function|number} fn    The function, or ID number for the function, that should be turned off
   */
  static off(hook, fn) {
    if ( typeof fn === "number" ) {
      let id = fn;
      fn = this._ids[fn];
      delete this._ids[id];
    }
    if ( !this._hooks.hasOwnProperty(hook) ) return;
    const fns = this._hooks[hook];
    let idx = fns.indexOf(fn);
    if ( idx !== -1 ) fns.splice(idx, 1);
    console.debug(`${vtt} | Unregistered callback for ${hook} hook`);
  }

  /* -------------------------------------------- */

  /**
   * Call all hook listeners in the order in which they were registered
   * Hooks called this way can not be handled by returning false and will always trigger every hook callback.
   *
   * @param {string} hook   The hook being triggered
   * @param {...*} args     Arguments passed to the hook callback functions
   * @returns {boolean}     Were all hooks called without execution being prevented?
   */
  static callAll(hook, ...args) {
    if ( CONFIG.debug.hooks ) {
      console.log(`DEBUG | Calling ${hook} hook with args:`);
      console.log(args);
    }
    if ( !this._hooks.hasOwnProperty(hook) ) return true;
    const fns = new Array(...this._hooks[hook]);
    for ( let fn of fns ) {
      this._call(hook, fn, args);
    }
    return true;
  }

  /* -------------------------------------------- */

  /**
   * Call hook listeners in the order in which they were registered.
   * Continue calling hooks until either all have been called or one returns false.
   *
   * Hook listeners which return false denote that the original event has been adequately handled and no further
   * hooks should be called.
   *
   * @param {string} hook   The hook being triggered
   * @param {...*} args     Arguments passed to the hook callback functions
   * @returns {boolean}     Were all hooks called without execution being prevented?
   */
  static call(hook, ...args) {
    if ( CONFIG.debug.hooks ) {
      console.log(`DEBUG | Calling ${hook} hook with args:`);
      console.log(args);
    }
    if ( !this._hooks.hasOwnProperty(hook) ) return true;
    const fns = new Array(...this._hooks[hook]);
    for ( let fn of fns ) {
      let callAdditional = this._call(hook, fn, args);
      if ( callAdditional === false ) return false;
    }
    return true;
  }

  /* -------------------------------------------- */

  /**
   * Call a hooked function using provided arguments and perhaps unregister it.
   * @private
   */
  static _call(hook, fn, args) {
    if ( this._once.includes(fn) ) this.off(hook, fn);
    try {
      return fn(...args);
    } catch(err) {
      const msg = `Error thrown in hooked function '${fn?.name}' for hook '${hook}'`;
      console.warn(`${vtt} | ${msg}`);
      if ( hook !== "error" ) this.onError("Hooks._call", err, {msg, hook, fn, log: "error"});
    }
  }

  /* --------------------------------------------- */

  /**
   * Notify subscribers that an error has occurred within foundry.
   * @param {string} location                The method where the error was caught.
   * @param {Error} err                      The error.
   * @param {object} [options={}]            Additional options to configure behaviour.
   * @param {string} [options.msg=""]        A message to prefix the caught error with and/or to use in the notification.
   * @param {?string} [options.log=null]     The level at which to log the error to console (if at all).
   * @param {?string} [options.notify=null]  The level at which to spawn a notification in the UI (if at all).
   * @param {object} [options.data={}]       Additional data to pass to the hook subscribers.
   */
  static onError(location, err, {msg="", notify=null, log=null, ...data}={}) {
    let message = err instanceof Error ? err.message : err;
    message = msg ? `${msg}: ${message}` : `${message}`;
    const error = new Error(message);
    if ( err instanceof Error ) error.stack = err.stack;
    if ( log ) console[log]?.(err);
    if ( notify ) ui.notifications[notify]?.(msg || err.message);

    /**
     * A hook event that fires whenever foundry experiences an error.
     *
     * @function error
     * @memberof hookEvents
     * @param {string} location      The method where the error was caught.
     * @param {Error} err            The error.
     * @param {object} [data={}]     Additional data that might be provided, based on the nature of the error.
     */
    Hooks.callAll("error", location, err, data);
  }
}

// Static class attributes
Hooks._hooks = {};
Hooks._once = [];
Hooks._ids = {};
Hooks._id = 1;
