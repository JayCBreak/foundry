/**
 * A library of package management commands which are used by various interfaces around the software.
 * @extends {Game}
 */
class Setup extends Game {

  /**
   * A reference to the setup URL used under the current route prefix, if any
   * @return {string}
   */
  static get setupURL() {
    return foundry.utils.getRoute("setup");
  }

  /* -------------------------------------------- */

  /**
   * Register core game settings
   * @override
   */
  registerSettings() {
    super.registerSettings();
    game.settings.register("core", "declinedManifestUpgrades", {
      scope: "client",
      config: false,
      type: Object,
      default: {}
    });
  }

  /* -------------------------------------------- */

  /** @override */
  setupPackages(data) {
    super.setupPackages(data);

    /**
     * The game World which is currently active
     * @type {Object}
     */
    this.worlds = new Map((data.worlds || []).map(m => {
      m.data = new foundry.packages.WorldData(m.data);
      return [m.id, m]
    }));

    /**
     * The System which is used to power this game world
     * @type {Object}
     */
    this.systems = new Map((data.systems || []).map(m => {
      m.data = new foundry.packages.SystemData(m.data);
      return [m.id, m]
    }));
  }

  /* -------------------------------------------- */

  /** @override */
  static async getData(socket, view) {
    let req;
    switch (view) {
      case "auth": case "license": req = "getAuthData"; break;
      case "join": req = "getJoinData"; break;
      case "players": req = "getPlayersData"; break;
      case "setup": req = "getSetupData"; break;
    }
    return new Promise(resolve => {
      socket.emit(req, resolve);
    });
  }

  /* -------------------------------------------- */
  /*  View Handlers                               */
  /* -------------------------------------------- */

  /** @override */
  async _initializeView() {
    switch (this.view) {
      case "auth":
        return this._authView();
      case "license":
        return this._licenseView();
      case "setup":
        return this._setupView();
      case "players":
        return this._playersView();
      case "join":
        return this._joinView();
      default:
        throw new Error(`Unknown view URL ${this.view} provided`);
    }
  }

  /* -------------------------------------------- */

  /**
   * The application view which displays the End User License Agreement (EULA).
   * @private
   */
  _licenseView() {
    ui.notifications = new Notifications().render(true);
    const setup = document.getElementById("setup");
    if ( setup.dataset.step === "eula" ) new EULA().render(true);
  }

  /* -------------------------------------------- */

  /**
   * The application view which displays the admin authentication application.
   * @private
   */
  _authView() {
    if ( !SIGNED_EULA ) window.location.href = foundry.utils.getRoute("license");
    ui.notifications = new Notifications().render(true);
    new SetupAuthenticationForm().render(true);
  }

  /* -------------------------------------------- */

  /**
   * The application view which displays the application Setup and Configuration.
   * @private
   */
  _setupView() {
    if ( !SIGNED_EULA ) window.location.href = foundry.utils.getRoute("license");
    ui.notifications = new Notifications().render(true);
    ui.setup = new SetupConfigurationForm(game.data).render(true);
    Setup._activateSocketListeners();
  }

  /* -------------------------------------------- */

  /**
   * The application view which displays the User Configuration.
   * @private
   */
  _playersView() {
    if ( !SIGNED_EULA ) window.location.href = foundry.utils.getRoute("license");
    this.users = new Users(this.data.users);
    this.collections.set("User", this.users);
    this.collections.set("Setting", this.settings.storage.get("world"));

    // Render applications
    ui.notifications = new Notifications().render(true);
    ui.players = new UserManagement(this.users);
    ui.players.render(true);

    // Game is ready for use
    this.ready = true;
  }

  /* -------------------------------------------- */

  /**
   * The application view which displays the Game join and authentication screen.
   * @private
   */
  _joinView() {
    if ( !SIGNED_EULA ) window.location.href = foundry.utils.getRoute("license");

    // Configure Join view data
    this.users = new Users(this.data.users);
    this.collections.set("User", this.users);

    // Activate Join view socket listeners
    Users._activateSocketListeners(this.socket);

    // Render Join view applications
    ui.notifications = new Notifications().render(true);
    ui.join = new JoinGameForm().render(true);
  }

  /* -------------------------------------------- */
  /*  Package Management                          */
  /* -------------------------------------------- */

  /**
   * Check with the server whether a package of a certain type is able to be installed or updated.
   * @param {string} type       The package type to check
   * @param {string} name       The package name to check
   * @param {string} manifest   The manifest URL to check
   * @param {number} timeout    A timeout in milliseconds after which the check will fail
   * @return {Promise<Object>}  The return manifest
   */
  static async checkPackage({type="module", name, manifest, timeout=20000}={}) {
    return this.post({action: "checkPackage", type, name, manifest}, timeout).then(r => r.json());
  }

  /* -------------------------------------------- */

  /**
   * Prepares the cache of available and owned packages
   * @param type
   * @return {Promise<void>}
   */
  static async warmPackages({type="system"}={}) {
    if ( Setup.cache[type].state > Setup.CACHE_STATES.COLD ) return;
    Setup.cache[type].state = Setup.CACHE_STATES.WARMING;
    const result = await this.getPackages({type});
    // If we fail to get packages, there's no point asking for Owned
    if ( result.size > 0 ) await this.getOwned({type});
    Setup.cache[type].state = Setup.CACHE_STATES.WARMED;
  }

  /* -------------------------------------------- */

  /**
   * Get a Map of available packages of a given type which may be installed
   * @param {string} type
   * @return {Promise<Map<string, Package>>}
   */
  static async getPackages({type="system"}={}) {
    if ( this.cache[type].packages?.size > 0 ) return this.cache[type].packages;
    const packages = new Map();
    let request;
    try {
      request = await this.post({action: "getPackages", type: type});
    } catch(err) {
      ui.notifications.error("PACKAGE.GetPackagesTimedOut", {localize: true});
      return packages;
    }
    if ( !request.ok ) return packages;
    let response = await request.json();
    response.packages.forEach(p => packages.set(p[0], p[1]));
    this.cache[type].packages = packages;
    return packages;
  }

  /* -------------------------------------------- */

  /**
   * Get a map of owned packages of a given type
   * @param {string} type
   * @return {Promise<Map<string, Package>>}
   */
  static async getOwned({type="system"}={}) {
    if ( this.cache[type].owned?.size > 0 ) return this.cache[type].owned;
    let request;
    try {
      request = await this.post({action: "getPackages", type: type});
    } catch(err) {
      ui.notifications.error("PACKAGE.WarmCacheTimedOut", {localize: true});
      return new Map();
    }
    const response = await request.json();
    this.cache[type].owned = response.owned;
    return response.owned;
  }

  /* -------------------------------------------- */

  /**
   * Install a Package
   * @param {string} type          The type of package being installed, in ["module", "system", "world"]
   * @param {string} name          The canonical package name
   * @param {string} manifest      The package manifest URL
   * @param {function} onProgress  A function that will receive progress updates during the installation process
   * @return {Promise<Object>}     A Promise which resolves to the installed package
   */
  static installPackage = ({type="module", name, manifest}={}, onProgress) => new Promise(async resolve => {
    const error = response => {
      const err = new Error(response.error);
      err.stack = response.stack;
      ui.notifications.error(game.i18n.format("SETUP.InstallFailure", {message: err.message}));
      console.error(err);
      Setup._removeProgressListener(progress);
      if ( onProgress ) Setup._removeProgressListener(onProgress);
      resolve(response);
    };

    const done = async ({pkg}) => {
      ui.notifications.info(game.i18n.format("SETUP.InstallSuccess", {type: type.titleCase(), name: pkg.data.name}));

      // Trigger dependency installation (asynchronously)
      if ( pkg.data.dependencies?.length ) {
        // noinspection ES6MissingAwait
        this.installDependencies(pkg);
      }

      // Update in-memory data
      game[`${type}s`].set(pkg.data.name, pkg);

      // Update application views
      if ( ui.setup ) await ui.setup.reload();
      Setup._removeProgressListener(progress);
      if ( onProgress ) Setup._removeProgressListener(onProgress);
      resolve(pkg);
    };

    const progress = data => {
      if ( data.step === "Error" ) return error(data);
      if ( data.step === "Package" ) return done(data);
    };

    Setup._addProgressListener(progress);
    if ( onProgress ) Setup._addProgressListener(onProgress);
    let request;
    try {
      request = await this.post({action: "installPackage", type, name, manifest});
    } catch(err) {
      ui.notifications.error("PACKAGE.PackageInstallTimedOut", {localize: true});
      resolve();
      return;
    }

    /** @type {InstallPackageResponse} */
    const response = await request.json();

    // Handle errors
    if ( response.error ) error(response);
    // Handle warnings
    if ( response.warning ) ui.notifications.warn(response.warning);
  });

  /* -------------------------------------------- */

  /**
   * Install a set of dependency modules which are required by an installed package
   * @param {InstallPackageResponse} pkg   The package which was installed that requested dependencies
   * @return {Promise<void>}
   */
  static async installDependencies(pkg) {
    const toInstall = [];
    const dependencies = pkg.data.dependencies || [];

    // Obtain known package data for requested dependency types
    for ( let d of dependencies ) {
      if (!d.name) continue;
      d.type = d.type || "module";
      const installed = game.data[`${d.type}s`].find(p => p.id === d.name);
      if ( installed ) {
        console.debug(`Dependency ${d.type} ${d.name} is already installed.`);
        continue;
      }

      // Manifest URL provided
      if (d.manifest) {
        toInstall.push(d);
        continue;
      }

      // Discover from package listing
      const packages = await Setup.getPackages({type: d.type});
      const dep = packages.get(d.name);
      if (!dep) {
        console.warn(`Requested dependency ${d.name} not found in ${d.type} directory.`);
        continue;
      }
      d.manifest = dep.version.manifest;
      d.version = dep.version.version;
      toInstall.push(d);
    }
    if ( !toInstall.length ) return;

    // Prompt the user to confirm installation of dependency packages
    const html = await renderTemplate("templates/setup/install-dependencies.html", {
      title: pkg.data.title,
      dependencies: toInstall
    });
    let agree = false;
    await Dialog.confirm({
      title: game.i18n.localize("SETUP.PackageDependenciesTitle"),
      content: html,
      yes: () => agree = true
    });
    if ( !agree ) return ui.notifications.warn(game.i18n.format("SETUP.PackageDependenciesDecline", {
      title: pkg.data.title
    }));

    // Install dependency packages
    for ( let d of toInstall ) {
      await this.installPackage(d);
    }
    return ui.notifications.info(game.i18n.format("SETUP.PackageDependenciesSuccess", {
      title: pkg.data.title,
      number: toInstall.length
    }));
  }

  /* -------------------------------------------- */

  /**
   * Uninstall a single Package by name and type.
   * @param {string} type       The type of package being installed, in ["module", "system", "world"]
   * @param {string} name       The canonical package name
   * @return {Promise<object>}  A Promise which resolves to the uninstalled package manifest
   */
  static async uninstallPackage({type="module", name}={}) {
    let request;
    try {
      request = await this.post({action: "uninstallPackage", type, name});
    } catch(err) {
      return {error: err.message, stack: err.stack};
    }
    // Update in-memory data
    game[`${type}s`].delete(name);
    return request.json();
  }

  /* -------------------------------------------- */
  /*  Socket Listeners and Handlers               */
  /* -------------------------------------------- */

  /**
   * Activate socket listeners related to Setup
   */
  static _activateSocketListeners() {
    game.socket.on("progress", Setup._onProgress);
  }

  /* --------------------------------------------- */

  /**
   * A list of functions to call on progress events.
   * @type {function[]}
   */
  static _progressListeners = [];

  /* --------------------------------------------- */

  /**
   * Handle a progress event from the server.
   * @param {Object} data  The progress update data.
   * @private
   */
  static _onProgress(data) {
    Setup._progressListeners.forEach(l => l(data));
  }

  /* --------------------------------------------- */

  /**
   * Add a function to be called on a progress event.
   * @param {function} listener
   */
  static _addProgressListener(listener) {
    Setup._progressListeners.push(listener);
  }

  /* --------------------------------------------- */

  /**
   * Stop sending progress events to a given function.
   * @param {function} listener
   */
  static _removeProgressListener(listener) {
    Setup._progressListeners = Setup._progressListeners.filter(l => l !== listener);
  }

  /* -------------------------------------------- */
  /*  Helper Functions                            */
  /* -------------------------------------------- */

  /**
   * A helper method to submit a POST request to setup configuration with a certain body, returning the JSON response
   * @param {Object} body             The request body to submit
   * @param {number} [timeout=30000]  The time, in milliseconds, to wait before aborting the request
   * @return {Promise<Object>}        The response body
   * @private
   */
  static post(body, timeout = 30000) {
    if (!((game.view === "setup") || (game.ready && game.user.isGM && body.shutdown))) {
      throw new Error("You may not submit POST requests to the setup page while a game world is currently active.");
    }
    return fetchWithTimeout(this.setupURL, {
      method: "POST",
      headers: {'Content-Type': 'application/json'},
      body: JSON.stringify(body),
      redirect: "manual"
    }, {timeoutMs: timeout});
  }
}

/**
 * An enum that indicates a state the Cache is in
 * @enum {number}
 */
Setup.CACHE_STATES = {
  COLD: 0,
  WARMING: 1,
  WARMED: 2
};


/**
 * A cached object of retrieved packages from the web server
 * @type {{world: object[], system: object[], module: object[]}}
 */
Setup.cache = {
  world: { packages: new Map(), state: Setup.CACHE_STATES.COLD },
  module: { packages: new Map(), state: Setup.CACHE_STATES.COLD },
  system: { packages: new Map(), state: Setup.CACHE_STATES.COLD }
};
