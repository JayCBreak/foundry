/**
 * The Package Configuration setup application
 * @extends {Application}
 */
class SetupConfigurationForm extends FormApplication {
  constructor({systems, modules, worlds}={}) {
    super({});

    /**
     * Valid Game Systems to choose from
     * @type {object[]}
     */
    this.systems = systems;

    /**
     * Install Modules to configure
     * @type {object[]}
     */
    this.modules = modules;

    /**
     * The Array of available Worlds to load
     * @type {object[]}
     */
    this.worlds = worlds;

    /**
     * The currently viewed tab
     * @type {string}
     */
    this._tab = "worlds";

    /**
     * Track the button elements which represent updates for different named packages
     * @type {HTMLElement|null}
     */
    this._progressButton = null;

    /**
     * Keeps track of which packages were updated to enable displaying their state on redraw
     * @type {Set<string>}
     * @private
     */
    this._updatedPackages = new Set();
  }

	/* -------------------------------------------- */

  /** @override */
	static get defaultOptions() {
	  return foundry.utils.mergeObject(super.defaultOptions, {
	    id: "setup-configuration",
      classes: ["dark"],
      template: "templates/setup/setup-config.html",
      popOut: false,
      scrollY: ["#world-list", "#system-list", "#module-list"],
      tabs: [{navSelector: ".tabs", contentSelector: ".content", initial: "worlds"}],
      filters: [
        {inputSelector: '#world-filter', contentSelector: "#world-list"},
        {inputSelector: '#system-filter', contentSelector: "#system-list"},
        {inputSelector: '#module-filter', contentSelector: "#module-list"}
      ]
    });
  }

  /* -------------------------------------------- */

  /** @override */
  _onSearchFilter(event, query, rgx, html) {
    let anyMatch = !query;
    for ( let li of html.children ) {
      if ( !query ) {
        li.classList.remove("hidden");
        continue;
      }
      const id = li.dataset.packageId;
      const title = li.querySelector(".package-title")?.textContent;
      const match = rgx.test(id) || rgx.test(SearchFilter.cleanQuery(title));
      li.classList.toggle("hidden", !match);
      if ( match ) anyMatch = true;
    }
    const empty = !anyMatch || !html.children.length;
    html.classList.toggle("empty", empty);
    html.previousElementSibling.classList.toggle("hidden", anyMatch);
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  _onChangeTab(event, tabs, active) {
    super._onChangeTab(event, tabs, active);
    // Clear the search filter.
    this._searchFilters.forEach(f => {
      if ( f._input ) f._input.value = "";
      f.filter(null, "");
    });
  }

  /* -------------------------------------------- */

  /** @override */
  getData(options) {

    // Configuration options
    options = game.data.options;
    options.upnp = options.upnp !== false;

    // Prepare Systems
    const systems = this.systems.map(s => {
      foundry.packages.tagPackageAvailability(s);
      s.data.updated = this._updatedPackages.has(s.id);
      return s;
    }).sort((a, b) => a.data.title.localeCompare(b.data.title));

    // Prepare Modules
    const modules = this.modules.map(m => {
      foundry.packages.tagPackageAvailability(m);
      const deps = (m.data?.dependencies ?? []).reduce((arr, d) => {
        if ( d?.name ) arr.push(d.name);
        return arr;
      }, []);
      m.dependencies = deps.length ? deps : null;
      m.data.updated = this._updatedPackages.has(m.id);
      return m;
    }).sort((a, b) => a.data.title.localeCompare(b.data.title));

    // Prepare Worlds
    const worlds = this.worlds.map(w => {
      w.shortDesc = TextEditor.previewHTML(w.data.description);
      w.system = game.systems.get(w.data.system);
      foundry.packages.tagPackageAvailability(w);
      w.data.updated = this._updatedPackages.has(w.id);
      return w;
    }).sort((a, b) => a.data.title.localeCompare(b.data.title));

    systems.concat(modules).forEach(p => {
      p.authors = p.data.author;
      const singular = [0, 1, undefined].includes(p.data.authors?.length);
      p.labels = {authors: game.i18n.localize(`Author${singular ? "" : "Pl"}`)};
      if ( p.data.authors?.length ) p.authors = p.data.authors.map(a => {
        if ( a.url ) return `<a href="${a.url}" target="_blank">${a.name}</a>`;
        return a.name;
      }).join(", ");
    });

    // Return data for rendering
    const coreVersion = game.version;
    const versionDisplay = game.release.display;
    const canReachInternet = game.data.addresses.remote;
    const couldReachWebsite = game.data.coreUpdate.couldReachWebsite;
    return {
      coreVersion: coreVersion,
      release: game.release,
      coreVersionHint: game.i18n.format("SETUP.CoreVersionHint", {versionDisplay}),
      noSystems: !systems.length,
      systems: systems,
      modules: modules,
      worlds: worlds,
      languages: game.data.languages,
      options: options,
      adminKey: game.data.passwordString,
      updateChannels: Object.entries(CONST.SOFTWARE_UPDATE_CHANNELS).reduce((obj, c) => {
        obj[c[0]] = game.i18n.localize(c[1]);
        return obj;
      }, {}),
      updateChannelHints: Object.entries(CONST.SOFTWARE_UPDATE_CHANNELS).reduce((obj, c) => {
        obj[c[0]] = game.i18n.localize(c[1]+ "Hint");
        return obj;
      }, {}),
      coreUpdate: game.data.coreUpdate.hasUpdate ? game.i18n.format("SETUP.UpdateAvailable", game.data.coreUpdate) : false,
      canReachInternet: canReachInternet,
      couldReachWebsite: couldReachWebsite,
      slowResponse: game.data.coreUpdate.slowResponse,
      updateButtonEnabled: canReachInternet && couldReachWebsite
    };
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  async _renderInner(...args) {
    await loadTemplates(["templates/setup/parts/package-tags.html"]);
    return super._renderInner(...args);
  }

  /* -------------------------------------------- */
  /*  Event Listeners and Handlers                */
  /* -------------------------------------------- */

  /** @override */
  activateListeners(html) {
    super.activateListeners(html);

    // Confirm Admin Changes
    html.find("button#admin-save").click(this._onSaveAdmin.bind(this));

    // Create or Edit World
    html.find("button#create-world, button.edit-world").click(this._onWorldConfig.bind(this));

    // Generic Submission Button
    html.find('button[data-action]').click(this._onActionButton.bind(this));

    // Install Package
    html.find("button.install-package").click(this._onInstallPackageDialog.bind(this));

    // Update Package
    html.find("button.update").click(this._onUpdatePackage.bind(this));

    // Update All Packages
    html.find("button.update-packages").click(this._onUpdatePackages.bind(this));

    // Uninstall Package
    html.find("button.uninstall").click(this._onUninstallPackage.bind(this));

    // Update Core
    html.find("button#update-core").click(this._onCoreUpdate.bind(this));

    // Lock
    html.find("button.lock-toggle").click(this._onToggleLock.bind(this));

    html.find(`input[name=${this._tab}Filter]`).focus();

    html.find("a.system-install").click(this._onClickSystemInstall.bind(this));
  }

  /* -------------------------------------------- */

  /**
   * Post the setup configuration form
   * @param {Object} data
   * @return {Promise}
   * @private
   */
  async _post(data) {

    // Construct form data
    const formData = new FormDataExtended(this.form);
    for ( let [k, v] of Object.entries(data) ) {
      formData.set(k, v);
    }

    // Post the request and handle redirects
    try {
      const response = await fetchWithTimeout(Setup.setupURL, {method: "POST", body: formData});
      if (response.redirected) return window.location.href = response.url;

      // Process response
      const json = await response.json();
      if (json.error) {
        const message = game.i18n.localize(json.error);
        const err = new Error(message);
        err.stack = json.stack;
        throw err;
      }

      return json;
    }
    catch (e) {
      ui.notifications.error(e, {permanent: true});
      throw e;
    }
  }

  /* -------------------------------------------- */

  /**
   * Reload the setup view by re-acquiring setup data and re-rendering the form
   * @private
   */
  async reload() {
    this._progressButton = null;
    return Setup.getData(game.socket, game.view).then(setupData => {
      foundry.utils.mergeObject(game.data, setupData);
      foundry.utils.mergeObject(this, setupData);
      this.render();
      Object.values(ui.windows).forEach(app => {
        if ( app instanceof InstallPackage ) app.render();
      });
    });
  }

  /* -------------------------------------------- */

  /**
   * Generic button handler for the setup form which submits a POST request including any dataset on the button itself
   * @param {MouseEvent} event    The originating mouse click event
   * @return {Promise}
   * @private
   */
  async _onActionButton(event) {
    event.preventDefault();

    // Construct data to post
    const button = event.currentTarget;
    button.disabled = true;
    const data = duplicate(button.dataset);

    // Warn about world migration
    if ( data.action === "launchWorld" ) {
      const world = game.data.worlds.find(w => w.data.name === data.world);
      if ( !world ) return;
      if ( game.release.isGenerationalChange(world.data.coreVersion) ) {
        const confirm = await Dialog.confirm({
          title: game.i18n.localize("SETUP.WorldMigrationRequiredTitle"),
          content: game.i18n.format("SETUP.WorldMigrationRequired", {
            world: world.data.title,
            oldVersion: world.data.coreVersion,
            newVersion: game.release.display,
            nIncompatible: game.data.modules.filter(m => m.incompatible).length,
            nModules: game.data.modules.length
          }),
        });
        if ( !confirm ) return button.disabled = false;
      }
    }

    // Submit the post request
    const response = await this._post(data);
    button.disabled = false;
    return response;
  }

  /* -------------------------------------------- */

  /**
   * Confirm user intent when saving admin changes to the application configuration
   * @param {MouseEvent} event    The originating mouse click event
   * @return {Promise}
   * @private
   */
  async _onSaveAdmin(event) {
    event.preventDefault();
    event.stopImmediatePropagation();
    return Dialog.confirm({
      title: game.i18n.localize("SETUP.ConfigSave"),
      content: `<p class="notification">${game.i18n.localize("SETUP.ConfigSaveWarning")}</p>`,
      yes: async () => {
        await this._post({action: "adminConfigure"})
        this.element.html(`<p>${game.i18n.localize("SETUP.ConfigSaveRestart")}</p>`);
      },
      defaultYes: false
    });
  }

  /* -------------------------------------------- */

  /**
   * Begin creation of a new World using the config form
   * @param {MouseEvent} event    The originating mouse click event
   * @private
   */
  _onWorldConfig(event) {
    event.preventDefault();
    const button = event.currentTarget;
    let data = {};
    const options = {};
    if ( button.dataset.world ) {
      data = game.data.worlds.find(w => w.data.name === button.dataset.world);
    } else {
      if ( game.systems.size === 0 ) {
        ui.notifications.warn(game.i18n.localize("SETUP.YouMustInstallASystem"));
        return;
      }
      options.create = true;
    }
    new WorldConfig(data, options).render(true)
  }

  /* -------------------------------------------- */
  /*  Package Management                          */
  /* -------------------------------------------- */

  /**
   * Handle install button clicks to add new packages
   * @param {Event} event
   * @private
   */
  async _onInstallPackageDialog(event) {
    event.preventDefault();
    let button = this._progressButton = event.currentTarget;
    const list = button.closest(".tab").querySelector(".package-list");
    const type = list.dataset.packageType;
    new InstallPackage({packageType: type, setup: this}).render(true);
  }

  /* -------------------------------------------- */

  /**
   * Handle update button press for a single Package
   * @param {Event} event
   * @private
   */
  async _onUpdatePackage(event) {
    event.preventDefault();

    // Disable the button
    const button = event.currentTarget;

    let pack = this._getPackageFromButton(button);
    if ( !pack?.data?.manifest ) return;

    // Inquire with the server for updates
    if ( button.dataset.state === "check" ) {
      let data = await this._updateCheckOne(pack.type, pack.id, button);
      let manifest = data.manifest;

      if ( data.state === "error" ) {
        ui.notifications.error(data.error, {permanent: true});
        return;
      }

      if ( data.state === "warning" ) {
        ui.notifications.warn(data.warning);
        return;
      }

      if ( data.state === "trackChange" ) {
        if ( await this._promptTrackChange(pack, data.trackChange) ) {
          manifest = data.trackChange.manifest;
          data.state = "update";
        }
      }

      if ( data.state === "sidegrade" && ui.setup ) {
        await ui.setup.reload();
      }
      else if ( data.state === "update" ) {
        await this._updateDownloadOne(pack.type, pack.id, button, manifest);
        if ( ui.setup ) await ui.setup.reload();
      }
    }
  }

  /* -------------------------------------------- */

  /**
   * Prompt the user to use a new Package track it if they haven't previously declined.
   * @param {Object} pack          The local Package
   * @param {string} manifest      The local manifest url
   * @return {Promise<boolean>}    If the new track manifest should be used
   * @private
   */
  async _promptTrackChange(pack, trackChange) {
    let declinedManifestUpgrades = game.settings.get("core", "declinedManifestUpgrades");
    let declinedForVersion = declinedManifestUpgrades[pack.data.name] === pack.data.version;
    if ( declinedForVersion ) return false;

    // Display prompt
    let content = await renderTemplate("templates/setup/manifest-update.html", {
      localManifest: pack.data.manifest,
      localTitle: game.i18n.format("SETUP.PriorManifestUrl", {version: pack.data.version}),
      remoteManifest: trackChange.manifest,
      remoteTitle: game.i18n.format("SETUP.UpdatedManifestUrl", {version: trackChange.version}),
      package: pack.data.title
    });
    let accepted = await Dialog.confirm({
      title: `${pack.data.title} ${game.i18n.localize("SETUP.ManifestUpdate")}`,
      content: content,
      yes: () => {
        delete declinedManifestUpgrades[pack.data.name];
        return true;
      },
      no: () => {
        declinedManifestUpgrades[pack.data.name] = pack.data.version;
        return false;
      },
      defaultYes: true
    });
    game.settings.set("core", "declinedManifestUpgrades", declinedManifestUpgrades);
    return accepted;
  }


  /* -------------------------------------------- */

  /**
   * Traverses the HTML structure to find the Package this button belongs to
   * @param {HTMLElement} button  The clicked button
   * @returns {Promise<Object>} A Package
   * @private
   */
  _getPackageFromButton(button) {
    let li = button.closest("li.package");
    let id = li.dataset.packageId;
    let type = li.closest("ul.package-list").dataset.packageType;
    return game[`${type}s`].get(id, {strict: true});
  }

  /* -------------------------------------------- */

  /**
   * Execute upon an update check for a single Package
   * @param {string} type         The package type to check
   * @param {string} name         The package name to check
   * @param {HTMLElement} button  The update button for the package
   * @return {Promise<object>}    The status of the update check
   * @private
   */
  async _updateCheckOne(type, name, button) {
    button.disabled = true;

    // Get the update manifest from the server
    let manifest;
    const checkData = {
      type: type,
      name: name,
      button: button,
      state: ""
    };

    try {
      manifest = await Setup.checkPackage({type, name});
    } catch(err) {
      checkData.state = "error";
      checkData.error = game.i18n.localize("PACKAGE.UpdateCheckTimedOut");
      button.disabled = false;
      return checkData;
    }

    if ( manifest.error ) {
      checkData.state = "error";
      checkData.error = manifest.error;
      return checkData;
    }

    // Packages which cannot be updated because they require a more modern core version
    if ( !manifest.isSupported ) {
      button.innerHTML = `<i class="fas fa-ban"></i><label>${game.i18n.format("SETUP.PackageStatusBlocked")}</label>`;
      checkData.state = "warning";
      checkData.warning = game.i18n.format("SETUP.PackageUpdateBlocked", {
        name: manifest.name,
        vreq: manifest.minimumCoreVersion,
        vcur: game.version
      });
      return checkData;
    }

    // Packages which can be updated
    if ( manifest.isUpgrade ) {
      button.innerHTML = `<i class="fas fa-download"></i><label>${game.i18n.format("SETUP.PackageStatusUpdate")}</label>`;
      checkData.state = "update";
      checkData.manifest = manifest.manifest;
    }

    // Packages which are already current
    else {
      button.innerHTML = `<i class="fas fa-check"></i><label>${game.i18n.format("SETUP.PackageStatusCurrent")}</label>`;
    }

    if ( manifest.hasSidegraded ) {
      checkData.state = "sidegrade";
    }
    else if ( manifest.hasTrackChangeAvailable ) {
      checkData.state = "trackChange";
      checkData.trackChange = manifest.trackChange;
      checkData.manifest = manifest.manifest;
    }

    button.dataset.state = checkData.state;
    return checkData;
  }

  /* -------------------------------------------- */

  /**
   * Execute upon an update download for a single Package
   * Returns a Promise which resolves once the download has successfully started
   * @param {string} type         The package type to install
   * @param {string} name         The package name to install
   * @param {HTMLElement} button  The Download button
   * @return {Promise}
   * @private
   */
  async _updateDownloadOne(type, name, button, manifestUrl) {
    this._progressButton = button;
    this._progressButton.innerHTML = `<i class="fas fa-spinner fa-pulse"></i><label>${game.i18n.format("SETUP.PackageStatusUpdating")}</label>`;
    const manifest = await Setup.installPackage({type, name, manifest: manifestUrl}, data => {
      this.updateProgressBar(data);
      this.updateProgressButton(data);
    });
    this._updatedPackages.add(manifest.id);
    this._progressButton = null;
    return manifest;
  }

  /* -------------------------------------------- */

  /**
   * Handle uninstall button clicks to remove existing packages
   * @param {Event} event
   * @private
   */
  _onUninstallPackage(event) {
    event.preventDefault();

    // Disable the button
    let button = event.currentTarget;
    button.disabled = true;

    // Obtain the package metadata
    const li = button.closest(".package");
    const name = li.dataset.packageId;
    const type = li.closest(".package-list").dataset.packageType;

    // Get the target package
    let collection = game.data[type+"s"];
    let idx = collection.findIndex(p => p.id === name);
    let pack = collection[idx];

    // Define a warning message
    const title = pack.data.title;
    let warning = `<p>${game.i18n.format("SETUP.PackageDeleteConfirm", {type: type.titleCase(), title})}</p>`;
    // Based on https://stackoverflow.com/a/8084248
    const code = (Math.random() + 1).toString(36).substring(7, 11);
    if ( type === "world" ) {
      warning += `<p class="notification">${game.i18n.localize("SETUP.WorldDeleteConfirm1")}</p>`
      warning += `<p>${game.i18n.format("SETUP.WorldDeleteConfirm2")}<b>${code}</b></p>`;
      warning += `<p><input id="delete-confirm" type="text" required autocomplete="off"></p>`;
    } else {
      warning += `<p class="notification">${game.i18n.localize("SETUP.PackageDeleteNoUndo")}</p>`
    }

    // Confirm deletion request
    Dialog.confirm({
      title: game.i18n.format("SETUP.PackageDeleteTitle", {type: type.titleCase(), title}),
      content: warning,
      yes: async html => {

        // Confirm World deletion
        if ( type === "world" ) {
          const confirm = html.find("#delete-confirm").val();
          if ( confirm !== code ) {
            return ui.notifications.error("SETUP.PackageDeleteWorldConfirm", {localize: true});
          }
        }

        // Submit the server request
        const response = await Setup.uninstallPackage({type, name});
        if ( response.error ) {
          const err = new Error(response.error);
          err.stack = response.stack;
          ui.notifications.error(game.i18n.localize("SETUP.UninstallFailure") + ": " + err.message);
          console.error(err);
        } else {
          ui.notifications.info(`${type.titleCase()} ${name} ${game.i18n.localize("SETUP.UninstallSuccess")}.`);
          collection.splice(idx, 1);
        }

        // Re-render the setup form
        ui.setup.reload();
      }
    }).then(() => button.disabled = false);
  }

  /* -------------------------------------------- */

  /**
   * Execute upon an update-all workflow to update all packages of a certain type
   * @param {Event} event
   * @private
   */
  async _onUpdatePackages(event) {
    event.preventDefault();
    let button = event.currentTarget;
    button.disabled = true;
    const icon = button.querySelector("i");
    icon.className = "fas fa-spinner fa-pulse";
    let ol = $(".tab.active .package-list");
    let type = ol.data("packageType");

    // Get Packages
    let packages = [];
    ol.children(".package").each((i, li) => {
      const id = li.dataset.packageId;
      const pack = game[`${type}s`].get(id);
      if ( pack && pack.data.manifest && !pack.locked ) packages.push({
        id: id,
        status: "none",
        button: li.querySelector("button.update")
      });
    });

    // Ensure the package cache is warm
    await Setup.warmPackages({type});

    // Check for updates in parallel
    let shouldReload = false;
    const checks = [];
    for ( let [i, p] of packages.entries() ) {
      const check = this._updateCheckOne(type, p.id, p.button);
      checks.push(check);
      if (((i+1) % 10) === 0) await check; // Batch in groups of 10
    }
    const checkedPackages = await Promise.all(checks);

    // Execute updates one at a time
    let updateLog = [];
    for (let p of checkedPackages ) {
      const pack = game[`${p.type}s`].get(p.name);
      if ( p.state === "error" ) updateLog.push({package: pack, action: game.i18n.localize("Error"), actionClass: "fa-exclamation-circle", description: p.error});
      else if ( p.state === "warning" ) updateLog.push({package: pack, action: game.i18n.localize("Warning"), actionClass: "fa-exclamation-triangle", description: p.warning});
      if ( !(p.state === "update" || p.state === "trackChange") ) continue;
      if ( p.state === "sidegrade" ) shouldReload = true;
      let manifest = p.manifest;
      let shouldUpdate = true;
      if ( p.trackChange ) {
        shouldUpdate = await this._promptTrackChange(pack, p.trackChange);
        manifest = p.trackChange.manifest;
      }
      if ( shouldUpdate ) {
        try {
          let updated = await this._updateDownloadOne(type, p.name, p.button, manifest);
          if ( p.state !== "sidegrade" ) {
            updateLog.push({
              package: pack,
              action: game.i18n.localize("Update"),
              actionClass: "fa-check-circle",
              description: `${pack.data.version} ➞ ${updated.data.version}`
            });
            shouldReload = true;
          }
        }
        catch (exception) {
          updateLog.push({package: pack, action: game.i18n.localize("Error"), actionClass: "fa-exclamation-circle", description: exception.message});
        }
      }
      p.available = false;
    }

    // Display Updatelog
    if ( updateLog.length > 0 ) {
      let content = await renderTemplate("templates/setup/updated-packages.html", {
        changed: updateLog,
      });
      await Dialog.prompt({
        title: game.i18n.localize("SETUP.UpdatedPackages"),
        content: content,
        callback: () => {},
        options: {width: 600},
        rejectClose: false
      });
    }
    if (shouldReload && ui.setup) {
      await ui.setup.reload();
    }
    icon.className = "fas fa-cloud-download-alt";
    button.disabled = false;
  }

  /* -------------------------------------------- */

  /**
   * Handle lock button clicks to lock / unlock a Package
   * @param {Event} event
   * @private
   */
  async _onToggleLock(event) {
    event.preventDefault();

    // Submit a lock request and update package data
    const button = event.currentTarget;
    let pack = this._getPackageFromButton(button);
    let shouldLock = !pack.locked;
    await this._post({action: "lockPackage", type: pack.type, name: pack.id, shouldLock: shouldLock});
    pack.locked = shouldLock;

    // Update the setup interface
    let icon = button.querySelector(".fas");
    if (shouldLock) {
      button.classList.replace("lock", "unlock");
      icon.classList.replace("fa-unlock", "fa-lock");
    }
    else {
      button.classList.replace("unlock", "lock");
      icon.classList.replace("fa-lock", "fa-unlock");
    }
    let li = button.closest("li.package");
    let uninstall = li.querySelector(".uninstall");
    uninstall.hidden = shouldLock;
    let update = li.querySelector(".update");
    if ( update ) {
      update.hidden = shouldLock;
    }
  }

  /* -------------------------------------------- */

  /**
   * Spawn the system install dialog with a given system name already filled in.
   * @param {TriggeredEvent} event  The triggering event.
   * @private
   */
  _onClickSystemInstall(event) {
    event.preventDefault();
    const query = event.currentTarget.dataset.query;
    new InstallPackage({packageType: "system", setup: this, filterValue: query}).render(true);
  }

  /* -------------------------------------------- */
  /*  Core Software Update                        */
  /* -------------------------------------------- */

  /**
   * Handle button clicks to update the core VTT software
   * @param {Event} event
   * @private
   */
  async _onCoreUpdate(event) {
    const button = event.currentTarget;
    const form = button.form;
    const label = button.children[1];

    // Disable the form
    button.disabled = true;
    form.disabled = true;

    const progress = data => {
      if ( ["UpdateComplete", "Error"].includes(data.step) ) {
        // After the update has completed or was interrupted due to error, remove any listeners and update the UI
        // appropriately.
        Setup._removeProgressListener(progress);

        // Final form updates
        button.disabled = false;
        const icon = button.querySelector("i");
        icon.className = data.step === "UpdateComplete" ? "fas fa-check" : "fas fa-times";
        form.disabled = false;

        // Display a notification message
        const level = data.step === "UpdateComplete" ? "info" : "error";
        ui.notifications[level](data.message, {localize: true});
        return;
      }
      this.updateProgressBar(data);
      this.updateProgressButton(data);
      UpdateNotes.updateButton(data);
    };

    // Condition next step based on action
    if ( button.value === "updateDownload" ) {
      this._progressButton = button;
      // Attach a listener to accept progress events from the server after the update process has begun.
      Setup._addProgressListener(progress);
    }

    // Post the update request
    const response = await this._post({action: button.value}).catch(err => {
      button.disabled = false;
      form.disabled = false;
      throw err;
    });

    if ( response.warn ) {
      button.disabled = false;
      form.disabled = false;
      return ui.notifications.warn(response.warn, {localize: true});
    }

    // Proceed to download step
    if ( button.value === "updateCheck" ) {
      let releaseData = new foundry.config.ReleaseData(response);
      ui.notifications.info(game.i18n.format("SETUP.UpdateInfoAvailable", {display: releaseData.display}));
      label.textContent = game.i18n.format("SETUP.UpdateButtonDownload", {display: releaseData.display});
      button.value = "updateDownload";
      button.disabled = false;
      if ( response.notes ) new UpdateNotes(response).render(true);
      if ( response.willDisableModules ) {
        ui.notifications.warn(game.i18n.format("SETUP.UpdateWarningWillDisable", {
          nIncompatible: game.data.modules.filter(m => m.incompatible).length,
          nModules: game.data.modules.length
        }), {permanent: true});
      }
    }
  }

  /* -------------------------------------------- */
  /*  Socket Listeners and Handlers               */
  /* -------------------------------------------- */

  /**
   * Update the display of an installation progress bar for a particular progress packet
   * @param {Object} data   The progress update data
   */
  updateProgressBar(data) {
    const tabName = data.type === "core" ? "update" : data.type+"s";
    const tab = this.element.find(`.tab[data-tab="${tabName}"]`);
    if ( !tab.hasClass("active") ) return;
    const progress = tab.find(".progress-bar");
    progress.css("visibility", "visible");

    // Update bar and label position
    let pl = `${data.pct}%`;
    let bar = progress.children(".bar");
    bar.css("width", pl);
    let barLabel = progress.children(".pct");
    barLabel.text(pl);
    barLabel.css("left", pl);
  }

  /* -------------------------------------------- */

  /**
   * Update installation progress for a particular button which triggered the action
   * @param {Object} data   The progress update data
   */
  updateProgressButton(data) {
    const button = this._progressButton;
    if ( !button ) return;
    button.disabled = data.pct < 100;

    // Update Icon
    const icon = button.querySelector("i");
    if ( data.pct < 100 ) icon.className = "fas fa-spinner fa-pulse";

    // Update label
    const label = button.querySelector("label");
    const step = game.i18n.localize(data.step);
    if ( label ) label.textContent = step;
    else button.textContent = " " + step;
  }
}
