/**
 * The Camera UI View that displays all the camera feeds as individual video elements.
 * @type {Application}
 *
 * @param {WebRTC} webrtc                 The WebRTC Implementation to display
 * @param {ApplicationOptions} [options]  Application configuration options.
 */
class CameraViews extends Application {

  /** @override */
  static get defaultOptions() {
    return foundry.utils.mergeObject(super.defaultOptions, {
      id: "camera-views",
      template: "templates/hud/camera-views.html",
      popOut: false
    });
  }

  /* -------------------------------------------- */

  /**
   * A reference to the master AV orchestrator instance
   * @type {AVMaster}
   */
  get webrtc() {
    return game.webrtc;
  }

  /* -------------------------------------------- */
  /* Public API                                   */
  /* -------------------------------------------- */

  /**
   * Obtain a reference to the div.camera-view which is used to portray a given Foundry User.
   * @param {string} userId     The ID of the User document
   * @return {HTMLElement|null}
   */
  getUserCameraView(userId) {
    return this.element.find(`.camera-view[data-user=${userId}]`)[0] || null;
  }

  /* -------------------------------------------- */

  /**
   * Obtain a reference to the video.user-camera which displays the video channel for a requested Foundry User.
   * If the user is not broadcasting video this will return null.
   * @param {string} userId     The ID of the User document
   * @return {HTMLVideoElement|null}
   */
  getUserVideoElement(userId) {
    return this.element.find(`.camera-view[data-user=${userId}] video.user-camera`)[0] || null;
  }

  /* -------------------------------------------- */

  /**
   * Sets whether a user is currently speaking or not
   *
   * @param {string} userId     The ID of the user
   * @param {boolean} speaking  Whether the user is speaking
   */
  setUserIsSpeaking(userId, speaking) {
    const view = this.getUserCameraView(userId);
    if ( view ) view.classList.toggle("speaking", speaking);
  }

  /* -------------------------------------------- */
  /*  Application Rendering                       */
  /* -------------------------------------------- */

  /**
   * Extend the render logic to first check whether a render is necessary based on the context
   * If a specific context was provided, make sure an update to the navigation is necessary before rendering
   */
  render(force, context={}) {
    const { renderContext, renderData } = context;
    if (this.webrtc.mode === AVSettings.AV_MODES.DISABLED)
      return this;
    if (renderContext) {
      if (renderContext !== "updateUser")
        return this;
      const updateKeys = ["name", "permissions", "role", "active", "color", "sort", "character", "avatar"];
      if (!updateKeys.some(k => renderData.hasOwnProperty(k)))
        return this;
    }
    return super.render(force, context);
  }

  /* -------------------------------------------- */

  /** @override */
  async _render(force = false, options = {}) {
    await super._render(force, options);
    this._setPlayerListVisibility();
    this.webrtc.onRender();
  }

  /* -------------------------------------------- */

  /** @override */
  getData(options) {
    const settings = this.webrtc.settings;
    const userSettings = settings.users;

    // Get the sorted array of connected users
    const connectedIds = this.webrtc.client.getConnectedUsers();
    const users = connectedIds.reduce((users, u) => {
      const data = this._getDataForUser(u, userSettings[u]);
      if ( data ) users.push(data);
      return users;
    }, []);
    users.sort(this.constructor._sortUsers);

    // Maximum Z of all user popout windows
    this.maxZ = Math.max(...users.map(u => userSettings[u.id].z));

    // Define a dynamic class for the camera dock container which affects it's rendered style
    let dockClass = `camera-size-${settings.client.dockSize} camera-position-${settings.client.dockPosition}`;
    if (!users.some(u => !u.settings.popout)) dockClass += " webrtc-dock-empty";

    // Alter the body class depending on whether the players list is hidden
    if (settings.client.hidePlayerList) document.body.classList.add("players-hidden");
    else document.body.classList.remove("players-hidden");

    // Return data for rendering
    return {
      self: game.user,
      users: users,
      dockClass: dockClass,
      muteAll: settings.muteAll
    };
  }

  /* -------------------------------------------- */

  /**
   * Prepare rendering data for a single user
   * @private
   */
  _getDataForUser(userId, settings) {
    const user = game.users.get(userId);
    if ( !user || !user.active ) return null;
    const charname = user.character ? user.character.name.split(" ")[0] : "";

    // CSS classes for the frame
    const frameClass = settings.popout ? "camera-box-popout" : "camera-box-dock";
    const audioClass = this.webrtc.canUserShareAudio(userId) ? null : "no-audio";
    const videoClass = this.webrtc.canUserShareVideo(userId) ? null : "no-video";

    // Return structured User data
    return {
      user: user,
      id: user.id,
      local: user.isSelf,
      name: user.name,
      color: user.data.color,
      colorAlpha: hexToRGBAString(colorStringToHex(user.data.color), 0.20),
      charname: user.isGM ? game.i18n.localize("GM") : charname,
      avatar: user.avatar,
      settings: settings,
      volume: AudioHelper.volumeToInput(settings.volume),
      cameraViewClass: [frameClass, videoClass, audioClass].filterJoin(" ")
    };
  }

  /* -------------------------------------------- */

  /**
   * A custom sorting function that orders/arranges the user display frames
   * @return {number}
   * @private
   */
  static _sortUsers(a, b) {
    const as = a.settings;
    const bs = b.settings;
    if (as.popout && bs.popout) return as.z - bs.z; // Sort popouts by z-index
    if (as.popout) return -1;                       // Show popout feeds first
    if (bs.popout) return 1;
    if (a.user.isSelf) return -1;                   // Show local feed first
    if (b.user.isSelf) return 1;
    if (a.hasVideo && !b.hasVideo) return -1;       // Show remote users with a camera before those without
    if (b.hasVideo && !a.hasVideo) return 1;
    return a.user.data.sort - b.user.data.sort;     // Sort according to user order
  }

  /* -------------------------------------------- */
  /*  Event Listeners and Handlers                */
  /* -------------------------------------------- */

  /** @override */
  activateListeners(html) {

    // Display controls when hovering over the video container
    let cvh = this._onCameraViewHover.bind(this);
    html.find('.camera-view').hover(cvh, cvh);

    // Handle clicks on AV control buttons
    html.find(".av-control").click(this._onClickControl.bind(this));

    // Handle volume changes
    html.find(".webrtc-volume-slider").change(this._onVolumeChange.bind(this));

    // Hide Global permission icons depending on the A/V mode
    const mode = this.webrtc.mode;
    if (mode === AVSettings.AV_MODES.VIDEO) html.find('[data-action="toggle-audio"]').hide();
    if (mode === AVSettings.AV_MODES.AUDIO) html.find('[data-action="toggle-video"]').hide();

    // Make each popout window draggable
    for (let popout of this.element.find(".app.camera-view-popout")) {
      let box = popout.querySelector(".camera-view");
      new CameraPopoutAppWrapper(this, box.dataset.user, $(popout));
    }

    // Listen to the video's srcObjectSet event to set the display mode of the user.
    for (let video of this.element.find("video")) {
      const view = video.closest(".camera-view");
      this._refreshView(view);
      video.addEventListener('webrtcVideoSet', ev => {
        const view = video.closest(".camera-view");
        if ( view.dataset.user !== ev.detail ) return;
        this._refreshView(view);
      });
    }
  }

  /* -------------------------------------------- */

  /**
   * On hover in a camera container, show/hide the controls.
   * @event {Event} event   The original mouseover or mouseout hover event
   * @private
   */
  _onCameraViewHover(event) {
    this._toggleControlVisibility(event.currentTarget, event.type === "mouseenter", null);
  }

  /* -------------------------------------------- */

  /**
   * On clicking on a toggle, disable/enable the audio or video stream.
   * @event {MouseEvent} event   The originating click event
   * @private
   */
  async _onClickControl(event) {
    event.preventDefault();

    // Reference relevant data
    const button = event.currentTarget;
    const action = button.dataset.action;
    const view = button.closest(".camera-view");
    const user = game.users.get(view.dataset.user);
    const settings = this.webrtc.settings;
    const userSettings = settings.getUser(user.id);

    // Handle different actions
    switch ( action ) {

      // Globally block video
      case "block-video": {
        if (!game.user.isGM) break;
        await user.update({"permissions.BROADCAST_VIDEO": !userSettings.canBroadcastVideo});
        this._refreshView(view);
        break;
      }

      // Globally block audio
      case "block-audio": {
        if (!game.user.isGM) break;
        await user.update({"permissions.BROADCAST_AUDIO": !userSettings.canBroadcastAudio});
        this._refreshView(view);
        break;
      }

      // Toggle video display
      case "toggle-video": {
        if ( !user.isSelf ) break;
        if ( userSettings.hidden && !userSettings.canBroadcastVideo ) {
          return ui.notifications.warn("WEBRTC.WarningCannotEnableVideo", {localize: true});
        }
        await settings.set("client", `users.${user.id}.hidden`, !userSettings.hidden);
        this._refreshView(view);
        break;
      }

      // Toggle audio output
      case "toggle-audio":
        if ( !user.isSelf ) break;
        if ( userSettings.muted && !userSettings.canBroadcastAudio ) {
          return ui.notifications.warn("WEBRTC.WarningCannotEnableAudio", {localize: true})
        }
        await settings.set("client", `users.${user.id}.muted`, !userSettings.muted);
        this._refreshView(view);
        break;

      // Toggle mute all peers
      case "mute-peers":
        if ( !user.isSelf ) break;
        await settings.set("client", "muteAll", !settings.client.muteAll);
        this._refreshView(view);
        break;

      // Configure settings
      case "configure":
        return this.webrtc.config.render(true);

      // Toggle popout
      case "toggle-popout":
        await settings.set("client", `users.${user.id}.popout`, !userSettings.popout);
        return this.render();

      // Hide players
      case "toggle-players":
        await settings.set("client", "hidePlayerList", !settings.client.hidePlayerList);
        return this.render();

      // Cycle camera size
      case "change-size":
        const sizes = ["large", "medium", "small"];
        const size = sizes.indexOf(settings.client.dockSize);
        const next = size+1 >= sizes.length ? 0 : size+1;
        await settings.set("client", "dockSize", sizes[next]);
        return this.render();
    }
  }

  /* -------------------------------------------- */

  /**
   * Change volume control for a stream
   * @param {Event} event   The originating change event from interaction with the range input
   * @private
   */
  _onVolumeChange(event) {
    const input = event.currentTarget;
    const box = input.closest(".camera-view");
    const userId = box.dataset.user;
    let volume = AudioHelper.inputToVolume(input.value);
    box.getElementsByTagName("video")[0].volume = volume;
    this.webrtc.settings.set("client", `users.${userId}.volume`, volume);
  }

  /* -------------------------------------------- */
  /*  Internal Helpers                            */
  /* -------------------------------------------- */

  /**
   * Dynamically refresh the state of a single camera view
   * @param {HTMLElement} view      The view container div
   * @private
   */
  _refreshView(view) {
    const userId = view.dataset.user;
    const isSelf = game.user.id === userId;
    const clientSettings = game.webrtc.settings.client;
    const userSettings = game.webrtc.settings.getUser(userId);

    // Identify permissions
    const cbv = game.webrtc.canUserBroadcastVideo(userId);
    const csv = game.webrtc.canUserShareVideo(userId);
    const cba = game.webrtc.canUserBroadcastAudio(userId);
    const csa = game.webrtc.canUserShareAudio(userId);

    // Refresh video display
    const video = view.querySelector("video.user-camera");
    const avatar = view.querySelector("img.user-avatar");
    if (video && avatar) {
      video.style.visibility = csv ? 'visible' : "hidden";
      video.style.display = csv ? "block" : "none";
      avatar.style.display = csv ? "none" : "unset";
    }

    // Hidden and muted status icons
    view.querySelector(".status-hidden").classList.toggle("hidden", csv);
    view.querySelector(".status-muted").classList.toggle("hidden", csa);

    // Volume bar and video output volume
    video.volume = userSettings.volume;
    video.muted = isSelf || clientSettings.muteAll; // Mute your own video
    const volBar = view.querySelector(".volume-bar");
    const displayBar = (userId !== game.user.id) && cba;
    volBar.style.display = displayBar ? "block" : "none";
    volBar.disabled = !displayBar;

    // Control toggle states
    const actions = {
      "block-video": {state: !cbv, display: game.user.isGM && !isSelf},
      "block-audio": {state: !cba, display: game.user.isGM && !isSelf},
      "toggle-video": {state: !csv, display: isSelf},
      "toggle-audio": {state: !csa, display: isSelf},
      "mute-peers": {state: clientSettings.muteAll, display: isSelf},
      "toggle-players": {state: !clientSettings.hidePlayerList, display: isSelf}
    };
    const toggles = view.querySelectorAll(".av-control.toggle");
    for ( let button of toggles ) {
      const action = button.dataset.action;
      if (!(action in actions) ) continue;
      const state = actions[action].state;
      const displayed = actions[action].display;
      button.style.display = displayed ? "block" : "none";
      button.enabled = displayed;
      button.children[0].classList.remove(this._getToggleIcon(action, !state));
      button.children[0].classList.add(this._getToggleIcon(action, state));
      button.setAttribute("title", this._getToggleTooltip(action, state));
    }
  }

  /* -------------------------------------------- */

  /**
   * Render changes needed to the PlayerList ui.
   * Show/Hide players depending on option.
   * @private
   */
  _setPlayerListVisibility() {
    const hidePlayerList = this.webrtc.settings.client.hidePlayerList;
    const players = document.getElementById("players");
    const top = document.getElementById("ui-top");
    if ( players ) players.classList.toggle("hidden", hidePlayerList);
    if ( top ) top.classList.toggle("offset", !hidePlayerList);
  }

  /* -------------------------------------------- */

  /**
   * Get the icon class that should be used for various action buttons with different toggled states.
   * The returned icon should represent the visual status of the NEXT state (not the CURRENT state).
   *
   * @param {string} action     The named av-control button action
   * @param {boolean} state     The CURRENT action state.
   * @return {string}           The icon that represents the NEXT action state.
   * @private
   */
  _getToggleIcon(action, state) {
    const actionMapping = {
      "block-video": ["fa-video", "fa-video-slash"],            // True means "blocked"
      "block-audio": ["fa-microphone", "fa-microphone-slash"],  // True means "blocked"
      "toggle-video": ["fa-video", "fa-video-slash"],           // True means "enabled"
      "toggle-audio": ["fa-microphone", "fa-microphone-slash"], // True means "enabled"
      "mute-peers": ["fa-volume-up", "fa-volume-mute"],         // True means "muted"
      "toggle-players": ["fa-caret-square-right", "fa-caret-square-left"] // True means "displayed"
    };
    const icons = actionMapping[action];
    return icons ? icons[state ? 1: 0] : null;
  }

  /* -------------------------------------------- */

  /**
   * Get the text title that should be used for various action buttons with different toggled states.
   * The returned title should represent the tooltip of the NEXT state (not the CURRENT state).
   *
   * @param {string} action     The named av-control button action
   * @param {boolean} state     The CURRENT action state.
   * @return {string}           The icon that represents the NEXT action state.
   * @private
   */
  _getToggleTooltip(action, state) {
    const actionMapping = {
      "block-video": ["BlockUserVideo", "AllowUserVideo"],      // True means "blocked"
      "block-audio": ["BlockUserAudio", "AllowUserAudio"],      // True means "blocked"
      "toggle-video": ["DisableMyVideo", "EnableMyVideo"],      // True means "enabled"
      "toggle-audio": ["DisableMyAudio", "EnableMyAudio"],      // True means "enabled"
      "mute-peers": ["MutePeers", "UnmutePeers"],               // True means "muted"
      "toggle-players": ["ShowPlayers", "HidePlayers"]          // True means "displayed"
    };
    const labels = actionMapping[action];
    return game.i18n.localize(`WEBRTC.Tooltip${labels ? labels[state ? 1 : 0] : ""}`);
  }

  /* -------------------------------------------- */

  /**
   * Show or hide UI control elements
   * This replaces the use of jquery.show/hide as it simply adds a class which has display:none
   * which allows us to have elements with display:flex which can be hidden then shown without
   * breaking their display style.
   * This will show/hide the toggle buttons, volume controls and overlay sidebars
   * @param {jQuery} container    The container for which to show/hide control elements
   * @param {boolean} show        Whether to show or hide the controls
   * @param {string} selector     Override selector to specify which controls to show or hide
   * @private
   */
  _toggleControlVisibility(container, show, selector) {
    selector = selector || `.control-bar`;
    container.querySelectorAll(selector).forEach(c => c.classList.toggle("hidden", !show));
  }
}
