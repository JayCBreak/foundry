/**
 * The Application responsible for configuring a single Tile document within a parent Scene.
 * @extends {DocumentSheet}
 *
 * @param {Tile} tile                    The Tile object being configured
 * @param {DocumentSheetOptions} [options]  Additional application rendering options
 */
class TileConfig extends DocumentSheet {

  /** @inheritdoc */
	static get defaultOptions() {
    return foundry.utils.mergeObject(super.defaultOptions, {
      id: "tile-config",
      title: game.i18n.localize("TILE.ConfigTitle"),
      template: "templates/scene/tile-config.html",
      width: 420,
      height: "auto",
      submitOnChange: true,
      tabs: [{navSelector: ".tabs", contentSelector: "form", initial: "basic"}]
    });
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  async close(options={}) {

    // If the config was closed without saving, reset the initial display of the Tile
    if ( !options.force ) {
      this.document.prepareData();
      this.document.object.refresh();
    }

    // Remove the preview tile and close
    const layer = this.object.layer;
    layer.preview?.removeChildren();
    return super.close(options);
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  getData(options) {
    const data = super.getData(options);
    const label = game.i18n.localize("DOCUMENT.Tile");
    data.submitText = game.i18n.format(this.object.id ? "DOCUMENT.Update" : "DOCUMENT.Create", {type: label});
    data.occlusionModes = Object.entries(CONST.TILE_OCCLUSION_MODES).reduce((obj, e) => {
      obj[e[1]] = game.i18n.localize(`TILE.OcclusionMode${e[0].titleCase()}`);
      return obj;
    }, {})
    return data;
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  async _onChangeInput(event) {

    // Handle form element updates
    const el = event.target;
    if ( (el.type === "color") && el.dataset.edit ) this._onChangeColorPicker(event);
    else if ( el.type === "range" ) this._onChangeRange(event);

    // Update preview object
    const fd = new FormDataExtended(this.form);
    for ( let [k, v] of Object.entries(fd.toObject()) ) {
      this.document.data[k] = v;
    }
    this.document.object.refresh();
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  async _updateObject(event, formData) {
    if ( this.document.id ) return this.document.update(formData);
    else return this.document.constructor.create(formData, {
      parent: this.document.parent,
      pack: this.document.pack
    });
  }
}
