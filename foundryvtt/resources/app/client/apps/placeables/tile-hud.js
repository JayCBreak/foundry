/**
 * An implementation of the PlaceableHUD base class which renders a heads-up-display interface for Tile objects.
 * @extends {BasePlaceableHUD}
 */
class TileHUD extends BasePlaceableHUD {

  /**
   * @inheritdoc
   * @type {Tile}
   */
  object = undefined;

  /** @inheritdoc */
	static get defaultOptions() {
	  return foundry.utils.mergeObject(super.defaultOptions, {
	    id: "tile-hud",
      template: "templates/hud/tile-hud.html"
    });
  }

	/* -------------------------------------------- */

  /** @inheritdoc */
  getData(options) {
    const d = this.object.data;
    const isVideo = this.object.isVideo;
    const isPlaying = isVideo && !this.object.sourceElement.paused;
    return foundry.utils.mergeObject(super.getData(options), {
      isVideo: isVideo,
      lockedClass: d.locked ? "active" : "",
      visibilityClass: d.hidden ? "active" : "",
      overheadClass: d.overhead ? "active" : "",
      underfootClass: !d.overhead ? "active" : "",
      videoIcon: isPlaying ? "fas fa-pause" : "fas fa-play",
      videoTitle: game.i18n.localize(isPlaying ? "HUD.TilePause" : "HUD.TilePlay")
    })
  }

	/* -------------------------------------------- */

  /** @inheritdoc */
  setPosition(options) {
	  let {x, y, width, height} = this.object.hitArea;
	  const c = 70;
	  const p = -10;
	  const position = {
	    width: width + (c * 2) + (p * 2),
      height: height + (p * 2),
      left: x + this.object.data.x - c - p,
      top: y + this.object.data.y - p
    };
    this.element.css(position);
  }

	/* -------------------------------------------- */

  /** @inheritdoc */
  _onClickControl(event) {
    super._onClickControl(event);
    if ( event.defaultPrevented ) return;
    const button = event.currentTarget;
    switch ( button.dataset.action ) {
      case "overhead":
        return this._onToggleOverhead(event, true);
      case "underfoot":
        return this._onToggleOverhead(event, false);
      case "video":
        return this._onControlVideo(event);
    }
  }

	/* -------------------------------------------- */

  /**
   * Handle toggling the overhead state of the Tile.
   * @private
   */
  _onToggleOverhead(event, overhead) {
    // Grab before release
    const documentName = this.object.document.documentName;

    // Toggle the underhead/overhead state
    const updates = this.layer.controlled.map(o => {
      return {_id: o.id, overhead: overhead};
    });

    // Update all objects
    return canvas.scene.updateEmbeddedDocuments(documentName, updates);
  }

	/* -------------------------------------------- */

  /**
   * Control video playback by toggling play or paused state for a video Tile.
   * @private
   */
  _onControlVideo(event) {
    const button = event.currentTarget;
    const isPaused = this.object.sourceElement.paused;

    // Update the icon
    const icon = button.children[0];
    if ( isPaused ) icon.classList.replace("fa-play", "fa-pause");
    else icon.classList.replace("fa-pause", "fa-play");

    // Trigger playback
    return this.object.document.update({"video.autoplay": false}, {diff: false, playVideo: isPaused});
  }
}
