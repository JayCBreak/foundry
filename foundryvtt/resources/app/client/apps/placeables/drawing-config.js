/**
 * @typedef {FormApplicationOptions} DrawingConfigOptions
 * @property {boolean} [configureDefault=false]  Configure the default drawing settings, instead of a specific Drawing
 */

/**
 * The Application responsible for configuring a single Drawing document within a parent Scene.
 * @extends {FormApplication}
 *
 * @param {Drawing} drawing               The Drawing object being configured
 * @param {DrawingConfigOptions} options  Additional application rendering options
 */
class DrawingConfig extends FormApplication {
  /**
   * @override
   * @returns {DrawingConfigOptions}
   */
	static get defaultOptions() {
	  return foundry.utils.mergeObject(super.defaultOptions, {
	    id: "drawing-config",
      classes: ["sheet"],
      template: "templates/scene/drawing-config.html",
      width: 480,
      height: "auto",
      configureDefault: false,
      tabs: [{navSelector: ".tabs", contentSelector: "form", initial: "position"}]
    });
  }

  /* -------------------------------------------- */

  /** @override */
  get title() {
    const title = this.options.configureDefault ? "DRAWING.ConfigDefaultTitle" : "DRAWING.ConfigTitle";
    return game.i18n.localize(title);
  }

  /* -------------------------------------------- */

  get id() {
    const name = this.options.id || `${this.object.documentName.toLowerCase()}-sheet`;
    return `${name}-${this.object.id}`;
  }

  /* -------------------------------------------- */

  /** @override */
  getData(options) {
    const author = game.users.get(this.object.data.author);

    // Submit text
    let submit;
    if ( this.options.configureDefault ) submit = "DRAWING.SubmitDefault";
    else submit = this.object.id ? "DRAWING.SubmitUpdate" : "DRAWING.SubmitCreate";

    // Return data
    return {
      author: author ? author.name : "",
      isDefault: this.options.configureDefault,
      fillTypes: this.constructor._getFillTypes(),
      fontFamilies: CONFIG.fontFamilies.reduce((obj, f) => {
        obj[f] = f;
        return obj;
      }, {}),
      object: this.object.toJSON(),
      options: this.options,
      submitText: submit
    }
  }

  /* -------------------------------------------- */

  /**
   * Get the names and labels of fill type choices which can be applied
   * @return {Object}
   * @private
   */
  static _getFillTypes() {
    return Object.entries(CONST.DRAWING_FILL_TYPES).reduce((obj, v) => {
      obj[v[1]] = `DRAWING.FillType${v[0].titleCase()}`;
      return obj;
    }, {});
  }

  /* -------------------------------------------- */

  /** @override */
  async _updateObject(event, formData) {
    if ( !this.object.isOwner ) throw new Error("You do not have the ability to configure this Drawing object.");

    // Configure the default Drawing settings
    if ( this.options.configureDefault ) {
      formData.author = game.user.id;
      const newDefault = new DrawingDocument(formData);
      return game.settings.set("core", DrawingsLayer.DEFAULT_CONFIG_SETTING, newDefault.toJSON());
    }

    // Create or update a Drawing
    if ( this.object.id ) return this.object.update(formData);
    return this.object.constructor.create(formData);
  }

  /* -------------------------------------------- */

  /** @override */
  async close(options) {
    await super.close(options);
    if ( this.preview ) {
      this.preview.removeChildren();
      this.preview = null;
    }
  }

  /* -------------------------------------------- */
  /*  Event Listeners and Handlers                */
  /* -------------------------------------------- */

  /** @override */
	activateListeners(html) {
	  super.activateListeners(html);
    html.find('button[name="resetDefault"]').click(this._onResetDefaults.bind(this));
  }

  /* -------------------------------------------- */

  /**
   * Reset the user Drawing configuration settings to their default values
   * @param {PointerEvent} event      The originating mouse-click event
   * @protected
   */
  _onResetDefaults(event) {
    event.preventDefault();
	  game.settings.set("core", DrawingsLayer.DEFAULT_CONFIG_SETTING, {});
	  const defaultValues = new foundry.data.DrawingData(canvas.drawings._getNewDrawingData({})).toJSON();
	  this.object.data.update(defaultValues);
    this.render();
  }
}
