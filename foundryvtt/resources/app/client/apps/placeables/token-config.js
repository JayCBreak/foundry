/**
 * The Application responsible for configuring a single Token document within a parent Scene.
 * Note that due to an oversight, this class does not inherit from {@link DocumentSheet} as it was intended to, and will
 * be changed in v10.
 * @extends {FormApplication}
 * @param {TokenDocument|Actor} object        The {@link TokenDocument} being configured, or the prototype token of an
 *                                            {@link Actor}.
 * @param {FormApplicationOptions} [options]    Application configuration options.
 */
class TokenConfig extends FormApplication {
  constructor(object, options) {
    super(object, options);
    this.token = this.object;
    if ( this.isPrototype ) {
      this.token = new PrototypeTokenDocument(this.object.data.token, {actor: this.object});
      this.options.sheetConfig = false;
    }
  }

	/* -------------------------------------------- */

  /** @inheritdoc */
	static get defaultOptions() {
	  return foundry.utils.mergeObject(super.defaultOptions, {
      classes: ["sheet", "token-sheet"],
      template: "templates/scene/token-config.html",
      width: 480,
      height: "auto",
      tabs: [
        {navSelector: '.tabs[data-group="main"]', contentSelector: "form", initial: "character"},
        {navSelector: '.tabs[data-group="light"]', contentSelector: '.tab[data-tab="light"]', initial: "basic"}
      ],
      sheetConfig: true
    });
  }

	/* -------------------------------------------- */

  /** @inheritdoc */
  get id() {
    return `token-config-${this.object.id}`;
  }

	/* -------------------------------------------- */

  /**
   * A convenience accessor to test whether we are configuring the prototype Token for an Actor.
   * @type {boolean}
   */
  get isPrototype() {
    return this.object instanceof Actor;
  }

  /* -------------------------------------------- */

  /**
   * Convenience access to the Actor document that this Token represents
   * @type {Actor}
   */
  get actor() {
    return this.isPrototype ? this.object : this.token.actor;
  }

	/* -------------------------------------------- */


  /** @inheritdoc */
  get title() {
    if ( this.isPrototype ) return `${game.i18n.localize("TOKEN.TitlePrototype")}: ${this.actor.name}`;
    return `${game.i18n.localize("TOKEN.Title")}: ${this.token.name}`;
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  async getData(options) {
    let hasAlternates = this.actor?.data.token.randomImg ?? false;
    const attributes = TokenDocument.implementation.getTrackedAttributes(this.actor?.data.data ?? {});
    const data = this.isPrototype ? this.actor.data.token : this.token.data;
    const gridUnits = (this.isPrototype || !canvas.ready) ? game.system.data.gridUnits : canvas.scene.data.gridUnits;
    return {
      cssClasses: [this.isPrototype ? "prototype" : null].filter(c => !!c).join(" "),
      isPrototype: this.isPrototype,
      hasAlternates: hasAlternates,
      alternateImages: hasAlternates ? await this._getAlternateTokenImages() : [],
      object: data,
      options: this.options,
      gridUnits: gridUnits ? `(${gridUnits})` : "",
      barAttributes: TokenDocument.implementation.getTrackedAttributeChoices(attributes),
      bar1: this.token.getBarAttribute?.("bar1"),
      bar2: this.token.getBarAttribute?.("bar2"),
      colorationTechniques: AdaptiveLightingShader.COLORATION_TECHNIQUES,
      displayModes: Object.entries(CONST.TOKEN_DISPLAY_MODES).reduce((obj, e) => {
        obj[e[1]] = game.i18n.localize(`TOKEN.DISPLAY_${e[0]}`);
        return obj;
      }, {}),
      actors: game.actors.reduce((actors, a) => {
        if ( !a.isOwner ) return actors;
        actors.push({'_id': a.id, 'name': a.name});
        return actors;
      }, []).sort((a, b) => a.name.localeCompare(b.name)),
      dispositions: Object.entries(CONST.TOKEN_DISPOSITIONS).reduce((obj, e) => {
        obj[e[1]] = game.i18n.localize(`TOKEN.${e[0]}`);
        return obj;
      }, {}),
      lightAnimations: Object.entries(CONFIG.Canvas.lightAnimations).reduce((obj, e) => {
        obj[e[0]] = game.i18n.localize(e[1].label);
        return obj;
      }, {"": game.i18n.localize("None")}),
      isGM: game.user.isGM
    };
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  render(force, options) {
    const canConfigure = game.user.isGM || this.actor?.isOwner;
    if ( !game.user.can("TOKEN_CONFIGURE") || !canConfigure ) {
      ui.notifications.warn("You do not have permission to configure this Token!");
      return this;
    }
    return super.render(force, options);
  }

  /* --------------------------------------------- */

  /** @inheritdoc */
  async _renderInner(...args) {
    await loadTemplates([
      "templates/scene/parts/token-lighting.html",
      "templates/scene/parts/token-vision.html",
      "templates/scene/parts/token-resources.html"
    ]);
    return super._renderInner(...args);
  }

  /* -------------------------------------------- */

  /**
   * Get an Object of image paths and filenames to display in the Token sheet
   * @return {Promise}
   * @private
   */
  async _getAlternateTokenImages() {
    const images = await this.actor.getTokenImages();
    return images.reduce((obj, i) => {
      obj[i] = i.split("/").pop();
      return obj;
    }, {});
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  _getHeaderButtons() {
    const buttons = super._getHeaderButtons();
    if ( this.options.sheetConfig && this.object.isOwner ) {
      buttons.unshift({
        label: "Sheet",
        class: "configure-sheet",
        icon: "fas fa-cog",
        onclick: ev => this._onConfigureSheet(ev)
      });
    }
    return buttons;
  }

  /* -------------------------------------------- */

  /**
   * Shim for {@link DocumentSheet#_onConfigureSheet} that will be replaced in v10 when this class subclasses it.
   * @private
   */
  _onConfigureSheet(event) {
    event.preventDefault();
    new DocumentSheetConfig(this.object, {
      top: this.position.top + 40,
      left: this.position.left + ((this.position.width - TokenConfig.defaultOptions.width) / 2)
    }).render(true);
  }

  /* -------------------------------------------- */
  /*  Event Listeners and Handlers                */
  /* -------------------------------------------- */

  /** @inheritdoc */
  activateListeners(html) {
    super.activateListeners(html);
    html.find(".bar-attribute").change(this._onBarChange.bind(this));
    html.find(".alternate-images").change(ev => ev.target.form.img.value = ev.target.value);
    html.find('button.assign-token').click(this._onAssignToken.bind(this));
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  async _updateObject(event, formData) {
    return this.token.update(formData);
  }

  /* -------------------------------------------- */

  /**
   * Handle Token assignment requests to update the default prototype Token
   * @param {MouseEvent} event  The left-click event on the assign token button
   * @private
   */
  async _onAssignToken(event) {
    event.preventDefault();

    // Get controlled Token data
    let tokens = canvas.ready ? canvas.tokens.controlled : [];
    if ( tokens.length !== 1 ) {
      ui.notifications.warn("TOKEN.AssignWarn", {localize: true});
      return;
    }
    const token = tokens.pop().data.toJSON();
    token.tokenId = token.x = token.y = null;

    // Update the prototype token for the actor using the existing Token instance
    await this.actor.update({token: token}, {diff: false, recursive: false, noHook: true});
    ui.notifications.info(game.i18n.format("TOKEN.AssignSuccess", {name: this.actor.name}));
    return this.close();
  }

  /* -------------------------------------------- */

  /**
   * Handle changing the attribute bar in the drop-down selector to update the default current and max value
   * @private
   */
  async _onBarChange(ev) {
    const form = ev.target.form;
    const attr = this.token.getBarAttribute("", {alternative: ev.target.value});
    const bar = ev.target.name.split(".").shift();
    form.querySelector(`input.${bar}-value`).value = attr !== null ? attr.value : "";
    form.querySelector(`input.${bar}-max`).value = ((attr !== null) && (attr.type === "bar")) ? attr.max : "";
  }
}

/**
 * A sheet that alters the values of the default Token configuration used when new Token documents are created.
 * @extends {FormApplication}
 */
class DefaultTokenConfig extends TokenConfig {
  constructor(object, options) {
    super(object, options);
    const setting = game.settings.get("core", DefaultTokenConfig.SETTING);
    this.data = new foundry.data.TokenData(setting);
    this.object = new TokenDocument(this.data, { actor: null });
    this.token = this.object;
  }

  /**
   * The named world setting that stores the default Token configuration
   * @type {string}
   */
  static SETTING = "defaultToken";

  /** @inheritdoc */
	static get defaultOptions() {
	  return foundry.utils.mergeObject(super.defaultOptions, {
      template: "templates/scene/default-token-config.html",
      sheetConfig: false
    });
  }

  /* --------------------------------------------- */

  /** @inheritdoc */
  get id() {
    return "default-token-config";
  }

  /* --------------------------------------------- */

  /** @inheritdoc */
  get title() {
    return game.i18n.localize("SETTINGS.DefaultTokenN");
  }

  /* -------------------------------------------- */

  /** @override */
  async getData(options) {
    return foundry.utils.mergeObject(await super.getData(options), {
      object: this.data.toObject(),
      isDefault: true,
      barAttributes: TokenDocument.implementation.getTrackedAttributeChoices(),
      bar1: this.data.bar1,
      bar2: this.data.bar2
    });
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  _getSubmitData(updateData = {}) {
    const formData = foundry.utils.expandObject(super._getSubmitData(updateData));
    formData.light.color = formData.light.color || undefined;
    formData.bar1.attribute = formData.bar1.attribute || null;
    formData.bar2.attribute = formData.bar2.attribute || null;
    return formData;
  }

  /* -------------------------------------------- */

  /** @override */
  async _updateObject(event, formData) {
    try {
      this.data.update(formData);
    } catch(err) {
      Hooks.onError("DefaultTokenConfig#_updateObject", err, {notify: "error"});
    }
    const defaults = new foundry.data.TokenData();
    const delta = foundry.utils.diffObject(defaults.toObject(false), this.data.toObject(false));
    return game.settings.set("core", DefaultTokenConfig.SETTING, delta);
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  activateListeners(html) {
    super.activateListeners(html);
    html.find('button[data-action="reset"]').click(this.reset.bind(this));
  }

  /* -------------------------------------------- */

  /**
   * Reset the form to default values
   * @returns {Promise<void>}
   */
  async reset() {
    this.data = new foundry.data.TokenData({});
    this.object = new TokenDocument(this.data, { actor: null });
    this.token = this.object;
    this.render();
  }

  /* --------------------------------------------- */

  /** @inheritdoc */
  async _onBarChange() {}
}
