/**
 * The Application responsible for configuring a single AmbientLight document within a parent Scene.
 * @extends {DocumentSheet}
 * @param {AmbientLight} light              The AmbientLight object for which settings are being configured
 * @param {DocumentSheetOptions} [options]  Additional application configuration options
 */
class AmbientLightConfig extends DocumentSheet {

  /** @inheritdoc */
	static get defaultOptions() {
    return foundry.utils.mergeObject(super.defaultOptions, {
      id: "ambient-light-config",
      classes: ["sheet", "ambient-light-config"],
      title: "LIGHT.ConfigTitle",
      template: "templates/scene/ambient-light-config.html",
      width: 480,
      height: "auto",
      tabs: [{navSelector: ".tabs", contentSelector: "form", initial: "basic"}]
    });
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  getData(options) {
    const data = super.getData(options);
    const animationTypes = {"": "None"};
    for ( let [k, v] of Object.entries(CONFIG.Canvas.lightAnimations) ) {
      animationTypes[k] = v.label;
    }
    return foundry.utils.mergeObject(data, {
      isAdvanced: this._tabs[0].active === "advanced",
      colorationTechniques: AdaptiveLightingShader.COLORATION_TECHNIQUES,
      lightAnimations: animationTypes,
      gridUnits: canvas.scene.data.gridUnits,
      submitText: game.i18n.localize(this.options["preview"] ? "LIGHT.Create" : "LIGHT.Update")
    });
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  async close(options={}) {
    if ( !options.force ) { // Reset if closing the form without submitting
      this.document.prepareData();
      this._refresh();
    }
    return super.close(options);
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  activateListeners(html) {
    html.find('button[type="reset"]').click(this._onResetForm.bind(this));
    return super.activateListeners(html);
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  async _onChangeInput(event) {
    await super._onChangeInput(event);
    const previewData = this._getSubmitData();
    foundry.utils.mergeObject(this.document.data, previewData, {inplace: true});
    this._refresh();
  }

  /* -------------------------------------------- */

  /**
   * Reset the values of advanced attributes to their default state.
   * @param {PointerEvent} event    The originating click event
   * @private
   */
  _onResetForm(event) {
    event.preventDefault();
    const defaults = foundry.utils.flattenObject(new foundry.data.AmbientLightData().toObject());
    const reset = new Set([
      "walls", "vision",
      ...["coloration", "contrast", "gradual", "luminosity", "saturation", "shadows"].map(k => `config.${k}`)
    ]);
    for ( const k in defaults ) {
      if ( !reset.has(k) ) delete defaults[k];
    }
    foundry.utils.mergeObject(this.document.data, foundry.utils.expandObject(defaults), {inplace: true});
    this._refresh();
    this.render();
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  _onChangeTab(event, tabs, active) {
    super._onChangeTab(event, tabs, active);
    this.element.find('button[type="reset"]').toggleClass("hidden", active !== "advanced");
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  async _updateObject(event, formData) {
    this.object.data.reset();
    if ( this.object.id ) return this.object.update(formData);
    return this.object.constructor.create(formData, {parent: canvas.scene});
  }

  /* -------------------------------------------- */

  /**
   * Refresh the display of the AmbientLight object
   * @private
   */
  _refresh() {
    if ( !this.document.object ) return;
    this.document.object.updateSource();
    this.document.object.refresh();
  }
}

/**
 * @deprecated since v9
 * @ignore
 */
class LightConfig extends AmbientLightConfig {
  constructor(...args) {
    super(...args);
    console.warn(`You are using the LightConfig class which is deprecated in favor of the AmbientLightConfig class and will be removed in V10.`)
  }
}
