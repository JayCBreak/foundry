/**
 * A simple main menu application
 * @type {Application}
 */
class MainMenu extends Application {
  /** @inheritdoc */
	static get defaultOptions() {
	  const options = super.defaultOptions;
	  options.id = "menu";
	  options.template = "templates/hud/menu.html";
	  options.popOut = false;
	  return options;
  }

  /* ----------------------------------------- */

  /**
   * The structure of menu items
   * @return {Object}
   */
  get items() {
    return {
       reload: {
        label: "MENU.Reload",
        icon: '<i class="fas fa-redo"></i>',
        enabled: true,
        onClick: () => window.location.reload()
      },
      logout: {
        label: "MENU.Logout",
        icon: '<i class="fas fa-user"></i>',
        enabled: true,
        onClick: () => game.logOut()
      },
      players: {
        label: "MENU.Players",
        icon: '<i class="fas fa-users"></i>',
        enabled: game.user.isGM && !game.data.options.demo,
        onClick: () => window.location.href = "./players"
      },
      world: {
        label: "MENU.Setup",
        icon: '<i class="fas fa-globe"></i>',
        enabled: game.user.hasRole("GAMEMASTER")  && !game.data.options.demo,
        onClick: () => game.shutDown()
      }
    }
  }

	/* -------------------------------------------- */

  /** @inheritdoc */
  getData(options) {
    return {
      items: this.items
    }
  }

  /* ----------------------------------------- */

  /** @inheritdoc */
  activateListeners(html) {
    for ( let [k, v] of Object.entries(this.items) ) {
      html.find('.menu-'+k).click(ev => v.onClick());
    }
  }

  /* ----------------------------------------- */

  /**
   * Toggle display of the menu (or render it in the first place)
   */
  toggle() {
    let menu = this.element;
    if ( !menu.length ) this.render(true);
    else menu.slideToggle(150);
  }
}
