/**
 * @typedef {Object} SceneControlTool
 * @property {string} name
 * @property {string} title
 * @property {string} icon
 * @property {boolean} visible
 * @property {boolean} toggle
 * @property {boolean} active
 * @property {boolean} button
 * @property {function} onClick
 */

/**
 * @typedef {Object} SceneControl
 * @property {string} name
 * @property {string} title
 * @property {string} layer
 * @property {string} icon
 * @property {boolean} visible
 * @property {SceneControlTool[]} tools
 * @property {string} activeTool
 */

/**
 * Scene controls navigation menu
 * @extends {Application}
 */
class SceneControls extends Application {
	constructor(options) {
	  super(options);

    /**
     * The name of the active Scene Control toolset
     * @type {string}
     */
	  this.activeControl = "token";

    /**
     * The Array of Scene Control buttons which are currently rendered
     * @type {SceneControl[]}
     */
	  this.controls = this._getControlButtons();
	}

	/* -------------------------------------------- */
  /*  Properties                                  */
	/* -------------------------------------------- */

  /** @inheritdoc */
	static get defaultOptions() {
	  return foundry.utils.mergeObject(super.defaultOptions, {
	    width: 100,
      id: "controls",
      template: "templates/hud/controls.html",
      popOut: false
    });
  }

  /* -------------------------------------------- */

  /**
   * Return the active control set
   * @type {SceneControl|null}
   */
  get control() {
	  if ( !this.controls ) return null;
	  return this.controls.find(c => c.name === this.activeControl) || null;
  }

  /* -------------------------------------------- */

  /**
   * Return the name of the active tool within the active control set
   * @type {string|null}
   */
	get activeTool() {
	  const control = this.control;
	  return control ? control.activeTool : null;
  }

  /* -------------------------------------------- */

  /**
   * Return the actively controlled tool
   * @type {SceneControlTool|null}
   */
  get tool() {
    const control = this.control;
    if ( !control ) return null;
    const tool = control.tools.find(t => t.name === control.activeTool);
    return tool || null;
  }

  /* -------------------------------------------- */

  /**
   * A convenience reference for whether the currently active tool is a Ruler
   * @type {boolean}
   */
  get isRuler() {
	  return this.activeTool === "ruler";
  }

	/* -------------------------------------------- */
  /*  Methods                                     */
	/* -------------------------------------------- */

  /**
   * Initialize the Scene Controls by obtaining the set of control buttons and rendering the HTML
   * @param {object} options      Options which modify how the controls UI is initialized
   * @param {string} [options.control]      An optional control set to set as active
   * @param {string} [options.layer]        An optional layer name to target as the active control
   * @param {string} [options.tool]         A specific named tool to set as active for the palette
   */
  initialize({control, layer, tool}={}) {
    const tools = this.controls.reduce((obj, t) => {
      obj[t.name] = t.activeTool;
      return obj;
    }, {});

    // Set the new control
    if ( control ) this.activeControl = control;
    else if ( layer && this.controls ) {
      const control = this.controls.find(c => c.layer === layer);
      if ( control ) this.activeControl = control.name;
    }

    // Update the control buttons
    this.controls = this._getControlButtons();
    if ( tool ) tools[this.activeControl] = tool;
    for ( let c of this.controls ) {
      c.activeTool = tools[c.name];
    }
    this.render(true);
  }

	/* -------------------------------------------- */

  /** @inheritdoc */
	getData(options) {
	  const isActive = !!canvas.scene;

	  // Filter to control tool sets which can be displayed
    let controls = this.controls.filter(s => s.visible !== false).map(s => {
      s = foundry.utils.deepClone(s);

      // Add styling rules
      s.css = isActive && (this.activeControl === s.name) ? "active" : "";

      // Prepare contained tools
      s.tools = s.tools.filter(t => t.visible !== false).map(t => {
        let active = isActive && ((s.activeTool === t.name) || (t.toggle && t.active));
        t.css = [
          t.toggle ? "toggle" : null,
          active ? "active" : null
        ].filter(t => !!t).join(" ");
        return t;
      });
      return s;
    });

    // Return data for rendering
	  return {
	    active: isActive,
      cssClass: isActive ? "" : "disabled",
      controls: controls.filter(s => s.tools.length)
    };
  }


	/* -------------------------------------------- */
  /*  Event Listeners and Handlers                */
	/* -------------------------------------------- */

  /** @inheritdoc */
  activateListeners(html) {
    html.find('.scene-control').click(this._onClickLayer.bind(this));
    html.find('.control-tool').click(this._onClickTool.bind(this));
  }

	/* -------------------------------------------- */

  /**
   * Handle click events on a Control set
   * @param {Event} event   A click event on a tool control
   * @private
   */
  _onClickLayer(event) {
    event.preventDefault();
    if ( !canvas.ready ) return;
    const li = event.currentTarget;
    const controlName = li.dataset.control;
    if ( this.activeControl === controlName ) return;
    this.activeControl = controlName;
    const control = this.controls.find(c => c.name === controlName);
    if ( control ) canvas[control.layer].activate();
  }

	/* -------------------------------------------- */

  /**
   * Handle click events on Tool controls
   * @param {Event} event   A click event on a tool control
   * @private
   */
  _onClickTool(event) {
    event.preventDefault();
    if ( !canvas.ready ) return;
    const li = event.currentTarget;
    const control = this.control;
    const toolName = li.dataset.tool;
    const tool = control.tools.find(t => t.name === toolName);

    // Handle Toggles
    if ( tool.toggle ) {
      tool.active = !tool.active;
      if ( tool.onClick instanceof Function ) tool.onClick(tool.active);
    }

    // Handle Buttons
    else if ( tool.button ) {
      if ( tool.onClick instanceof Function ) tool.onClick();
    }

    // Handle Tools
    else {
      control.activeTool = toolName;
      if ( tool.onClick instanceof Function ) tool.onClick();
    }

    // Render the controls
    this.render();
  }

	/* -------------------------------------------- */

  /**
   * Get the set of Control sets and tools that are rendered as the Scene Controls.
   * These controls may be extended using the "getSceneControlButtons" Hook.
   * @return {SceneControl[]}
   * @private
   */
	_getControlButtons() {
    const controls = [];
    const isGM = game.user.isGM;

    // Token Controls
    controls.push({
      name: "token",
      title: "CONTROLS.GroupToken",
      layer: "tokens",
      icon: "fas fa-user-alt",
      tools: [
        {
          name: "select",
          title: "CONTROLS.BasicSelect",
          icon: "fas fa-expand"
        },
        {
          name: "target",
          title: "CONTROLS.TargetSelect",
          icon: "fas fa-bullseye"
        },
        {
          name: "ruler",
          title: "CONTROLS.BasicMeasure",
          icon: "fas fa-ruler"
        }
      ],
      activeTool: "select"
    });

    // Measurement Layer Tools
    controls.push({
      name: "measure",
      title: "CONTROLS.GroupMeasure",
      layer: "templates",
      icon: "fas fa-ruler-combined",
      visible: game.user.can("TEMPLATE_CREATE"),
      tools: [
        {
          name: "circle",
          title: "CONTROLS.MeasureCircle",
          icon: "far fa-circle"
        },
        {
          name: "cone",
          title: "CONTROLS.MeasureCone",
          icon: "fas fa-angle-left"
        },
        {
          name: "rect",
          title: "CONTROLS.MeasureRect",
          icon: "far fa-square"
        },
        {
          name: "ray",
          title: "CONTROLS.MeasureRay",
          icon: "fas fa-arrows-alt-v"
        },
        {
          name: "clear",
          title: "CONTROLS.MeasureClear",
          icon: "fas fa-trash",
          visible: isGM,
          onClick: () => canvas.templates.deleteAll(),
          button: true
        }
      ],
      activeTool: "circle"
    });

    // Tiles Layer
    controls.push({
      name: "tiles",
      title: "CONTROLS.GroupTile",
      layer: "background",
      icon: "fas fa-cubes",
      visible: isGM,
      tools: [
        {
          name: "select",
          title: "CONTROLS.TileSelect",
          icon: "fas fa-expand"
        },
        {
          name: "tile",
          title: "CONTROLS.TilePlace",
          icon: "fas fa-cube"
        },
        {
          name: "browse",
          title: "CONTROLS.TileBrowser",
          icon: "fas fa-folder",
          button: true,
          onClick: () => {
            new FilePicker({
              type: "imagevideo",
              displayMode: "tiles",
              tileSize: true
            }).render(true);
          }
        },
        {
          name: "foreground",
          title: "CONTROLS.TileForeground",
          icon: "fas fa-home",
          toggle: true,
          active: canvas.foreground?._active ?? false,
          onClick: toggled => {
            canvas[toggled ? "foreground" : "background"].activate();
          }
        }
      ],
      activeTool: "select"
    });

    // Drawing Tools
    controls.push({
      name: "drawings",
      title: "CONTROLS.GroupDrawing",
      layer: "drawings",
      icon: "fas fa-pencil-alt",
      visible: game.user.can("DRAWING_CREATE"),
      tools: [
        {
          name: "select",
          title: "CONTROLS.DrawingSelect",
          icon: "fas fa-expand"
        },
        {
          name: "rect",
          title: "CONTROLS.DrawingRect",
          icon: "fas fa-square"
        },
        {
          name: "ellipse",
          title: "CONTROLS.DrawingEllipse",
          icon: "fas fa-circle"
        },
        {
          name: "polygon",
          title: "CONTROLS.DrawingPoly",
          icon: "fas fa-draw-polygon"
        },
        {
          name: "freehand",
          title: "CONTROLS.DrawingFree",
          icon: "fas fa-signature"
        },
        {
          name: "text",
          title: "CONTROLS.DrawingText",
          icon: "fas fa-font"
        },
        {
          name: "configure",
          title: "CONTROLS.DrawingConfig",
          icon: "fas fa-cog",
          onClick: () => canvas.drawings.configureDefault(),
          button: true
        },
        {
          name: "clear",
          title: "CONTROLS.DrawingClear",
          icon: "fas fa-trash",
          visible: isGM,
          onClick: () => canvas.drawings.deleteAll(),
          button: true
        }
      ],
      activeTool: "select"
    });

    // Walls Layer Tools
    controls.push({
      name: "walls",
      title: "CONTROLS.GroupWall",
      layer: "walls",
      icon: "fas fa-university",
      visible: isGM,
      tools: [
        {
          name: "select",
          title: "CONTROLS.WallSelect",
          icon: "fas fa-expand"
        },
        {
          name: "walls",
          title: "CONTROLS.WallDraw",
          icon: "fas fa-bars"
        },
        {
          name: "terrain",
          title: "CONTROLS.WallTerrain",
          icon: "fas fa-mountain"
        },
        {
          name: "invisible",
          title: "CONTROLS.WallInvisible",
          icon: "fas fa-eye-slash"
        },
        {
          name: "ethereal",
          title: "CONTROLS.WallEthereal",
          icon: "fas fa-mask"
        },
        {
          name: "doors",
          title: "CONTROLS.WallDoors",
          icon: "fas fa-door-open"
        },
        {
          name: "secret",
          title: "CONTROLS.WallSecret",
          icon: "fas fa-user-secret"
        },
        {
          name: "clone",
          title: "CONTROLS.WallClone",
          icon: "far fa-clone"
        },
        {
          name: "snap",
          title: "CONTROLS.WallSnap",
          icon: "fas fa-plus",
          toggle: true,
          active: canvas.walls?._forceSnap || false,
          onClick: toggled => canvas.walls._forceSnap = toggled
        },
        {
          name: "clear",
          title: "CONTROLS.WallClear",
          icon: "fas fa-trash",
          onClick: () => canvas.walls.deleteAll(),
          button: true
        }
      ],
      activeTool: "walls"
    });

    // Lighting Layer Tools
    controls.push({
      name: "lighting",
      title: "CONTROLS.GroupLighting",
      layer: "lighting",
      icon: "far fa-lightbulb",
      visible: isGM,
      tools: [
        {
          name: "light",
          title: "CONTROLS.LightDraw",
          icon: "fas fa-lightbulb"
        },
        {
          name: "day",
          title: "CONTROLS.LightDay",
          icon: "fas fa-sun",
          onClick: () => canvas.scene.update({darkness: 0.0}, {animateDarkness: 10000}),
          button: true
        },
        {
          name: "night",
          title: "CONTROLS.LightNight",
          icon: "fas fa-moon",
          onClick: () => canvas.scene.update({darkness: 1.0}, {animateDarkness: 10000}),
          button: true
        },
        {
          name: "reset",
          title: "CONTROLS.LightReset",
          icon: "fas fa-cloud",
          onClick: () => {
            new Dialog({
              title: game.i18n.localize("CONTROLS.FOWResetTitle"),
              content: `<p>${game.i18n.localize("CONTROLS.FOWResetDesc")}</p>`,
              buttons: {
                yes: {
                  icon: '<i class="fas fa-check"></i>',
                  label: "Yes",
                  callback: () => canvas.sight.resetFog()
                },
                no: {
                  icon: '<i class="fas fa-times"></i>',
                  label: "No"
                }
              }
            }).render(true);
          },
          button: true
        },
        {
          name: "clear",
          title: "CONTROLS.LightClear",
          icon: "fas fa-trash",
          onClick: () => canvas.lighting.deleteAll(),
          button: true
        }
      ],
      activeTool: "light"
    });

    // Sounds Layer Tools
    controls.push({
      name: "sounds",
      title: "CONTROLS.GroupSound",
      layer: "sounds",
      icon: "fas fa-music",
      visible: isGM,
      tools: [
        {
          name: "sound",
          title: "CONTROLS.SoundDraw",
          icon: "fas fa-volume-up"
        },
        {
          name: "preview",
          title: "CONTROLS.SoundPreview",
          icon: "fas fa-headphones",
          toggle: true,
          active: canvas.sounds?.livePreview ?? false,
          onClick: toggled => {
            canvas.sounds.livePreview = toggled;
            canvas.sounds.refresh();
          }
        },
        {
          name: "clear",
          title: "CONTROLS.SoundClear",
          icon: "fas fa-trash",
          onClick: () => canvas.sounds.deleteAll(),
          button: true
        }
      ],
      activeTool: "sound"
    });

    // Notes Layer Tools
    controls.push({
      name: "notes",
      title: "CONTROLS.GroupNotes",
      layer: "notes",
      icon: "fas fa-bookmark",
      tools: [
        {
          name: "select",
          title: "CONTROLS.NoteSelect",
          icon: "fas fa-expand"
        },
        {
          name: "toggle",
          title: "CONTROLS.NoteToggle",
          icon: "fas fa-map-pin",
          toggle: true,
          active: game.settings.get("core", NotesLayer.TOGGLE_SETTING),
          onClick: toggled => game.settings.set("core", NotesLayer.TOGGLE_SETTING, toggled)
        },
        {
          name: "clear",
          title: "CONTROLS.NoteClear",
          icon: "fas fa-trash",
          visible: isGM,
          onClick: () => canvas.notes.deleteAll(),
          button: true
        }
      ],
      activeTool: 'select'
    });

    // Pass the Scene Controls to a hook function to allow overrides or changes
    /**
     * A hook event that fires when the Scene controls are initialized.
     * @function getSceneControlButtons
     * @memberof hookEvents
     * @param {SceneControl[]} controls The SceneControl configurations
     */
    Hooks.callAll(`getSceneControlButtons`, controls);
    return controls;
  }
}
