/**
 * The Chat Bubble Class
 * This application displays a temporary message sent from a particular Token in the active Scene.
 * The message is displayed on the HUD layer just above the Token.
 */
class ChatBubbles {
  constructor() {
    this.template = "templates/hud/chat-bubble.html";

    /**
     * Track active Chat Bubbles
     * @type {Object}
     */
    this.bubbles = {};

    /**
     * Track which Token was most recently panned to highlight
     * Use this to avoid repeat panning
     * @type {Token}
     * @private
     */
    this._panned = null;
  }

	/* -------------------------------------------- */

  /**
   * A reference to the chat bubbles HTML container in which rendered bubbles should live
   * @return {jQuery}
   */
  get container() {
    return $("#chat-bubbles");
  }

	/* -------------------------------------------- */

  /**
   * Speak a message as a particular Token, displaying it as a chat bubble
   * @param {Token} token       The speaking Token
   * @param {string} message    The spoken message text
   * @param {boolean} emote     Whether to style the speech bubble as an emote
   * @returns {Promise<void>}   A Promise which resolves once the chat bubble has been created
   */
  async say(token, message, {emote=false}={}) {
    if ( !token || !message ) return;
    let allowBubbles = game.settings.get("core", "chatBubbles");
    if ( !allowBubbles ) return;
    const panToSpeaker = game.settings.get("core", "chatBubblesPan");

    // Clear any existing bubble for the speaker
    await this._clearBubble(token);

    // Create the HTML and call the chatBubble hook
    let html = $(await this._renderHTML({token, message, emote}));
    /**
     * A hook event that fires when a chat bubble is rendered.
     * @function chatBubble
     * @memberof hookEvents
     * @param {Token} token           The speaking token
     * @param {jQuery} html           The HTML of the chat bubble
     * @param {string} message        The spoken message text
     * @param {object} options
     * @param {boolean} options.emote Whether to style the speech bubble as an emote
     */
    const allowed = Hooks.call("chatBubble", token, html, message, {emote});
    if ( allowed === false ) return;

    // Set initial dimensions
    let dimensions = this._getMessageDimensions(message);
    this._setPosition(token, html, dimensions);

    // Append to DOM
    this.container.append(html);

    // Pan to the speaker
    if ( panToSpeaker && (this._panned !== token) ) {
      canvas.animatePan({x: token.x, y: token.y, scale: Math.max(1, canvas.stage.scale.x), duration: 1000});
      this._panned = token;
    }

    // Get animation duration and settings
    const duration = this._getDuration(html);
    const scroll = dimensions.unconstrained - dimensions.height;

    // Animate the bubble
    html.fadeIn(250, () => {
      if ( scroll > 0 ) {
        html.find(".bubble-content").animate({ top: -1 * scroll }, duration - 1000, 'linear');
      }
      setTimeout(() => html.fadeOut(250, () => html.remove()), duration);
    });
  }

	/* -------------------------------------------- */

  /**
   * Clear any existing chat bubble for a certain Token
   * @param {Token} token
   * @private
   */
  async _clearBubble(token) {
    let existing = $(`.chat-bubble[data-token-id="${token.id}"]`);
    if ( !existing.length ) return;
    return new Promise(resolve => {
      existing.fadeOut(100, () => {
        existing.remove();
        resolve();
      });
    })
  }

	/* -------------------------------------------- */

  /**
   * Render the HTML template for the chat bubble
   * @param {Object} data         Template data
   * @return {Promise<string>}    The rendered HTML
   * @private
   */
  async _renderHTML(data) {
    data.cssClasses = [
      data.emote ? "emote" : null
    ].filter(c => c !== null).join(" ");
    return renderTemplate(this.template, data);
  }

	/* -------------------------------------------- */

  /**
   * Before displaying the chat message, determine it's constrained and unconstrained dimensions
   * @param {string} message    The message content
   * @return {Object}           The rendered message dimensions
   * @private
   */
  _getMessageDimensions(message) {
    let div = $(`<div class="chat-bubble" style="visibility:hidden">${message}</div>`);
    $('body').append(div);
    let dims = {
      width: div[0].clientWidth + 8,
      height: div[0].clientHeight
    };
    div.css({maxHeight: "none"});
    dims.unconstrained = div[0].clientHeight;
    div.remove();
    return dims;
  }

	/* -------------------------------------------- */

  /**
   * Assign styling parameters to the chat bubble, toggling either a left or right display (randomly)
   * @private
   */
  _setPosition(token, html, dimensions) {
    let cls = Math.random() > 0.5 ? "left" : "right";
    html.addClass(cls);
    const pos = {
      height: dimensions.height,
      width: dimensions.width,
      top: token.y - dimensions.height - 8
    };
    if ( cls === "right" ) pos.left = token.x - (dimensions.width - token.w);
    else pos.left = token.x;
    html.css(pos);
  }

  /* -------------------------------------------- */

  /**
  * Determine the length of time for which to display a chat bubble.
  * Research suggests that average reading speed is 200 words per minute.
  * Since these are short-form messages, we multiply reading speed by 1.5.
  * Clamp the result between 1 second (minimum) and 20 seconds (maximum)
  * @param {jQuery} html     The HTML message
  * @returns {number}        The number of milliseconds for which to display the message
  */
  _getDuration(html) {
    let words = html.text().split(" ").map(w => w.trim()).length;
    let ms = (words * 60 * 1000) / 300;
    return Math.clamped(1000, ms, 20000);
  }
}







