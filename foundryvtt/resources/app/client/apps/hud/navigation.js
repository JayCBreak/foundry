/**
 * The UI element which displays the Scene documents which are currently enabled for quick navigation.
 * @extends {Application}
 */
class SceneNavigation extends Application {
	constructor(options) {
	  super(options);
	  game.scenes.apps.push(this);

    /**
     * Navigation collapsed state
     * @type {boolean}
     */
    this._collapsed = false;
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
	static get defaultOptions() {
    return foundry.utils.mergeObject(super.defaultOptions, {
      id: "navigation",
      template: "templates/hud/navigation.html",
      popOut: false,
      dragDrop: [{dragSelector: ".scene"}]
    });
  }

  /* -------------------------------------------- */

  /**
   * Return an Array of Scenes which are displayed in the Navigation bar
   * @return {Scene[]}
   */
  get scenes() {
    const scenes = game.scenes.filter(s => {
      return (s.data.navigation && s.visible) || s.active || s.isView;
    });
    scenes.sort((a, b) => a.data.navOrder - b.data.navOrder);
    return scenes;
  }

	/* -------------------------------------------- */
  /*  Application Rendering
	/* -------------------------------------------- */

  /** @inheritdoc */
  render(force, context={}) {
    let { renderContext, renderData} = context;
    if ( renderContext ) {
      const events = ["createScene", "updateScene", "deleteScene"];
      if ( !events.includes(renderContext) ) return this;
      const updateKeys = ["name", "permission", "permission.default", "active", "navigation", "navName", "navOrder"];
      if ( renderContext === "updateScene" && !updateKeys.some(k => renderData.hasOwnProperty(k)) ) return this;
    }
    return super.render(force, context);
  }

	/* -------------------------------------------- */

  /** @inheritdoc */
  async _render(force, options) {
    await super._render(force, options);
    const loading = document.getElementById("loading");
    const nav = this.element[0];
    loading.style.top = `${nav.offsetTop + nav.offsetHeight}px`;
  }

	/* -------------------------------------------- */

  /** @inheritdoc */
	getData(options) {

    // Modify Scene data
    const scenes = this.scenes.map(scene => {
      let data = scene.data.toObject(false);
      let users = game.users.filter(u => u.active && (u.viewedScene === scene.id));
      data.name = TextEditor.truncateText(data.navName || data.name, {maxLength: 32});
	    data.users = users.map(u => { return {letter: u.name[0], color: u.data.color} });
	    data.visible = (game.user.isGM || scene.isOwner || scene.active);
	    data.css = [
	      scene.isView ? "view" : null,
        scene.active ? "active" : null,
        data.permission.default === 0 ? "gm" : null
      ].filter(c => !!c).join(" ");
	    return data;
    });

    // Return data for rendering
    return {
      collapsed: this._collapsed,
      scenes: scenes
    }
  }

	/* -------------------------------------------- */

  /**
   * A hook event that fires when the SceneNavigation menu is expanded or collapsed.
   * @function collapseSceneNavigation
   * @memberof hookEvents
   * @param {SceneNavigation} sceneNavigation The SceneNavigation application
   * @param {boolean} collapsed               Whether the SceneNavigation is now collapsed or not
   */

  /* -------------------------------------------- */

  /**
   * Expand the SceneNavigation menu, sliding it down if it is currently collapsed
   */
  expand() {
    if ( !this._collapsed ) return true;
    const nav = this.element;
    const icon = nav.find("#nav-toggle i.fas");
    const ul = nav.children("#scene-list");
    return new Promise(resolve => {
      ul.slideDown(200, () => {
        nav.removeClass("collapsed");
        icon.removeClass("fa-caret-down").addClass("fa-caret-up");
        this._collapsed = false;
        Hooks.callAll("collapseSceneNavigation", this, this._collapsed);
        return resolve(true);
      });
    });
  }

	/* -------------------------------------------- */

  /**
   * Collapse the SceneNavigation menu, sliding it up if it is currently expanded
   * @return {Promise<boolean>}
   */
  async collapse() {
    if ( this._collapsed ) return true;
    const nav = this.element;
    const icon = nav.find("#nav-toggle i.fas");
    const ul = nav.children("#scene-list");
    return new Promise(resolve => {
      ul.slideUp(200, () => {
        nav.addClass("collapsed");
        icon.removeClass("fa-caret-up").addClass("fa-caret-down");
        this._collapsed = true;
        Hooks.callAll("collapseSceneNavigation", this, this._collapsed);
        return resolve(true);
      });
    });
  }

	/* -------------------------------------------- */
  /*  Event Listeners and Handlers
	/* -------------------------------------------- */

  /** @inheritdoc */
  activateListeners(html) {
    super.activateListeners(html);

    // Click event listener
    const scenes = html.find('.scene');
    scenes.click(this._onClickScene.bind(this));
    html.find('#nav-toggle').click(this._onToggleNav.bind(this));

    // Activate Context Menu
    const contextOptions = this._getContextMenuOptions();
    /**
     * A hook event that fires when the context menu for a SceneNavigation
     * entry is constructed.
     * @function getSceneNavigationContext
     * @memberof hookEvents
     * @param {jQuery} html                     The HTML element to which the context options are attached
     * @param {ContextMenuEntry[]} entryOptions The context menu entries
     */
    Hooks.call("getSceneNavigationContext", html, contextOptions);
    if ( contextOptions ) new ContextMenu(html, ".scene", contextOptions);
  }

  /* -------------------------------------------- */

  /**
   * Get the set of ContextMenu options which should be applied for Scenes in the menu
   * @return {object[]}   The Array of context options passed to the ContextMenu instance
   * @private
   */
  _getContextMenuOptions() {
    return [
      {
        name: "SCENES.Activate",
        icon: '<i class="fas fa-bullseye"></i>',
        condition: li => game.user.isGM && !game.scenes.get(li.data("sceneId")).data.active,
        callback: li => {
          let scene = game.scenes.get(li.data("sceneId"));
          scene.activate();
        }
      },
      {
        name: "SCENES.Configure",
        icon: '<i class="fas fa-cogs"></i>',
        condition: game.user.isGM,
        callback: li => {
          let scene = game.scenes.get(li.data("sceneId"));
          scene.sheet.render(true);
        }
      },
      {
        name: "SCENES.Notes",
        icon: '<i class="fas fa-scroll"></i>',
        condition: li => {
          if ( !game.user.isGM ) return false;
          const scene = game.scenes.get(li.data("sceneId"));
          return !!scene.journal;
        },
        callback: li => {
          const scene = game.scenes.get(li.data("sceneId"));
          const entry = scene.journal;
          if ( entry ) {
            const sheet = entry.sheet;
            sheet.options.sheetMode = "text";
            sheet.render(true);
          }
        }
      },
      {
        name: "SCENES.Preload",
        icon: '<i class="fas fa-download"></i>',
        condition: game.user.isGM,
        callback: li => {
          let sceneId = li.attr("data-scene-id");
          game.scenes.preload(sceneId, true);
        }
      },
      {
        name: "SCENES.ToggleNav",
        icon: '<i class="fas fa-compass"></i>',
        condition: li => {
          const scene = game.scenes.get(li.data("sceneId"));
          return game.user.isGM && ( !scene.data.active );
        },
        callback: li => {
          const scene = game.scenes.get(li.data("sceneId"));
          scene.update({navigation: !scene.data.navigation});
        }
      }
    ];
  }

  /* -------------------------------------------- */

  /**
   * Handle left-click events on the scenes in the navigation menu
   * @param {PointerEvent} event
   * @private
   */
  _onClickScene(event) {
    event.preventDefault();
    let sceneId = event.currentTarget.dataset.sceneId;
    game.scenes.get(sceneId).view();
  }

  /* -------------------------------------------- */

  /** @override */
  _onDragStart(event) {
    const sceneId = event.currentTarget.dataset.sceneId;
    event.dataTransfer.setData("text/plain", JSON.stringify({
      type: "SceneNavigation",
      id: sceneId,
    }));
  }

  /* -------------------------------------------- */

  /** @override */
  async _onDrop(event) {
    const data = TextEditor.getDragEventData(event);
    if ( data.type !== "SceneNavigation" ) return;

    // Identify the document, the drop target, and the set of siblings
    const scene = game.scenes.get(data.id);
    const dropTarget = event.target.closest(".scene") || null;
    const sibling = dropTarget ? game.scenes.get(dropTarget.dataset.sceneId) : null;
    if ( sibling && (sibling.id === scene.id) ) return;
    const siblings = this.scenes.filter(s => s.id !== scene.id);

    // Update the navigation sorting for each Scene
    return scene.sortRelative({
      target: sibling,
      siblings: siblings,
      sortKey: "navOrder",
      sortBefore: true
    });
  }

  /* -------------------------------------------- */

  /**
   * Handle navigation menu toggle click events
   * @param {Event} event
   * @private
   */
  _onToggleNav(event) {
    event.preventDefault();
    if ( this._collapsed ) return this.expand();
    else return this.collapse();
  }

  /* -------------------------------------------- */

  /**
   * Display progress of some major operation like loading Scene textures.
   * @param {string} label      A text label to display
   * @param {number} pct        A percentage of progress between 0 and 100
   */
  static displayProgressBar({label, pct}={}) {
    const loader = document.getElementById("loading");
    pct = Math.clamped(pct, 0, 100);
    loader.querySelector("#context").textContent = label;
    loader.querySelector("#loading-bar").style.width = `${pct}%`;
    loader.querySelector("#progress").textContent = `${pct}%`;
    loader.style.display = "block";
    if ( (pct === 100 ) && !loader.hidden) $(loader).fadeOut(2000);
  }
}
