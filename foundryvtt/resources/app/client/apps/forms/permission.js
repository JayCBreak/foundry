/**
 * A generic application for configuring permissions for various Document types
 * @extends {DocumentSheet}
 */
class PermissionControl extends DocumentSheet {

  /** @override */
	static get defaultOptions() {
	  return foundry.utils.mergeObject(super.defaultOptions, {
      id: "permission",
      template: "templates/apps/permission.html",
      width: 400
    });
  }

  /* -------------------------------------------- */

  /** @override */
  get title() {
    return `${game.i18n.localize("PERMISSION.Title")}: ${this.document.name}`;
  }

  /* -------------------------------------------- */

  /** @override */
  getData(options) {
    const d = this.document;
    const isFolder = d instanceof Folder;

    // User permission levels
    const playerLevels = {};
    if ( isFolder ) {
      playerLevels["-2"] = game.i18n.localize("PERMISSION.DEFAULT");
      playerLevels["-1"] = game.i18n.localize("PERMISSION.NOCHANGE");
    } else {
      playerLevels["-1"] = game.i18n.localize("PERMISSION.DEFAULT");
    }
    for ( let [n, l] of Object.entries(CONST.DOCUMENT_PERMISSION_LEVELS) ) {
      playerLevels[l] = game.i18n.localize(`PERMISSION.${n}`);
    }

    // Default permission levels
    const defaultLevels = foundry.utils.deepClone(playerLevels);
    if ( isFolder ) delete defaultLevels["-2"];
    else delete defaultLevels["-1"];

    // Player users
    const users = game.users.map(u => {
      return {
        user: u,
        level: d.data.permission?.[u.id] ?? "-1"
      };
    });

    // Construct and return the data object
    return {
      currentDefault: d.data.permission?.default ?? "-1",
      instructions: game.i18n.localize(isFolder ? "PERMISSION.HintFolder" : "PERMISSION.HintDocument"),
      defaultLevels,
      playerLevels,
      isFolder,
      users
    };
  }

  /* -------------------------------------------- */

  /** @override */
  async _updateObject(event, formData) {
    event.preventDefault();
    if ( !game.user.isGM ) throw new Error("You do not have the ability to configure permissions.");

    // Collect user permissions
    const perms = {};
    for ( let [user, level] of Object.entries(formData) ) {
      if ( level === -1 ) {
        delete perms[user];
        continue;
      }
      perms[user] = level;
    }

    // Update all documents in a Folder
    if ( this.document instanceof Folder ) {
      const cls = getDocumentClass(this.document.type);
      const updates = this.document.content.map(e => {
        const p = foundry.utils.deepClone(e.data.permission);
        for ( let [k, v] of Object.entries(perms) ) {
          if ( v === -2 ) delete p[k];
          else p[k] = v;
        }
        return {_id: e.id, permission: p}
      });
      return cls.updateDocuments(updates, {diff: false, recursive: false, noHook: true});
    }

    // Update a single Document
    return this.document.update({permission: perms}, {diff: false, recursive: false, noHook: true});
  }
}
