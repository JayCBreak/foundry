/**
 * Document Sheet Configuration Application
 * @extends {FormApplication}
 */
class DocumentSheetConfig extends FormApplication {
  /** @inheritdoc */
	static get defaultOptions() {
	  return foundry.utils.mergeObject(super.defaultOptions, {
      classes: ["form", "sheet-config"],
      template: "templates/sheets/sheet-config.html",
      width: 400
    });
  }

  /**
   * An array of pending sheet assignments which are submitted before other elements of the framework are ready.
   * @type {object[]}
   * @private
   */
  static _pending = [];

  /* -------------------------------------------- */

  /** @inheritdoc */
  get title() {
    return `${this.object.name}: Sheet Configuration`;
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  getData(options) {
    const config = CONFIG[this.object.documentName];
    const type = this.object.data.type || CONST.BASE_DOCUMENT_TYPE;
    let defaultClass = null;

    // Classes which can be chosen
    const classes = Object.values(config.sheetClasses[type]).reduce((obj, c) => {
      obj[c.id] = c.label;
      if ( c.default && !defaultClass ) defaultClass = c.id;
      return obj;
    }, {});

    // Return data
    return {
      isGM: game.user.isGM,
      object: this.object.toObject(),
      options: this.options,
      sheetClass: this.object.getFlag("core", "sheetClass") ?? "",
      sheetClasses: classes,
      defaultClass: defaultClass,
      blankLabel: game.i18n.localize("SHEETS.DefaultSheet")
    }
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  async _updateObject(event, formData) {
    event.preventDefault();
    const original = this.getData({});

    // De-register the current sheet class
    const sheet = this.object.sheet;
    await sheet.close();
    this.object._sheet = null;
    delete this.object.apps[sheet.appId];

    // Update world settings
    if ( game.user.isGM && (formData.defaultClass !== original.defaultClass) ) {
      const setting = game.settings.get("core", "sheetClasses") || {};
      const type = this.object.data.type || CONST.BASE_DOCUMENT_TYPE;
      foundry.utils.mergeObject(setting, {[`${this.object.documentName}.${type}`]: formData.defaultClass});
      await game.settings.set("core", "sheetClasses", setting);
    }

    // Update the document-specific override
    if ( formData.sheetClass !== original.sheetClass ) {
      await this.object.setFlag("core", "sheetClass", formData.sheetClass);
    }

    // Re-draw the updated sheet
    this.object.sheet.render(true);
  }

  /* -------------------------------------------- */
  /*  Configuration Methods
  /* -------------------------------------------- */

  /**
   * Initialize the configured Sheet preferences for Documents which support dynamic Sheet assignment
   * Create the configuration structure for supported documents
   * Process any pending sheet registrations
   * Update the default values from settings data
   */
  static initializeSheets() {

    for ( let cls of Object.values(foundry.documents) ) {
      const types = this._getDocumentTypes(cls);
      CONFIG[cls.documentName].sheetClasses = types.reduce((obj, type) => {
        obj[type] = {};
        return obj;
      }, {});
    }

    // Register any pending sheets
    this._pending.forEach(p => {
      if ( p.action === "register" ) this._registerSheet(p);
      else if ( p.action === "unregister" ) this._unregisterSheet(p);
    });
    this._pending = [];

    // Update default sheet preferences
    const defaults = game.settings.get("core", "sheetClasses");
    this.updateDefaultSheets(defaults)
  }

  /* -------------------------------------------- */

  static _getDocumentTypes(cls, types=[]) {
    if ( types.length ) return types;
    const systemTypes = game.system?.documentTypes[cls.documentName];
    return systemTypes?.length ? systemTypes : [CONST.BASE_DOCUMENT_TYPE];
  }

  /* -------------------------------------------- */

  /**
   * Register a sheet class as a candidate which can be used to display documents of a given type
   * @param {Function} documentClass           The Document class for which to register a new Sheet option
   * @param {string} scope                     Provide a unique namespace scope for this sheet
   * @param {Application} sheetClass           A defined Application class used to render the sheet
   * @param {Object} options                   Additional options used for sheet registration
   * @param {string|function} [options.label]  A human readable label for the sheet name, which will be localized
   * @param {string[]} [options.types]         An array of document types for which this sheet should be used
   * @param {boolean} [options.makeDefault]    Whether to make this sheet the default for provided types
   */
  static registerSheet(documentClass, scope, sheetClass, {label, types, makeDefault=false}={}) {
    const id = `${scope}.${sheetClass.name}`;
    const config = {documentClass, id, label, sheetClass, types, makeDefault};
    if ( game.ready ) this._registerSheet(config);
    else {
      config["action"] = "register";
      this._pending.push(config);
    }
  }

  /**
   * Perform the sheet registration
   * @private
   */
  static _registerSheet({documentClass, id, label, sheetClass, types, makeDefault}={}) {
    types = this._getDocumentTypes(documentClass, types);
    const classes = CONFIG[documentClass.documentName]?.sheetClasses;
    const defaults = game.ready ? game.settings.get("core", "sheetClasses") : {};
    if ( typeof classes !== "object" ) return;
    for ( let t of types ) {
      const existingDefault = defaults[documentClass.documentName]?.[t];
      const isDefault = existingDefault ? (existingDefault === id) : makeDefault;
      if ( isDefault ) Object.values(classes[t]).forEach(s => s.default = false);
      if ( label instanceof Function ) label = label();
      else if ( label ) label = game.i18n.localize(label);
      else label = id;
      classes[t][id] = {
        id, label,
        cls: sheetClass,
        default: isDefault
      };
    }
  }

  /* -------------------------------------------- */

  /**
   * Unregister a sheet class, removing it from the list of available Applications to use for a Document type
   * @param {Function} documentClass  The Document class for which to register a new Sheet option
   * @param {string} scope            Provide a unique namespace scope for this sheet
   * @param {Application} sheetClass  A defined Application class used to render the sheet
   * @param {object[]} types             An Array of types for which this sheet should be removed
   */
  static unregisterSheet(documentClass, scope, sheetClass, {types}={}) {
    const id = `${scope}.${sheetClass.name}`;
    const config = {documentClass, id, types};
    if ( game.ready ) this._unregisterSheet(config);
    else {
      config["action"] = "unregister";
      this._pending.push(config);
    }
  }

  /**
   * Perform the sheet de-registration
   * @private
   */
  static _unregisterSheet({documentClass, id, types}={}) {
    types = this._getDocumentTypes(documentClass, types);
    const classes = CONFIG[documentClass.documentName]?.sheetClasses;
    if ( typeof classes !== "object" ) return;
    for ( let t of types ) {
      delete classes[t][id];
    }
  }

  /* -------------------------------------------- */

  /**
   * Update the currently default Sheets using a new core world setting
   * @param {object} setting
   */
  static updateDefaultSheets(setting={}) {
    if ( !Object.keys(setting).length ) return;
    for ( let cls of Object.values(foundry.documents) ) {
      const documentName = cls.documentName;
      const cfg = CONFIG[documentName];
      const classes = cfg.sheetClasses;
      const collection = cfg.collection?.instance ?? [];
      let defaults = setting[documentName] || {};
      if ( !defaults ) continue;

      // Update default preference for registered sheets
      for ( let [type, sheetId] of Object.entries(defaults) ) {
        const sheets = Object.values(classes[type] || {});
        let requested = sheets.find(s => s.id === sheetId);
        if ( requested ) sheets.forEach(s => s.default = s.id === sheetId);
      }

      // Close and de-register any existing sheets
      for ( let document of collection ) {
        Object.values(document.apps).forEach(app => app.close());
        document.apps = {};
        document._sheet = null;
      }
    }
  }

  /* -------------------------------------------- */

  /**
   * Initialize default sheet configurations for all document types.
   * @ignore
   */
  static _registerDefaultSheets() {
    const defaultSheets = {
      // Documents
      Actor: ActorSheet,
      Folder: FolderConfig,
      Item: ItemSheet,
      JournalEntry: JournalSheet,
      Macro: MacroConfig,
      Playlist: PlaylistConfig,
      RollTable: RollTableConfig,
      Scene: SceneConfig,
      User: UserConfig,
      // Embedded Documents
      ActiveEffect: ActiveEffectConfig,
      AmbientLight: AmbientLightConfig,
      AmbientSound: AmbientSoundConfig,
      Card: CardConfig,
      Combatant: CombatantConfig,
      Drawing: DrawingConfig,
      MeasuredTemplate: MeasuredTemplateConfig,
      Note: NoteConfig,
      PlaylistSound: PlaylistSoundConfig,
      Tile: TileConfig,
      Token: TokenConfig,
      Wall: WallConfig
    };

    Object.values(foundry.documents).forEach(base => {
      const type = base.documentName;
      const cfg = CONFIG[type];
      cfg.sheetClasses = {};
      const defaultSheet = defaultSheets[type];
      if ( !defaultSheet ) return;
      DocumentSheetConfig.registerSheet(cfg.documentClass, "core", defaultSheet, {
        makeDefault: true,
        label: () => game.i18n.format("SHEETS.DefaultDocumentSheet", {document: game.i18n.localize(`DOCUMENT.${type}`)})
      });
    });
    DocumentSheetConfig.registerSheet(Cards, "core", CardsConfig, {
      label: "CARDS.CardsDeck",
      types: ["deck"],
      makeDefault: true
    });
    DocumentSheetConfig.registerSheet(Cards, "core", CardsHand, {
      label: "CARDS.CardsHand",
      types: ["hand"],
      makeDefault: true
    });
    DocumentSheetConfig.registerSheet(Cards, "core", CardsPile, {
      label: "CARDS.CardsPile",
      types: ["pile"],
      makeDefault: true
    });
  }
}

/**
 * @deprecated since v9
 * @ignore
 */
class EntitySheetConfig extends DocumentSheetConfig {}
