/**
 * A DocumentSheet application responsible for displaying and editing a single Cards stack.
 * @extends {DocumentSheet}
 * @param {Cards} object                    The {@link Cards} object being configured.
 * @param {DocumentSheetOptions} [options]  Application configuration options.
 */
class CardsConfig extends DocumentSheet {
  constructor(object, options) {
    super(object, options);

    /**
     * The sorting mode used to display the sheet, "standard" if true, otherwise "shuffled"
     * @type {boolean}
     * @private
     */
    this._sortStandard = false;

    // Add document type to the window classes list
    this.options.classes.push(object.type);
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  static get defaultOptions() {
    return foundry.utils.mergeObject(super.defaultOptions, {
      classes: ["sheet", "cards-config"],
      template: "templates/cards/cards-deck.html",
      width: 620,
      height: "auto",
      closeOnSubmit: false,
      viewPermission: CONST.DOCUMENT_PERMISSION_LEVELS.OBSERVER,
      dragDrop: [{dragSelector: "ol.cards li.card", dropSelector: "ol.cards"}],
      tabs: [{navSelector: ".tabs", contentSelector: "form", initial: "cards"}],
      scrollY: ["ol.cards"]
    });
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  getData(options) {

    // Sort the cards
    let cards = Array.from(this.object.cards);
    const sortFn = this._sortStandard ? this.object.sortStandard : this.object.sortShuffled;
    cards.sort((a, b) => sortFn.call(this.object, a, b));

    // Return rendering context
    return foundry.utils.mergeObject(super.getData(options), {
      cards: cards,
      types: CONFIG.Cards.typeLabels,
      inCompendium: !!this.object.pack
    });
  }

  /* -------------------------------------------- */
  /* 	Event Listeners and Handlers								*/
  /* -------------------------------------------- */

  /** @inheritdoc */
  activateListeners(html) {
    super.activateListeners(html);

    // Card Actions
    html.find(".card-control").click(this._onCardControl.bind(this));

    // Intersection Observer
    const cards = html.find("ol.cards");
    const entries = cards.find("li.card");
    const observer = new IntersectionObserver(this._onLazyLoadImage.bind(this), {root: cards[0]});
    entries.each((i, li) => observer.observe(li));
  }

  /* -------------------------------------------- */

  /**
   * Handle card control actions which modify single cards on the sheet.
   * @param {PointerEvent} event          The originating click event
   * @returns {Promise}                   A Promise which resolves once the handler has completed
   * @protected
   */
  async _onCardControl(event) {
    const button = event.currentTarget;
    const li = button.closest(".card");
    const card = li ? this.object.cards.get(li.dataset.cardId) : null;
    const cls = getDocumentClass("Card");

    // Save any pending change to the form
    await this._onSubmit(event, {preventClose: true, preventRender: true});

    // Handle the control action
    switch ( button.dataset.action ) {
      case "create":
        return cls.createDialog({}, {parent: this.object, pack: this.object.pack});
      case "edit":
        return card.sheet.render(true);
      case "delete":
        return card.deleteDialog();
      case "deal":
        return this.object.dealDialog();
      case "draw":
        return this.object.drawDialog();
      case "pass":
        return this.object.passDialog();
      case "play":
        return this.object.playDialog(card);
      case "reset":
        return this.object.resetDialog();
      case "shuffle":
        this._sortStandard = false;
        return this.object.shuffle();
      case "toggleSort":
        this._sortStandard = !this._sortStandard;
        return this.render();
      case "nextFace":
        return card.update({face: card.data.face === null ? 0 : card.data.face+1});
      case "prevFace":
        return card.update({face: card.data.face === 0 ? null : card.data.face-1});
    }
  }

  /* -------------------------------------------- */

  /**
   * Handle lazy-loading card face images.
   * See {@link SidebarTab#_onLazyLoadImage}
   * @protected
   */
  _onLazyLoadImage(entries, observer) {
    return ui.cards._onLazyLoadImage.call(this, entries, observer);
  }

	/* -------------------------------------------- */

  /** @inheritdoc */
  _canDragStart(selector) {
    return this.isEditable;
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  _onDragStart(event) {
    const li = event.currentTarget;
    const card = this.object.cards.get(li.dataset["cardId"]);
    if ( !card ) return;

    // Create drag data
    const dragData = {
      type: "Card",
      cardsId: this.object.id,
      cardId: card.id
    };

    // Set data transfer
    event.dataTransfer.setData("text/plain", JSON.stringify(dragData));
  }

	/* -------------------------------------------- */

  /** @inheritdoc */
  _canDragDrop(selector) {
    return this.isEditable;
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  async _onDrop(event) {
    const data = TextEditor.getDragEventData(event);
    if ( data.type !== "Card" ) return;
    const source = game.cards.get(data.cardsId);
    const card = source.cards.get(data.cardId);
    if ( source.id === this.object.id ) return this._onSortCard(event, card);
    else return card.pass(this.object);
  }

  /* -------------------------------------------- */

  /**
   * Handle sorting a Card relative to other siblings within this document
   * @param {Event} event     The drag drop event
   * @param {Card} card       The card being dragged
   * @private
   */
  _onSortCard(event, card) {
    const li = event.target.closest("[data-card-id]");
    const target = this.object.cards.get(li.dataset.cardId);
    const siblings = this.object.cards.filter(c => c.id !== card.id);
    const updateData = SortingHelpers.performIntegerSort(card, {target, siblings}).map(u => {
      return {_id: u.target.id, sort: u.update.sort}
    });
    return this.object.updateEmbeddedDocuments("Card", updateData);
  }
}

/**
 * A subclass of CardsConfig which provides a sheet representation for Cards documents with the "hand" type.
 * @extends CardsConfig
 */
class CardsHand extends CardsConfig {
  static get defaultOptions() {
    return foundry.utils.mergeObject(super.defaultOptions, {
      template: "templates/cards/cards-hand.html",
    })
  }
}

/**
 * A subclass of CardsConfig which provides a sheet representation for Cards documents with the "pile" type.
 * @extends CardsConfig
 */
class CardsPile extends CardsConfig {
  static get defaultOptions() {
    return foundry.utils.mergeObject(super.defaultOptions, {
      template: "templates/cards/cards-pile.html",
    })
  }
}
