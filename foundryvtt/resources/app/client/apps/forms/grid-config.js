/**
 * A tool for fine tuning the grid in a Scene
 * @extends {FormApplication}
 * @param {Scene} scene                       The scene whose grid is being configured.
 * @param {SceneConfig} sheet                 The Scene Configuration sheet that spawned this dialog.
 * @param {FormApplicationOptions} [options]  Application configuration options.
 */
class GridConfig extends FormApplication {
  constructor(scene, sheet, ...args) {
    super(scene, ...args);

    /**
     * Track the Scene Configuration sheet reference
     * @type {SceneConfig}
     */
    this.sheet = sheet;

    /**
     * The counter-factual dimensions being evaluated
     * @type {Object}
     */
    this._dimensions = {};

    /**
     * A reference to the bound key handler function so it can be removed
     * @type {Function|null}
     * @private
     */
    this._keyHandler = null;

    /**
     * A reference to the bound mousewheel handler function so it can be removed
     * @type {Function|null}
     * @private
     */
    this._wheelHandler = null;
  }

  /* -------------------------------------------- */

  /** @override */
  static get defaultOptions() {
    return foundry.utils.mergeObject(super.defaultOptions, {
      id: "grid-config",
      template: "templates/scene/grid-config.html",
      title: game.i18n.localize("SCENES.GridConfigTool"),
      width: 480,
      height: "auto",
      closeOnSubmit: true,
      submitOnChange: true
    });
  }

	/* -------------------------------------------- */

  /** @override */
  getData(options) {
    return {
      gridTypes: SceneConfig._getGridTypes(),
      scale: canvas.background.bg ? this.object.data.width / canvas.background.bgSource.width : 1,
      scene: this.object.data
    };
  }

	/* -------------------------------------------- */

  /** @override */
  async _render(...args) {
    await super._render(...args);
    if ( !this.object.data.img ) {
      ui.notifications.warn("WARNING.GridConfigNoBG", {localize: true});
    }
    for ( let l of canvas.layers ) {
      if ( !["BackgroundLayer", "ForegroundLayer", "GridLayer"].includes(l.name) ) l.visible = false;
    }
    this._refresh({grid: true, background: true});
  }

	/* -------------------------------------------- */
  /*  Event Listeners and Handlers                */
	/* -------------------------------------------- */

  /** @override */
  activateListeners(html) {
    super.activateListeners(html);
    this._keyHandler = this._keyHandler || this._onKeyDown.bind(this);
    document.addEventListener("keydown", this._keyHandler);
    this._wheelHandler = this._wheelHandler || this._onWheel.bind(this);
    document.addEventListener("wheel", this._wheelHandler, {passive: false});
    html.find('button[name="reset"]').click(this._onReset.bind(this));
  }

	/* -------------------------------------------- */

  /** @override */
  async close(options) {
    document.removeEventListener("keydown", this._keyHandler);
    document.removeEventListener("wheel", this._wheelHandler);
    this._keyHandler = this._wheelHandler = null;
    if ( canvas.ready ) await canvas.draw();
    await this.sheet.maximize();
    return super.close(options);
  }

	/* -------------------------------------------- */

  /**
   * Handle keyboard events.
   * @param {KeyboardEvent} event    The original keydown event
   * @private
   */
  _onKeyDown(event) {
    const key = event.code;
    const up = ["KeyW", "ArrowUp"];
    const down = ["KeyS", "ArrowDown"];
    const left = ["KeyA", "ArrowLeft"];
    const right = ["KeyD", "ArrowRight"];
    const moveKeys = up.concat(down).concat(left).concat(right);
    if ( !( moveKeys.includes(key) ) || game.keyboard.hasFocus ) return;
    event.preventDefault();

    // Increase the Scene scale on shift + up or down
    if ( event.shiftKey ) {
      let delta = up.includes(key) ? 1 : (down.includes(key) ? -1 : 0);
      this._scaleBackgroundSize(delta);
    }

    // Resize grid size on ALT
    else if ( event.altKey ) {
      let delta = up.includes(key) ? 1 : (down.includes(key) ? -1 : 0);
      this._scaleGridSize(delta);
    }

    // Shift grid position
    else {
      if ( up.includes(key) ) this._shiftBackground({deltaY: -1});
      else if ( down.includes(key) ) this._shiftBackground({deltaY: 1});
      else if ( left.includes(key) ) this._shiftBackground({deltaX: -1});
      else if ( right.includes(key) ) this._shiftBackground({deltaX: 1});
    }
  }

	/* -------------------------------------------- */

  /**
   * Handle mousewheel events.
   * @param {WheelEvent} event    The original wheel event
   * @private
   */
  _onWheel(event) {
    if ( event.deltaY === 0 ) return;

    // Increase the Scene scale on shift
    if ( event.shiftKey ) {
      event.preventDefault();
      event.stopImmediatePropagation();
      this._scaleBackgroundSize(-Math.sign(event.deltaY));
    }

    // Increase the Grid scale on alt
    if ( event.altKey ) {
      event.preventDefault();
      event.stopImmediatePropagation();
      this._scaleGridSize(-Math.sign(event.deltaY));
    }
  }

	/* -------------------------------------------- */

  /**
   * Handle resetting the form and re-drawing back to the original dimensions
   * @param {MouseEvent} event    The original click event
   * @private
   */
  _onReset(event) {
    event.preventDefault();
    this._dimensions = {};
    this.render();
  }

	/* -------------------------------------------- */
  /*  Previewing and Updating Functions           */
	/* -------------------------------------------- */

  /**
   * Scale the background size relative to the grid size
   * @param {number} delta          The directional change in background size
   * @private
   */
  _scaleBackgroundSize(delta) {
    const scale = Math.round((parseFloat(this.form.scale.value) + (0.05 * delta)) * 100) / 100;
    this.form.scale.value = Math.clamped(scale, 0.25, 10.0);
    this._refresh({background: true});
  }

	/* -------------------------------------------- */

  /**
   * Scale the grid size relative to the background image.
   * When scaling the grid size in this way, constrain the allowed values between 50px and 300px.
   * @param {number} delta          The grid size in pixels
   * @private
   */
  _scaleGridSize(delta) {
    this.form.grid.value = Math.clamped(parseInt(this.form.grid.value) + delta, 50, 300);
    this._refresh({background: true, grid: true});
  }

	/* -------------------------------------------- */

  /**
   * Shift the background image relative to the grid layer
   * @param {number} deltaX         The number of pixels to shift in the x-direction
   * @param {number} deltaY         The number of pixels to shift in the y-direction
   * @private
   */
  _shiftBackground({deltaX=0, deltaY=0}={}) {
    this.form.shiftX.value = parseInt(this.form.shiftX.value) + deltaX;
    this.form.shiftY.value = parseInt(this.form.shiftY.value) + deltaY;
    this._refresh({background: true});
  }

	/* -------------------------------------------- */

  /**
   * Temporarily refresh the display of the BackgroundLayer and GridLayer for the new pending dimensions
   * @param {boolean} background      Refresh the background display?
   * @param {boolean} grid            Refresh the grid display?
   * @private
   */
  _refresh({background=false, grid=false}={}) {
    const form = this.form;
    const bg = canvas.background.bg;
    const fg = canvas.foreground.bg;
    const tex = bg ? bg.texture : {width: canvas.scene.data.width, height: canvas.scene.data.height};

    // Establish new Scene dimensions
    const scale = parseFloat(form.scale.value);
    const d = this._dimensions = Canvas.getDimensions({
      width: tex.width * scale,
      height: tex.height * scale,
      padding: this.object.data.padding,
      grid: Math.max(parseInt(form.grid.value), 50),
      gridDistance: this.object.data.gridDistance,
      shiftX: parseInt(form.shiftX.value),
      shiftY: parseInt(form.shiftY.value)
    });
    canvas.dimensions = d;

    // Update the background and foreground sizing
    if ( background && bg ) {
      bg.position.set(d.paddingX - d.shiftX, d.paddingY - d.shiftY);
      bg.width = d.sceneWidth;
      bg.height = d.sceneHeight;
      grid = true;
    }
    if ( background && fg ) {
      fg.position.set(d.paddingX - d.shiftX, d.paddingY - d.shiftY);
      fg.width = d.sceneWidth;
      fg.height = d.sceneHeight;
    }

    // Update the grid layer
    if ( grid ) {
      canvas.grid.tearDown();
      canvas.grid.draw({type: parseInt(form.gridType.value), dimensions: d, gridColor: 0xFF0000, gridAlpha: 1.0});
      canvas.stage.hitArea = new PIXI.Rectangle(0, 0, d.width, d.height);
    }
  }

	/* -------------------------------------------- */

  /** @override */
  async _onChangeInput(event) {
    event.preventDefault();
    this._refresh({background: true, grid: true});
  }

	/* -------------------------------------------- */

  /** @override */
  async _updateObject(event, formData) {
    formData.width = Math.round(this._dimensions.sceneWidth);
    formData.height = Math.round(this._dimensions.sceneHeight);
    const delta = foundry.utils.diffObject(this.object.data, formData);
    if ( ["width", "height", "padding", "shiftX", "shiftY", "size"].some(k => k in delta) ) {
      const confirm = await Dialog.confirm({
        title: game.i18n.localize("SCENES.DimensionChangeTitle"),
        content: `<p>${game.i18n.localize("SCENES.DimensionChangeWarning")}</p>`
      });
      if ( !confirm ) return;
    }
    return this.object.update(formData, {fromSheet: true});
  }
}
