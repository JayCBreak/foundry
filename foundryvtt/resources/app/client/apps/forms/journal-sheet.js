/**
 * @typedef {DocumentSheetOptions} JournalSheetOptions
 * @property {string|null} [sheetMode]  The current display mode of the journal. Either 'text' or 'image'.
 */

/**
 * The Application responsible for displaying and editing a single JournalEntry document.
 * @extends {DocumentSheet}
 * @param {JournalEntry} object            The JournalEntry instance which is being edited
 * @param {JournalSheetOptions} [options]  Application options
 */
class JournalSheet extends DocumentSheet {
  constructor(object, options={}) {
    super(object, options);

    /**
     * The current display mode of the journal. Either 'text' or 'image'.
     * @type {string|null}
     * @private
     */
    this._sheetMode = this.options.sheetMode || this._inferDefaultMode();

    /**
     * The size of the application when it was in text mode, so we can go back
     * to it when we switch modes.
     * @type {object|null}
     * @private
     */
    this._textPos = null;
  }

	/* -------------------------------------------- */

  /**
   * @override
   * @returns {JournalSheetOptions}
   */
	static get defaultOptions() {
    return foundry.utils.mergeObject(super.defaultOptions, {
      classes: ["sheet", "journal-sheet"],
      width: 720,
      height: 800,
      resizable: true,
      closeOnSubmit: false,
      submitOnClose: true,
      viewPermission: CONST.DOCUMENT_PERMISSION_LEVELS.NONE
    });
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  get template() {
    if ( this._sheetMode === "image" ) return ImagePopout.defaultOptions.template;
    return "templates/journal/sheet.html";
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  get title() {
    return this.object.permission ? this.object.name : "";
  }

	/* -------------------------------------------- */

  /**
   * Guess the default view mode for the sheet based on the player's permissions to the Entry
   * @return {string}
   * @private
   */
  _inferDefaultMode() {
    const hasImage = !!this.object.data.img;
    const hasText = this.object.data.content;

    // If the user only has limited permission, show an image or nothing
    if ( this.object.limited ) return hasImage ? "image" : null;

    // Otherwise prefer text if it exists
    return hasText || !hasImage ? "text" : "image";
  }

	/* -------------------------------------------- */

  /** @inheritdoc */
  async _render(force, options={}) {

    // Determine the sheet rendering mode
    const mode = options.sheetMode || this._sheetMode;
    if ( mode === null ) return;

    // Close the existing sheet if the mode has changed
    if ( (mode !== this._sheetMode) && this.rendered ) {
      await this.close({submit: false});
      options.sheetMode = this._sheetMode = mode;
      if ( mode === "image" ) this._textPos = deepClone(this.position);
      else if ( this._textPos ) mergeObject(options, this._textPos);
      return this.render(true, options);
    }

    // Display image mode
    if ( mode === "image" ) {
      const img = this.object.data.img;
      const pos = await ImagePopout.getPosition(img);
      foundry.utils.mergeObject(options, pos);
      options.classes = this.constructor.defaultOptions.classes.concat(ImagePopout.defaultOptions.classes);
    }

    // Adjust for text mode
    else if ( mode === "text" ) {
      options.classes = this.constructor.defaultOptions.classes;
    }

    this._sheetMode = mode;
    // Normal rendering
    await super._render(force, options);

    // If the sheet was first created, activate the editor
    if ( options.action === "create" ) this.activateEditor("content");
  }

	/* -------------------------------------------- */

  /** @inheritdoc */
  _getHeaderButtons() {
    const buttons = super._getHeaderButtons();
    const isOwner = this.object.isOwner;
    const atLeastLimited = !!this.object.compendium || this.object.testUserPermission(game.user, "LIMITED");
    const atLeastObserver = !!this.object.compendium || this.object.testUserPermission(game.user, "OBSERVER");
    const hasMultipleModes = !!this.object.data.img && !!this.object.data.content;

    // Image Mode
    if ( isOwner || (atLeastLimited && hasMultipleModes) ) {
      buttons.unshift({
        label: "JOURNAL.ModeImage",
        class: "entry-image",
        icon: "fas fa-image",
        onclick: ev => this._onSwapMode(ev, "image")
      })
    }

    // Text Mode
    if ( isOwner || (atLeastObserver && hasMultipleModes) ) {
      buttons.unshift({
        label: "JOURNAL.ModeText",
        class: "entry-text",
        icon: "fas fa-file-alt",
        onclick: ev => this._onSwapMode(ev, "text")
      })
    }

    // Share Entry
    if ( game.user.isGM ) {
      buttons.unshift({
        label: "JOURNAL.ActionShow",
        class: "share-image",
        icon: "fas fa-eye",
        onclick: ev => this._onShowPlayers(ev)
      });
    }
    return buttons;
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  getData(options) {
    const data = super.getData(options);
    data.title = this.title; // Needed for image mode
    data.image = this.object.data.img;
    data.folders = game.folders.filter(f => (f.data.type === "JournalEntry") && f.displayed);
    return data;
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  async _updateObject(event, formData) {
    if ( this._sheetMode === "image" ) {
      formData.name = formData.title;
      delete formData["title"];
      formData.img = formData.image;
      delete formData["image"];
    }
    // Remove <form> tags which will break the display of the sheet.
    if ( formData.content ) formData.content = formData.content.replace(/<\s*\/?\s*form(\s+[^>]*)?>/g, "");
    return super._updateObject(event, formData);
  }

  /* -------------------------------------------- */

  /**
   * Handle requests to switch the rendered mode of the Journal Entry sheet
   * Save the form before triggering the show request, in case content has changed
   * @param {Event} event   The triggering click event
   * @param {string} mode   The journal mode to display
   */
  async _onSwapMode(event, mode) {
    event.preventDefault();
    await this.submit();
    this.render(true, {sheetMode: mode});
  }

  /* -------------------------------------------- */

  /**
   * Handle requests to show the referenced Journal Entry to other Users
   * Save the form before triggering the show request, in case content has changed
   * @param {Event} event   The triggering click event
   */
  async _onShowPlayers(event) {
    event.preventDefault();
    await this.submit();
    return this.object.show(this._sheetMode, true);
  }
}
