/**
 * The sidebar directory which organizes and displays world-level Scene documents.
 * @extends {SidebarDirectory}
 */
class SceneDirectory extends SidebarDirectory {

  /** @override */
  static documentName = "Scene";

  /** @override */
  static documentPartial = "templates/sidebar/scene-partial.html";

  /* -------------------------------------------- */

  /** @inheritdoc */
  async _render(force, options) {
    if ( !game.user.isGM ) return;
    return super._render(force, options);
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  _getEntryContextOptions() {
    let options = super._getEntryContextOptions();
    options = [
      {
        name: "SCENES.View",
        icon: '<i class="fas fa-eye"></i>',
        condition: li => !canvas.ready || (li.data("documentId") !== canvas.scene.id),
        callback: li => {
          const scene = game.scenes.get(li.data("documentId"));
          scene.view();
        }
      },
      {
        name: "SCENES.Activate",
        icon: '<i class="fas fa-bullseye"></i>',
        condition: li => game.user.isGM && !game.scenes.get(li.data("documentId")).data.active,
        callback: li => {
          const scene = game.scenes.get(li.data("documentId"));
          scene.activate();
        }
      },
      {
        name: "SCENES.Configure",
        icon: '<i class="fas fa-cogs"></i>',
        callback: li => {
          const scene = game.scenes.get(li.data("documentId"));
          scene.sheet.render(true);
        }
      },
      {
        name: "SCENES.Notes",
        icon: '<i class="fas fa-scroll"></i>',
        condition: li => {
          const scene = game.scenes.get(li.data("documentId"));
          return !!scene.journal;
        },
        callback: li => {
          const scene = game.scenes.get(li.data("documentId"));
          const entry = scene.journal;
          if ( entry ) {
            const sheet = entry.sheet;
            sheet.options.sheetMode = "text";
            sheet.render(true);
          }
        }
      },
      {
        name: "SCENES.ToggleNav",
        icon: '<i class="fas fa-compass"></i>',
        condition: li => {
          const scene = game.scenes.get(li.data("documentId"));
          return game.user.isGM && ( !scene.data.active );
        },
        callback: li => {
          const scene = game.scenes.get(li.data("documentId"));
          scene.update({navigation: !scene.data.navigation});
        }
      },
      {
        name: "SCENES.GenerateThumb",
        icon: '<i class="fas fa-image"></i>',
        condition: li => {
          const scene = game.scenes.get(li[0].dataset.documentId);
          return (scene.data.img || scene.data.tiles.size) && !game.settings.get("core", "noCanvas");
        },
        callback: li => {
          const scene = game.scenes.get(li[0].dataset.documentId);
          scene.createThumbnail().then(data => {
            scene.update({thumb: data.thumb}, {diff: false});
            ui.notifications.info(`Regenerated thumbnail image for ${scene.name} background image`);
          }).catch(err => ui.notifications.error(err.message));
        }
      }
    ].concat(options);

    // Remove the permissions entry
    let idx = options.findIndex(o => o.name === "PERMISSION.Configure");
    options.splice(idx, 1);
    return options;
  }
}
