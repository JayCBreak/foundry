/**
 * The sidebar directory which organizes and displays world-level Cards documents.
 * @extends {SidebarDirectory}
 */
class CardsDirectory extends SidebarDirectory {

  /** @override */
  static documentName = "Cards";
}
