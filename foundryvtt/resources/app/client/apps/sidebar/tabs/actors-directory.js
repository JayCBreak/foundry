/**
 * The sidebar directory which organizes and displays world-level Actor documents.
 * @extends {SidebarDirectory}
 */
class ActorDirectory extends SidebarDirectory {
  constructor(...args) {
    super(...args);
    this._dragDrop[0].permissions["dragstart"] = () => game.user.can("TOKEN_CREATE");
    this._dragDrop[0].permissions["dragdrop"] = () => game.user.can("ACTOR_CREATE");
  }

  /* -------------------------------------------- */

  /** @override */
  static documentName = "Actor";

  /* -------------------------------------------- */

  /** @override */
  _canDragStart(selector) {
    return game.user.can("TOKEN_CREATE");
  }

  /* -------------------------------------------- */

  /** @override */
  _onDragStart(event) {
    const li = event.currentTarget.closest(".directory-item");
    let actor = null;
    if ( li.dataset.documentId ) {
      actor = game.actors.get(li.dataset.documentId);
      if ( !actor || !actor.visible ) return false;
    }

    // Parent directory drag start handling
    super._onDragStart(event);

    // Create the drag preview for the Token
    if ( actor && canvas.ready ) {
      const img = li.querySelector("img");
      const td = actor.data.token;
      const w = td.width * canvas.dimensions.size * td.scale * canvas.stage.scale.x;
      const h = td.height * canvas.dimensions.size * td.scale * canvas.stage.scale.y;
      const preview = DragDrop.createDragImage(img, w, h);
      event.dataTransfer.setDragImage(preview, w / 2, h / 2);
    }
  }

  /* -------------------------------------------- */

  /** @override */
  _canDragDrop(selector) {
    return game.user.can("ACTOR_CREATE");
  }

  /* -------------------------------------------- */

  /** @override */
  _getEntryContextOptions() {
    const options = super._getEntryContextOptions();
    return [
      {
        name: "SIDEBAR.CharArt",
        icon: '<i class="fas fa-image"></i>',
        condition: li => {
          const actor = game.actors.get(li.data("documentId"));
          return actor.data.img !== CONST.DEFAULT_TOKEN;
        },
        callback: li => {
          const actor = game.actors.get(li.data("documentId"));
          new ImagePopout(actor.data.img, {
            title: actor.name,
            shareable: true,
            uuid: actor.uuid
          }).render(true);
        }
      },
      {
        name: "SIDEBAR.TokenArt",
        icon: '<i class="fas fa-image"></i>',
        condition: li => {
          const actor = game.actors.get(li.data("documentId"));
          if ( actor.data.token.randomImg ) return false;
          return ![null, undefined, CONST.DEFAULT_TOKEN].includes(actor.data.token.img);
        },
        callback: li => {
          const actor = game.actors.get(li.data("documentId"));
          new ImagePopout(actor.data.token.img, {
            title: actor.name,
            shareable: true,
            uuid: actor.uuid
          }).render(true);
        }
      }
    ].concat(options);
  }
}
