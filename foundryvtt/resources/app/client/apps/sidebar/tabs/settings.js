/**
 * The sidebar tab which displays various game settings, help messages, and configuration options.
 * The Settings sidebar is the furthest-to-right using a triple-cogs icon.
 * @extends {SidebarTab}
 */
class Settings extends SidebarTab {

  /** @override */
	static get defaultOptions() {
	  const options = super.defaultOptions;
	  options.id = "settings";
	  options.template = "templates/sidebar/settings.html";
	  options.title = "Settings";
	  return options;
  }

	/* -------------------------------------------- */

  /** @override */
  getData(options) {
    if (game.data.coreUpdate.hasUpdate) game.data.coreUpdate.type = game.i18n.localize("Software");
    if (game.data.systemUpdate) game.data.systemUpdate.type = game.i18n.localize("System");
    const isDemo = game.data.options.demo === true;
    return {
      user: game.user,
      system: game.system,
      release: game.data.release,
      versionDisplay: game.release.display,
      isDemo: isDemo,
      canConfigure: game.user.can("SETTINGS_MODIFY") && !isDemo,
      canEditWorld: game.user.hasRole("GAMEMASTER") && !isDemo,
      canManagePlayers: game.user.isGM && !isDemo,
      canReturnSetup: game.user.hasRole("GAMEMASTER") && !isDemo,
      coreUpdate: game.user.isGM && game.data.coreUpdate.hasUpdate ? game.i18n.format("SETUP.UpdateAvailable", game.data.coreUpdate) : false,
      systemUpdate: game.user.isGM && game.data.systemUpdate ? game.i18n.format("SETUP.UpdateAvailable",
          { type: game.data.systemUpdate.type, channel: game.data.system.data.title, version: game.data.systemUpdate.version}) : false,
      modules: game.data.modules.reduce((n, m) => n + (m.active ? 1 : 0), 0)
    };
  }

	/* -------------------------------------------- */

  /** @override */
	activateListeners(html) {
	  html.find("button[data-action]").click(this._onSettingsButton.bind(this));
	  html.find(".notification-pip.update").click(this._onUpdateNotificationClick.bind(this));
  }

  /* -------------------------------------------- */

  /**
   * Delegate different actions for different settings buttons
   * @param {MouseEvent} event    The originating click event
   * @private
   */
  _onSettingsButton(event) {
    event.preventDefault();
    const button = event.currentTarget;
    switch (button.dataset.action) {
      case "configure":
        game.settings.sheet.render(true);
        break;
      case "modules":
        new ModuleManagement().render(true);
        break;
      case "world":
        new WorldConfig(game.world, { inWorld: true }).render(true);
        break;
      case "players":
        return ui.menu.items.players.onClick();
      case "setup":
        return game.shutDown();
      case "support":
        new SupportDetails().render(true);
        break;
      case "controls":
        new KeybindingsConfig().render(true);
        break;
      case "docs":
        new FrameViewer("https://foundryvtt.com/kb", {
          title: "SIDEBAR.Documentation"
        }).render(true);
        break;
      case "wiki":
        new FrameViewer("https://foundryvtt.wiki/", {
          title: "SIDEBAR.Wiki"
        }).render(true);
        break;
      case "invitations":
        new InvitationLinks().render(true);
        break;
      case "logout":
        return ui.menu.items.logout.onClick();
    }
  }

  /* -------------------------------------------- */

  /**
   * Executes with the update notification pip is clicked
   * @param {MouseEvent} event    The originating click event
   * @private
   */
  _onUpdateNotificationClick(event)
  {
    event.preventDefault();
    ui.notifications.notify(game.i18n.localize(event.target.dataset.action === "core-update" ? "SETUP.CoreUpdateInstructions" : "SETUP.SystemUpdateInstructions"))
  }
}


/* -------------------------------------------- */


/**
 * A simple window application which shows the built documentation pages within an iframe
 * @type {Application}
 */
class FrameViewer extends Application {
  constructor(url, options) {
    super(options);
    this.url = url;
  }

	/* -------------------------------------------- */

  /** @override */
	static get defaultOptions() {
	  const options = super.defaultOptions;

	  // Default positioning
	  let h = window.innerHeight * 0.9,
        w = Math.min(window.innerWidth * 0.9, 1200);
    options.height = h;
    options.width = w;
    options.top = (window.innerHeight - h) / 2;
    options.left = (window.innerWidth - w) / 2;
    options.id = "documentation";
    options.template = "templates/apps/documentation.html";
    return options;
  }

	/* -------------------------------------------- */

  /** @override */
  async getData(options) {
    return {
      src: this.url
    };
  }

  /* -------------------------------------------- */

  /** @override */
  async close(options) {
    this.element.find("#docs").remove();
    return super.close(options);
  }
}

/* -------------------------------------------- */
