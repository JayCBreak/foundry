/**
 * @typedef {FormApplicationOptions} WorldConfigOptions
 * @property {boolean} [create=false]  Whether the world is being created or updated.
 */

/**
 * The World Management setup application
 * @extends {FormApplication}
 * @param {object} object                 The world being configured.
 * @param {WorldConfigOptions} [options]  Application configuration options.
 */
class WorldConfig extends FormApplication {
  /**
   * @override
   * @returns {WorldConfigOptions}
   */
  static get defaultOptions() {
    return foundry.utils.mergeObject(super.defaultOptions, {
      id: "world-config",
      template: "templates/sidebar/apps/world-config.html",
      width: 600,
      height: "auto",
      create: false
    });
  }

  static WORLD_KB_URL = "https://foundryvtt.com/article/game-worlds/";

  /* -------------------------------------------- */

  /** @override */
  get title() {
    return this.options.create ? "Create New World" : `Edit World: ${this.object.data.title}`;
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  activateListeners(html) {
    super.activateListeners(html);
    html.find('[name="title"]').on("input", this._onTitleChange.bind(this));
  }

  /* -------------------------------------------- */

  /** @override */
  getData(options) {
    const ac = CONST.PACKAGE_AVAILABILITY_CODES;
    const nextDate = new Date(this.object?.data?.nextSession || undefined);
    const worldConfigData = {
      world: this.object,
      isCreate: this.options.create,
      submitText: this.options.create ? "Create World" : "Update World",
      nextDate: nextDate.isValid() ? nextDate.toDateInputString() : "",
      nextTime: nextDate.isValid() ? nextDate.toTimeInputString() : "",
      worldKbUrl: WorldConfig.WORLD_KB_URL,
      inWorld: options.inWorld ?? false
    };
    worldConfigData.showEditFields = !worldConfigData.isCreate && !worldConfigData.inWorld;
    if ( game.data.systems ) {
      worldConfigData.systems = game.data.systems.filter(s => {
        if ( this.object.data && ( this.object.data.system === s.id ) ) return true;
        return ![ac.REQUIRES_DEPENDENCY, ac.REQUIRES_CORE].includes( s.availability );
      });
    }
    return worldConfigData;
  }

  /* -------------------------------------------- */
  /*  Event Listeners and Handlers                */
  /* -------------------------------------------- */

  /** @override */
  async _onSubmit(event) {
    event.preventDefault();
    const form = event.target || this.form;
    form.disabled = true;

    const required = ["title", "system"];
    if ( required.some(name => form.elements[name] && !form.elements[name].reportValidity()) ) return;

    // Get form data
    const data = this._getSubmitData();
    if ( this.options.create ) data.action = "createWorld";
    else {
      data.action = "editWorld";
      data.name = this.object.data.name;
      if ( !data.resetKeys ) delete data.resetKeys;
      if ( !data.safeMode ) delete data.safeMode;
    }

    // Handle automatic name generation.
    if ( this.options.create && !data.name?.length ) {
      data.name = data.title.slugify({strict: true});
      if ( !data.name.length ) {
        form.elements.name.reportValidity();
        return;
      }
    }

    // Handle next session schedule fields
    if ( data.nextSession.some(t => !!t) ) {
      const now = new Date();
      const dateStr = `${data.nextSession[0] || now.toDateString()} ${data.nextSession[1] || now.toTimeString()}`;
      const date = new Date(dateStr);
      data.nextSession = isNaN(date) ? null : date.toISOString();
    }
    else data.nextSession = null;

    // Dispatch the POST request
    let response;
    try {
      response = await fetchJsonWithTimeout(foundry.utils.getRoute("setup"), {
        method: "POST",
        headers: {'Content-Type': 'application/json'},
        body: JSON.stringify(data)
      });
      form.disabled = false;

      // Display error messages
      if (response.error) return ui.notifications.error(response.error);
    }
    catch (e) {
      return ui.notifications.error(e);
    }

    // Handle successful creation
    if ( game.data.worlds ) {
      const world = game.data.worlds.find(w => w.id === response.id);
      if ( world ) mergeObject(world.data, response.data);
      else game.data.worlds.push(response);
      if ( ui.setup ) ui.setup.render();
    }
    else {
      game.world.data.update(response.data);
    }
    return this.close();
  }

  /* -------------------------------------------- */

  /**
   * Update the world name placeholder when the title is changed.
   * @param {TriggeredEvent} event
   * @private
   */
  _onTitleChange(event) {
    let slug = this.form.elements.title.value.slugify({strict: true});
    if ( !slug.length ) slug = "world-name";
    this.form.elements.name?.setAttribute("placeholder", slug);
  }

  /* -------------------------------------------- */
  /*  TinyMCE Editor                              */
  /* -------------------------------------------- */

  /** @override **/
  activateEditor (name, options={}, initialContent="") {
    const toolbar = CONFIG.TinyMCE.toolbar.split(" ").filter(t => t !== "save").join(" ");
    mergeObject(options, { toolbar });
    super.activateEditor(name, options, initialContent);
  }
}
