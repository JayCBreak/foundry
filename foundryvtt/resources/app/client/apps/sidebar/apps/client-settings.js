/**
 * The Application responsible for displaying and editing the client and world settings for this world.
 * This form renders the settings defined via the game.settings.register API which have config = true
 *
 * @extends {FormApplication}
 */
class SettingsConfig extends FormApplication {

  /** @override */
	static get defaultOptions() {
    return foundry.utils.mergeObject(super.defaultOptions, {
      title: game.i18n.localize("SETTINGS.Title"),
      id: "client-settings",
      template: "templates/sidebar/apps/settings-config.html",
      width: 600,
      height: "auto",
      tabs: [
        {navSelector: ".tabs", contentSelector: ".content", initial: "core"}
      ]
    })
  }

  /* -------------------------------------------- */

  /** @override */
  getData(options) {
    const gs = game.settings;
    const canConfigure =  game.user.can("SETTINGS_MODIFY");

    // Set-up placeholder structure for core, system, and module settings
    const data = {
      core: {version: game.version, menus: [], settings: []},
      system: {title: game.system.data.title, menus: [], settings: []},
      modules: {}
    };

    // Register a module the first time it is seen
    const registerModule = name => {
      const module = game.modules.get(name);
      data.modules[name] = {title: module ? module.data.title : "General Module Settings", menus: [], settings: []};
    };

    // Classify all menus
    for ( let menu of gs.menus.values() ) {
      if ( menu.restricted && !canConfigure ) continue;
      if ( menu.namespace === "core" ) {
        data.core.menus.push(menu);
      }
      else if ( menu.namespace === game.system.id ) {
        data.system.menus.push(menu);
      }
      else {
        const name = menu.namespace || "module";
        if ( !data.modules[name] ) registerModule(name);
        data.modules[name].menus.push(menu);
      }
    }

    // Classify all settings
    for ( let setting of gs.settings.values() ) {

      // Exclude settings the user cannot change
      if ( !setting.config || (!canConfigure && (setting.scope !== "client")) ) continue;

      // Update setting data
      const s = foundry.utils.deepClone(setting);
      s.id = `${s.namespace}.${s.key}`;
      s.name = game.i18n.localize(s.name);
      s.hint = game.i18n.localize(s.hint);
      s.value = game.settings.get(s.namespace, s.key);
      s.type = setting.type instanceof Function ? setting.type.name : "String";
      s.isCheckbox = setting.type === Boolean;
      s.isSelect = s.choices !== undefined;
      s.isRange = (setting.type === Number) && s.range;
      s.filePickerType = s.filePicker === true ? "any" : s.filePicker;

      // Classify setting
      const name = s.namespace;
      if ( name === "core" ) data.core.settings.push(s);
      else if ( name === game.system.id ) data.system.settings.push(s);
      else {
        if ( !data.modules[name] ) registerModule(name);
        data.modules[name].settings.push(s);
      }
    }

    // Sort Module headings by name
    data.modules = Object.values(data.modules).sort((a, b) => a.title.localeCompare(b.title));

    // Flag categories that have nothing
    data.core.none = (data.core.menus.length + data.core.settings.length) === 0;
    data.system.none = (data.system.menus.length + data.system.settings.length) === 0;

    // Return data
    return {
      user: game.user,
      canConfigure: canConfigure,
      systemTitle: game.system.data.title,
      data: data
    };
  }

  /* -------------------------------------------- */
  /*  Event Listeners and Handlers                */
  /* -------------------------------------------- */

  /** @override */
  activateListeners(html) {
    super.activateListeners(html);
    html.find('.submenu button').click(this._onClickSubmenu.bind(this));
    html.find('button[name="reset"]').click(this._onResetDefaults.bind(this));
    html.find('[name="core.fontSize"]').change(this._previewFontScaling.bind(this));
  }

  /* -------------------------------------------- */

  /**
   * Handle activating the button to configure User Role permissions
   * @param {Event} event   The initial button click event
   * @private
   */
  _onClickSubmenu(event) {
    event.preventDefault();
    const menu = game.settings.menus.get(event.currentTarget.dataset.key);
    if ( !menu ) return ui.notifications.error("No submenu found for the provided key");
    const app = new menu.type();
    return app.render(true);
  }

  /* -------------------------------------------- */

  /**
   * Handle button click to reset default settings
   * @param {Event} event   The initial button click event
   * @private
   */
  _onResetDefaults(event) {
    event.preventDefault();
    const button = event.currentTarget;
    const form = button.form;
    for ( let [k, v] of game.settings.settings.entries() ) {
      if ( v.config ) {
        let input = form[k];
        if ( input.type === "checkbox" ) input.checked = v.default;
        else if ( input ) input.value = v.default;
        $(input).change();
      }
    }
  }

  /* -------------------------------------------- */

  /**
   * Preview font scaling as the setting is changed.
   * @param {Event} event  The triggering event.
   * @private
   */
  _previewFontScaling(event) {
    const scale = Number(event.currentTarget.value);
    game.scaleFonts(scale);
    this.setPosition();
  }

  /* --------------------------------------------- */

  async close(options={}) {
    game.scaleFonts();
    return super.close(options);
  }

  /* -------------------------------------------- */

  /** @override */
  async _updateObject(event, formData) {
    for ( let [k, v] of Object.entries(foundry.utils.flattenObject(formData)) ) {
      let s = game.settings.settings.get(k);
      let current = game.settings.get(s.namespace, s.key);
      if ( v !== current ) {
        await game.settings.set(s.namespace, s.key, v);
      }
    }
  }
}
