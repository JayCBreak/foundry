// Global Variables
globalThis.vtt = "Foundry VTT";
globalThis.game = {};
globalThis.socket = null;

// Utilize SmoothGraphics by default
PIXI.LegacyGraphics = PIXI.Graphics;
PIXI.Graphics = PIXI.smooth.SmoothGraphics;

/**
 * The global constants object
 * @external CONST
 */

/**
 * The global boolean for whether the EULA is signed
 * @external SIGNED_EULA
 */

/**
 * A collection of application instances
 * @type {Object<Application>}
 */
globalThis.ui = {
  windows: {}
};

// The client-side console is the default logger
globalThis.logger = console;
