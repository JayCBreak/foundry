/**
 * An extension of the native FormData implementation.
 *
 * This class functions the same way that the default FormData does, but it is more opinionated about how
 * input fields of certain types should be evaluated and handled.
 *
 * It also adds support for certain Foundry VTT specific concepts including:
 *  Support for defined data types and type conversion
 *  Support for TinyMCE editors
 *  Support for editable HTML elements
 *
 * @extends {FormData}
 *
 * @param {HTMLFormElement} form               The form being processed
 * @param {Object<string, object>} [editors]   A record of TinyMCE editor metadata objects, indexed by their update key
 * @param {{string, string}} [dtypes]          A mapping of data types for form fields
 */
class FormDataExtended extends FormData {
  constructor(form, {editors={}, dtypes={}}={}) {
    super();

    /**
     * A mapping of data types requested for each form field
     * @type {{string, string}}
     */
    this.dtypes = dtypes;

    /**
     * A record of TinyMCE editors which are linked to this form
     * @type {Object<string, object>}
     */
    this.editors = editors;

    // Process the provided form
    this.process(form);
  }

  /* -------------------------------------------- */

  /**
   * Process the HTML form element to populate the FormData instance.
   * @param {HTMLFormElement} form      The HTML form
   */
  process(form) {

    // Process standard form elements
    for ( let el of form.elements ) {
      if ( !el.name || el.disabled || (el.tagName === "BUTTON") ) continue;
      if ( this.has(el.name) ) continue;
      const field = form.elements[el.name];
      this.dtypes[el.name] = el.dataset.dtype ?? "String";

      // Radio Checkboxes
      if ( el.type === "radio" ) {
        const chosen = field instanceof HTMLInputElement ? ( field.checked ? field : null ) : Array.from(field).find(r => r.checked);
        this.set(el.name, chosen ? chosen.value : null);
        continue;
      }

      // Multi-Select
      if ( el.type === "select-multiple" ) {
        const chosen = [];
        for ( let opt of el.options ) {
          if ( opt.selected ) chosen.push(opt.value);
        }
        this.dtypes[el.name] = "JSON";
        this.set(el.name, JSON.stringify(chosen));
        continue;
      }

      // Duplicate Fields
      if ( field instanceof RadioNodeList ) {
        const values = [];
        for ( let f of field ) {
          if ( f.disabled ) values.push(null);
          else if ( f.type === "checkbox" ) {
            if ( f.checked ) values.push(f.value);
          }
          else values.push(f.value)
        }
        this.set(el.name, JSON.stringify(values));
        this.dtypes[el.name] = "JSON";
        continue;
      }

      // Boolean Checkboxes
      if ( el.type === "checkbox" ) {
        this.set(el.name, el.checked || "");
        this.dtypes[el.name] = "Boolean";
        continue;
      }

      // Other Inputs
      if ( ["number", "range"].includes(el.type) ) {
        this.dtypes[el.name] = "Number";
      }
      this.set(el.name, el.value.trim());
    }

    // Process MCE editors
    for ( let [name, editor] of Object.entries(this.editors) ) {
      if ( editor.mce ) {
        this.set(name, editor.mce.getContent());
        this.dtypes[name] = "String";
        this.delete(editor.mce.id); // Delete hidden MCE inputs
      }
    }

    // Process editable HTML fields
    const editableFields = form.querySelectorAll('[data-edit]');
    for ( let el of editableFields ) {
      const name = el.dataset.edit;
      if ( this.has(name) || el.getAttribute("disabled") || (name in this.editors) ) continue;
      if (el.tagName === "IMG") this.set(name, el.getAttribute("src"));
      else this.set(name, el.innerHTML.trim());
      this.dtypes[name] = el.dataset.dtype ?? "String";
    }
  }

  /* -------------------------------------------- */

  /**
   * Export the FormData as an object
   * @return {object}
   */
  toObject() {
    const data = {};
    for ( let [k, v] of this.entries() ) {
      const dtype = this.dtypes[k];
      if ( dtype === "Boolean" ) v = v === "true";
      else if ( dtype === "JSON" ) v = JSON.parse(v);
      else if ( (dtype === "String") && !v ) v = "";
      else {
        if ( v === "" ) v = null;
        if ( (v !== null) && ( window[dtype] instanceof Function ) ) {
          try {
            v = window[dtype](v);
          } catch(err) {
            console.warn(`The form field ${k} was not able to be cast to the requested data type ${dtype}`);
          }
        }
      }
      data[k] = v;
    }
    return data;
  }
}
