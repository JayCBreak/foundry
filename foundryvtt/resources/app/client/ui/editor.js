/**
 * A collection of helper functions and utility methods related to the rich text editor
 */
class TextEditor {

  /**
   * Create a Rich Text Editor. The current implementation uses TinyMCE
   * @param {Object} options          Configuration options provided to the Editor init
   * @param {string} content          Initial HTML or text content to populate the editor with
   * @return {tinyMCE.Editor}         The editor instance.
   */
  static async create(options={}, content="") {
    const mceConfig = foundry.utils.mergeObject(CONFIG.TinyMCE, options, {inplace: false});
    mceConfig.target = options.target;

    mceConfig.file_picker_callback = function (pickerCallback, value, meta) {
      let filePicker = new FilePicker({
        type: "image",
        callback: path => {
          pickerCallback(path);
          // Reset our z-index for next open
          $(".tox-tinymce-aux").css({zIndex: ''});
        },
      });
      filePicker.render();
      // Set the TinyMCE dialog to be below the FilePicker
      $(".tox-tinymce-aux").css({zIndex: Math.min(++_maxZ, 9999)});
    };
    if ( mceConfig.content_css instanceof Array ) {
      mceConfig.content_css = mceConfig.content_css.map(c => foundry.utils.getRoute(c)).join(",");
    }
    mceConfig.init_instance_callback = editor => {
      const window = editor.getWin();
      editor.focus();
      if ( content ) editor.resetContent(content);
      editor.selection.setCursorLocation(editor.getBody(), editor.getBody().childElementCount);
      window.addEventListener("wheel", event => {
        if ( event.ctrlKey ) event.preventDefault();
      }, {passive: false});
      window.addEventListener("drop", ev => this._onDropEditorData(ev, editor));
    };
    const editors = await tinyMCE.init(mceConfig);
    return editors[0];
  }

  /**
   * A list of elements that are retained when truncating HTML.
   * @type {Set<string>}
   * @private
   */
  static _PARAGRAPH_ELEMENTS = new Set([
    'header', 'main', 'section', 'article', 'div', 'footer', // Structural Elements
    'h1', 'h2', 'h3', 'h4', 'h5', 'h6', // Headers
    'p', 'blockquote', 'summary', 'span', 'a', 'mark', // Text Types
    'strong', 'em', 'b', 'i', 'u', // Text Styles
  ]);

  /* -------------------------------------------- */
  /*  HTML Manipulation Helpers
  /* -------------------------------------------- */

  /**
   * Safely decode an HTML string, removing invalid tags and converting entities back to unicode characters.
   * @param {string} html     The original encoded HTML string
   * @return {string}         The decoded unicode string
   */
  static decodeHTML(html) {
    const txt = this._decoder;
    txt.innerHTML = html;
    return txt.value;
  }

  /* -------------------------------------------- */

  /**
   * Enrich HTML content by replacing or augmenting components of it
   * @param {string} content        The original HTML content (as a string)
   * @param {object} [options={}]   Additional options which configure how HTML is enriched
   * @param {boolean} [options.secrets=false]      Include secret tags in the final HTML? If false secret blocks will be removed.
   * @param {boolean} [options.documents=true]     Replace dynamic document links?
   * @param {boolean} [options.links=true]         Replace hyperlink content?
   * @param {boolean} [options.rolls=true]         Replace inline dice rolls?
   * @param {Object|Function} [options.rollData]   The data object providing context for inline rolls
   * @return {string}               The enriched HTML content
   */
  static enrichHTML(content, {secrets=false, documents=true, links=true, rolls=true, rollData, ...options}={}) {

    // Create the HTML element
    const html = document.createElement("div");
    html.innerHTML = String(content || "");

    // Remove secret blocks
    if ( !secrets ) {
      let elements = html.querySelectorAll("section.secret");
      elements.forEach(e => e.parentNode.removeChild(e));
    }

    // Plan text content replacements
    let updateTextArray = true;
    let text = [];

    // Replace document links
    if ( options.entities ) {
      console.warn("The 'entities' option for TextEditor.enrichHTML is deprecated. Please use 'documents' instead.");
      documents = options.entities;
    }

    if ( documents ) {
      if ( updateTextArray ) text = this._getTextNodes(html);
      const documentTypes = CONST.DOCUMENT_LINK_TYPES.concat("Compendium");
      const rgx = new RegExp(`@(${documentTypes.join("|")})\\[([^\\]]+)\\](?:{([^}]+)})?`, 'g');
      updateTextArray = this._replaceTextContent(text, rgx, this._createContentLink);
    }

    // Replace hyperlinks
    if ( links ) {
      if ( updateTextArray ) text = this._getTextNodes(html);
      const rgx = /(https?:\/\/)(www\.)?([^\s<]+)/gi;
      updateTextArray = this._replaceTextContent(text, rgx, this._createHyperlink);
    }

    // Replace inline rolls
    if ( rolls ) {
      rollData = rollData instanceof Function ? rollData() : (rollData || {});
      if (updateTextArray) text = this._getTextNodes(html);
      const rgx = /\[\[(\/[a-zA-Z]+\s)?(.*?)([\]]{2,3})(?:{([^}]+)})?/gi;
      updateTextArray = this._replaceTextContent(text, rgx, (...args) => this._createInlineRoll(...args, rollData));
    }

    // Return the enriched HTML
    return html.innerHTML;
  };

  /* -------------------------------------------- */

  /**
   * Preview an HTML fragment by constructing a substring of a given length from its inner text.
   * @param {string} content    The raw HTML to preview
   * @param {number} length     The desired length
   * @return {string}           The previewed HTML
   */
  static previewHTML(content, length=250) {
    let div = document.createElement("div");
    div.innerHTML = content;
    div = this.truncateHTML(div);
    div.innerText = this.truncateText(div.innerText, {maxLength: length});
    return div.innerHTML;
  }

  /* --------------------------------------------- */

  /**
   * Sanitises an HTML fragment and removes any non-paragraph-style text.
   * @param {HTMLElement} html       The root HTML element.
   * @return {HTMLElement}
   */
  static truncateHTML(html) {
    const truncate = (root) => {
      for ( const node of root.childNodes ) {
        if ( [Node.COMMENT_NODE, Node.TEXT_NODE].includes(node.nodeType) ) continue;
        if ( node.nodeType === Node.ELEMENT_NODE ) {
          if ( this._PARAGRAPH_ELEMENTS.has(node.tagName.toLowerCase()) ) truncate(node);
          else node.remove();
        }
      }
    };

    const clone = html.cloneNode(true);
    truncate(clone);
    return clone;
  }

  /* -------------------------------------------- */

  /**
   * Truncate a fragment of text to a maximum number of characters.
   * @param {string} text           The original text fragment that should be truncated to a maximum length
   * @param {number} [maxLength]    The maximum allowed length of the truncated string.
   * @param {boolean} [splitWords]  Whether to truncate by splitting on white space (if true) or breaking words.
   * @param {string|null} [suffix]  A suffix string to append to denote that the text was truncated.
   * @return {*}
   */
  static truncateText(text, {maxLength=50, splitWords=true, suffix="…"}={}) {
    if ( text.length <= maxLength ) return text;

    // Split the string (on words if desired)
    let short = "";
    if ( splitWords ) {
      short = text.slice(0, maxLength + 10);
      while ( short.length > maxLength ) {
        if ( /\s/.test(short) ) short = short.replace(/[\s]+([\S]+)?$/, "");
        else short = short.slice(0, maxLength);
      }
    } else {
      short = text.slice(0, maxLength);
    }

    // Add a suffix and return
    suffix = suffix ?? "";
    return short + suffix;
  }

  /* -------------------------------------------- */
  /*  Text Node Manipulation
  /* -------------------------------------------- */

  /**
   * Recursively identify the text nodes within a parent HTML node for potential content replacement.
   * @param {HTMLElement} parent    The parent HTML Element
   * @return {Text[]}               An array of contained Text nodes
   * @private
   */
  static _getTextNodes(parent) {
    const text = [];
    const walk = document.createTreeWalker(parent, NodeFilter.SHOW_TEXT);
    while( walk.nextNode() ) text.push(walk.currentNode);
    return text;
  }

  /* -------------------------------------------- */

  /**
   * Facilitate the replacement of text node content using a matching regex rule and a provided replacement function.
   * @private
   */
  static _replaceTextContent(text, rgx, func) {
    let replaced = false;
    for ( let t of text ) {
      const matches = t.textContent.matchAll(rgx);
      for ( let match of Array.from(matches).reverse() ) {
        const replacement = func(...match);
        if ( replacement ) {
          this._replaceTextNode(t, match, replacement);
          replaced = true;
        }
      }
    }
    return replaced;
  }

  /* -------------------------------------------- */

  /**
   * Replace a matched portion of a Text node with a replacement Node
   * @param {Text} text
   * @param {RegExpMatchArray} match
   * @param {Node} replacement
   * @private
   */
  static _replaceTextNode(text, match, replacement) {
    let target = text;
    if ( match.index > 0 ) {
      target = text.splitText(match.index);
    }
    if ( match[0].length < target.length ) {
      target.splitText(match[0].length);
    }
    target.replaceWith(replacement);
  }

  /* -------------------------------------------- */
  /*  Text Replacement Functions
  /* -------------------------------------------- */

  /**
   * Create a dynamic document link from a regular expression match
   * @param {string} match          The full matched string
   * @param {string} type           The matched document type or "Compendium"
   * @param {string} target         The requested match target (_id or name)
   * @param {string} name           A customized or over-ridden display name for the link
   * @return {HTMLAnchorElement}    An HTML element for the document link
   * @private
   */
  static _createContentLink(match, type, target, name) {

    // Prepare replacement data
    const data = {
      cls: ["entity-link", "content-link"],
      icon: null,
      dataset: {},
      name: name
    };
    let broken = false;

    // Get a matched World document
    if (CONST.DOCUMENT_TYPES.includes(type)) {

      // Get the linked Document
      const config = CONFIG[type];
      const collection = game.collections.get(type);
      const document = /^[a-zA-Z0-9]{16}$/.test(target) ? collection.get(target) : collection.getName(target);
      if (!document) broken = true;

      // Update link data
      data.name = data.name || (broken ? target : document.name);
      data.icon = config.sidebarIcon;
      data.dataset = {type, entity: type, id: broken ? null : document.id};
    }

    // Get a matched PlaylistSound
    else if ( type === "PlaylistSound" ) {
      const [, playlistId, , soundId] = target.split(".");
      const playlist = game.playlists.get(playlistId);
      const sound = playlist?.sounds.get(soundId);
      if ( !playlist || !sound ) broken = true;

      data.name = data.name || (broken ? target : sound.name);
      data.icon = CONFIG.Playlist.sidebarIcon;
      data.dataset = {type, playlistId, soundId};
      const playing = Array.from(game.audio.playing.values()).find(s => s._sourceId === sound.uuid);
      if ( playing ) data.cls.push("playing");
    }

    // Get a matched Compendium document
    else if (type === "Compendium") {

      // Get the linked Document
      let [scope, packName, id] = target.split(".");
      const pack = game.packs.get(`${scope}.${packName}`);
      if ( pack ) {
        data.dataset = {pack: pack.collection};
        data.icon = CONFIG[pack.documentName].sidebarIcon;

        // If the pack is indexed, retrieve the data
        if (pack.index.size) {
          const index = pack.index.find(i => (i._id === id) || (i.name === id));
          if ( index ) {
            if ( !data.name ) data.name = index.name;
            data.dataset.id = index._id;
          }
          else broken = true;
        }

        // Otherwise assume the link may be valid, since the pack has not been indexed yet
        if ( !data.name ) data.name = data.dataset.lookup = id;
      }
      else broken = true;
    }

    // Flag a link as broken
    if (broken) {
      data.icon = "fas fa-unlink";
      data.cls.push("broken");
    }

    // Construct the formed link
    const a = document.createElement('a');
    a.classList.add(...data.cls);
    a.draggable = true;
    for (let [k, v] of Object.entries(data.dataset)) {
      a.dataset[k] = v;
    }
    a.innerHTML = `<i class="${data.icon}"></i> ${data.name}`;
    return a;
  }

  /* -------------------------------------------- */

  /**
   * Replace a hyperlink-like string with an actual HTML &lt;a> tag
   * @param {string} match          The full matched string
   * @return {HTMLAnchorElement}    An HTML element for the document link
   * @private
   */
  static _createHyperlink(match) {
    const a = document.createElement('a');
    a.classList.add("hyperlink");
    a.href = match;
    a.target = "_blank";
    a.rel = "nofollow noopener";
    a.textContent = match;
    return a;
  }

  /* -------------------------------------------- */

  /**
   * Replace an inline roll formula with a rollable &lt;a> element or an eagerly evaluated roll result
   * @param {string} match      The matched string
   * @param {string} command    An optional command
   * @param {string} formula    The matched formula
   * @param {string} closing    The closing brackets for the inline roll
   * @param {string} [label]    An optional label which configures the button text
   * @return {HTMLAnchorElement|null}           The replaced match
   */
  static _createInlineRoll(match, command, formula, closing, label, ...args) {
    const isDeferred = !!command;
    const rollData = args.pop();
    let roll;

    // Define default inline data
    const data = {
      cls: ["inline-roll"],
      dataset: {}
    };

    // Handle the possibility of closing brackets
    if ( closing.length === 3 ) formula += "]";

    // Extract roll data as a parsed chat command
    if ( isDeferred ) {
      const chatCommand = `${command}${formula}`;
      let parsedCommand = null;
      try {
        parsedCommand = ChatLog.parse(chatCommand);
      }
      catch(err) { return null; }
      if ( parsedCommand[0] === "invalid" ) return null;
      formula = parsedCommand[1][2].trim();
      const flavor = parsedCommand[1][3];

      // Set roll data
      data.cls.push(parsedCommand[0]);
      data.dataset.mode = parsedCommand[0];
      data.dataset.flavor = flavor ? flavor.trim() : (label || "");
      data.dataset.formula = Roll.replaceFormulaData(formula, rollData || {});
      data.label = label ?? data.dataset.formula;
      data.title = data.dataset.flavor || data.dataset.formula;
    }

    // Perform the roll immediately
    else {
      try {
        roll = Roll.create(formula, rollData).roll({async: false});
        data.cls.push("inline-result");
        data.label = label ? `${label}: ${roll.total}` : roll.total;
        data.title = formula;
        data.dataset.roll = escape(JSON.stringify(roll));
      }
      catch(err) { return null; }
    }

    // Construct and return the formed link element
    const a = document.createElement('a');
    a.classList.add(...data.cls);
    a.title = data.title;
    for (let [k, v] of Object.entries(data.dataset)) {
      a.dataset[k] = v;
    }
    a.innerHTML = `<i class="fas fa-dice-d20"></i> ${data.label}`;
    return a;
  }

  /* -------------------------------------------- */
  /*  Event Listeners and Handlers                */
  /* -------------------------------------------- */

  static activateListeners() {
    const body = $("body");
    body.on("click", "a.content-link", this._onClickContentLink);
    body.on("dragstart", "a.content-link", this._onDragContentLink);
    body.on("click", "a.inline-roll", this._onClickInlineRoll);
  }

  /* -------------------------------------------- */

  /**
   * Handle click events on Document Links
   * @param {Event} event
   * @private
   */
  static async _onClickContentLink(event) {
    event.preventDefault();
    const  a = event.currentTarget;
    let doc = null;
    let id = a.dataset.id;

    // Target 1 - Compendium Link
    if ( a.dataset.pack ) {
      const pack = game.packs.get(a.dataset.pack);
      if ( a.dataset.lookup ) {
        if ( !pack.index.length ) await pack.getIndex();
        const entry = pack.index.find(i => (i._id === a.dataset.lookup) || (i.name === a.dataset.lookup));
        if ( entry ) {
          a.dataset.id = id = entry._id;
          delete a.dataset.lookup;
        }
      }
      doc = id ? await pack.getDocument(id) : null;
    }

    // Target 2 - PlaylistSound Link
    else if ( a.dataset.soundId ) {
      const playlist = game.playlists.get(a.dataset.playlistId);
      doc = playlist?.sounds.get(a.dataset.soundId);
    }

    // Target 3 - World Document Link
    else {
      const collection = game.collections.get(a.dataset.type);
      doc = collection.get(id);
      if ( !doc ) return;
      if ( (doc.documentName === "Scene") && doc.journal ) doc = doc.journal;
      if ( !doc.testUserPermission(game.user, "LIMITED") ) {
        return ui.notifications.warn(`You do not have permission to view this ${doc.documentName} sheet.`);
      }
    }
    if ( !doc ) return;

    // Action 1 - Execute an Action
    if ( doc.documentName === "Macro" ) return doc.execute();

    // Action 2 - Play the sound
    else if ( doc.documentName === "PlaylistSound" ) return TextEditor._onPlaySound(doc);

    // Action 3 - Render the Document sheet
    return doc.sheet.render(true);
  }

  /* -------------------------------------------- */

  /**
   * Handle left-mouse clicks on an inline roll, dispatching the formula or displaying the tooltip
   * @param {MouseEvent} event    The initiating click event
   * @private
   */
  static async _onClickInlineRoll(event) {
    event.preventDefault();
    const a = event.currentTarget;

    // For inline results expand or collapse the roll details
    if ( a.classList.contains("inline-result") ) {
      if ( a.classList.contains("expanded") ) {
        return Roll.collapseInlineResult(a);
      } else {
        return Roll.expandInlineResult(a);
      }
    }

    // Get the current speaker
    const cls = ChatMessage.implementation;
    const speaker = cls.getSpeaker();
    let actor = cls.getSpeakerActor(speaker);
    let rollData = actor ? actor.getRollData() : {};

    // Obtain roll data from the contained sheet, if the inline roll is within an Actor or Item sheet
    const sheet = a.closest(".sheet");
    if ( sheet ) {
      const app = ui.windows[sheet.dataset.appid];
      if ( ["Actor", "Item"].includes(app?.object?.documentName) ) rollData = app.object.getRollData();
    }

    // Execute a deferred roll
    const roll = Roll.create(a.dataset.formula, rollData);
    return roll.toMessage({flavor: a.dataset.flavor, speaker}, {rollMode: a.dataset.mode});
  }

  /* -------------------------------------------- */

  /**
   * Toggle playing or stopping an embedded {@link PlaylistSound} link.
   * @param {PlaylistSound} doc  The PlaylistSound document to play/stop.
   * @private
   */
  static _onPlaySound(doc) {
    let sound = Array.from(game.audio.playing.values()).find(s => s._sourceId === doc.uuid);
    if ( sound ) return sound.stop();
    sound = game.audio.create({
      src: doc.data.path,
      preload: true,
      autoplay: true,
      singleton: false,
      autoplayOptions: {volume: doc.volume}
    });
    sound._sourceId = doc.uuid;
    this._getSoundContentLinks(doc).forEach(a => a.classList.add("playing"));
    const stop = () => this._getSoundContentLinks(doc).forEach(a => a.classList.remove("playing"));
    sound.on("end", stop, {once: true});
    sound.on("stop", stop, {once: true});
  }

  /* -------------------------------------------- */

  /**
   * Find all content links belonging to a given {@link PlaylistSound}.
   * @param {PlaylistSound} doc     The PlaylistSound.
   * @returns {NodeListOf<Element>}
   * @private
   */
  static _getSoundContentLinks(doc) {
    return document.querySelectorAll(`a.content-link[data-sound-id="${doc.id}"][data-playlist-id="${doc.parent.id}"]`);
  }

  /* -------------------------------------------- */

  /**
   * Begin a Drag+Drop workflow for a dynamic content link
   * @param {Event} event   The originating drag event
   * @private
   */
  static _onDragContentLink(event) {
    event.stopPropagation();
    const a = event.currentTarget;
    let dragData = null;

    // Case 1 - Compendium Link
    if ( a.dataset.pack ) {
      const pack = game.packs.get(a.dataset.pack);
      let id = a.dataset.id;
      if ( a.dataset.lookup && pack.index.size ) {
        const entry = pack.index.find(i => (i._id === a.dataset.lookup) || (i.name === a.dataset.lookup));
        if ( entry ) id = entry._id;
      }
      if ( !id ) return false;
      dragData = { type: pack.documentName, pack: pack.collection, id: id };
    }

    // Case 2 - PlaylistSound Link
    else if ( a.dataset.soundId ) dragData = foundry.utils.deepClone(a.dataset);

    // Case 3 - World Document Link
    else dragData = { type: a.dataset.type, id: a.dataset.id };
    event.originalEvent.dataTransfer.setData("text/plain", JSON.stringify(dragData));
  }

  /* -------------------------------------------- */

  /**
   * Handle dropping of transferred data onto the active rich text editor
   * @param {DragEvent} event     The originating drop event which triggered the data transfer
   * @param {TinyMCE} editor      The TinyMCE editor instance being dropped on
   * @private
   */
  static async _onDropEditorData(event, editor) {
    event.preventDefault();
    const eventData = this.getDragEventData(event);
    const link = await TextEditor.getContentLink(eventData);
    if ( link ) editor.insertContent(link);
  }

  /* -------------------------------------------- */

  /**
   * Extract JSON data from a drag/drop event.
   * @param {DragEvent} event       The drag event which contains JSON data.
   * @returns {object}              The extracted JSON data. The object will be empty if the DragEvent did not contain
   *                                JSON-parseable data.
   */
  static getDragEventData(event) {
    if ( !("dataTransfer" in event) ) {  // Clumsy because (event instanceof DragEvent) doesn't work
      console.warn("Incorrectly attempted to process drag event data for an event which was not a DragEvent.");
      return {};
    }
    try {
      return JSON.parse(event.dataTransfer.getData('text/plain'));
    } catch(err) {
      return {};
    }
  }

  /* -------------------------------------------- */

  /**
   * Given a Drop event, returns a Content link if possible such as @Actor[ABC123], else null
   * @param {object} eventData        The parsed object of data provided by the transfer event
   * @return {Promise<string|null>}
   */
  static async getContentLink(eventData) {

    // Case 1 - Document from Compendium Pack
    if ( eventData.pack ) {
      const pack = game.packs.get(eventData.pack);
      if ( !pack ) return null;
      const document = await pack.getDocument(eventData.id);
      return document.link;
    }

    // Case 2 - A PlaylistSound
    if ( eventData.soundId && eventData.playlistId ) {
      const playlist = game.playlists.get(eventData.playlistId);
      const sound = playlist?.sounds.get(eventData.soundId);
      if ( !sound ) return null;
      return sound.link;
    }

    // Case 3 - Document from World
    if ( eventData.type ) {
      const config = CONFIG[eventData.type];
      if ( !config ) return null;
      const doc = config.collection.instance.get(eventData.id);
      if ( !doc ) return null;
      return doc.link;
    }
  }

  /* ---------------------------------------- */
  /*  Deprecations                            */
  /* ---------------------------------------- */

  /**
   * @deprecated since v9 - Use _onDragContentLink instead.
   * @private
   * @ignore
   */
  static _onDragEntityLink(...args) {
    console.warn("TextEditor._onDragEntityLink is deprecated. Please use TextEditor._onDragContentLink instead.");
    this._onDragContentLink(...args);
  }
}

// Singleton decoder area
TextEditor._decoder = document.createElement('textarea');

// Global Export
window.TextEditor = TextEditor;
